%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  intersection of stepwise hedge automata A and B
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% variables X,Y for states,
%%           L,T for labels and their types
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% hedge states
p_state(X,Y)   :- p_start_tree(X,Y).
p_state(X,Y)   :- p_initial(X,Y).
p_state(X1,Y1) :- p_elserule(X,Y,X1,Y1).
p_state(X1,Y1) :- p_typedelserule(X,Y,T,X1,Y1).
p_state(X1,Y1) :- p_typedelseArule(X,Y,T,X1,Y1).
p_state(X1,Y1) :- p_typedelseBrule(X,Y,T,X1,Y1).
p_state(X3,Y3) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyelserule(X1,Y1,X3,Y3).
p_state(X3,Y3) :- p_applyAelserule(X1,Y1,X3,Y3).
p_state(X3,Y3) :- p_applyBelserule(X1,Y1,X3,Y3).
p_state(X1,Y1) :- p_internalrule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_internalArule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_internalBrule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_epsilonArule(X,Y,X1,Y1).
p_state(X1,Y1) :- p_epsilonBrule(X,Y,X1,Y1).
p_state(IX,IY) :- p_tree_istate(X2,Y2,IX,IY).

%%% tree states that are copies of hedge states
p_copy_of(X1,X2,Z1,Z2) :- p_state(X1,X2), copy_of(A,X1,Z1), state(A,Z1),
                          copy_of(B,X2,Z2), state(B,Z2), inter(A,B,C).

%%% tree states and istates of tree states
p_tree_state(X2,Y2)        :- p_tree_istate(X2,Y2,IX,IY).
p_tree_istate(X2,Y2,IX,IY) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyelserule(X1,Y1,X3,Y3), 
                               tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).

p_tree_state(X2,Y2) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyelserule(X1,Y1,X3,Y3), tree_state(A,X2), tree_state(B,Y2), inter(A,B,C).  

p_tree_state(Z1,Z2) :- p_copy_of(X1,X2,Z1,Z2).
p_tree_state(U,V)   :- p_treerule(X,Y,U,V).

%%% distinguished states
p_initial(X,Y)    :- initial(A,X), initial(B,Y), inter(A,B,C).
p_else(X,Y)       :- p_tree_state(X,Y), else(A,X), else(B,Y),  inter(A,B,C).
p_elseA(X,Y)      :- else(A,X), p_tree_state(X,Y), inter(A,B,C).
p_elseB(X,Y)      :- p_tree_state(X,Y), else(B,Y), inter(A,B,C).
p_final(X,Y)      :- final(A,X), final(B,Y), p_state(X,Y), inter(A,B,C).
p_finalA(X,Y)      :- final(A,X), p_state(X,Y), inter(A,B,C).
p_finalB(X,Y)      :- final(B,Y), p_state(X,Y), inter(A,B,C).
p_start_tree(X,Y) :- start_tree(A,X), start_tree(B,Y), inter(A,B,C).
p_sinkA(X,Y)       :- sink(A,X), state(B,Y), inter(A,B,C).
p_sinkB(X,Y)       :- state(A,X), sink(B,Y), inter(A,B,C).
p_sink(X,Y)        :- p_sinkA(X,Y).
p_sink(X,Y)        :- p_sinkB(X,Y).

%% origins
%  p_originA_of(X,Y,X1,Y2)   :- origin_of(A,X,X1,X2), state(B,Y), inter(A,B,C).

%%% internalrules

internalfromelse(B,Y,L,T,Y1) :- elserule(B,Y,Y1), no_internalrule(B,Y,L,T).
internalfromelse(B,Y,L,T,Y1) :- typedelserule(B,Y,T,Y1), no_internalrule(B,Y,L,T).
internalfromelse(B,Y,L,T,Y1) :- elserule(B,Y,Y1), not_in_signature(B,L,T).
internalfromelse(B,Y,L,T,Y1) :- typedelserule(B,Y,T,Y1), not_in_signature(B,L,T).


% internal / else
p_internalArule(X,Y,L,T,X1,Y1) :- internalrule(A,X,L,T,X1), internalfromelse(B,Y,L,T,Y1),
                                  p_state(X,Y), inter(A,B,C).
% else / internal
p_internalBrule(X,Y,L,T,X1,Y1) :- internalfromelse(A,X,L,T,X1), internalrule(B,Y,L,T,Y1),
			       	  p_state(X,Y), inter(A,B,C).

% internal / internal
p_internalrule(X,Y,L,T,X1,Y1) :- internalrule(A,X,L,T,X1), internalrule(B,Y,L,T,Y1),
			      	 p_state(X,Y), inter(A,B,C).

%%% else rules typed and untyped

% else / else
p_elserule(X,Y,X1,Y1) :- elserule(A,X,X1), elserule(B,Y,Y1), p_state(X,Y), inter(A,B,C).

% typed_else / typed_else
p_typedelserule(X,Y,T,X1,Y1) :- typedelserule(A,X,T,X1), typedelserule(B,Y,T,Y1),
			     p_state(X,Y), inter(A,B,C).
% typed_else / else
p_typedelseArule(X,Y,T,X1,Y1) :- typedelserule(A,X,T,X1), elserule(B,Y,Y1),
			      p_state(X,Y), inter(A,B,C).

% else / typed_else
p_typedelseBrule(X,Y,T,X1,Y1) :- elserule(A,X,X1), typedelserule(B,Y,T,Y1),
			      p_state(X,Y), inter(A,B,C).


%%% applyrules

applyfromelse(B,Y1,Y2,Y3) :- else(B,Y2), no_applyrule(B,Y1,Y2), applyelserule(B,Y1,Y3).

% apply / apply
p_applyrule(X1,Y1,X2,Y2,X3,Y3) :- applyrule(A,X1,X2,X3), applyrule(B,Y1,Y2,Y3),
			          p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
% apply / applyelse
p_applyArule(X1,Y1,X2,Y2,X3,Y3) :- applyrule(A,X1,X2,X3), applyfromelse(B,Y1,Y2,Y3),
				p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
p_applyAelserule(X1,Y1,X2,else,X3,Y3) :- applyrule(A,X1,X2,X3), applyelserule(B,Y1,Y3),
				      p_state(X1,Y1), else(B,Y2),
				      p_tree_state(X2,Y2), inter(A,B,C).
% applyelse / apply
p_applyBrule(X1,Y1,X2,Y2,X3,Y3) :- applyfromelse(A,X1,X2,X3), applyrule(B,Y1,Y2,Y3),
				p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
p_applyBelserule(X1,Y1,else,Y2,X3,Y3) :- applyelserule(A,X1,X3), applyrule(B,Y1,Y2,Y3),
				      p_state(X1,Y1), else(A,Y1),
				      p_tree_state(X2,Y2), inter(A,B,C).
% applyelse / applyelse
p_applyelserule(X1,Y1,X3,Y3) :- applyelserule(A,X1,X3), applyelserule(B,Y1,Y3),
			     else(A,X2), no_applyrule(A,X1,X2),
			     else(B,Y2), no_applyrule(B,Y1,Y2),
			     p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).

% else states
p_else(X,Y) :-  else(A,X), else(B,Y), inter(A,B,C).

% tree / tree
p_treerule(X,Y,X1,Y1) :- treerule(A,X,X1), treerule(B,Y,Y1), p_state(X,Y), inter(A,B,C). 

% epsilon / skip
p_epsilonArule(X,Y,X1,Y) :- epsilonrule(A,X,X1), p_state(X,Y), inter(A,B,C).

% skip / epsilon
p_epsilonBrule(X,Y,X,Y1) :- epsilonrule(B,Y,Y1), p_state(X,Y), inter(A,B,C).

% difference of signature
no_internalrule(A,X,L,T) :- not_in_signature(A,L,T), state(A,X), inter(A,B,C).
no_internalrule(B,Y,L,T) :- not_in_signature(B,L,T), state(B,Y), inter(A,B,C).

% title
p_title(X,Y) :- title(A,X), title(B,Y), inter(A,B,C).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% co-accessibility
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p_state(co_acc,X,Y) :- p_final(X,Y).
p_state(co_acc,X,Y) :- p_treerule(X,Y,X1,Y1), p_tree_state(co_acc,X1,Y1).
p_state(co_acc,X,Y) :- p_applyelserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).

p_tree_state(co_acc,X2,Y2) :- p_applyelserule(co_acc,X,Y,X1,Y1),
			      p_state(co_acc,X1,Y1), p_else(X2,Y2).

p_state(co_acc,X1,Y1) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_state(co_acc,X1,Y1)      :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_state(co_acc,X1,Y1)      :- p_applyrule(X1,Y1,X2,Y2,X3,Y3),  p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3),  p_state(co_acc,X3,Y3).
p_state(co_acc,X,Y)   :- p_internalrule(X,Y,L,T,X1,Y1),  p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_internalArule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_internalBrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_epsilonArule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_epsilonBrule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_elserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_typedelseArule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_typedelseBrule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_typedelserule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).

p_initial(co_acc,X,Y) :- p_state(co_acc,X,Y), p_initial(X,Y).
p_final(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_final(X,Y).
p_finalA(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_finalA(X,Y).
p_finalB(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_finalB(X,Y).
p_sink(co_acc,X,Y)    :- p_state(co_acc,X,Y), p_sink(X,Y).
p_sinkA(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_sinkA(X,Y).
p_else(co_acc,X,Y)    :- p_tree_state(co_acc,X,Y), p_else(X,Y).
p_elseA(co_acc,X,Y)   :- p_tree_state(co_acc,X,Y), p_elseA(X,Y).
p_elseB(co_acc,X,Y)   :- p_tree_state(co_acc,X,Y), p_elseB(X,Y).
p_start_tree(co_acc,X,Y) :- p_state(co_acc,X,Y), p_start_tree(X,Y).

p_copy_of(co_acc,X,Y,X1,Y1)     :- p_copy_of(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_tree_istate(co_acc,X,Y,X1,Y1) :- p_tree_istate(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_internalrule(co_acc,X,Y,L,T,X1,Y1)  :- p_internalrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_internalArule(co_acc,X,Y,L,T,X1,Y1) :- p_internalArule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_internalBrule(co_acc,X,Y,L,T,X1,Y1) :- p_internalBrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_epsilonArule(co_acc,X,Y,X1,Y1) :- p_epsilonArule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_epsilonBrule(co_acc,X,Y,X1,Y1) :- p_epsilonBrule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).

p_treerule(co_acc,X,Y,X1,Y1) :- p_treerule(X,Y,X1,Y1), p_tree_state(co_acc,X1,Y1).	
p_elserule(co_acc,X,Y,X1,Y1) :- p_elserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelserule(co_acc,X,Y,T,X1,Y1) :- p_typedelserule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelseArule(co_acc,X,Y,T,X1,Y1) :- p_typedelseArule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelseBrule(co_acc,X,Y,T,X1,Y1) :- p_typedelseBrule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_applyrule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyArule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyArule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyBrule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyBrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).

p_applyelserule(co_acc,X,Y,X2,Y2) :- p_applyelserule(X,Y,X2,Y2), p_state(co_acc,X2,Y2).
p_applyAelserule(co_acc,X,Y,X1,Y1,X2,Y2) :- p_applyAelserule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyelseBrule(co_acc,X,Y,X1,Y1,X2,Y2) :- p_applyelseBrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).


n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalrule(X1,Y1,L,T,X,Y).

n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalBrule(X1,Y1,L,T,X,Y).

n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalArule(X1,Y1,L,T,X,Y).

p_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- n_internalrule(co_acc,X1,Y1,L,T,X,Y).
p_internalArule(co_acc,X1,Y1,L,T,X,Y) :- n_internalArule(co_acc,X1,Y1,L,T,X,Y).
p_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- n_internalBrule(co_acc,X1,Y1,L,T,X,Y).

needed_p_state(co_acc,X,Y)            :- n_internalrule(co_acc,X1,Y1,L,T,X,Y). 
needed_p_stateA(co_acc,X,Y)           :- n_internalArule(co_acc,X1,Y1,L,T,X,Y).
needed_p_state (co_acc,X,Y)           :- n_internalBrule(co_acc,X1,Y1,L,T,X,Y).

p_originA(X,U,Y,Z) :- p_state(X,U),      origin_of(A,X,Y,Z), inter(A,B,C).
p_originA(X,U,Y,Z) :- p_tree_state(X,U), origin_of(A,X,Y,Z), inter(A,B,C).
p_labelA(L,T,Id)   :- label(A,L,T,Id), inter(A,B,C).
p_labelB(L,T,Id)   :- label(B,L,T,Id), inter(A,B,C).
p_max_label_idA(N)   :- max_label_id(A,N), inter(A,B,C).
p_max_label_idB(N)   :- max_label_id(B,N), inter(A,B,C).

p_originA(co_acc,X,U,Y,Z) :- p_originA(X,U,Y,Z), p_state(co_acc,X,U).
p_originA(co_acc,X,U,Y,Z) :- p_originA(X,U,Y,Z), p_tree_state(co_acc,X,U).
p_labelA(co_acc,L,T,Id)   :- p_labelA(L,T,Id).
p_labelB(co_acc,L,T,Id)   :- p_labelB(L,T,Id).
p_max_label_idA(co_acc,N)   :- p_max_label_idA(N).
p_max_label_idB(co_acc,N)   :- p_max_label_idB(N).


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXPRESSION=02909 AUTOMATON=schema-det/det-stepwise
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule

% negative information only for elserules and applyelserules (NEGATIVE=false)
%   no_applyrule
%   no_internalrule
  
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(auto1,'det_and[xml-document][one-x][schema-clean[pc[step[(μx_d1.(&amp;lt;elem⋅default⋅bibliomisc⋅x⋅T⋅&amp;lt;att⋅default⋅role⋅_⋅s⋅e⋅r⋅i⋅e&amp;gt;⋅T&amp;gt;⋅T_bl_Z+_bl_Z&amp;lt;(elem⋅_⋅__bl_Z+_bl_Zdoc)⋅_⋅T⋅x_d1&amp;gt;⋅T))]]]]').
xpath(auto1,'.//bibliomisc[@role=_pos_Zserie_pos_Z]').
var(auto1,'x',1).
no_typedelserule(auto1,20,type).
no_typedelserule(auto1,11,type).
no_typedelserule(auto1,21,type).
no_typedelserule(auto1,23,type).
no_typedelserule(auto1,7,type).
no_typedelserule(auto1,26,type).
no_typedelserule(auto1,22,type).
no_typedelserule(auto1,25,type).
no_typedelserule(auto1,19,type).
no_typedelserule(auto1,24,type).
no_typedelserule(auto1,13,type).
no_typedelserule(auto1,20,char).
no_typedelserule(auto1,11,char).
no_typedelserule(auto1,21,char).
no_typedelserule(auto1,23,char).
no_typedelserule(auto1,7,char).
no_typedelserule(auto1,26,char).
no_typedelserule(auto1,22,char).
no_typedelserule(auto1,25,char).
no_typedelserule(auto1,19,char).
no_typedelserule(auto1,24,char).
no_typedelserule(auto1,13,char).
no_typedelserule(auto1,20,negvar).
no_typedelserule(auto1,11,negvar).
no_typedelserule(auto1,21,negvar).
no_typedelserule(auto1,23,negvar).
no_typedelserule(auto1,7,negvar).
no_typedelserule(auto1,26,negvar).
no_typedelserule(auto1,22,negvar).
no_typedelserule(auto1,25,negvar).
no_typedelserule(auto1,19,negvar).
no_typedelserule(auto1,24,negvar).
no_typedelserule(auto1,13,negvar).
no_typedelserule(auto1,20,name).
no_typedelserule(auto1,11,name).
no_typedelserule(auto1,21,name).
no_typedelserule(auto1,23,name).
no_typedelserule(auto1,7,name).
no_typedelserule(auto1,26,name).
no_typedelserule(auto1,22,name).
no_typedelserule(auto1,25,name).
no_typedelserule(auto1,19,name).
no_typedelserule(auto1,24,name).
no_typedelserule(auto1,13,name).
no_typedelserule(auto1,20,var).
no_typedelserule(auto1,11,var).
no_typedelserule(auto1,21,var).
no_typedelserule(auto1,23,var).
no_typedelserule(auto1,7,var).
no_typedelserule(auto1,26,var).
no_typedelserule(auto1,22,var).
no_typedelserule(auto1,25,var).
no_typedelserule(auto1,19,var).
no_typedelserule(auto1,24,var).
no_typedelserule(auto1,13,var).
no_typedelserule(auto1,20,namespace).
no_typedelserule(auto1,11,namespace).
no_typedelserule(auto1,21,namespace).
no_typedelserule(auto1,23,namespace).
no_typedelserule(auto1,7,namespace).
no_typedelserule(auto1,26,namespace).
no_typedelserule(auto1,22,namespace).
no_typedelserule(auto1,25,namespace).
no_typedelserule(auto1,19,namespace).
no_typedelserule(auto1,24,namespace).
no_typedelserule(auto1,13,namespace).
state(auto1,20).
state(auto1,14).
state(auto1,8).
state(auto1,11).
state(auto1,15).
state(auto1,1).
state(auto1,21).
state(auto1,10).
state(auto1,12).
state(auto1,17).
state(auto1,2).
state(auto1,23).
state(auto1,7).
state(auto1,26).
state(auto1,3).
state(auto1,5).
state(auto1,16).
state(auto1,9).
state(auto1,22).
state(auto1,0).
state(auto1,18).
state(auto1,25).
state(auto1,19).
state(auto1,4).
state(auto1,6).
state(auto1,24).
state(auto1,13).
start_tree(auto1,0).
initial(auto1,0).
origin_of(auto1,20,'33','23','35','38','40','32','22','21','15','20').
origin_of(auto1,14,'35','15','17','18').
origin_of(auto1,8,'13','35','10').
origin_of(auto1,11,'33','35','38','40','32','22','21','15','20').
origin_of(auto1,15,'35','16','17','18').
origin_of(auto1,1,'29','35','7','3').
origin_of(auto1,21,'5','36','19').
origin_of(auto1,10,'14','35','14').
origin_of(auto1,12,'19','35','17','18').
origin_of(auto1,17,'35','18','17','18').
origin_of(auto1,2,'31','35','1','13').
origin_of(auto1,23,'33','34','35','38','32','40','39','20','26').
origin_of(auto1,7,'33','35','38','40','32','6','26','16','5','22','15','20').
origin_of(auto1,26,'41','33','34','35','38','40','32','20','26').
origin_of(auto1,3,'12','35','4').
origin_of(auto1,5,'8','35','30','9').
origin_of(auto1,16,'35','17','17','18').
origin_of(auto1,9,'31','9','35','13').
origin_of(auto1,22,'24','35','25').
origin_of(auto1,0,'25','27','35','28','4','6','11','0').
origin_of(auto1,18,'35','20','17','18').
origin_of(auto1,25,'37','35','25','29').
origin_of(auto1,19,'10','36','23','24').
origin_of(auto1,4,'35','10','14','27','7','29','18','8','25','2','17').
origin_of(auto1,6,'35','30','9').
origin_of(auto1,24,'26','36','19','28').
origin_of(auto1,13,'36','23','12','24','28','19','21','11').
not_sink(auto1,20).
not_sink(auto1,14).
not_sink(auto1,8).
not_sink(auto1,11).
not_sink(auto1,15).
not_sink(auto1,1).
not_sink(auto1,21).
not_sink(auto1,10).
not_sink(auto1,12).
not_sink(auto1,17).
not_sink(auto1,2).
not_sink(auto1,23).
not_sink(auto1,7).
not_sink(auto1,26).
not_sink(auto1,3).
not_sink(auto1,5).
not_sink(auto1,16).
not_sink(auto1,9).
not_sink(auto1,22).
not_sink(auto1,0).
not_sink(auto1,18).
not_sink(auto1,25).
not_sink(auto1,19).
not_sink(auto1,4).
not_sink(auto1,6).
not_sink(auto1,24).
not_sink(auto1,13).
else(auto1,21).
else(auto1,19).
else(auto1,24).
else(auto1,13).
nonelse(auto1,20).
nonelse(auto1,14).
nonelse(auto1,8).
nonelse(auto1,11).
nonelse(auto1,15).
nonelse(auto1,1).
nonelse(auto1,10).
nonelse(auto1,12).
nonelse(auto1,17).
nonelse(auto1,2).
nonelse(auto1,23).
nonelse(auto1,7).
nonelse(auto1,26).
nonelse(auto1,3).
nonelse(auto1,5).
nonelse(auto1,16).
nonelse(auto1,9).
nonelse(auto1,22).
nonelse(auto1,0).
nonelse(auto1,18).
nonelse(auto1,25).
nonelse(auto1,4).
nonelse(auto1,6).
final(auto1,22).
final(auto1,25).
nonfinal(auto1,20).
nonfinal(auto1,14).
nonfinal(auto1,8).
nonfinal(auto1,11).
nonfinal(auto1,15).
nonfinal(auto1,1).
nonfinal(auto1,21).
nonfinal(auto1,10).
nonfinal(auto1,12).
nonfinal(auto1,17).
nonfinal(auto1,2).
nonfinal(auto1,23).
nonfinal(auto1,7).
nonfinal(auto1,26).
nonfinal(auto1,3).
nonfinal(auto1,5).
nonfinal(auto1,16).
nonfinal(auto1,9).
nonfinal(auto1,0).
nonfinal(auto1,18).
nonfinal(auto1,19).
nonfinal(auto1,4).
nonfinal(auto1,6).
nonfinal(auto1,24).
nonfinal(auto1,13).
tree_state(auto1,21).
tree_istate(auto1,21,0).
 tree_state(auto1,19).
tree_istate(auto1,19,0).
 tree_state(auto1,24).
tree_istate(auto1,24,0).
 tree_state(auto1,13).
tree_istate(auto1,13,0).
 hedge_state(auto1,20).
    hedge_state(auto1,14).
    hedge_state(auto1,8).
    hedge_state(auto1,11).
    hedge_state(auto1,15).
    hedge_state(auto1,1).
    hedge_state(auto1,10).
    hedge_state(auto1,12).
    hedge_state(auto1,17).
    hedge_state(auto1,2).
    hedge_state(auto1,23).
    hedge_state(auto1,7).
    hedge_state(auto1,26).
    hedge_state(auto1,3).
    hedge_state(auto1,5).
    hedge_state(auto1,16).
    hedge_state(auto1,9).
    hedge_state(auto1,22).
    hedge_state(auto1,0).
    hedge_state(auto1,18).
    hedge_state(auto1,25).
    hedge_state(auto1,4).
    hedge_state(auto1,6).
    treerule(auto1,26,24).
applyrule(auto1,4,24,4).
applyrule(auto1,7,24,26).
internalrule(auto1,15,'r',char,16).
treerule(auto1,20,21).
elserule(auto1,9,7).
internalrule(auto1,12,'s',char,14).
applyrule(auto1,7,19,7).
internalrule(auto1,0,'doc',type,2).
internalrule(auto1,8,'role',name,10).
treerule(auto1,15,13).
treerule(auto1,7,13).
treerule(auto1,16,13).
applyrule(auto1,20,13,20).
elserule(auto1,5,2).
elserule(auto1,15,4).
internalrule(auto1,1,'default',namespace,5).
internalrule(auto1,16,'i',char,17).
treerule(auto1,17,13).
applyrule(auto1,4,21,4).
applyrule(auto1,0,24,25).
applyrule(auto1,23,13,23).
treerule(auto1,23,24).
applyrule(auto1,0,13,4).
applyrule(auto1,20,19,20).
applyrule(auto1,7,13,7).
internalrule(auto1,17,'e',char,18).
applyrule(auto1,11,13,11).
elserule(auto1,12,4).
applyrule(auto1,7,21,23).
internalrule(auto1,0,'att',type,3).
internalrule(auto1,3,'default',namespace,8).
treerule(auto1,18,19).
elserule(auto1,2,7).
elserule(auto1,14,4).
applyrule(auto1,26,13,26).
applyrule(auto1,25,13,25).
treerule(auto1,11,13).
internalrule(auto1,9,'x',var,11).
internalrule(auto1,0,'elem',type,1).
elserule(auto1,10,12).
internalrule(auto1,14,'e',char,15).
elserule(auto1,17,4).
elserule(auto1,18,4).
elserule(auto1,8,4).
elserule(auto1,4,4).
applyrule(auto1,4,13,4).
elserule(auto1,1,6).
treerule(auto1,12,13).
elserule(auto1,3,4).
treerule(auto1,4,13).
elserule(auto1,6,2).
applyrule(auto1,11,19,20).
internalrule(auto1,5,'bibliomisc',name,9).
elserule(auto1,0,4).
treerule(auto1,14,13).
elserule(auto1,16,4).
applyrule(auto1,0,21,22).
applyrule(auto1,22,13,22).
label(auto1,'doc',type,4).
no_internalrule(auto1,14,'doc',type).
no_internalrule(auto1,15,'doc',type).
no_internalrule(auto1,8,'doc',type).
no_internalrule(auto1,15,'doc',type).
no_internalrule(auto1,16,'doc',type).
no_internalrule(auto1,1,'doc',type).
no_internalrule(auto1,5,'doc',type).
no_internalrule(auto1,10,'doc',type).
no_internalrule(auto1,14,'doc',type).
no_internalrule(auto1,12,'doc',type).
no_internalrule(auto1,17,'doc',type).
no_internalrule(auto1,18,'doc',type).
no_internalrule(auto1,2,'doc',type).
no_internalrule(auto1,3,'doc',type).
no_internalrule(auto1,12,'doc',type).
no_internalrule(auto1,5,'doc',type).
no_internalrule(auto1,8,'doc',type).
no_internalrule(auto1,16,'doc',type).
no_internalrule(auto1,17,'doc',type).
no_internalrule(auto1,9,'doc',type).
no_internalrule(auto1,9,'doc',type).
no_internalrule(auto1,4,'doc',type).
no_internalrule(auto1,6,'doc',type).
no_internalrule(auto1,18,'doc',type).
no_internalrule(auto1,10,'doc',type).
no_internalrule(auto1,4,'doc',type).
no_internalrule(auto1,6,'doc',type).
label(auto1,'s',char,10).
no_internalrule(auto1,14,'s',char).
no_internalrule(auto1,15,'s',char).
no_internalrule(auto1,8,'s',char).
no_internalrule(auto1,15,'s',char).
no_internalrule(auto1,16,'s',char).
no_internalrule(auto1,1,'s',char).
no_internalrule(auto1,5,'s',char).
no_internalrule(auto1,10,'s',char).
no_internalrule(auto1,14,'s',char).
no_internalrule(auto1,17,'s',char).
no_internalrule(auto1,18,'s',char).
no_internalrule(auto1,2,'s',char).
no_internalrule(auto1,3,'s',char).
no_internalrule(auto1,5,'s',char).
no_internalrule(auto1,8,'s',char).
no_internalrule(auto1,16,'s',char).
no_internalrule(auto1,17,'s',char).
no_internalrule(auto1,9,'s',char).
no_internalrule(auto1,9,'s',char).
no_internalrule(auto1,0,'s',char).
no_internalrule(auto1,4,'s',char).
no_internalrule(auto1,6,'s',char).
no_internalrule(auto1,18,'s',char).
no_internalrule(auto1,10,'s',char).
no_internalrule(auto1,4,'s',char).
no_internalrule(auto1,6,'s',char).
label(auto1,'elem',type,6).
no_internalrule(auto1,14,'elem',type).
no_internalrule(auto1,15,'elem',type).
no_internalrule(auto1,8,'elem',type).
no_internalrule(auto1,15,'elem',type).
no_internalrule(auto1,16,'elem',type).
no_internalrule(auto1,1,'elem',type).
no_internalrule(auto1,5,'elem',type).
no_internalrule(auto1,10,'elem',type).
no_internalrule(auto1,14,'elem',type).
no_internalrule(auto1,12,'elem',type).
no_internalrule(auto1,17,'elem',type).
no_internalrule(auto1,18,'elem',type).
no_internalrule(auto1,2,'elem',type).
no_internalrule(auto1,3,'elem',type).
no_internalrule(auto1,12,'elem',type).
no_internalrule(auto1,5,'elem',type).
no_internalrule(auto1,8,'elem',type).
no_internalrule(auto1,16,'elem',type).
no_internalrule(auto1,17,'elem',type).
no_internalrule(auto1,9,'elem',type).
no_internalrule(auto1,9,'elem',type).
no_internalrule(auto1,4,'elem',type).
no_internalrule(auto1,6,'elem',type).
no_internalrule(auto1,18,'elem',type).
no_internalrule(auto1,10,'elem',type).
no_internalrule(auto1,4,'elem',type).
no_internalrule(auto1,6,'elem',type).
label(auto1,'text',type,17).
no_internalrule(auto1,14,'text',type).
no_internalrule(auto1,15,'text',type).
no_internalrule(auto1,8,'text',type).
no_internalrule(auto1,15,'text',type).
no_internalrule(auto1,16,'text',type).
no_internalrule(auto1,1,'text',type).
no_internalrule(auto1,5,'text',type).
no_internalrule(auto1,10,'text',type).
no_internalrule(auto1,14,'text',type).
no_internalrule(auto1,12,'text',type).
no_internalrule(auto1,17,'text',type).
no_internalrule(auto1,18,'text',type).
no_internalrule(auto1,2,'text',type).
no_internalrule(auto1,3,'text',type).
no_internalrule(auto1,12,'text',type).
no_internalrule(auto1,5,'text',type).
no_internalrule(auto1,8,'text',type).
no_internalrule(auto1,16,'text',type).
no_internalrule(auto1,17,'text',type).
no_internalrule(auto1,9,'text',type).
no_internalrule(auto1,9,'text',type).
no_internalrule(auto1,0,'text',type).
no_internalrule(auto1,4,'text',type).
no_internalrule(auto1,6,'text',type).
no_internalrule(auto1,18,'text',type).
no_internalrule(auto1,10,'text',type).
no_internalrule(auto1,4,'text',type).
no_internalrule(auto1,6,'text',type).
label(auto1,'x',negvar,18).
no_internalrule(auto1,14,'x',negvar).
no_internalrule(auto1,15,'x',negvar).
no_internalrule(auto1,8,'x',negvar).
no_internalrule(auto1,15,'x',negvar).
no_internalrule(auto1,16,'x',negvar).
no_internalrule(auto1,1,'x',negvar).
no_internalrule(auto1,5,'x',negvar).
no_internalrule(auto1,10,'x',negvar).
no_internalrule(auto1,14,'x',negvar).
no_internalrule(auto1,12,'x',negvar).
no_internalrule(auto1,17,'x',negvar).
no_internalrule(auto1,18,'x',negvar).
no_internalrule(auto1,2,'x',negvar).
no_internalrule(auto1,3,'x',negvar).
no_internalrule(auto1,12,'x',negvar).
no_internalrule(auto1,5,'x',negvar).
no_internalrule(auto1,8,'x',negvar).
no_internalrule(auto1,16,'x',negvar).
no_internalrule(auto1,17,'x',negvar).
no_internalrule(auto1,9,'x',negvar).
no_internalrule(auto1,9,'x',negvar).
no_internalrule(auto1,0,'x',negvar).
no_internalrule(auto1,4,'x',negvar).
no_internalrule(auto1,6,'x',negvar).
no_internalrule(auto1,18,'x',negvar).
no_internalrule(auto1,10,'x',negvar).
no_internalrule(auto1,4,'x',negvar).
no_internalrule(auto1,6,'x',negvar).
label(auto1,'bibliomisc',name,2).
no_internalrule(auto1,14,'bibliomisc',name).
no_internalrule(auto1,15,'bibliomisc',name).
no_internalrule(auto1,8,'bibliomisc',name).
no_internalrule(auto1,15,'bibliomisc',name).
no_internalrule(auto1,16,'bibliomisc',name).
no_internalrule(auto1,1,'bibliomisc',name).
no_internalrule(auto1,10,'bibliomisc',name).
no_internalrule(auto1,14,'bibliomisc',name).
no_internalrule(auto1,12,'bibliomisc',name).
no_internalrule(auto1,17,'bibliomisc',name).
no_internalrule(auto1,18,'bibliomisc',name).
no_internalrule(auto1,2,'bibliomisc',name).
no_internalrule(auto1,3,'bibliomisc',name).
no_internalrule(auto1,12,'bibliomisc',name).
no_internalrule(auto1,8,'bibliomisc',name).
no_internalrule(auto1,16,'bibliomisc',name).
no_internalrule(auto1,17,'bibliomisc',name).
no_internalrule(auto1,9,'bibliomisc',name).
no_internalrule(auto1,9,'bibliomisc',name).
no_internalrule(auto1,0,'bibliomisc',name).
no_internalrule(auto1,4,'bibliomisc',name).
no_internalrule(auto1,6,'bibliomisc',name).
no_internalrule(auto1,18,'bibliomisc',name).
no_internalrule(auto1,10,'bibliomisc',name).
no_internalrule(auto1,4,'bibliomisc',name).
no_internalrule(auto1,6,'bibliomisc',name).
label(auto1,'e',char,5).
no_internalrule(auto1,15,'e',char).
no_internalrule(auto1,8,'e',char).
no_internalrule(auto1,15,'e',char).
no_internalrule(auto1,16,'e',char).
no_internalrule(auto1,1,'e',char).
no_internalrule(auto1,5,'e',char).
no_internalrule(auto1,10,'e',char).
no_internalrule(auto1,12,'e',char).
no_internalrule(auto1,18,'e',char).
no_internalrule(auto1,2,'e',char).
no_internalrule(auto1,3,'e',char).
no_internalrule(auto1,12,'e',char).
no_internalrule(auto1,5,'e',char).
no_internalrule(auto1,8,'e',char).
no_internalrule(auto1,16,'e',char).
no_internalrule(auto1,9,'e',char).
no_internalrule(auto1,9,'e',char).
no_internalrule(auto1,0,'e',char).
no_internalrule(auto1,4,'e',char).
no_internalrule(auto1,6,'e',char).
no_internalrule(auto1,18,'e',char).
no_internalrule(auto1,10,'e',char).
no_internalrule(auto1,4,'e',char).
no_internalrule(auto1,6,'e',char).
label(auto1,'att',type,1).
no_internalrule(auto1,14,'att',type).
no_internalrule(auto1,15,'att',type).
no_internalrule(auto1,8,'att',type).
no_internalrule(auto1,15,'att',type).
no_internalrule(auto1,16,'att',type).
no_internalrule(auto1,1,'att',type).
no_internalrule(auto1,5,'att',type).
no_internalrule(auto1,10,'att',type).
no_internalrule(auto1,14,'att',type).
no_internalrule(auto1,12,'att',type).
no_internalrule(auto1,17,'att',type).
no_internalrule(auto1,18,'att',type).
no_internalrule(auto1,2,'att',type).
no_internalrule(auto1,3,'att',type).
no_internalrule(auto1,12,'att',type).
no_internalrule(auto1,5,'att',type).
no_internalrule(auto1,8,'att',type).
no_internalrule(auto1,16,'att',type).
no_internalrule(auto1,17,'att',type).
no_internalrule(auto1,9,'att',type).
no_internalrule(auto1,9,'att',type).
no_internalrule(auto1,4,'att',type).
no_internalrule(auto1,6,'att',type).
no_internalrule(auto1,18,'att',type).
no_internalrule(auto1,10,'att',type).
no_internalrule(auto1,4,'att',type).
no_internalrule(auto1,6,'att',type).
label(auto1,'r',char,8).
no_internalrule(auto1,14,'r',char).
no_internalrule(auto1,8,'r',char).
no_internalrule(auto1,16,'r',char).
no_internalrule(auto1,1,'r',char).
no_internalrule(auto1,5,'r',char).
no_internalrule(auto1,10,'r',char).
no_internalrule(auto1,14,'r',char).
no_internalrule(auto1,12,'r',char).
no_internalrule(auto1,17,'r',char).
no_internalrule(auto1,18,'r',char).
no_internalrule(auto1,2,'r',char).
no_internalrule(auto1,3,'r',char).
no_internalrule(auto1,12,'r',char).
no_internalrule(auto1,5,'r',char).
no_internalrule(auto1,8,'r',char).
no_internalrule(auto1,16,'r',char).
no_internalrule(auto1,17,'r',char).
no_internalrule(auto1,9,'r',char).
no_internalrule(auto1,9,'r',char).
no_internalrule(auto1,0,'r',char).
no_internalrule(auto1,4,'r',char).
no_internalrule(auto1,6,'r',char).
no_internalrule(auto1,18,'r',char).
no_internalrule(auto1,10,'r',char).
no_internalrule(auto1,4,'r',char).
no_internalrule(auto1,6,'r',char).
label(auto1,'comment',type,14).
no_internalrule(auto1,14,'comment',type).
no_internalrule(auto1,15,'comment',type).
no_internalrule(auto1,8,'comment',type).
no_internalrule(auto1,15,'comment',type).
no_internalrule(auto1,16,'comment',type).
no_internalrule(auto1,1,'comment',type).
no_internalrule(auto1,5,'comment',type).
no_internalrule(auto1,10,'comment',type).
no_internalrule(auto1,14,'comment',type).
no_internalrule(auto1,12,'comment',type).
no_internalrule(auto1,17,'comment',type).
no_internalrule(auto1,18,'comment',type).
no_internalrule(auto1,2,'comment',type).
no_internalrule(auto1,3,'comment',type).
no_internalrule(auto1,12,'comment',type).
no_internalrule(auto1,5,'comment',type).
no_internalrule(auto1,8,'comment',type).
no_internalrule(auto1,16,'comment',type).
no_internalrule(auto1,17,'comment',type).
no_internalrule(auto1,9,'comment',type).
no_internalrule(auto1,9,'comment',type).
no_internalrule(auto1,0,'comment',type).
no_internalrule(auto1,4,'comment',type).
no_internalrule(auto1,6,'comment',type).
no_internalrule(auto1,18,'comment',type).
no_internalrule(auto1,10,'comment',type).
no_internalrule(auto1,4,'comment',type).
no_internalrule(auto1,6,'comment',type).
label(auto1,'x',var,11).
no_internalrule(auto1,14,'x',var).
no_internalrule(auto1,15,'x',var).
no_internalrule(auto1,8,'x',var).
no_internalrule(auto1,15,'x',var).
no_internalrule(auto1,16,'x',var).
no_internalrule(auto1,1,'x',var).
no_internalrule(auto1,5,'x',var).
no_internalrule(auto1,10,'x',var).
no_internalrule(auto1,14,'x',var).
no_internalrule(auto1,12,'x',var).
no_internalrule(auto1,17,'x',var).
no_internalrule(auto1,18,'x',var).
no_internalrule(auto1,2,'x',var).
no_internalrule(auto1,3,'x',var).
no_internalrule(auto1,12,'x',var).
no_internalrule(auto1,5,'x',var).
no_internalrule(auto1,8,'x',var).
no_internalrule(auto1,16,'x',var).
no_internalrule(auto1,17,'x',var).
no_internalrule(auto1,0,'x',var).
no_internalrule(auto1,4,'x',var).
no_internalrule(auto1,6,'x',var).
no_internalrule(auto1,18,'x',var).
no_internalrule(auto1,10,'x',var).
no_internalrule(auto1,4,'x',var).
no_internalrule(auto1,6,'x',var).
label(auto1,'role',name,9).
no_internalrule(auto1,14,'role',name).
no_internalrule(auto1,15,'role',name).
no_internalrule(auto1,15,'role',name).
no_internalrule(auto1,16,'role',name).
no_internalrule(auto1,1,'role',name).
no_internalrule(auto1,5,'role',name).
no_internalrule(auto1,10,'role',name).
no_internalrule(auto1,14,'role',name).
no_internalrule(auto1,12,'role',name).
no_internalrule(auto1,17,'role',name).
no_internalrule(auto1,18,'role',name).
no_internalrule(auto1,2,'role',name).
no_internalrule(auto1,3,'role',name).
no_internalrule(auto1,12,'role',name).
no_internalrule(auto1,5,'role',name).
no_internalrule(auto1,16,'role',name).
no_internalrule(auto1,17,'role',name).
no_internalrule(auto1,9,'role',name).
no_internalrule(auto1,9,'role',name).
no_internalrule(auto1,0,'role',name).
no_internalrule(auto1,4,'role',name).
no_internalrule(auto1,6,'role',name).
no_internalrule(auto1,18,'role',name).
no_internalrule(auto1,10,'role',name).
no_internalrule(auto1,4,'role',name).
no_internalrule(auto1,6,'role',name).
label(auto1,'i',char,7).
no_internalrule(auto1,14,'i',char).
no_internalrule(auto1,15,'i',char).
no_internalrule(auto1,8,'i',char).
no_internalrule(auto1,15,'i',char).
no_internalrule(auto1,1,'i',char).
no_internalrule(auto1,5,'i',char).
no_internalrule(auto1,10,'i',char).
no_internalrule(auto1,14,'i',char).
no_internalrule(auto1,12,'i',char).
no_internalrule(auto1,17,'i',char).
no_internalrule(auto1,18,'i',char).
no_internalrule(auto1,2,'i',char).
no_internalrule(auto1,3,'i',char).
no_internalrule(auto1,12,'i',char).
no_internalrule(auto1,5,'i',char).
no_internalrule(auto1,8,'i',char).
no_internalrule(auto1,17,'i',char).
no_internalrule(auto1,9,'i',char).
no_internalrule(auto1,9,'i',char).
no_internalrule(auto1,0,'i',char).
no_internalrule(auto1,4,'i',char).
no_internalrule(auto1,6,'i',char).
no_internalrule(auto1,18,'i',char).
no_internalrule(auto1,10,'i',char).
no_internalrule(auto1,4,'i',char).
no_internalrule(auto1,6,'i',char).
label(auto1,'default',namespace,3).
no_internalrule(auto1,14,'default',namespace).
no_internalrule(auto1,15,'default',namespace).
no_internalrule(auto1,8,'default',namespace).
no_internalrule(auto1,15,'default',namespace).
no_internalrule(auto1,16,'default',namespace).
no_internalrule(auto1,5,'default',namespace).
no_internalrule(auto1,10,'default',namespace).
no_internalrule(auto1,14,'default',namespace).
no_internalrule(auto1,12,'default',namespace).
no_internalrule(auto1,17,'default',namespace).
no_internalrule(auto1,18,'default',namespace).
no_internalrule(auto1,2,'default',namespace).
no_internalrule(auto1,12,'default',namespace).
no_internalrule(auto1,5,'default',namespace).
no_internalrule(auto1,8,'default',namespace).
no_internalrule(auto1,16,'default',namespace).
no_internalrule(auto1,17,'default',namespace).
no_internalrule(auto1,9,'default',namespace).
no_internalrule(auto1,9,'default',namespace).
no_internalrule(auto1,0,'default',namespace).
no_internalrule(auto1,4,'default',namespace).
no_internalrule(auto1,6,'default',namespace).
no_internalrule(auto1,18,'default',namespace).
no_internalrule(auto1,10,'default',namespace).
no_internalrule(auto1,4,'default',namespace).
no_internalrule(auto1,6,'default',namespace).
max_label_id(auto1,18).
    %%% different origins %%%%%%%
    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXPRESSION=one-x AUTOMATON=schema/det-stepwise
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule

% negative information only for elserules and applyelserules (NEGATIVE=false)
%   no_applyrule
%   no_internalrule
  
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(auto2,'det[stepwise[stepwise[det[pc[stepwise[(μone.(x_bl_Z+_bl_Z(μzero.((one-xd1e19_bl_Z+_bl_Z&lt;zero&gt;))*)⋅(x_bl_Z+_bl_Z&lt;one&gt;)⋅(μzero.((one-xd1e56_bl_Z+_bl_Z&lt;zero&gt;))*)))]]]]]]').
var(auto2,'x',1).
no_typedelserule(auto2,4,var).
no_typedelserule(auto2,2,var).
no_typedelserule(auto2,3,var).
state(auto2,1).
state(auto2,4).
state(auto2,0).
state(auto2,2).
state(auto2,3).
start_tree(auto2,0).
initial(auto2,0).
     sink(auto2,3).
not_sink(auto2,1).
not_sink(auto2,4).
not_sink(auto2,0).
not_sink(auto2,2).
else(auto2,4).
else(auto2,2).
nonelse(auto2,1).
nonelse(auto2,0).
nonelse(auto2,3).
final(auto2,1).
nonfinal(auto2,4).
nonfinal(auto2,0).
nonfinal(auto2,2).
nonfinal(auto2,3).
tree_state(auto2,4).
tree_istate(auto2,4,0).
 tree_state(auto2,2).
tree_istate(auto2,2,0).
 hedge_state(auto2,1).
    hedge_state(auto2,0).
    hedge_state(auto2,3).
    applyrule(auto2,0,4,1).
internalrule(auto2,1,'x',var,3).
elserule(auto2,0,0).
elserule(auto2,1,1).
treerule(auto2,0,2).
treerule(auto2,1,4).
applyrule(auto2,0,2,0).
applyrule(auto2,1,2,1).
internalrule(auto2,0,'x',var,1).
label(auto2,'x',var,1).
max_label_id(auto2,1).
    %%% different origins %%%%%%%
    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% not in signature

      %%%%% difference of signatures %%%%%%%%%%%
      
    not_in_signature(auto2,'doc',type).
not_in_signature(auto2,'s',char).
not_in_signature(auto2,'elem',type).
not_in_signature(auto2,'text',type).
not_in_signature(auto2,'x',negvar).
not_in_signature(auto2,'bibliomisc',name).
not_in_signature(auto2,'e',char).
not_in_signature(auto2,'att',type).
not_in_signature(auto2,'r',char).
not_in_signature(auto2,'comment',type).
not_in_signature(auto2,'role',name).
not_in_signature(auto2,'i',char).
not_in_signature(auto2,'default',namespace).


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% intersection auto=auto1.auto2
inter(auto1,auto2,auto).
