%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  intersection of stepwise hedge automata A and B
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% variables X,Y for states,
%%           L,T for labels and their types
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% hedge states
p_state(X,Y)   :- p_start_tree(X,Y).
p_state(X,Y)   :- p_initial(X,Y).
p_state(X1,Y1) :- p_elserule(X,Y,X1,Y1).
p_state(X1,Y1) :- p_typedelserule(X,Y,T,X1,Y1).
p_state(X1,Y1) :- p_typedelseArule(X,Y,T,X1,Y1).
p_state(X1,Y1) :- p_typedelseBrule(X,Y,T,X1,Y1).
p_state(X3,Y3) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyelserule(X1,Y1,X3,Y3).
p_state(X3,Y3) :- p_applyAelserule(X1,Y1,X3,Y3).
p_state(X3,Y3) :- p_applyBelserule(X1,Y1,X3,Y3).
p_state(X1,Y1) :- p_internalrule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_internalArule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_internalBrule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_epsilonArule(X,Y,X1,Y1).
p_state(X1,Y1) :- p_epsilonBrule(X,Y,X1,Y1).
p_state(IX,IY) :- p_tree_istate(X2,Y2,IX,IY).

%%% tree states that are copies of hedge states
p_copy_of(X1,X2,Z1,Z2) :- p_state(X1,X2), copy_of(A,X1,Z1), state(A,Z1),
                          copy_of(B,X2,Z2), state(B,Z2), inter(A,B,C).

%%% tree states and istates of tree states
p_tree_state(X2,Y2)        :- p_tree_istate(X2,Y2,IX,IY).
p_tree_istate(X2,Y2,IX,IY) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyelserule(X1,Y1,X3,Y3), 
                               tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).

p_tree_state(X2,Y2) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyelserule(X1,Y1,X3,Y3), tree_state(A,X2), tree_state(B,Y2), inter(A,B,C).  

p_tree_state(Z1,Z2) :- p_copy_of(X1,X2,Z1,Z2).
p_tree_state(U,V)   :- p_treerule(X,Y,U,V).

%%% distinguished states
p_initial(X,Y)    :- initial(A,X), initial(B,Y), inter(A,B,C).
p_else(X,Y)       :- p_tree_state(X,Y), else(A,X), else(B,Y),  inter(A,B,C).
p_elseA(X,Y)      :- else(A,X), p_tree_state(X,Y), inter(A,B,C).
p_elseB(X,Y)      :- p_tree_state(X,Y), else(B,Y), inter(A,B,C).
p_final(X,Y)      :- final(A,X), final(B,Y), p_state(X,Y), inter(A,B,C).
p_finalA(X,Y)      :- final(A,X), p_state(X,Y), inter(A,B,C).
p_finalB(X,Y)      :- final(B,Y), p_state(X,Y), inter(A,B,C).
p_start_tree(X,Y) :- start_tree(A,X), start_tree(B,Y), inter(A,B,C).
p_sinkA(X,Y)       :- sink(A,X), state(B,Y), inter(A,B,C).
p_sinkB(X,Y)       :- state(A,X), sink(B,Y), inter(A,B,C).
p_sink(X,Y)        :- p_sinkA(X,Y).
p_sink(X,Y)        :- p_sinkB(X,Y).

%% origins
%  p_originA_of(X,Y,X1,Y2)   :- origin_of(A,X,X1,X2), state(B,Y), inter(A,B,C).

%%% internalrules

internalfromelse(B,Y,L,T,Y1) :- elserule(B,Y,Y1), no_internalrule(B,Y,L,T).
internalfromelse(B,Y,L,T,Y1) :- typedelserule(B,Y,T,Y1), no_internalrule(B,Y,L,T).
internalfromelse(B,Y,L,T,Y1) :- elserule(B,Y,Y1), not_in_signature(B,L,T).
internalfromelse(B,Y,L,T,Y1) :- typedelserule(B,Y,T,Y1), not_in_signature(B,L,T).


% internal / else
p_internalArule(X,Y,L,T,X1,Y1) :- internalrule(A,X,L,T,X1), internalfromelse(B,Y,L,T,Y1),
                                  p_state(X,Y), inter(A,B,C).
% else / internal
p_internalBrule(X,Y,L,T,X1,Y1) :- internalfromelse(A,X,L,T,X1), internalrule(B,Y,L,T,Y1),
			       	  p_state(X,Y), inter(A,B,C).

% internal / internal
p_internalrule(X,Y,L,T,X1,Y1) :- internalrule(A,X,L,T,X1), internalrule(B,Y,L,T,Y1),
			      	 p_state(X,Y), inter(A,B,C).

%%% else rules typed and untyped

% else / else
p_elserule(X,Y,X1,Y1) :- elserule(A,X,X1), elserule(B,Y,Y1), p_state(X,Y), inter(A,B,C).

% typed_else / typed_else
p_typedelserule(X,Y,T,X1,Y1) :- typedelserule(A,X,T,X1), typedelserule(B,Y,T,Y1),
			     p_state(X,Y), inter(A,B,C).
% typed_else / else
p_typedelseArule(X,Y,T,X1,Y1) :- typedelserule(A,X,T,X1), elserule(B,Y,Y1),
			      p_state(X,Y), inter(A,B,C).

% else / typed_else
p_typedelseBrule(X,Y,T,X1,Y1) :- elserule(A,X,X1), typedelserule(B,Y,T,Y1),
			      p_state(X,Y), inter(A,B,C).


%%% applyrules

applyfromelse(B,Y1,Y2,Y3) :- else(B,Y2), no_applyrule(B,Y1,Y2), applyelserule(B,Y1,Y3).

% apply / apply
p_applyrule(X1,Y1,X2,Y2,X3,Y3) :- applyrule(A,X1,X2,X3), applyrule(B,Y1,Y2,Y3),
			          p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
% apply / applyelse
p_applyArule(X1,Y1,X2,Y2,X3,Y3) :- applyrule(A,X1,X2,X3), applyfromelse(B,Y1,Y2,Y3),
				p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
p_applyAelserule(X1,Y1,X2,else,X3,Y3) :- applyrule(A,X1,X2,X3), applyelserule(B,Y1,Y3),
				      p_state(X1,Y1), else(B,Y2),
				      p_tree_state(X2,Y2), inter(A,B,C).
% applyelse / apply
p_applyBrule(X1,Y1,X2,Y2,X3,Y3) :- applyfromelse(A,X1,X2,X3), applyrule(B,Y1,Y2,Y3),
				p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
p_applyBelserule(X1,Y1,else,Y2,X3,Y3) :- applyelserule(A,X1,X3), applyrule(B,Y1,Y2,Y3),
				      p_state(X1,Y1), else(A,Y1),
				      p_tree_state(X2,Y2), inter(A,B,C).
% applyelse / applyelse
p_applyelserule(X1,Y1,X3,Y3) :- applyelserule(A,X1,X3), applyelserule(B,Y1,Y3),
			     else(A,X2), no_applyrule(A,X1,X2),
			     else(B,Y2), no_applyrule(B,Y1,Y2),
			     p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).

% else states
p_else(X,Y) :-  else(A,X), else(B,Y), inter(A,B,C).

% tree / tree
p_treerule(X,Y,X1,Y1) :- treerule(A,X,X1), treerule(B,Y,Y1), p_state(X,Y), inter(A,B,C). 

% epsilon / skip
p_epsilonArule(X,Y,X1,Y) :- epsilonrule(A,X,X1), p_state(X,Y), inter(A,B,C).

% skip / epsilon
p_epsilonBrule(X,Y,X,Y1) :- epsilonrule(B,Y,Y1), p_state(X,Y), inter(A,B,C).

% difference of signature
no_internalrule(A,X,L,T) :- not_in_signature(A,L,T), state(A,X), inter(A,B,C).
no_internalrule(B,Y,L,T) :- not_in_signature(B,L,T), state(B,Y), inter(A,B,C).

% title
p_title(X,Y) :- title(A,X), title(B,Y), inter(A,B,C).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% co-accessibility
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p_state(co_acc,X,Y) :- p_final(X,Y).
p_state(co_acc,X,Y) :- p_treerule(X,Y,X1,Y1), p_tree_state(co_acc,X1,Y1).
p_state(co_acc,X,Y) :- p_applyelserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).

p_tree_state(co_acc,X2,Y2) :- p_applyelserule(co_acc,X,Y,X1,Y1),
			      p_state(co_acc,X1,Y1), p_else(X2,Y2).

p_state(co_acc,X1,Y1) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_state(co_acc,X1,Y1)      :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_state(co_acc,X1,Y1)      :- p_applyrule(X1,Y1,X2,Y2,X3,Y3),  p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3),  p_state(co_acc,X3,Y3).
p_state(co_acc,X,Y)   :- p_internalrule(X,Y,L,T,X1,Y1),  p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_internalArule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_internalBrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_epsilonArule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_epsilonBrule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_elserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_typedelseArule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_typedelseBrule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_typedelserule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).

p_initial(co_acc,X,Y) :- p_state(co_acc,X,Y), p_initial(X,Y).
p_final(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_final(X,Y).
p_finalA(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_finalA(X,Y).
p_finalB(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_finalB(X,Y).
p_sink(co_acc,X,Y)    :- p_state(co_acc,X,Y), p_sink(X,Y).
p_sinkA(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_sinkA(X,Y).
p_else(co_acc,X,Y)    :- p_tree_state(co_acc,X,Y), p_else(X,Y).
p_elseA(co_acc,X,Y)   :- p_tree_state(co_acc,X,Y), p_elseA(X,Y).
p_elseB(co_acc,X,Y)   :- p_tree_state(co_acc,X,Y), p_elseB(X,Y).
p_start_tree(co_acc,X,Y) :- p_state(co_acc,X,Y), p_start_tree(X,Y).

p_copy_of(co_acc,X,Y,X1,Y1)     :- p_copy_of(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_tree_istate(co_acc,X,Y,X1,Y1) :- p_tree_istate(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_internalrule(co_acc,X,Y,L,T,X1,Y1)  :- p_internalrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_internalArule(co_acc,X,Y,L,T,X1,Y1) :- p_internalArule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_internalBrule(co_acc,X,Y,L,T,X1,Y1) :- p_internalBrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_epsilonArule(co_acc,X,Y,X1,Y1) :- p_epsilonArule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_epsilonBrule(co_acc,X,Y,X1,Y1) :- p_epsilonBrule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).

p_treerule(co_acc,X,Y,X1,Y1) :- p_treerule(X,Y,X1,Y1), p_tree_state(co_acc,X1,Y1).	
p_elserule(co_acc,X,Y,X1,Y1) :- p_elserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelserule(co_acc,X,Y,T,X1,Y1) :- p_typedelserule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelseArule(co_acc,X,Y,T,X1,Y1) :- p_typedelseArule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelseBrule(co_acc,X,Y,T,X1,Y1) :- p_typedelseBrule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_applyrule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyArule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyArule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyBrule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyBrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).

p_applyelserule(co_acc,X,Y,X2,Y2) :- p_applyelserule(X,Y,X2,Y2), p_state(co_acc,X2,Y2).
p_applyAelserule(co_acc,X,Y,X1,Y1,X2,Y2) :- p_applyAelserule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyelseBrule(co_acc,X,Y,X1,Y1,X2,Y2) :- p_applyelseBrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).


n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalrule(X1,Y1,L,T,X,Y).

n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalBrule(X1,Y1,L,T,X,Y).

n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalArule(X1,Y1,L,T,X,Y).

p_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- n_internalrule(co_acc,X1,Y1,L,T,X,Y).
p_internalArule(co_acc,X1,Y1,L,T,X,Y) :- n_internalArule(co_acc,X1,Y1,L,T,X,Y).
p_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- n_internalBrule(co_acc,X1,Y1,L,T,X,Y).

needed_p_state(co_acc,X,Y)            :- n_internalrule(co_acc,X1,Y1,L,T,X,Y). 
needed_p_stateA(co_acc,X,Y)           :- n_internalArule(co_acc,X1,Y1,L,T,X,Y).
needed_p_state (co_acc,X,Y)           :- n_internalBrule(co_acc,X1,Y1,L,T,X,Y).

p_originA(X,U,Y,Z) :- p_state(X,U),      origin_of(A,X,Y,Z), inter(A,B,C).
p_originA(X,U,Y,Z) :- p_tree_state(X,U), origin_of(A,X,Y,Z), inter(A,B,C).
p_labelA(L,T,Id)   :- label(A,L,T,Id), inter(A,B,C).
p_labelB(L,T,Id)   :- label(B,L,T,Id), inter(A,B,C).
p_max_label_idA(N)   :- max_label_id(A,N), inter(A,B,C).
p_max_label_idB(N)   :- max_label_id(B,N), inter(A,B,C).

p_originA(co_acc,X,U,Y,Z) :- p_originA(X,U,Y,Z), p_state(co_acc,X,U).
p_originA(co_acc,X,U,Y,Z) :- p_originA(X,U,Y,Z), p_tree_state(co_acc,X,U).
p_labelA(co_acc,L,T,Id)   :- p_labelA(L,T,Id).
p_labelB(co_acc,L,T,Id)   :- p_labelB(L,T,Id).
p_max_label_idA(co_acc,N)   :- p_max_label_idA(N).
p_max_label_idB(co_acc,N)   :- p_max_label_idB(N).


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXPRESSION=d1e42 AUTOMATON=stepwise
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule

% negative information only for elserules and applyelserules (NEGATIVE=false)
%   no_applyrule
%   no_internalrule
  
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(auto1,'step[&lt;elem⋅tei⋅front⋅_⋅T&gt;⋅T]').
expression(auto1,'d1e42').
no_typedelserule(auto1,2,type).
no_typedelserule(auto1,4,type).
no_typedelserule(auto1,5,type).
no_typedelserule(auto1,6,type).
no_typedelserule(auto1,7,type).
no_typedelserule(auto1,12,type).
no_typedelserule(auto1,2,name).
no_typedelserule(auto1,4,name).
no_typedelserule(auto1,5,name).
no_typedelserule(auto1,6,name).
no_typedelserule(auto1,7,name).
no_typedelserule(auto1,12,name).
no_typedelserule(auto1,2,namespace).
no_typedelserule(auto1,4,namespace).
no_typedelserule(auto1,5,namespace).
no_typedelserule(auto1,6,namespace).
no_typedelserule(auto1,7,namespace).
no_typedelserule(auto1,12,namespace).
state(auto1,2).
state(auto1,4).
state(auto1,5).
state(auto1,6).
state(auto1,7).
state(auto1,8).
state(auto1,9).
state(auto1,11).
state(auto1,12).
state(auto1,14).
start_tree(auto1,5).
start_tree(auto1,11).
initial(auto1,2).
not_sink(auto1,2).
not_sink(auto1,4).
not_sink(auto1,5).
not_sink(auto1,6).
not_sink(auto1,7).
not_sink(auto1,8).
not_sink(auto1,9).
not_sink(auto1,11).
not_sink(auto1,12).
not_sink(auto1,14).
else(auto1,12).
nonelse(auto1,2).
nonelse(auto1,4).
nonelse(auto1,5).
nonelse(auto1,6).
nonelse(auto1,7).
nonelse(auto1,8).
nonelse(auto1,9).
nonelse(auto1,11).
nonelse(auto1,14).
final(auto1,14).
nonfinal(auto1,2).
nonfinal(auto1,4).
nonfinal(auto1,5).
nonfinal(auto1,6).
nonfinal(auto1,7).
nonfinal(auto1,8).
nonfinal(auto1,9).
nonfinal(auto1,11).
nonfinal(auto1,12).
copy_of(auto1,4,4).
copy_of(auto1,5,4).
copy_of(auto1,6,4).
copy_of(auto1,7,4).
copy_of(auto1,8,4).
copy_of(auto1,9,4).
copy_of(auto1,11,12).
copy_of(auto1,12,12).
tree_state(auto1,4).
tree_istate(auto1,4,5).
 tree_state(auto1,12).
tree_istate(auto1,12,11).
 hedge_state(auto1,2).
    hedge_state(auto1,5).
    hedge_state(auto1,6).
    hedge_state(auto1,7).
    hedge_state(auto1,8).
    hedge_state(auto1,9).
    hedge_state(auto1,11).
    hedge_state(auto1,14).
    internalrule(auto1,5,'elem',type,6).
internalrule(auto1,6,'tei',namespace,7).
internalrule(auto1,7,'front',name,8).
applyelserule(auto1,9,9).
no_applyrule(auto1,9,4).
no_applyrule(auto1,9,12).
elserule(auto1,9,9).
elserule(auto1,8,9).
treerule(auto1,9,4).
applyelserule(auto1,14,14).
no_applyrule(auto1,14,4).
no_applyrule(auto1,14,12).
elserule(auto1,14,14).
applyelserule(auto1,11,11).
no_applyrule(auto1,11,4).
no_applyrule(auto1,11,12).
elserule(auto1,11,11).
treerule(auto1,11,12).
applyrule(auto1,2,4,14).
label(auto1,'elem',type,1).
no_internalrule(auto1,8,'elem',type).
no_internalrule(auto1,9,'elem',type).
no_internalrule(auto1,11,'elem',type).
no_internalrule(auto1,14,'elem',type).
label(auto1,'front',name,2).
no_internalrule(auto1,8,'front',name).
no_internalrule(auto1,9,'front',name).
no_internalrule(auto1,11,'front',name).
no_internalrule(auto1,14,'front',name).
label(auto1,'tei',namespace,3).
no_internalrule(auto1,8,'tei',namespace).
no_internalrule(auto1,9,'tei',namespace).
no_internalrule(auto1,11,'tei',namespace).
no_internalrule(auto1,14,'tei',namespace).
max_label_id(auto1,3).
    %%% different origins %%%%%%%
    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXPRESSION=d1e74 AUTOMATON=stepwise
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule

% negative information only for elserules and applyelserules (NEGATIVE=false)
%   no_applyrule
%   no_internalrule
  
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(auto2,'step[(μx_d1.(&lt;elem⋅tei⋅titlePart⋅_⋅T⋅&lt;elem⋅tei⋅title⋅x⋅T&gt;⋅T&gt;⋅T_bl_Z+_bl_Z&lt;(elem⋅_⋅__bl_Z+_bl_Zdoc)⋅_⋅T⋅x_d1&gt;⋅T))]').
expression(auto2,'d1e74').
var(auto2,'x',1).
no_typedelserule(auto2,1,type).
no_typedelserule(auto2,2,type).
no_typedelserule(auto2,3,type).
no_typedelserule(auto2,4,type).
no_typedelserule(auto2,5,type).
no_typedelserule(auto2,7,type).
no_typedelserule(auto2,8,type).
no_typedelserule(auto2,9,type).
no_typedelserule(auto2,10,type).
no_typedelserule(auto2,11,type).
no_typedelserule(auto2,13,type).
no_typedelserule(auto2,17,type).
no_typedelserule(auto2,18,type).
no_typedelserule(auto2,19,type).
no_typedelserule(auto2,20,type).
no_typedelserule(auto2,24,type).
no_typedelserule(auto2,26,type).
no_typedelserule(auto2,28,type).
no_typedelserule(auto2,30,type).
no_typedelserule(auto2,32,type).
no_typedelserule(auto2,1,namespace).
no_typedelserule(auto2,2,namespace).
no_typedelserule(auto2,3,namespace).
no_typedelserule(auto2,4,namespace).
no_typedelserule(auto2,5,namespace).
no_typedelserule(auto2,7,namespace).
no_typedelserule(auto2,8,namespace).
no_typedelserule(auto2,9,namespace).
no_typedelserule(auto2,10,namespace).
no_typedelserule(auto2,11,namespace).
no_typedelserule(auto2,13,namespace).
no_typedelserule(auto2,17,namespace).
no_typedelserule(auto2,18,namespace).
no_typedelserule(auto2,19,namespace).
no_typedelserule(auto2,20,namespace).
no_typedelserule(auto2,24,namespace).
no_typedelserule(auto2,26,namespace).
no_typedelserule(auto2,28,namespace).
no_typedelserule(auto2,30,namespace).
no_typedelserule(auto2,32,namespace).
no_typedelserule(auto2,1,name).
no_typedelserule(auto2,2,name).
no_typedelserule(auto2,3,name).
no_typedelserule(auto2,4,name).
no_typedelserule(auto2,5,name).
no_typedelserule(auto2,7,name).
no_typedelserule(auto2,8,name).
no_typedelserule(auto2,9,name).
no_typedelserule(auto2,10,name).
no_typedelserule(auto2,11,name).
no_typedelserule(auto2,13,name).
no_typedelserule(auto2,17,name).
no_typedelserule(auto2,18,name).
no_typedelserule(auto2,19,name).
no_typedelserule(auto2,20,name).
no_typedelserule(auto2,24,name).
no_typedelserule(auto2,26,name).
no_typedelserule(auto2,28,name).
no_typedelserule(auto2,30,name).
no_typedelserule(auto2,32,name).
no_typedelserule(auto2,1,var).
no_typedelserule(auto2,2,var).
no_typedelserule(auto2,3,var).
no_typedelserule(auto2,4,var).
no_typedelserule(auto2,5,var).
no_typedelserule(auto2,7,var).
no_typedelserule(auto2,8,var).
no_typedelserule(auto2,9,var).
no_typedelserule(auto2,10,var).
no_typedelserule(auto2,11,var).
no_typedelserule(auto2,13,var).
no_typedelserule(auto2,17,var).
no_typedelserule(auto2,18,var).
no_typedelserule(auto2,19,var).
no_typedelserule(auto2,20,var).
no_typedelserule(auto2,24,var).
no_typedelserule(auto2,26,var).
no_typedelserule(auto2,28,var).
no_typedelserule(auto2,30,var).
no_typedelserule(auto2,32,var).
state(auto2,1).
state(auto2,2).
state(auto2,3).
state(auto2,4).
state(auto2,5).
state(auto2,6).
state(auto2,7).
state(auto2,8).
state(auto2,9).
state(auto2,10).
state(auto2,11).
state(auto2,12).
state(auto2,13).
state(auto2,14).
state(auto2,15).
state(auto2,16).
state(auto2,17).
state(auto2,18).
state(auto2,19).
state(auto2,20).
state(auto2,21).
state(auto2,22).
state(auto2,23).
state(auto2,24).
state(auto2,25).
state(auto2,26).
state(auto2,27).
state(auto2,28).
state(auto2,29).
state(auto2,30).
state(auto2,31).
state(auto2,32).
state(auto2,33).
start_tree(auto2,3).
start_tree(auto2,8).
start_tree(auto2,19).
start_tree(auto2,20).
start_tree(auto2,27).
initial(auto2,1).
initial(auto2,17).
not_sink(auto2,1).
not_sink(auto2,2).
not_sink(auto2,3).
not_sink(auto2,4).
not_sink(auto2,5).
not_sink(auto2,6).
not_sink(auto2,7).
not_sink(auto2,8).
not_sink(auto2,9).
not_sink(auto2,10).
not_sink(auto2,11).
not_sink(auto2,12).
not_sink(auto2,13).
not_sink(auto2,14).
not_sink(auto2,15).
not_sink(auto2,16).
not_sink(auto2,17).
not_sink(auto2,18).
not_sink(auto2,19).
not_sink(auto2,20).
not_sink(auto2,21).
not_sink(auto2,22).
not_sink(auto2,23).
not_sink(auto2,24).
not_sink(auto2,25).
not_sink(auto2,26).
not_sink(auto2,27).
not_sink(auto2,28).
not_sink(auto2,29).
not_sink(auto2,30).
not_sink(auto2,31).
not_sink(auto2,32).
not_sink(auto2,33).
else(auto2,28).
nonelse(auto2,1).
nonelse(auto2,2).
nonelse(auto2,3).
nonelse(auto2,4).
nonelse(auto2,5).
nonelse(auto2,6).
nonelse(auto2,7).
nonelse(auto2,8).
nonelse(auto2,9).
nonelse(auto2,10).
nonelse(auto2,11).
nonelse(auto2,12).
nonelse(auto2,13).
nonelse(auto2,14).
nonelse(auto2,15).
nonelse(auto2,16).
nonelse(auto2,17).
nonelse(auto2,18).
nonelse(auto2,19).
nonelse(auto2,20).
nonelse(auto2,21).
nonelse(auto2,22).
nonelse(auto2,23).
nonelse(auto2,24).
nonelse(auto2,25).
nonelse(auto2,26).
nonelse(auto2,27).
nonelse(auto2,29).
nonelse(auto2,30).
nonelse(auto2,31).
nonelse(auto2,32).
nonelse(auto2,33).
final(auto2,16).
final(auto2,29).
nonfinal(auto2,1).
nonfinal(auto2,2).
nonfinal(auto2,3).
nonfinal(auto2,4).
nonfinal(auto2,5).
nonfinal(auto2,6).
nonfinal(auto2,7).
nonfinal(auto2,8).
nonfinal(auto2,9).
nonfinal(auto2,10).
nonfinal(auto2,11).
nonfinal(auto2,12).
nonfinal(auto2,13).
nonfinal(auto2,14).
nonfinal(auto2,15).
nonfinal(auto2,17).
nonfinal(auto2,18).
nonfinal(auto2,19).
nonfinal(auto2,20).
nonfinal(auto2,21).
nonfinal(auto2,22).
nonfinal(auto2,23).
nonfinal(auto2,24).
nonfinal(auto2,25).
nonfinal(auto2,26).
nonfinal(auto2,27).
nonfinal(auto2,28).
nonfinal(auto2,30).
nonfinal(auto2,31).
nonfinal(auto2,32).
nonfinal(auto2,33).
copy_of(auto2,2,2).
copy_of(auto2,3,2).
copy_of(auto2,4,2).
copy_of(auto2,5,2).
copy_of(auto2,6,2).
copy_of(auto2,7,7).
copy_of(auto2,8,7).
copy_of(auto2,9,7).
copy_of(auto2,10,7).
copy_of(auto2,11,7).
copy_of(auto2,12,7).
copy_of(auto2,13,2).
copy_of(auto2,14,2).
copy_of(auto2,15,2).
copy_of(auto2,18,18).
copy_of(auto2,19,18).
copy_of(auto2,20,18).
copy_of(auto2,21,18).
copy_of(auto2,22,18).
copy_of(auto2,23,18).
copy_of(auto2,24,18).
copy_of(auto2,25,18).
copy_of(auto2,26,18).
copy_of(auto2,27,28).
copy_of(auto2,28,28).
copy_of(auto2,30,18).
copy_of(auto2,31,18).
copy_of(auto2,32,18).
copy_of(auto2,33,18).
tree_state(auto2,2).
tree_istate(auto2,2,3).
 tree_state(auto2,7).
tree_istate(auto2,7,8).
 tree_state(auto2,18).
tree_istate(auto2,18,19).
 tree_istate(auto2,18,20).
 tree_state(auto2,28).
tree_istate(auto2,28,27).
 hedge_state(auto2,1).
    hedge_state(auto2,3).
    hedge_state(auto2,4).
    hedge_state(auto2,5).
    hedge_state(auto2,6).
    hedge_state(auto2,8).
    hedge_state(auto2,9).
    hedge_state(auto2,10).
    hedge_state(auto2,11).
    hedge_state(auto2,12).
    hedge_state(auto2,13).
    hedge_state(auto2,14).
    hedge_state(auto2,15).
    hedge_state(auto2,16).
    hedge_state(auto2,17).
    hedge_state(auto2,19).
    hedge_state(auto2,20).
    hedge_state(auto2,21).
    hedge_state(auto2,22).
    hedge_state(auto2,23).
    hedge_state(auto2,24).
    hedge_state(auto2,25).
    hedge_state(auto2,26).
    hedge_state(auto2,27).
    hedge_state(auto2,29).
    hedge_state(auto2,30).
    hedge_state(auto2,31).
    hedge_state(auto2,32).
    hedge_state(auto2,33).
    internalrule(auto2,3,'elem',type,4).
internalrule(auto2,4,'tei',namespace,5).
internalrule(auto2,5,'titlePart',name,6).
applyelserule(auto2,14,14).
no_applyrule(auto2,14,7).
no_applyrule(auto2,14,2).
no_applyrule(auto2,14,18).
no_applyrule(auto2,14,28).
elserule(auto2,14,14).
elserule(auto2,6,14).
internalrule(auto2,8,'elem',type,9).
internalrule(auto2,9,'tei',namespace,10).
internalrule(auto2,10,'title',name,11).
applyelserule(auto2,12,12).
no_applyrule(auto2,12,7).
no_applyrule(auto2,12,2).
no_applyrule(auto2,12,18).
no_applyrule(auto2,12,28).
elserule(auto2,12,12).
internalrule(auto2,11,'x',var,12).
treerule(auto2,12,7).
applyelserule(auto2,14,13).
no_applyrule(auto2,14,7).
no_applyrule(auto2,14,2).
no_applyrule(auto2,14,18).
no_applyrule(auto2,14,28).
elserule(auto2,14,13).
elserule(auto2,6,13).
applyelserule(auto2,15,15).
no_applyrule(auto2,15,7).
no_applyrule(auto2,15,2).
no_applyrule(auto2,15,18).
no_applyrule(auto2,15,28).
elserule(auto2,15,15).
applyrule(auto2,13,7,15).
treerule(auto2,15,2).
applyelserule(auto2,16,16).
no_applyrule(auto2,16,7).
no_applyrule(auto2,16,2).
no_applyrule(auto2,16,18).
no_applyrule(auto2,16,28).
elserule(auto2,16,16).
applyrule(auto2,1,2,16).
internalrule(auto2,19,'elem',type,21).
elserule(auto2,21,22).
elserule(auto2,22,23).
internalrule(auto2,20,'doc',type,23).
applyelserule(auto2,25,25).
no_applyrule(auto2,25,7).
no_applyrule(auto2,25,2).
no_applyrule(auto2,25,18).
no_applyrule(auto2,25,28).
elserule(auto2,25,25).
elserule(auto2,23,25).
applyelserule(auto2,25,24).
no_applyrule(auto2,25,7).
no_applyrule(auto2,25,2).
no_applyrule(auto2,25,18).
no_applyrule(auto2,25,28).
elserule(auto2,25,24).
elserule(auto2,23,24).
treerule(auto2,26,18).
applyelserule(auto2,29,29).
no_applyrule(auto2,29,7).
no_applyrule(auto2,29,2).
no_applyrule(auto2,29,18).
no_applyrule(auto2,29,28).
elserule(auto2,29,29).
applyelserule(auto2,27,27).
no_applyrule(auto2,27,7).
no_applyrule(auto2,27,2).
no_applyrule(auto2,27,18).
no_applyrule(auto2,27,28).
elserule(auto2,27,27).
treerule(auto2,27,28).
applyrule(auto2,17,18,29).
applyelserule(auto2,31,31).
no_applyrule(auto2,31,7).
no_applyrule(auto2,31,2).
no_applyrule(auto2,31,18).
no_applyrule(auto2,31,28).
elserule(auto2,31,31).
applyrule(auto2,30,2,31).
applyelserule(auto2,33,33).
no_applyrule(auto2,33,7).
no_applyrule(auto2,33,2).
no_applyrule(auto2,33,18).
no_applyrule(auto2,33,28).
elserule(auto2,33,33).
applyrule(auto2,32,18,33).
epsilonrule(auto2,31,26).
epsilonrule(auto2,33,26).
epsilonrule(auto2,24,30).
epsilonrule(auto2,24,32).
label(auto2,'doc',type,1).
no_internalrule(auto2,6,'doc',type).
no_internalrule(auto2,12,'doc',type).
no_internalrule(auto2,14,'doc',type).
no_internalrule(auto2,15,'doc',type).
no_internalrule(auto2,16,'doc',type).
no_internalrule(auto2,21,'doc',type).
no_internalrule(auto2,22,'doc',type).
no_internalrule(auto2,23,'doc',type).
no_internalrule(auto2,25,'doc',type).
no_internalrule(auto2,27,'doc',type).
no_internalrule(auto2,29,'doc',type).
no_internalrule(auto2,31,'doc',type).
no_internalrule(auto2,33,'doc',type).
label(auto2,'elem',type,2).
no_internalrule(auto2,6,'elem',type).
no_internalrule(auto2,12,'elem',type).
no_internalrule(auto2,14,'elem',type).
no_internalrule(auto2,15,'elem',type).
no_internalrule(auto2,16,'elem',type).
no_internalrule(auto2,21,'elem',type).
no_internalrule(auto2,22,'elem',type).
no_internalrule(auto2,23,'elem',type).
no_internalrule(auto2,25,'elem',type).
no_internalrule(auto2,27,'elem',type).
no_internalrule(auto2,29,'elem',type).
no_internalrule(auto2,31,'elem',type).
no_internalrule(auto2,33,'elem',type).
label(auto2,'tei',namespace,3).
no_internalrule(auto2,6,'tei',namespace).
no_internalrule(auto2,12,'tei',namespace).
no_internalrule(auto2,14,'tei',namespace).
no_internalrule(auto2,15,'tei',namespace).
no_internalrule(auto2,16,'tei',namespace).
no_internalrule(auto2,21,'tei',namespace).
no_internalrule(auto2,22,'tei',namespace).
no_internalrule(auto2,23,'tei',namespace).
no_internalrule(auto2,25,'tei',namespace).
no_internalrule(auto2,27,'tei',namespace).
no_internalrule(auto2,29,'tei',namespace).
no_internalrule(auto2,31,'tei',namespace).
no_internalrule(auto2,33,'tei',namespace).
label(auto2,'titlePart',name,4).
no_internalrule(auto2,6,'titlePart',name).
no_internalrule(auto2,12,'titlePart',name).
no_internalrule(auto2,14,'titlePart',name).
no_internalrule(auto2,15,'titlePart',name).
no_internalrule(auto2,16,'titlePart',name).
no_internalrule(auto2,21,'titlePart',name).
no_internalrule(auto2,22,'titlePart',name).
no_internalrule(auto2,23,'titlePart',name).
no_internalrule(auto2,25,'titlePart',name).
no_internalrule(auto2,27,'titlePart',name).
no_internalrule(auto2,29,'titlePart',name).
no_internalrule(auto2,31,'titlePart',name).
no_internalrule(auto2,33,'titlePart',name).
label(auto2,'title',name,5).
no_internalrule(auto2,6,'title',name).
no_internalrule(auto2,12,'title',name).
no_internalrule(auto2,14,'title',name).
no_internalrule(auto2,15,'title',name).
no_internalrule(auto2,16,'title',name).
no_internalrule(auto2,21,'title',name).
no_internalrule(auto2,22,'title',name).
no_internalrule(auto2,23,'title',name).
no_internalrule(auto2,25,'title',name).
no_internalrule(auto2,27,'title',name).
no_internalrule(auto2,29,'title',name).
no_internalrule(auto2,31,'title',name).
no_internalrule(auto2,33,'title',name).
label(auto2,'x',var,6).
no_internalrule(auto2,6,'x',var).
no_internalrule(auto2,12,'x',var).
no_internalrule(auto2,14,'x',var).
no_internalrule(auto2,15,'x',var).
no_internalrule(auto2,16,'x',var).
no_internalrule(auto2,21,'x',var).
no_internalrule(auto2,22,'x',var).
no_internalrule(auto2,23,'x',var).
no_internalrule(auto2,25,'x',var).
no_internalrule(auto2,27,'x',var).
no_internalrule(auto2,29,'x',var).
no_internalrule(auto2,31,'x',var).
no_internalrule(auto2,33,'x',var).
max_label_id(auto2,6).
    %%% different origins %%%%%%%
    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% not in signature

      %%%%% difference of signatures %%%%%%%%%%%
      
    not_in_signature(auto2,'front',name).

not_in_signature(auto1,'doc',type).
not_in_signature(auto1,'titlePart',name).
not_in_signature(auto1,'title',name).
not_in_signature(auto1,'x',var).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% intersection auto=auto1.auto2
inter(auto1,auto2,auto).
