# MAIN	= XPathMark/base
# MAIN  = jo
# MAIN  = Antonio/CIAA
# MAIN 	= quadratic
MAIN  = lick/forward

-include Makefiles/$(MAIN).mk

nRegExp	= nRegExp

# SCHEMA-CLEAN-CO-ACC=false

OUTPUT_DIR	= ../automata-store/lick/forward/version-0.6
VALIDATE	= true

nRegExp-TARGET= one
# TIMEOUT	= 500
# OUTPUT_DIR	= o

# EXPRESSION	= Q1.1
SCHEMA-CLEAN-CO-ACC=false

# EXPRESSION 	= Q3.4
EXPRESSION	= 18330

# EXPRESSION	= 15809
# EXPRESSION	= 15809
# EXPRESSION	= 18330
# EXPRESSION	= 00744
# EXPRESSION	= 4
# nRegExp	= nRegExp
# nRegExp-one-TARGETS = create-schema-det
# nRegExp-one-TARGETS = ee
# ALL_TARGET = evaluate
# AUTOMATA-LIST	= XML-Prag

PRINT_FAILED_RUNS = true

t29:
	@echo USER=$(USER)

s100:
	@echo SAMPLES="$(SAMPLES)"

pro:
	@echo "//query[@id='$(ID)']/@project"
	@echo PROJECT=$(PROJECT)

watch:
	tail -f t.txt

t22:
	echo nRegExp=$(nRegExp)
