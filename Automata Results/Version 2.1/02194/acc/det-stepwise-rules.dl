%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Accessible states of stepwise hedge automata A
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% whether variable is allowed or not

kind(0).
kind(1).

%% whether we started a tree or a hedge

start(t).
start(h).

%% types different from type var


nonvar(type).
nonvar(char).
nonvar(negvar).
nonvar(name).
nonvar(namespace).

%% starting trees and hedges

acc(t,0,A,X) :- start_tree(A,X).
acc(h,0,A,X) :- initial(A,X).

%% ending trees

acc_tree_state(K,A,Z) :- acc(t,K,A,X), treerule(A,X,Z).
acc_tree_state(K,A,Y) :- acc_tree_state(K,A,X), is(A,X,Y).

%% internal rules

internalrule_all(A,X,L,T,Y) :- internalrule(A,X,L,T,Y).
internalrule_all(A,X,L,T,Y) :- elserule(A,X,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,L,T,Y) :- typedelserule(A,X,T,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- elserule(A,X,Y), typedelserule(A,L,T,Z).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- typedelserule(A,X,T,Y).

acc(S,K,A,Y) :- acc(S,K,A,X), internalrule_all(A,X,L,T  ,Y), nonvar(T).
acc(S,1,A,Y) :- acc(S,0,A,X), internalrule_all(A,X,L,var,Y).

%% epsilon rules

acc(S,K,A,Y) :- acc(S,K,A,X), epsilonrule(A,X,Y), kind(K).

%% apply rules

applyrule_all(A,X,Z,Y) :- applyrule(A,X,Z,Y).
applyrule_all(A,X,Z,Y) :- applyelserule(A,X,Y), no_applyrule(A,X,Z).

acc(S,K,A,Y) :- acc(S,K,A,X), acc_tree_state(0,A,Z), applyrule_all(A,X,Z,Y).
acc(S,1,A,Y) :- acc(S,0,A,X), acc_tree_state(1,A,Z), applyrule_all(A,X,Z,Y).

%% compatibility with previous predicates

acc_h(A,X) :- acc(h,1,A,X).
acc_t(A,X) :- acc(t,1,A,X).




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% label states where XML labels end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

internal_or_else(A,X,Y) :- internalrule(A,X,L,T,Y).
internal_or_else(A,X,Y) :- elserule(A,X,Y).
internal_or_else(A,X,Y) :- typedelserule(A,X,T,Y).

label_state(A,U) :- initial(A,X), internalrule(A,X,'elem',type,Y),internal_or_else(A,Y,Z),
		    internal_or_else(A,Z,U).
label_state(A,U) :- initial(A,X), internalrule(A,X,'att',type,Y), internal_or_else(A,Y,Z),
                    internal_or_else(A,Z,U).
label_state(A,Y) :- initial(A,X), internalrule(A,X,'text',type,Y). 
label_state(A,Y) :- initial(A,X), internalrule(A,X,'comment',type,Y). 
label_state(A,Y) :- initial(A,X), internalrule(A,X,'doc',type,Y). 

acc_label(A,X,X) :- label_state(A,X).
acc_label(A,X,Z) :- acc_label(A,X,Y), internalrule(A,Y,L,T,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), elserule(A,Y,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), typedelserule(A,Y,T,Z).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(A,Z), applyrule(A,X1,Z,Y).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(A,Z), applyelserule(A,X1,Y), no_applyrule(A,X1,Z).

acc_label_tree(A,X,Z) :- acc_label(A,X,Y), treerule(A,Y,Z).


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule
% negative information (NEGATIVE=true)
%   no_applyrule
%   no_elserule
%   no_treerule
%   no_internalrule
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(stepwise,'mini(det_and[xml-document][one-x][schema-clean[pc[stepwise[]]]])').
var(stepwise,'x',1).
no_typedelserule(stepwise,14,var).
no_typedelserule(stepwise,11,var).
no_typedelserule(stepwise,4,var).
no_typedelserule(stepwise,12,var).
no_typedelserule(stepwise,10,var).
no_typedelserule(stepwise,19,var).
no_typedelserule(stepwise,16,var).
no_typedelserule(stepwise,9,var).
no_typedelserule(stepwise,14,type).
no_typedelserule(stepwise,11,type).
no_typedelserule(stepwise,4,type).
no_typedelserule(stepwise,12,type).
no_typedelserule(stepwise,10,type).
no_typedelserule(stepwise,19,type).
no_typedelserule(stepwise,16,type).
no_typedelserule(stepwise,9,type).
no_typedelserule(stepwise,14,name).
no_typedelserule(stepwise,11,name).
no_typedelserule(stepwise,4,name).
no_typedelserule(stepwise,12,name).
no_typedelserule(stepwise,10,name).
no_typedelserule(stepwise,19,name).
no_typedelserule(stepwise,16,name).
no_typedelserule(stepwise,9,name).
no_typedelserule(stepwise,14,namespace).
no_typedelserule(stepwise,11,namespace).
no_typedelserule(stepwise,4,namespace).
no_typedelserule(stepwise,12,namespace).
no_typedelserule(stepwise,10,namespace).
no_typedelserule(stepwise,19,namespace).
no_typedelserule(stepwise,16,namespace).
no_typedelserule(stepwise,9,namespace).
no_typedelserule(stepwise,14,negvar).
no_typedelserule(stepwise,11,negvar).
no_typedelserule(stepwise,4,negvar).
no_typedelserule(stepwise,12,negvar).
no_typedelserule(stepwise,10,negvar).
no_typedelserule(stepwise,19,negvar).
no_typedelserule(stepwise,16,negvar).
no_typedelserule(stepwise,9,negvar).
state(stepwise,14).
state(stepwise,11).
state(stepwise,4).
state(stepwise,12).
state(stepwise,10).
state(stepwise,7).
state(stepwise,19).
state(stepwise,3).
state(stepwise,5).
state(stepwise,16).
state(stepwise,6).
state(stepwise,8).
state(stepwise,1).
state(stepwise,0).
state(stepwise,9).
state(stepwise,2).
start_tree(stepwise,0).
initial(stepwise,0).
origin_of(stepwise,14,'18','14','12','26').
origin_of(stepwise,11,'6','5','19').
origin_of(stepwise,4,'8','18','13','9','5','6').
origin_of(stepwise,12,'6','23','12','24','28','19','21','11').
origin_of(stepwise,10,'18','31','27','22','25','16','15','20','22').
origin_of(stepwise,7,'18','28','20','13').
origin_of(stepwise,19,'18','34','29').
origin_of(stepwise,3,'18','10','14','27','26','4','7','29','18','8','25','2','17').
origin_of(stepwise,5,'18','33','23','9').
origin_of(stepwise,16,'6','7','28').
origin_of(stepwise,6,'18','33','9').
origin_of(stepwise,8,'18','28','13').
origin_of(stepwise,1,'18','15','1').
origin_of(stepwise,0,'18','21','24','35','36','0').
origin_of(stepwise,9,'18','31','27','16','22','25','15','20').
origin_of(stepwise,2,'18','17','19','3').
not_sink(stepwise,14).
not_sink(stepwise,11).
not_sink(stepwise,4).
not_sink(stepwise,12).
not_sink(stepwise,10).
not_sink(stepwise,7).
not_sink(stepwise,19).
not_sink(stepwise,3).
not_sink(stepwise,5).
not_sink(stepwise,16).
not_sink(stepwise,6).
not_sink(stepwise,8).
not_sink(stepwise,1).
not_sink(stepwise,0).
not_sink(stepwise,9).
not_sink(stepwise,2).
else(stepwise,11).
else(stepwise,12).
else(stepwise,16).
nonelse(stepwise,14).
nonelse(stepwise,4).
nonelse(stepwise,10).
nonelse(stepwise,7).
nonelse(stepwise,19).
nonelse(stepwise,3).
nonelse(stepwise,5).
nonelse(stepwise,6).
nonelse(stepwise,8).
nonelse(stepwise,1).
nonelse(stepwise,0).
nonelse(stepwise,9).
nonelse(stepwise,2).
final(stepwise,19).
nonfinal(stepwise,14).
nonfinal(stepwise,11).
nonfinal(stepwise,4).
nonfinal(stepwise,12).
nonfinal(stepwise,10).
nonfinal(stepwise,7).
nonfinal(stepwise,3).
nonfinal(stepwise,5).
nonfinal(stepwise,16).
nonfinal(stepwise,6).
nonfinal(stepwise,8).
nonfinal(stepwise,1).
nonfinal(stepwise,0).
nonfinal(stepwise,9).
nonfinal(stepwise,2).
tree_state(stepwise,11).
tree_istate(stepwise,11,0).
 tree_state(stepwise,12).
tree_istate(stepwise,12,0).
 tree_state(stepwise,16).
tree_istate(stepwise,16,0).
 hedge_state(stepwise,14).
    no_applyrule(stepwise,14,11).
	  no_applyrule(stepwise,14,12).
	  no_applyrule(stepwise,14,16).
	  no_applyelserule(stepwise,14).
no_elserule(stepwise,14).
	% %
hedge_state(stepwise,4).
    no_applyrule(stepwise,4,16).
	  no_applyelserule(stepwise,4).
no_elserule(stepwise,4).
	% %
hedge_state(stepwise,10).
    no_applyrule(stepwise,10,16).
	  no_applyelserule(stepwise,10).
no_elserule(stepwise,10).
	% %
hedge_state(stepwise,7).
    no_applyrule(stepwise,7,11).
	  no_applyrule(stepwise,7,12).
	  no_applyrule(stepwise,7,16).
	  no_applyelserule(stepwise,7).
hedge_state(stepwise,19).
    no_applyrule(stepwise,19,11).
	  no_applyrule(stepwise,19,12).
	  no_applyrule(stepwise,19,16).
	  no_applyelserule(stepwise,19).
no_elserule(stepwise,19).
	% %
hedge_state(stepwise,3).
    no_applyrule(stepwise,3,16).
	  no_applyelserule(stepwise,3).
hedge_state(stepwise,5).
    no_applyrule(stepwise,5,11).
	  no_applyrule(stepwise,5,12).
	  no_applyrule(stepwise,5,16).
	  no_applyelserule(stepwise,5).
hedge_state(stepwise,6).
    no_applyrule(stepwise,6,11).
	  no_applyrule(stepwise,6,12).
	  no_applyrule(stepwise,6,16).
	  no_applyelserule(stepwise,6).
hedge_state(stepwise,8).
    no_applyrule(stepwise,8,11).
	  no_applyrule(stepwise,8,12).
	  no_applyrule(stepwise,8,16).
	  no_applyelserule(stepwise,8).
hedge_state(stepwise,1).
    no_applyrule(stepwise,1,11).
	  no_applyrule(stepwise,1,12).
	  no_applyrule(stepwise,1,16).
	  no_applyelserule(stepwise,1).
hedge_state(stepwise,0).
    no_applyelserule(stepwise,0).
hedge_state(stepwise,9).
    no_applyrule(stepwise,9,11).
	  no_applyrule(stepwise,9,16).
	  no_applyelserule(stepwise,9).
no_elserule(stepwise,9).
	% %
hedge_state(stepwise,2).
    no_applyrule(stepwise,2,11).
	  no_applyrule(stepwise,2,12).
	  no_applyrule(stepwise,2,16).
	  no_applyelserule(stepwise,2).
internalrule(stepwise,2,'default',namespace,5).
applyrule(stepwise,4,12,3).
applyrule(stepwise,10,11,9).
applyrule(stepwise,3,11,3).
applyrule(stepwise,0,11,3).
applyrule(stepwise,3,12,3).
elserule(stepwise,6,8).
treerule(stepwise,9,11).
applyrule(stepwise,4,11,14).
internalrule(stepwise,0,'elem',type,2).
internalrule(stepwise,5,'annotation',name,7).
elserule(stepwise,0,3).
internalrule(stepwise,0,'doc',type,1).
applyrule(stepwise,0,16,19).
elserule(stepwise,2,6).
elserule(stepwise,8,10).
elserule(stepwise,1,4).
elserule(stepwise,5,8).
applyrule(stepwise,9,12,9).
elserule(stepwise,7,10).
applyrule(stepwise,0,12,3).
internalrule(stepwise,7,'x',var,9).
treerule(stepwise,10,12).
treerule(stepwise,14,16).
treerule(stepwise,3,12).
elserule(stepwise,3,3).
applyrule(stepwise,10,12,10).
no_elserule(stepwise,14).
no_elserule(stepwise,11).
no_elserule(stepwise,4).
no_elserule(stepwise,12).
no_elserule(stepwise,10).
no_elserule(stepwise,19).
no_elserule(stepwise,16).
no_elserule(stepwise,9).
no_treerule(stepwise,11).
no_treerule(stepwise,4).
no_treerule(stepwise,12).
no_treerule(stepwise,7).
no_treerule(stepwise,19).
no_treerule(stepwise,5).
no_treerule(stepwise,16).
no_treerule(stepwise,6).
no_treerule(stepwise,8).
no_treerule(stepwise,1).
no_treerule(stepwise,0).
no_treerule(stepwise,2).
label(stepwise,'x',var,5).
no_internalrule(stepwise,14,'x',var).
no_internalrule(stepwise,11,'x',var).
no_internalrule(stepwise,4,'x',var).
no_internalrule(stepwise,12,'x',var).
no_internalrule(stepwise,10,'x',var).
no_internalrule(stepwise,19,'x',var).
no_internalrule(stepwise,3,'x',var).
no_internalrule(stepwise,5,'x',var).
no_internalrule(stepwise,16,'x',var).
no_internalrule(stepwise,6,'x',var).
no_internalrule(stepwise,8,'x',var).
no_internalrule(stepwise,1,'x',var).
no_internalrule(stepwise,0,'x',var).
no_internalrule(stepwise,9,'x',var).
no_internalrule(stepwise,2,'x',var).
label(stepwise,'text',type,11).
no_internalrule(stepwise,14,'text',type).
no_internalrule(stepwise,11,'text',type).
no_internalrule(stepwise,4,'text',type).
no_internalrule(stepwise,12,'text',type).
no_internalrule(stepwise,10,'text',type).
no_internalrule(stepwise,7,'text',type).
no_internalrule(stepwise,19,'text',type).
no_internalrule(stepwise,3,'text',type).
no_internalrule(stepwise,5,'text',type).
no_internalrule(stepwise,16,'text',type).
no_internalrule(stepwise,6,'text',type).
no_internalrule(stepwise,8,'text',type).
no_internalrule(stepwise,1,'text',type).
no_internalrule(stepwise,0,'text',type).
no_internalrule(stepwise,9,'text',type).
no_internalrule(stepwise,2,'text',type).
label(stepwise,'doc',type,3).
no_internalrule(stepwise,14,'doc',type).
no_internalrule(stepwise,11,'doc',type).
no_internalrule(stepwise,4,'doc',type).
no_internalrule(stepwise,12,'doc',type).
no_internalrule(stepwise,10,'doc',type).
no_internalrule(stepwise,7,'doc',type).
no_internalrule(stepwise,19,'doc',type).
no_internalrule(stepwise,3,'doc',type).
no_internalrule(stepwise,5,'doc',type).
no_internalrule(stepwise,16,'doc',type).
no_internalrule(stepwise,6,'doc',type).
no_internalrule(stepwise,8,'doc',type).
no_internalrule(stepwise,1,'doc',type).
no_internalrule(stepwise,9,'doc',type).
no_internalrule(stepwise,2,'doc',type).
label(stepwise,'annotation',name,1).
no_internalrule(stepwise,14,'annotation',name).
no_internalrule(stepwise,11,'annotation',name).
no_internalrule(stepwise,4,'annotation',name).
no_internalrule(stepwise,12,'annotation',name).
no_internalrule(stepwise,10,'annotation',name).
no_internalrule(stepwise,7,'annotation',name).
no_internalrule(stepwise,19,'annotation',name).
no_internalrule(stepwise,3,'annotation',name).
no_internalrule(stepwise,16,'annotation',name).
no_internalrule(stepwise,6,'annotation',name).
no_internalrule(stepwise,8,'annotation',name).
no_internalrule(stepwise,1,'annotation',name).
no_internalrule(stepwise,0,'annotation',name).
no_internalrule(stepwise,9,'annotation',name).
no_internalrule(stepwise,2,'annotation',name).
label(stepwise,'elem',type,4).
no_internalrule(stepwise,14,'elem',type).
no_internalrule(stepwise,11,'elem',type).
no_internalrule(stepwise,4,'elem',type).
no_internalrule(stepwise,12,'elem',type).
no_internalrule(stepwise,10,'elem',type).
no_internalrule(stepwise,7,'elem',type).
no_internalrule(stepwise,19,'elem',type).
no_internalrule(stepwise,3,'elem',type).
no_internalrule(stepwise,5,'elem',type).
no_internalrule(stepwise,16,'elem',type).
no_internalrule(stepwise,6,'elem',type).
no_internalrule(stepwise,8,'elem',type).
no_internalrule(stepwise,1,'elem',type).
no_internalrule(stepwise,9,'elem',type).
no_internalrule(stepwise,2,'elem',type).
label(stepwise,'default',namespace,2).
no_internalrule(stepwise,14,'default',namespace).
no_internalrule(stepwise,11,'default',namespace).
no_internalrule(stepwise,4,'default',namespace).
no_internalrule(stepwise,12,'default',namespace).
no_internalrule(stepwise,10,'default',namespace).
no_internalrule(stepwise,7,'default',namespace).
no_internalrule(stepwise,19,'default',namespace).
no_internalrule(stepwise,3,'default',namespace).
no_internalrule(stepwise,5,'default',namespace).
no_internalrule(stepwise,16,'default',namespace).
no_internalrule(stepwise,6,'default',namespace).
no_internalrule(stepwise,8,'default',namespace).
no_internalrule(stepwise,1,'default',namespace).
no_internalrule(stepwise,0,'default',namespace).
no_internalrule(stepwise,9,'default',namespace).
label(stepwise,'att',type,7).
no_internalrule(stepwise,14,'att',type).
no_internalrule(stepwise,11,'att',type).
no_internalrule(stepwise,4,'att',type).
no_internalrule(stepwise,12,'att',type).
no_internalrule(stepwise,10,'att',type).
no_internalrule(stepwise,7,'att',type).
no_internalrule(stepwise,19,'att',type).
no_internalrule(stepwise,3,'att',type).
no_internalrule(stepwise,5,'att',type).
no_internalrule(stepwise,16,'att',type).
no_internalrule(stepwise,6,'att',type).
no_internalrule(stepwise,8,'att',type).
no_internalrule(stepwise,1,'att',type).
no_internalrule(stepwise,0,'att',type).
no_internalrule(stepwise,9,'att',type).
no_internalrule(stepwise,2,'att',type).
label(stepwise,'x',negvar,12).
no_internalrule(stepwise,14,'x',negvar).
no_internalrule(stepwise,11,'x',negvar).
no_internalrule(stepwise,4,'x',negvar).
no_internalrule(stepwise,12,'x',negvar).
no_internalrule(stepwise,10,'x',negvar).
no_internalrule(stepwise,7,'x',negvar).
no_internalrule(stepwise,19,'x',negvar).
no_internalrule(stepwise,3,'x',negvar).
no_internalrule(stepwise,5,'x',negvar).
no_internalrule(stepwise,16,'x',negvar).
no_internalrule(stepwise,6,'x',negvar).
no_internalrule(stepwise,8,'x',negvar).
no_internalrule(stepwise,1,'x',negvar).
no_internalrule(stepwise,0,'x',negvar).
no_internalrule(stepwise,9,'x',negvar).
no_internalrule(stepwise,2,'x',negvar).
label(stepwise,'comment',type,8).
no_internalrule(stepwise,14,'comment',type).
no_internalrule(stepwise,11,'comment',type).
no_internalrule(stepwise,4,'comment',type).
no_internalrule(stepwise,12,'comment',type).
no_internalrule(stepwise,10,'comment',type).
no_internalrule(stepwise,7,'comment',type).
no_internalrule(stepwise,19,'comment',type).
no_internalrule(stepwise,3,'comment',type).
no_internalrule(stepwise,5,'comment',type).
no_internalrule(stepwise,16,'comment',type).
no_internalrule(stepwise,6,'comment',type).
no_internalrule(stepwise,8,'comment',type).
no_internalrule(stepwise,1,'comment',type).
no_internalrule(stepwise,0,'comment',type).
no_internalrule(stepwise,9,'comment',type).
no_internalrule(stepwise,2,'comment',type).
max_label_id(stepwise,12).
    %%% different origins %%%%%%%
    
