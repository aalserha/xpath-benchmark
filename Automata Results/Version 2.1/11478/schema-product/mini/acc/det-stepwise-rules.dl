%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Accessible states of stepwise hedge automata A
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% whether variable is allowed or not

kind(0).
kind(1).

%% whether we started a tree or a hedge

start(t).
start(h).

%% types different from type var


nonvar(type).
nonvar(char).
nonvar(negvar).
nonvar(name).
nonvar(namespace).

%% starting trees and hedges

acc(t,0,A,X) :- start_tree(A,X).
acc(h,0,A,X) :- initial(A,X).

%% ending trees

acc_tree_state(K,A,Z) :- acc(t,K,A,X), treerule(A,X,Z).
acc_tree_state(K,A,Y) :- acc_tree_state(K,A,X), is(A,X,Y).

%% internal rules

internalrule_all(A,X,L,T,Y) :- internalrule(A,X,L,T,Y).
internalrule_all(A,X,L,T,Y) :- elserule(A,X,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,L,T,Y) :- typedelserule(A,X,T,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- elserule(A,X,Y), typedelserule(A,L,T,Z).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- typedelserule(A,X,T,Y).

acc(S,K,A,Y) :- acc(S,K,A,X), internalrule_all(A,X,L,T  ,Y), nonvar(T).
acc(S,1,A,Y) :- acc(S,0,A,X), internalrule_all(A,X,L,var,Y).

%% epsilon rules

acc(S,K,A,Y) :- acc(S,K,A,X), epsilonrule(A,X,Y), kind(K).

%% apply rules

applyrule_all(A,X,Z,Y) :- applyrule(A,X,Z,Y).
applyrule_all(A,X,Z,Y) :- applyelserule(A,X,Y), no_applyrule(A,X,Z).

acc(S,K,A,Y) :- acc(S,K,A,X), acc_tree_state(0,A,Z), applyrule_all(A,X,Z,Y).
acc(S,1,A,Y) :- acc(S,0,A,X), acc_tree_state(1,A,Z), applyrule_all(A,X,Z,Y).

%% compatibility with previous predicates

acc_h(A,X) :- acc(h,1,A,X).
acc_t(A,X) :- acc(t,1,A,X).




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% label states where XML labels end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

internal_or_else(A,X,Y) :- internalrule(A,X,L,T,Y).
internal_or_else(A,X,Y) :- elserule(A,X,Y).
internal_or_else(A,X,Y) :- typedelserule(A,X,T,Y).

label_state(A,U) :- initial(A,X), internalrule(A,X,'elem',type,Y),internal_or_else(A,Y,Z),
		    internal_or_else(A,Z,U).
label_state(A,U) :- initial(A,X), internalrule(A,X,'att',type,Y), internal_or_else(A,Y,Z),
                    internal_or_else(A,Z,U).
label_state(A,Y) :- initial(A,X), internalrule(A,X,'text',type,Y). 
label_state(A,Y) :- initial(A,X), internalrule(A,X,'comment',type,Y). 
label_state(A,Y) :- initial(A,X), internalrule(A,X,'doc',type,Y). 

acc_label(A,X,X) :- label_state(A,X).
acc_label(A,X,Z) :- acc_label(A,X,Y), internalrule(A,Y,L,T,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), elserule(A,Y,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), typedelserule(A,Y,T,Z).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(A,Z), applyrule(A,X1,Z,Y).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(A,Z), applyelserule(A,X1,Y), no_applyrule(A,X1,Z).

acc_label_tree(A,X,Z) :- acc_label(A,X,Y), treerule(A,Y,Z).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% types of label states
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

elem(A,NS,N,U)   :- initial(A,X), internalrule(A,X,'elem',type,NS),internal_or_else(A,NS,N),
		    internal_or_else(A,N,U).
att(A,NS,N,U)    :- initial(A,X), internalrule(A,X,'att',type,NS), internal_or_else(A,NS,N),
                    internal_or_else(A,N,U).
text(A,Y)	 :- initial(A,X), internalrule(A,X,'text',type,Y). 
comment(A,Y)     :- initial(A,X), internalrule(A,X,'comment',type,Y). 
doc(A,Y)         :- initial(A,X), internalrule(A,X,'doc',type,Y).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule
% negative information (NEGATIVE=true)
%   no_applyrule
%   no_elserule
%   no_treerule
%   no_internalrule
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(stepwise,'mini(det_Ttyped[stepwise[]])').
var(stepwise,'x',1).
no_typedelserule(stepwise,4,type).
no_typedelserule(stepwise,12,type).
no_typedelserule(stepwise,9,type).
no_typedelserule(stepwise,21,type).
no_typedelserule(stepwise,25,type).
no_typedelserule(stepwise,13,type).
no_typedelserule(stepwise,20,type).
no_typedelserule(stepwise,11,type).
no_typedelserule(stepwise,18,type).
no_typedelserule(stepwise,19,type).
no_typedelserule(stepwise,14,type).
no_typedelserule(stepwise,29,type).
no_typedelserule(stepwise,26,type).
no_typedelserule(stepwise,27,type).
no_typedelserule(stepwise,7,type).
no_typedelserule(stepwise,17,type).
no_typedelserule(stepwise,1,type).
no_typedelserule(stepwise,16,type).
no_typedelserule(stepwise,15,type).
no_typedelserule(stepwise,24,type).
no_typedelserule(stepwise,3,type).
no_typedelserule(stepwise,31,type).
no_typedelserule(stepwise,0,type).
no_typedelserule(stepwise,6,type).
no_typedelserule(stepwise,2,type).
no_typedelserule(stepwise,10,type).
no_typedelserule(stepwise,23,type).
no_typedelserule(stepwise,5,type).
no_typedelserule(stepwise,8,type).
no_typedelserule(stepwise,4,var).
no_typedelserule(stepwise,12,var).
no_typedelserule(stepwise,9,var).
no_typedelserule(stepwise,21,var).
no_typedelserule(stepwise,25,var).
no_typedelserule(stepwise,13,var).
no_typedelserule(stepwise,20,var).
no_typedelserule(stepwise,11,var).
no_typedelserule(stepwise,18,var).
no_typedelserule(stepwise,19,var).
no_typedelserule(stepwise,14,var).
no_typedelserule(stepwise,29,var).
no_typedelserule(stepwise,26,var).
no_typedelserule(stepwise,27,var).
no_typedelserule(stepwise,7,var).
no_typedelserule(stepwise,17,var).
no_typedelserule(stepwise,1,var).
no_typedelserule(stepwise,16,var).
no_typedelserule(stepwise,15,var).
no_typedelserule(stepwise,24,var).
no_typedelserule(stepwise,3,var).
no_typedelserule(stepwise,31,var).
no_typedelserule(stepwise,0,var).
no_typedelserule(stepwise,6,var).
no_typedelserule(stepwise,2,var).
no_typedelserule(stepwise,10,var).
no_typedelserule(stepwise,23,var).
no_typedelserule(stepwise,5,var).
no_typedelserule(stepwise,8,var).
no_typedelserule(stepwise,4,negvar).
no_typedelserule(stepwise,12,negvar).
no_typedelserule(stepwise,9,negvar).
no_typedelserule(stepwise,21,negvar).
no_typedelserule(stepwise,25,negvar).
no_typedelserule(stepwise,13,negvar).
no_typedelserule(stepwise,20,negvar).
no_typedelserule(stepwise,11,negvar).
no_typedelserule(stepwise,18,negvar).
no_typedelserule(stepwise,19,negvar).
no_typedelserule(stepwise,14,negvar).
no_typedelserule(stepwise,29,negvar).
no_typedelserule(stepwise,26,negvar).
no_typedelserule(stepwise,27,negvar).
no_typedelserule(stepwise,7,negvar).
no_typedelserule(stepwise,17,negvar).
no_typedelserule(stepwise,1,negvar).
no_typedelserule(stepwise,16,negvar).
no_typedelserule(stepwise,15,negvar).
no_typedelserule(stepwise,24,negvar).
no_typedelserule(stepwise,3,negvar).
no_typedelserule(stepwise,31,negvar).
no_typedelserule(stepwise,0,negvar).
no_typedelserule(stepwise,6,negvar).
no_typedelserule(stepwise,2,negvar).
no_typedelserule(stepwise,10,negvar).
no_typedelserule(stepwise,23,negvar).
no_typedelserule(stepwise,5,negvar).
no_typedelserule(stepwise,8,negvar).
no_typedelserule(stepwise,4,name).
no_typedelserule(stepwise,12,name).
no_typedelserule(stepwise,21,name).
no_typedelserule(stepwise,25,name).
no_typedelserule(stepwise,13,name).
no_typedelserule(stepwise,20,name).
no_typedelserule(stepwise,11,name).
no_typedelserule(stepwise,18,name).
no_typedelserule(stepwise,19,name).
no_typedelserule(stepwise,14,name).
no_typedelserule(stepwise,29,name).
no_typedelserule(stepwise,26,name).
no_typedelserule(stepwise,27,name).
no_typedelserule(stepwise,17,name).
no_typedelserule(stepwise,1,name).
no_typedelserule(stepwise,16,name).
no_typedelserule(stepwise,15,name).
no_typedelserule(stepwise,24,name).
no_typedelserule(stepwise,3,name).
no_typedelserule(stepwise,31,name).
no_typedelserule(stepwise,0,name).
no_typedelserule(stepwise,2,name).
no_typedelserule(stepwise,10,name).
no_typedelserule(stepwise,23,name).
no_typedelserule(stepwise,5,name).
no_typedelserule(stepwise,4,namespace).
no_typedelserule(stepwise,12,namespace).
no_typedelserule(stepwise,9,namespace).
no_typedelserule(stepwise,21,namespace).
no_typedelserule(stepwise,25,namespace).
no_typedelserule(stepwise,13,namespace).
no_typedelserule(stepwise,20,namespace).
no_typedelserule(stepwise,11,namespace).
no_typedelserule(stepwise,18,namespace).
no_typedelserule(stepwise,19,namespace).
no_typedelserule(stepwise,14,namespace).
no_typedelserule(stepwise,29,namespace).
no_typedelserule(stepwise,26,namespace).
no_typedelserule(stepwise,27,namespace).
no_typedelserule(stepwise,7,namespace).
no_typedelserule(stepwise,17,namespace).
no_typedelserule(stepwise,1,namespace).
no_typedelserule(stepwise,16,namespace).
no_typedelserule(stepwise,15,namespace).
no_typedelserule(stepwise,24,namespace).
no_typedelserule(stepwise,31,namespace).
no_typedelserule(stepwise,0,namespace).
no_typedelserule(stepwise,6,namespace).
no_typedelserule(stepwise,10,namespace).
no_typedelserule(stepwise,23,namespace).
no_typedelserule(stepwise,5,namespace).
no_typedelserule(stepwise,8,namespace).
no_typedelserule(stepwise,4,char).
no_typedelserule(stepwise,12,char).
no_typedelserule(stepwise,9,char).
no_typedelserule(stepwise,21,char).
no_typedelserule(stepwise,25,char).
no_typedelserule(stepwise,13,char).
no_typedelserule(stepwise,20,char).
no_typedelserule(stepwise,11,char).
no_typedelserule(stepwise,18,char).
no_typedelserule(stepwise,19,char).
no_typedelserule(stepwise,14,char).
no_typedelserule(stepwise,29,char).
no_typedelserule(stepwise,26,char).
no_typedelserule(stepwise,27,char).
no_typedelserule(stepwise,7,char).
no_typedelserule(stepwise,1,char).
no_typedelserule(stepwise,15,char).
no_typedelserule(stepwise,24,char).
no_typedelserule(stepwise,3,char).
no_typedelserule(stepwise,31,char).
no_typedelserule(stepwise,0,char).
no_typedelserule(stepwise,6,char).
no_typedelserule(stepwise,2,char).
no_typedelserule(stepwise,10,char).
no_typedelserule(stepwise,23,char).
no_typedelserule(stepwise,8,char).
state(stepwise,4).
state(stepwise,12).
state(stepwise,9).
state(stepwise,21).
state(stepwise,25).
state(stepwise,13).
state(stepwise,20).
state(stepwise,11).
state(stepwise,18).
state(stepwise,19).
state(stepwise,14).
state(stepwise,29).
state(stepwise,26).
state(stepwise,27).
state(stepwise,7).
state(stepwise,17).
state(stepwise,1).
state(stepwise,16).
state(stepwise,15).
state(stepwise,24).
state(stepwise,3).
state(stepwise,31).
state(stepwise,0).
state(stepwise,6).
state(stepwise,2).
state(stepwise,10).
state(stepwise,23).
state(stepwise,5).
state(stepwise,8).
start_tree(stepwise,0).
initial(stepwise,0).
origin_of(stepwise,4,'57','0').
origin_of(stepwise,12,'52','26','0').
origin_of(stepwise,9,'37','23','0').
origin_of(stepwise,21,'6','1').
origin_of(stepwise,25,'3','1').
origin_of(stepwise,13,'26','0').
origin_of(stepwise,20,'7','6','1').
origin_of(stepwise,11,'5','1').
origin_of(stepwise,18,'39','20','56','54','51','0').
origin_of(stepwise,19,'33','40','0').
origin_of(stepwise,14,'17','10','11','0').
origin_of(stepwise,29,'2','1').
origin_of(stepwise,26,'41','0').
origin_of(stepwise,27,'9','43','0').
origin_of(stepwise,7,'15','0').
origin_of(stepwise,17,'25','0').
origin_of(stepwise,1,'22','0').
origin_of(stepwise,16,'27','25','0').
origin_of(stepwise,15,'17','10','0').
origin_of(stepwise,24,'33','42','40','0').
origin_of(stepwise,3,'19','28','29','0').
origin_of(stepwise,31,'34','0').
origin_of(stepwise,0,'30','45','44','24','12','36','0').
origin_of(stepwise,6,'15','49','0').
origin_of(stepwise,2,'16','8','0').
origin_of(stepwise,10,'47','14','35','0').
origin_of(stepwise,23,'38','50','53','18','55','0').
origin_of(stepwise,5,'21','0').
origin_of(stepwise,8,'37','23','48','0').
not_sink(stepwise,4).
not_sink(stepwise,12).
not_sink(stepwise,9).
not_sink(stepwise,21).
not_sink(stepwise,25).
not_sink(stepwise,13).
not_sink(stepwise,20).
not_sink(stepwise,11).
not_sink(stepwise,18).
not_sink(stepwise,19).
not_sink(stepwise,14).
not_sink(stepwise,29).
not_sink(stepwise,26).
not_sink(stepwise,27).
not_sink(stepwise,7).
not_sink(stepwise,17).
not_sink(stepwise,1).
not_sink(stepwise,16).
not_sink(stepwise,15).
not_sink(stepwise,24).
not_sink(stepwise,3).
not_sink(stepwise,31).
not_sink(stepwise,0).
not_sink(stepwise,6).
not_sink(stepwise,2).
not_sink(stepwise,10).
not_sink(stepwise,23).
not_sink(stepwise,5).
not_sink(stepwise,8).
nonelse(stepwise,4).
nonelse(stepwise,12).
nonelse(stepwise,9).
nonelse(stepwise,21).
nonelse(stepwise,25).
nonelse(stepwise,13).
nonelse(stepwise,20).
nonelse(stepwise,11).
nonelse(stepwise,18).
nonelse(stepwise,19).
nonelse(stepwise,14).
nonelse(stepwise,29).
nonelse(stepwise,26).
nonelse(stepwise,27).
nonelse(stepwise,7).
nonelse(stepwise,17).
nonelse(stepwise,1).
nonelse(stepwise,16).
nonelse(stepwise,15).
nonelse(stepwise,24).
nonelse(stepwise,3).
nonelse(stepwise,31).
nonelse(stepwise,0).
nonelse(stepwise,6).
nonelse(stepwise,2).
nonelse(stepwise,10).
nonelse(stepwise,23).
nonelse(stepwise,5).
nonelse(stepwise,8).
final(stepwise,31).
nonfinal(stepwise,4).
nonfinal(stepwise,12).
nonfinal(stepwise,9).
nonfinal(stepwise,21).
nonfinal(stepwise,25).
nonfinal(stepwise,13).
nonfinal(stepwise,20).
nonfinal(stepwise,11).
nonfinal(stepwise,18).
nonfinal(stepwise,19).
nonfinal(stepwise,14).
nonfinal(stepwise,29).
nonfinal(stepwise,26).
nonfinal(stepwise,27).
nonfinal(stepwise,7).
nonfinal(stepwise,17).
nonfinal(stepwise,1).
nonfinal(stepwise,16).
nonfinal(stepwise,15).
nonfinal(stepwise,24).
nonfinal(stepwise,3).
nonfinal(stepwise,0).
nonfinal(stepwise,6).
nonfinal(stepwise,2).
nonfinal(stepwise,10).
nonfinal(stepwise,23).
nonfinal(stepwise,5).
nonfinal(stepwise,8).
tree_state(stepwise,21).
tree_istate(stepwise,21,0).
 tree_state(stepwise,25).
tree_istate(stepwise,25,0).
 tree_state(stepwise,20).
tree_istate(stepwise,20,0).
 tree_state(stepwise,11).
tree_istate(stepwise,11,0).
 tree_state(stepwise,29).
tree_istate(stepwise,29,0).
 hedge_state(stepwise,4).
    no_applyrule(stepwise,4,21).
	  no_applyrule(stepwise,4,25).
	  no_applyrule(stepwise,4,20).
	  no_applyrule(stepwise,4,11).
	  no_applyrule(stepwise,4,29).
	  no_applyelserule(stepwise,4).
no_elserule(stepwise,4).
	% %
hedge_state(stepwise,12).
    no_applyrule(stepwise,12,21).
	  no_applyrule(stepwise,12,25).
	  no_applyrule(stepwise,12,20).
	  no_applyrule(stepwise,12,11).
	  no_applyrule(stepwise,12,29).
	  no_applyelserule(stepwise,12).
no_elserule(stepwise,12).
	% %
hedge_state(stepwise,9).
    no_applyrule(stepwise,9,21).
	  no_applyrule(stepwise,9,25).
	  no_applyrule(stepwise,9,20).
	  no_applyrule(stepwise,9,11).
	  no_applyrule(stepwise,9,29).
	  no_applyelserule(stepwise,9).
hedge_state(stepwise,13).
    no_applyrule(stepwise,13,21).
	  no_applyrule(stepwise,13,25).
	  no_applyrule(stepwise,13,20).
	  no_applyrule(stepwise,13,11).
	  no_applyrule(stepwise,13,29).
	  no_applyelserule(stepwise,13).
no_elserule(stepwise,13).
	% %
hedge_state(stepwise,18).
    no_applyrule(stepwise,18,29).
	  no_applyelserule(stepwise,18).
no_elserule(stepwise,18).
	% %
hedge_state(stepwise,19).
    no_applyrule(stepwise,19,25).
	  no_applyrule(stepwise,19,11).
	  no_applyrule(stepwise,19,29).
	  no_applyelserule(stepwise,19).
no_elserule(stepwise,19).
	% %
hedge_state(stepwise,14).
    no_applyrule(stepwise,14,21).
	  no_applyrule(stepwise,14,25).
	  no_applyrule(stepwise,14,20).
	  no_applyrule(stepwise,14,11).
	  no_applyrule(stepwise,14,29).
	  no_applyelserule(stepwise,14).
no_elserule(stepwise,14).
	% %
hedge_state(stepwise,26).
    no_applyrule(stepwise,26,21).
	  no_applyrule(stepwise,26,25).
	  no_applyrule(stepwise,26,20).
	  no_applyrule(stepwise,26,29).
	  no_applyelserule(stepwise,26).
no_elserule(stepwise,26).
	% %
hedge_state(stepwise,27).
    no_applyrule(stepwise,27,21).
	  no_applyrule(stepwise,27,25).
	  no_applyrule(stepwise,27,20).
	  no_applyrule(stepwise,27,11).
	  no_applyrule(stepwise,27,29).
	  no_applyelserule(stepwise,27).
no_elserule(stepwise,27).
	% %
hedge_state(stepwise,7).
    no_applyrule(stepwise,7,21).
	  no_applyrule(stepwise,7,25).
	  no_applyrule(stepwise,7,20).
	  no_applyrule(stepwise,7,11).
	  no_applyrule(stepwise,7,29).
	  no_applyelserule(stepwise,7).
hedge_state(stepwise,17).
    no_applyrule(stepwise,17,21).
	  no_applyrule(stepwise,17,25).
	  no_applyrule(stepwise,17,20).
	  no_applyrule(stepwise,17,11).
	  no_applyrule(stepwise,17,29).
	  no_applyelserule(stepwise,17).
hedge_state(stepwise,1).
    no_applyrule(stepwise,1,21).
	  no_applyrule(stepwise,1,25).
	  no_applyrule(stepwise,1,20).
	  no_applyrule(stepwise,1,11).
	  no_applyrule(stepwise,1,29).
	  no_applyelserule(stepwise,1).
no_elserule(stepwise,1).
	% %
hedge_state(stepwise,16).
    no_applyrule(stepwise,16,21).
	  no_applyrule(stepwise,16,25).
	  no_applyrule(stepwise,16,20).
	  no_applyrule(stepwise,16,11).
	  no_applyrule(stepwise,16,29).
	  no_applyelserule(stepwise,16).
hedge_state(stepwise,15).
    no_applyrule(stepwise,15,21).
	  no_applyrule(stepwise,15,25).
	  no_applyrule(stepwise,15,20).
	  no_applyrule(stepwise,15,11).
	  no_applyrule(stepwise,15,29).
	  no_applyelserule(stepwise,15).
no_elserule(stepwise,15).
	% %
hedge_state(stepwise,24).
    no_applyrule(stepwise,24,25).
	  no_applyrule(stepwise,24,29).
	  no_applyelserule(stepwise,24).
no_elserule(stepwise,24).
	% %
hedge_state(stepwise,3).
    no_applyrule(stepwise,3,21).
	  no_applyrule(stepwise,3,25).
	  no_applyrule(stepwise,3,20).
	  no_applyrule(stepwise,3,11).
	  no_applyrule(stepwise,3,29).
	  no_applyelserule(stepwise,3).
hedge_state(stepwise,31).
    no_applyrule(stepwise,31,21).
	  no_applyrule(stepwise,31,25).
	  no_applyrule(stepwise,31,20).
	  no_applyrule(stepwise,31,11).
	  no_applyrule(stepwise,31,29).
	  no_applyelserule(stepwise,31).
no_elserule(stepwise,31).
	% %
hedge_state(stepwise,0).
    no_applyrule(stepwise,0,21).
	  no_applyrule(stepwise,0,25).
	  no_applyrule(stepwise,0,20).
	  no_applyrule(stepwise,0,11).
	  no_applyelserule(stepwise,0).
no_elserule(stepwise,0).
	% %
hedge_state(stepwise,6).
    no_applyrule(stepwise,6,21).
	  no_applyrule(stepwise,6,25).
	  no_applyrule(stepwise,6,20).
	  no_applyrule(stepwise,6,11).
	  no_applyrule(stepwise,6,29).
	  no_applyelserule(stepwise,6).
hedge_state(stepwise,2).
    no_applyrule(stepwise,2,21).
	  no_applyrule(stepwise,2,25).
	  no_applyrule(stepwise,2,20).
	  no_applyrule(stepwise,2,11).
	  no_applyrule(stepwise,2,29).
	  no_applyelserule(stepwise,2).
hedge_state(stepwise,10).
    no_applyrule(stepwise,10,21).
	  no_applyrule(stepwise,10,20).
	  no_applyrule(stepwise,10,11).
	  no_applyrule(stepwise,10,29).
	  no_applyelserule(stepwise,10).
no_elserule(stepwise,10).
	% %
hedge_state(stepwise,23).
    no_applyrule(stepwise,23,21).
	  no_applyrule(stepwise,23,20).
	  no_applyrule(stepwise,23,29).
	  no_applyelserule(stepwise,23).
no_elserule(stepwise,23).
	% %
hedge_state(stepwise,5).
    no_applyrule(stepwise,5,21).
	  no_applyrule(stepwise,5,25).
	  no_applyrule(stepwise,5,20).
	  no_applyrule(stepwise,5,11).
	  no_applyrule(stepwise,5,29).
	  no_applyelserule(stepwise,5).
hedge_state(stepwise,8).
    no_applyrule(stepwise,8,21).
	  no_applyrule(stepwise,8,25).
	  no_applyrule(stepwise,8,20).
	  no_applyrule(stepwise,8,11).
	  no_applyrule(stepwise,8,29).
	  no_applyelserule(stepwise,8).
applyrule(stepwise,18,11,23).
typedelserule(stepwise,3,namespace,9).
internalrule(stepwise,0,'elem',type,3).
applyrule(stepwise,19,21,19).
treerule(stepwise,5,11).
internalrule(stepwise,15,'x',negvar,18).
internalrule(stepwise,0,'text',type,1).
treerule(stepwise,24,25).
applyrule(stepwise,10,25,27).
treerule(stepwise,23,11).
applyrule(stepwise,18,20,18).
typedelserule(stepwise,8,name,15).
applyrule(stepwise,23,11,23).
internalrule(stepwise,8,'p',name,14).
treerule(stepwise,26,25).
internalrule(stepwise,14,'x',var,19).
internalrule(stepwise,1,'x',negvar,5).
typedelserule(stepwise,7,name,13).
internalrule(stepwise,2,'default',namespace,6).
internalrule(stepwise,0,'doc',type,4).
typedelserule(stepwise,17,char,17).
applyrule(stepwise,26,11,26).
internalrule(stepwise,0,'comment',type,1).
applyrule(stepwise,18,21,18).
applyrule(stepwise,18,25,26).
typedelserule(stepwise,16,char,16).
treerule(stepwise,27,29).
typedelserule(stepwise,5,char,5).
internalrule(stepwise,3,'xhtml',namespace,8).
internalrule(stepwise,0,'att',type,2).
applyrule(stepwise,24,20,24).
treerule(stepwise,17,21).
applyrule(stepwise,24,11,26).
internalrule(stepwise,6,'class',name,12).
internalrule(stepwise,4,'x',negvar,10).
applyrule(stepwise,24,21,24).
applyrule(stepwise,23,25,26).
internalrule(stepwise,13,'x',negvar,17).
applyrule(stepwise,0,29,31).
internalrule(stepwise,12,'x',negvar,16).
internalrule(stepwise,14,'x',negvar,18).
typedelserule(stepwise,2,namespace,7).
typedelserule(stepwise,6,name,13).
treerule(stepwise,18,11).
typedelserule(stepwise,9,name,15).
applyrule(stepwise,19,20,24).
treerule(stepwise,16,20).
no_elserule(stepwise,4).
no_elserule(stepwise,12).
no_elserule(stepwise,21).
no_elserule(stepwise,25).
no_elserule(stepwise,13).
no_elserule(stepwise,20).
no_elserule(stepwise,11).
no_elserule(stepwise,18).
no_elserule(stepwise,19).
no_elserule(stepwise,14).
no_elserule(stepwise,29).
no_elserule(stepwise,26).
no_elserule(stepwise,27).
no_elserule(stepwise,1).
no_elserule(stepwise,15).
no_elserule(stepwise,24).
no_elserule(stepwise,31).
no_elserule(stepwise,0).
no_elserule(stepwise,10).
no_elserule(stepwise,23).
no_treerule(stepwise,4).
no_treerule(stepwise,12).
no_treerule(stepwise,9).
no_treerule(stepwise,21).
no_treerule(stepwise,25).
no_treerule(stepwise,13).
no_treerule(stepwise,20).
no_treerule(stepwise,11).
no_treerule(stepwise,19).
no_treerule(stepwise,14).
no_treerule(stepwise,29).
no_treerule(stepwise,7).
no_treerule(stepwise,1).
no_treerule(stepwise,15).
no_treerule(stepwise,3).
no_treerule(stepwise,31).
no_treerule(stepwise,0).
no_treerule(stepwise,6).
no_treerule(stepwise,2).
no_treerule(stepwise,10).
no_treerule(stepwise,8).
label(stepwise,'comment',type,3).
no_internalrule(stepwise,4,'comment',type).
no_internalrule(stepwise,12,'comment',type).
no_internalrule(stepwise,9,'comment',type).
no_internalrule(stepwise,21,'comment',type).
no_internalrule(stepwise,25,'comment',type).
no_internalrule(stepwise,13,'comment',type).
no_internalrule(stepwise,20,'comment',type).
no_internalrule(stepwise,11,'comment',type).
no_internalrule(stepwise,18,'comment',type).
no_internalrule(stepwise,19,'comment',type).
no_internalrule(stepwise,14,'comment',type).
no_internalrule(stepwise,29,'comment',type).
no_internalrule(stepwise,26,'comment',type).
no_internalrule(stepwise,27,'comment',type).
no_internalrule(stepwise,7,'comment',type).
no_internalrule(stepwise,17,'comment',type).
no_internalrule(stepwise,1,'comment',type).
no_internalrule(stepwise,16,'comment',type).
no_internalrule(stepwise,15,'comment',type).
no_internalrule(stepwise,24,'comment',type).
no_internalrule(stepwise,3,'comment',type).
no_internalrule(stepwise,31,'comment',type).
no_internalrule(stepwise,6,'comment',type).
no_internalrule(stepwise,2,'comment',type).
no_internalrule(stepwise,10,'comment',type).
no_internalrule(stepwise,23,'comment',type).
no_internalrule(stepwise,5,'comment',type).
no_internalrule(stepwise,8,'comment',type).
label(stepwise,'elem',type,6).
no_internalrule(stepwise,4,'elem',type).
no_internalrule(stepwise,12,'elem',type).
no_internalrule(stepwise,9,'elem',type).
no_internalrule(stepwise,21,'elem',type).
no_internalrule(stepwise,25,'elem',type).
no_internalrule(stepwise,13,'elem',type).
no_internalrule(stepwise,20,'elem',type).
no_internalrule(stepwise,11,'elem',type).
no_internalrule(stepwise,18,'elem',type).
no_internalrule(stepwise,19,'elem',type).
no_internalrule(stepwise,14,'elem',type).
no_internalrule(stepwise,29,'elem',type).
no_internalrule(stepwise,26,'elem',type).
no_internalrule(stepwise,27,'elem',type).
no_internalrule(stepwise,7,'elem',type).
no_internalrule(stepwise,17,'elem',type).
no_internalrule(stepwise,1,'elem',type).
no_internalrule(stepwise,16,'elem',type).
no_internalrule(stepwise,15,'elem',type).
no_internalrule(stepwise,24,'elem',type).
no_internalrule(stepwise,3,'elem',type).
no_internalrule(stepwise,31,'elem',type).
no_internalrule(stepwise,6,'elem',type).
no_internalrule(stepwise,2,'elem',type).
no_internalrule(stepwise,10,'elem',type).
no_internalrule(stepwise,23,'elem',type).
no_internalrule(stepwise,5,'elem',type).
no_internalrule(stepwise,8,'elem',type).
label(stepwise,'doc',type,5).
no_internalrule(stepwise,4,'doc',type).
no_internalrule(stepwise,12,'doc',type).
no_internalrule(stepwise,9,'doc',type).
no_internalrule(stepwise,21,'doc',type).
no_internalrule(stepwise,25,'doc',type).
no_internalrule(stepwise,13,'doc',type).
no_internalrule(stepwise,20,'doc',type).
no_internalrule(stepwise,11,'doc',type).
no_internalrule(stepwise,18,'doc',type).
no_internalrule(stepwise,19,'doc',type).
no_internalrule(stepwise,14,'doc',type).
no_internalrule(stepwise,29,'doc',type).
no_internalrule(stepwise,26,'doc',type).
no_internalrule(stepwise,27,'doc',type).
no_internalrule(stepwise,7,'doc',type).
no_internalrule(stepwise,17,'doc',type).
no_internalrule(stepwise,1,'doc',type).
no_internalrule(stepwise,16,'doc',type).
no_internalrule(stepwise,15,'doc',type).
no_internalrule(stepwise,24,'doc',type).
no_internalrule(stepwise,3,'doc',type).
no_internalrule(stepwise,31,'doc',type).
no_internalrule(stepwise,6,'doc',type).
no_internalrule(stepwise,2,'doc',type).
no_internalrule(stepwise,10,'doc',type).
no_internalrule(stepwise,23,'doc',type).
no_internalrule(stepwise,5,'doc',type).
no_internalrule(stepwise,8,'doc',type).
label(stepwise,'x',var,11).
no_internalrule(stepwise,4,'x',var).
no_internalrule(stepwise,12,'x',var).
no_internalrule(stepwise,9,'x',var).
no_internalrule(stepwise,21,'x',var).
no_internalrule(stepwise,25,'x',var).
no_internalrule(stepwise,13,'x',var).
no_internalrule(stepwise,20,'x',var).
no_internalrule(stepwise,11,'x',var).
no_internalrule(stepwise,18,'x',var).
no_internalrule(stepwise,19,'x',var).
no_internalrule(stepwise,29,'x',var).
no_internalrule(stepwise,26,'x',var).
no_internalrule(stepwise,27,'x',var).
no_internalrule(stepwise,7,'x',var).
no_internalrule(stepwise,17,'x',var).
no_internalrule(stepwise,1,'x',var).
no_internalrule(stepwise,16,'x',var).
no_internalrule(stepwise,15,'x',var).
no_internalrule(stepwise,24,'x',var).
no_internalrule(stepwise,3,'x',var).
no_internalrule(stepwise,31,'x',var).
no_internalrule(stepwise,0,'x',var).
no_internalrule(stepwise,6,'x',var).
no_internalrule(stepwise,2,'x',var).
no_internalrule(stepwise,10,'x',var).
no_internalrule(stepwise,23,'x',var).
no_internalrule(stepwise,5,'x',var).
no_internalrule(stepwise,8,'x',var).
label(stepwise,'x',negvar,10).
no_internalrule(stepwise,9,'x',negvar).
no_internalrule(stepwise,21,'x',negvar).
no_internalrule(stepwise,25,'x',negvar).
no_internalrule(stepwise,20,'x',negvar).
no_internalrule(stepwise,11,'x',negvar).
no_internalrule(stepwise,18,'x',negvar).
no_internalrule(stepwise,19,'x',negvar).
no_internalrule(stepwise,29,'x',negvar).
no_internalrule(stepwise,26,'x',negvar).
no_internalrule(stepwise,27,'x',negvar).
no_internalrule(stepwise,7,'x',negvar).
no_internalrule(stepwise,17,'x',negvar).
no_internalrule(stepwise,16,'x',negvar).
no_internalrule(stepwise,24,'x',negvar).
no_internalrule(stepwise,3,'x',negvar).
no_internalrule(stepwise,31,'x',negvar).
no_internalrule(stepwise,0,'x',negvar).
no_internalrule(stepwise,6,'x',negvar).
no_internalrule(stepwise,2,'x',negvar).
no_internalrule(stepwise,10,'x',negvar).
no_internalrule(stepwise,23,'x',negvar).
no_internalrule(stepwise,5,'x',negvar).
no_internalrule(stepwise,8,'x',negvar).
label(stepwise,'class',name,2).
no_internalrule(stepwise,4,'class',name).
no_internalrule(stepwise,12,'class',name).
no_internalrule(stepwise,9,'class',name).
no_internalrule(stepwise,21,'class',name).
no_internalrule(stepwise,25,'class',name).
no_internalrule(stepwise,13,'class',name).
no_internalrule(stepwise,20,'class',name).
no_internalrule(stepwise,11,'class',name).
no_internalrule(stepwise,18,'class',name).
no_internalrule(stepwise,19,'class',name).
no_internalrule(stepwise,14,'class',name).
no_internalrule(stepwise,29,'class',name).
no_internalrule(stepwise,26,'class',name).
no_internalrule(stepwise,27,'class',name).
no_internalrule(stepwise,7,'class',name).
no_internalrule(stepwise,17,'class',name).
no_internalrule(stepwise,1,'class',name).
no_internalrule(stepwise,16,'class',name).
no_internalrule(stepwise,15,'class',name).
no_internalrule(stepwise,24,'class',name).
no_internalrule(stepwise,3,'class',name).
no_internalrule(stepwise,31,'class',name).
no_internalrule(stepwise,0,'class',name).
no_internalrule(stepwise,2,'class',name).
no_internalrule(stepwise,10,'class',name).
no_internalrule(stepwise,23,'class',name).
no_internalrule(stepwise,5,'class',name).
no_internalrule(stepwise,8,'class',name).
label(stepwise,'xhtml',namespace,9).
no_internalrule(stepwise,4,'xhtml',namespace).
no_internalrule(stepwise,12,'xhtml',namespace).
no_internalrule(stepwise,9,'xhtml',namespace).
no_internalrule(stepwise,21,'xhtml',namespace).
no_internalrule(stepwise,25,'xhtml',namespace).
no_internalrule(stepwise,13,'xhtml',namespace).
no_internalrule(stepwise,20,'xhtml',namespace).
no_internalrule(stepwise,11,'xhtml',namespace).
no_internalrule(stepwise,18,'xhtml',namespace).
no_internalrule(stepwise,19,'xhtml',namespace).
no_internalrule(stepwise,14,'xhtml',namespace).
no_internalrule(stepwise,29,'xhtml',namespace).
no_internalrule(stepwise,26,'xhtml',namespace).
no_internalrule(stepwise,27,'xhtml',namespace).
no_internalrule(stepwise,7,'xhtml',namespace).
no_internalrule(stepwise,17,'xhtml',namespace).
no_internalrule(stepwise,1,'xhtml',namespace).
no_internalrule(stepwise,16,'xhtml',namespace).
no_internalrule(stepwise,15,'xhtml',namespace).
no_internalrule(stepwise,24,'xhtml',namespace).
no_internalrule(stepwise,31,'xhtml',namespace).
no_internalrule(stepwise,0,'xhtml',namespace).
no_internalrule(stepwise,6,'xhtml',namespace).
no_internalrule(stepwise,2,'xhtml',namespace).
no_internalrule(stepwise,10,'xhtml',namespace).
no_internalrule(stepwise,23,'xhtml',namespace).
no_internalrule(stepwise,5,'xhtml',namespace).
no_internalrule(stepwise,8,'xhtml',namespace).
label(stepwise,'p',name,7).
no_internalrule(stepwise,4,'p',name).
no_internalrule(stepwise,12,'p',name).
no_internalrule(stepwise,9,'p',name).
no_internalrule(stepwise,21,'p',name).
no_internalrule(stepwise,25,'p',name).
no_internalrule(stepwise,13,'p',name).
no_internalrule(stepwise,20,'p',name).
no_internalrule(stepwise,11,'p',name).
no_internalrule(stepwise,18,'p',name).
no_internalrule(stepwise,19,'p',name).
no_internalrule(stepwise,14,'p',name).
no_internalrule(stepwise,29,'p',name).
no_internalrule(stepwise,26,'p',name).
no_internalrule(stepwise,27,'p',name).
no_internalrule(stepwise,7,'p',name).
no_internalrule(stepwise,17,'p',name).
no_internalrule(stepwise,1,'p',name).
no_internalrule(stepwise,16,'p',name).
no_internalrule(stepwise,15,'p',name).
no_internalrule(stepwise,24,'p',name).
no_internalrule(stepwise,3,'p',name).
no_internalrule(stepwise,31,'p',name).
no_internalrule(stepwise,0,'p',name).
no_internalrule(stepwise,6,'p',name).
no_internalrule(stepwise,2,'p',name).
no_internalrule(stepwise,10,'p',name).
no_internalrule(stepwise,23,'p',name).
no_internalrule(stepwise,5,'p',name).
label(stepwise,'text',type,8).
no_internalrule(stepwise,4,'text',type).
no_internalrule(stepwise,12,'text',type).
no_internalrule(stepwise,9,'text',type).
no_internalrule(stepwise,21,'text',type).
no_internalrule(stepwise,25,'text',type).
no_internalrule(stepwise,13,'text',type).
no_internalrule(stepwise,20,'text',type).
no_internalrule(stepwise,11,'text',type).
no_internalrule(stepwise,18,'text',type).
no_internalrule(stepwise,19,'text',type).
no_internalrule(stepwise,14,'text',type).
no_internalrule(stepwise,29,'text',type).
no_internalrule(stepwise,26,'text',type).
no_internalrule(stepwise,27,'text',type).
no_internalrule(stepwise,7,'text',type).
no_internalrule(stepwise,17,'text',type).
no_internalrule(stepwise,1,'text',type).
no_internalrule(stepwise,16,'text',type).
no_internalrule(stepwise,15,'text',type).
no_internalrule(stepwise,24,'text',type).
no_internalrule(stepwise,3,'text',type).
no_internalrule(stepwise,31,'text',type).
no_internalrule(stepwise,6,'text',type).
no_internalrule(stepwise,2,'text',type).
no_internalrule(stepwise,10,'text',type).
no_internalrule(stepwise,23,'text',type).
no_internalrule(stepwise,5,'text',type).
no_internalrule(stepwise,8,'text',type).
label(stepwise,'default',namespace,4).
no_internalrule(stepwise,4,'default',namespace).
no_internalrule(stepwise,12,'default',namespace).
no_internalrule(stepwise,9,'default',namespace).
no_internalrule(stepwise,21,'default',namespace).
no_internalrule(stepwise,25,'default',namespace).
no_internalrule(stepwise,13,'default',namespace).
no_internalrule(stepwise,20,'default',namespace).
no_internalrule(stepwise,11,'default',namespace).
no_internalrule(stepwise,18,'default',namespace).
no_internalrule(stepwise,19,'default',namespace).
no_internalrule(stepwise,14,'default',namespace).
no_internalrule(stepwise,29,'default',namespace).
no_internalrule(stepwise,26,'default',namespace).
no_internalrule(stepwise,27,'default',namespace).
no_internalrule(stepwise,7,'default',namespace).
no_internalrule(stepwise,17,'default',namespace).
no_internalrule(stepwise,1,'default',namespace).
no_internalrule(stepwise,16,'default',namespace).
no_internalrule(stepwise,15,'default',namespace).
no_internalrule(stepwise,24,'default',namespace).
no_internalrule(stepwise,3,'default',namespace).
no_internalrule(stepwise,31,'default',namespace).
no_internalrule(stepwise,0,'default',namespace).
no_internalrule(stepwise,6,'default',namespace).
no_internalrule(stepwise,10,'default',namespace).
no_internalrule(stepwise,23,'default',namespace).
no_internalrule(stepwise,5,'default',namespace).
no_internalrule(stepwise,8,'default',namespace).
label(stepwise,'att',type,1).
no_internalrule(stepwise,4,'att',type).
no_internalrule(stepwise,12,'att',type).
no_internalrule(stepwise,9,'att',type).
no_internalrule(stepwise,21,'att',type).
no_internalrule(stepwise,25,'att',type).
no_internalrule(stepwise,13,'att',type).
no_internalrule(stepwise,20,'att',type).
no_internalrule(stepwise,11,'att',type).
no_internalrule(stepwise,18,'att',type).
no_internalrule(stepwise,19,'att',type).
no_internalrule(stepwise,14,'att',type).
no_internalrule(stepwise,29,'att',type).
no_internalrule(stepwise,26,'att',type).
no_internalrule(stepwise,27,'att',type).
no_internalrule(stepwise,7,'att',type).
no_internalrule(stepwise,17,'att',type).
no_internalrule(stepwise,1,'att',type).
no_internalrule(stepwise,16,'att',type).
no_internalrule(stepwise,15,'att',type).
no_internalrule(stepwise,24,'att',type).
no_internalrule(stepwise,3,'att',type).
no_internalrule(stepwise,31,'att',type).
no_internalrule(stepwise,6,'att',type).
no_internalrule(stepwise,2,'att',type).
no_internalrule(stepwise,10,'att',type).
no_internalrule(stepwise,23,'att',type).
no_internalrule(stepwise,5,'att',type).
no_internalrule(stepwise,8,'att',type).
max_label_id(stepwise,11).
    %%% different origins %%%%%%%
    
