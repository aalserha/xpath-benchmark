diff_final(A,X,X1) :- final(A,X), nonfinal(A,X1), hedge_state(A,X1).
diff_type(A,X,X1)  :- hedge_state(A,X), tree_state(A,X1).
diff_sym(A,X1,X)   :- diff(A,X,X1).
diff_else(A,X,X1)  :- else(A,X), nonelse(A,X1), tree_state(A,X1).

internal(A,X,L,Type,Y)  :- internalrule(A,X,L,Type,Y).
internal(A,X,L,Type,Y)  :- no_internalrule(A,X,L,Type), elserule(A,X,Y).
internal(A,X,L,Type,Y)  :- no_internalrule(A,X,L,Type), typedelserule(A,X,Type,Y).
no_internal(A,X,L,Type) :- hedge_state(A,X), no_internalrule(A,X,L,Type),
                           no_elserule(A,X), no_typedelserule(A,X,Type).

apply(A,X,Y,Z)  :- applyrule(A,X,Y,Z).
apply(A,X,Y,Z)  :- applyelse(A,X,Y,Z).

applyelse(A,X,Y,Z)  :- applyelserule(A,X,Z), else(A,Y), no_applyrule(A,X,Y).
no_apply(A,X,Y)     :- no_applyrule(A,X,Y),
		       no_applyelserule_with(A,X,Y), tree_state(A,Y).
no_applyelserule_with(A,X,Y) :- no_applyelserule(A,X), tree_state(A,Y). 
no_applyelserule_with(A,X,Y) :- applyelserule(A,X,Z),
			        nonelse(A,Y), tree_state(Y).
no_applyelserule_with(A,X,Y) :- applyelserule(A,X,Z),
			        applyrule(A,X,Y,Z1).

type_lab(A,T) :- typedeleserule(A,X,T,Y).
type_lab(A,T) :- no_typedeleserule(A,X,T).
typedelserule(A,X,T,Y) :- elserule(A,X,Y), type_lab(A,T).

diff_typedelse(A,X,T,X1) :- typedelserule(A,X,T,Y), no_typedelserule(A,X1,T).
% self_diff_typedelse(X)   :- diff_typedelse(A,X,T,X).
diff(A,X,X1) :- diff_typedelse(A,X,T,X1).
diff(A,X,X1) :- diff_typedelserule(A,X,T,X1).

diff(A,X,X1)      :- diff_tree(A,X,X1).
% self_diff_tree(X) :- diff_tree(A,X,X).
diff(A,X,X1)      :- diff_type(A,X,X1).
% self_diff_type(X) :- diff_type(A,X,X).
diff(A,X,X1)      :- diff_final(A,X,X1).
% self_diff_final(X):- diff_final(A,X,X).
diff(A,X,X1)      :- diff_else(A,X,X1).
% self_diff_else(X) :- diff_else(A,X,X).

diff(A,X,X1) :- diff_treerule(A,X,X1).
diff(A,X,X1) :- diff_no_treerule(A,X,X1).
diff(A,X,X1) :- diff_apply(A,X,X1).
diff(A,X,X1) :- diff_no_apply(A,X,X1).
diff(A,X,X1) :- diff_internal(A,X,X1).

diff(A,X,X1) :- diff_no_internal(A,X,X1).
diff(A,X,X1) :- diff_nonelse(A,X,X1).
diff(A,X,X1) :- diff_origin(A,X,X1).
diff(A,X,X1) :- diff_sym(A,X,X1).
diff(A,X,X1) :- diff_elserule(A,X,X1).
diff(A,X,X1) :- diff_no_elserule(A,X,X1).
diff(A,X,X1) :- diff_no_typedelserule(A,X,X1).

diff_apply_from1(A,X,X1,Z,Z1)  :- apply(A,X,Y,Z), apply(A,X1,Y,Z1), diff(A,Z,Z1).
diff_apply_from2(A,Y,Y1,Z,Z1)  :- apply(A,X,Y,Z), apply(A,X,Y1,Z1), diff(A,Z,Z1).
% self_debug_diff_apply_from1(X,Y,Z,Z1) :- apply(A,X,Y,Z), apply(A,X,Y,Z1), diff(A,Z,Z1).
% self_debug_diff_apply_from2(Y,X,Z,Z1) :- apply(A,X,Y,Z), apply(A,X,Y,Z1), diff(A,Z,Z1).
diff_apply(A,X,X1)             :- diff_apply_from1(A,X,X1,Z,Z1).
diff_apply(A,X,X1)             :- diff_apply_from2(A,X,X1,Z,Z1).

diff_no_apply_from1(A,X,X1,Y,Z):- apply(A,X,Y,Z), no_apply(A,X1,Y).
diff_no_apply_from2(A,Y,Y1,X,Z):- apply(A,X,Y,Z), no_apply(A,X,Y1).
% self_debug_diff_no_apply_from1(X,Y,Z) :- apply(A,X,Y,Z), no_apply(A,X,Y).
% self_debug_diff_no_apply_from2(Y,X,Z) :- apply(A,X,Y,Z), no_apply(A,X,Y).
diff_no_apply(A,X,X1)          :- diff_no_apply_from1(A,X,X1,Y,Z).
diff_no_apply(A,X,X1)          :- diff_no_apply_from2(A,X,X1,Y,Z).

diff_treerule(A,X,X1)     :- treerule(A,X,Y), treerule(A,X1,Y1), diff(A,Y,Y1).
% self_debug_diff_treerule(X,Y,Y1) :- treerule(A,X,Y), treerule(A,X,Y1), diff(A,Y,Y1).
diff_no_treerule(A,X,X1)  :- treerule(A,X,Y), no_treerule(A,X1), hedge_state(A,X1).
% self_diff_no_treerule(X)  :-  treerule(A,X,Y), no_treerule(A,X), hedge_state(A,X).

diff_internal(A,X,X1) :- diff_internal_from(A,X,X1,L,Type,Y,Y1).
diff_internal_from(A,X,X1,L,Type,Y,Y1)
                      :- internal(A,X,L,Type,Y),
                         internal(A,X1,L,Type,Y1), diff(A,Y,Y1).
% self_debug_diff_internal_from(A,X,L,Type,Y,Y1):-diff_internal_from(A,X,X,L,Type,Y,Y1).
			 
diff_no_internal(A,X,X1) :- no_internal(A,X,L,Type), internal(A,X1,L,Type,Y1).
% self_debug_diff_no_internal(X,L,Type,Y1) :- no_internal(A,X,L,Type), internal(A,X,L,Type,Y1).

diff_elserule(A,X,X1) :- elserule(A,X,Y), elserule(A,X1,Y1), diff(A,Y,Y1).
% self_debug_diff_elserule(X,Y,Y1) :- elserule(A,X,Y), elserule(A,X,Y1), diff(A,Y,Y1).
diff_typedelserule(A,X,T,X1) :- typedelserule(A,X,T,Y), typedelserule(A,X1,T,Y1), diff(A,Y,Y1).
% self_diff_typedelserule(X)   :- typedelserule(A,X,T,Y), typedelserule(A,X,T,Y1),  diff(A,Y,Y1).
% self_debug_diff_typedelserule(X,Y,Y1) :- typedelserule(A,X,T,Y), typedelserule(A,X,T,Y1), diff(A,Y,Y1).

diff_no_elserule(A,X,X1) :- elserule(A,X,Y), no_elserule(A,X1).
diff_no_typedelserule(A,X,X1) :- typedelserule(A,X,T,Y), no_elserule(A,X1),no_typedelserule(A,X1,T).

% self_diff_no_elserule(X) :- diff_no_elserule(A,X,X).

% don't fusion states with different schema origins
%    (Joachim: should first diff_orig rule be switched on or off?)

% diff_origin(A,X1,X) :- origin_of(A,X1,Y1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).
% self_diff_origin(X) :- origin_of(A,XY1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).
% self_debug_diff_origin(X,Y1,Z1,Y,Z) :- origin_of(A,X,Y1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).

% for minimization replace rule(A,X,Y,Z) by rule(A,min(X),min(Y),min(Z)) 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule
% negative information (NEGATIVE=true)
%   no_applyrule
%   no_elserule
%   no_treerule
%   no_internalrule
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(step,'schema-clean[det_Ttyped[schema-clean[pc[stepwise[]]]]]').
var(step,'x',1).
no_typedelserule(step,4,char).
no_typedelserule(step,22,char).
no_typedelserule(step,28,char).
no_typedelserule(step,20,char).
no_typedelserule(step,29,char).
no_typedelserule(step,21,char).
no_typedelserule(step,35,char).
no_typedelserule(step,13,char).
no_typedelserule(step,6,char).
no_typedelserule(step,38,char).
no_typedelserule(step,24,char).
no_typedelserule(step,26,char).
no_typedelserule(step,37,char).
no_typedelserule(step,14,char).
no_typedelserule(step,4,type).
no_typedelserule(step,22,type).
no_typedelserule(step,28,type).
no_typedelserule(step,20,type).
no_typedelserule(step,29,type).
no_typedelserule(step,21,type).
no_typedelserule(step,35,type).
no_typedelserule(step,13,type).
no_typedelserule(step,6,type).
no_typedelserule(step,38,type).
no_typedelserule(step,24,type).
no_typedelserule(step,26,type).
no_typedelserule(step,37,type).
no_typedelserule(step,14,type).
no_typedelserule(step,4,var).
no_typedelserule(step,22,var).
no_typedelserule(step,28,var).
no_typedelserule(step,20,var).
no_typedelserule(step,29,var).
no_typedelserule(step,21,var).
no_typedelserule(step,35,var).
no_typedelserule(step,13,var).
no_typedelserule(step,6,var).
no_typedelserule(step,38,var).
no_typedelserule(step,24,var).
no_typedelserule(step,26,var).
no_typedelserule(step,37,var).
no_typedelserule(step,14,var).
no_typedelserule(step,4,negvar).
no_typedelserule(step,22,negvar).
no_typedelserule(step,28,negvar).
no_typedelserule(step,20,negvar).
no_typedelserule(step,29,negvar).
no_typedelserule(step,21,negvar).
no_typedelserule(step,35,negvar).
no_typedelserule(step,13,negvar).
no_typedelserule(step,6,negvar).
no_typedelserule(step,38,negvar).
no_typedelserule(step,24,negvar).
no_typedelserule(step,26,negvar).
no_typedelserule(step,37,negvar).
no_typedelserule(step,14,negvar).
no_typedelserule(step,4,namespace).
no_typedelserule(step,22,namespace).
no_typedelserule(step,28,namespace).
no_typedelserule(step,20,namespace).
no_typedelserule(step,29,namespace).
no_typedelserule(step,21,namespace).
no_typedelserule(step,35,namespace).
no_typedelserule(step,13,namespace).
no_typedelserule(step,6,namespace).
no_typedelserule(step,38,namespace).
no_typedelserule(step,24,namespace).
no_typedelserule(step,26,namespace).
no_typedelserule(step,37,namespace).
no_typedelserule(step,14,namespace).
no_typedelserule(step,4,name).
no_typedelserule(step,22,name).
no_typedelserule(step,28,name).
no_typedelserule(step,20,name).
no_typedelserule(step,29,name).
no_typedelserule(step,21,name).
no_typedelserule(step,35,name).
no_typedelserule(step,13,name).
no_typedelserule(step,6,name).
no_typedelserule(step,38,name).
no_typedelserule(step,24,name).
no_typedelserule(step,26,name).
no_typedelserule(step,37,name).
no_typedelserule(step,14,name).
state(step,4).
state(step,22).
state(step,28).
state(step,20).
state(step,29).
state(step,7).
state(step,10).
state(step,21).
state(step,35).
state(step,19).
state(step,13).
state(step,6).
state(step,5).
state(step,11).
state(step,3).
state(step,8).
state(step,16).
state(step,2).
state(step,9).
state(step,17).
state(step,1).
state(step,0).
state(step,38).
state(step,15).
state(step,12).
state(step,18).
state(step,24).
state(step,26).
state(step,37).
state(step,14).
start_tree(step,0).
initial(step,0).
not_sink(step,4).
not_sink(step,22).
not_sink(step,28).
not_sink(step,20).
not_sink(step,29).
not_sink(step,7).
not_sink(step,10).
not_sink(step,21).
not_sink(step,35).
not_sink(step,19).
not_sink(step,13).
not_sink(step,6).
not_sink(step,5).
not_sink(step,11).
not_sink(step,3).
not_sink(step,8).
not_sink(step,16).
not_sink(step,2).
not_sink(step,9).
not_sink(step,17).
not_sink(step,1).
not_sink(step,0).
not_sink(step,38).
not_sink(step,15).
not_sink(step,12).
not_sink(step,18).
not_sink(step,24).
not_sink(step,26).
not_sink(step,37).
not_sink(step,14).
else(step,4).
else(step,22).
else(step,28).
else(step,20).
else(step,29).
nonelse(step,7).
nonelse(step,10).
nonelse(step,21).
nonelse(step,35).
nonelse(step,19).
nonelse(step,13).
nonelse(step,6).
nonelse(step,5).
nonelse(step,11).
nonelse(step,3).
nonelse(step,8).
nonelse(step,16).
nonelse(step,2).
nonelse(step,9).
nonelse(step,17).
nonelse(step,1).
nonelse(step,0).
nonelse(step,38).
nonelse(step,15).
nonelse(step,12).
nonelse(step,18).
nonelse(step,24).
nonelse(step,26).
nonelse(step,37).
nonelse(step,14).
final(step,38).
nonfinal(step,4).
nonfinal(step,22).
nonfinal(step,28).
nonfinal(step,20).
nonfinal(step,29).
nonfinal(step,7).
nonfinal(step,10).
nonfinal(step,21).
nonfinal(step,35).
nonfinal(step,19).
nonfinal(step,13).
nonfinal(step,6).
nonfinal(step,5).
nonfinal(step,11).
nonfinal(step,3).
nonfinal(step,8).
nonfinal(step,16).
nonfinal(step,2).
nonfinal(step,9).
nonfinal(step,17).
nonfinal(step,1).
nonfinal(step,0).
nonfinal(step,15).
nonfinal(step,12).
nonfinal(step,18).
nonfinal(step,24).
nonfinal(step,26).
nonfinal(step,37).
nonfinal(step,14).
copy_of(step,4,4).
copy_of(step,22,22).
copy_of(step,28,28).
copy_of(step,20,20).
copy_of(step,29,29).
tree_state(step,4).
tree_istate(step,4,0).
 tree_state(step,22).
tree_istate(step,22,0).
 tree_state(step,28).
tree_istate(step,28,0).
 tree_state(step,20).
tree_istate(step,20,0).
 tree_state(step,29).
tree_istate(step,29,0).
 hedge_state(step,7).
    no_applyrule(step,7,4).
	  no_applyrule(step,7,22).
	  no_applyrule(step,7,28).
	  no_applyrule(step,7,20).
	  no_applyrule(step,7,29).
	  no_applyelserule(step,7).
hedge_state(step,10).
    no_applyrule(step,10,4).
	  no_applyrule(step,10,22).
	  no_applyrule(step,10,28).
	  no_applyrule(step,10,20).
	  no_applyrule(step,10,29).
	  no_applyelserule(step,10).
hedge_state(step,21).
    no_applyrule(step,21,22).
	  no_applyrule(step,21,28).
	  no_applyrule(step,21,29).
	  no_applyelserule(step,21).
no_elserule(step,21).
	% %
hedge_state(step,35).
    no_applyrule(step,35,22).
	  no_applyrule(step,35,28).
	  no_applyrule(step,35,20).
	  no_applyrule(step,35,29).
	  no_applyelserule(step,35).
no_elserule(step,35).
	% %
hedge_state(step,19).
    no_applyrule(step,19,4).
	  no_applyrule(step,19,22).
	  no_applyrule(step,19,28).
	  no_applyrule(step,19,20).
	  no_applyrule(step,19,29).
	  no_applyelserule(step,19).
hedge_state(step,13).
    no_applyrule(step,13,22).
	  no_applyrule(step,13,28).
	  no_applyrule(step,13,29).
	  no_applyelserule(step,13).
no_elserule(step,13).
	% %
hedge_state(step,6).
    no_applyrule(step,6,4).
	  no_applyrule(step,6,20).
	  no_applyrule(step,6,29).
	  no_applyelserule(step,6).
no_elserule(step,6).
	% %
hedge_state(step,5).
    no_applyrule(step,5,4).
	  no_applyrule(step,5,22).
	  no_applyrule(step,5,28).
	  no_applyrule(step,5,20).
	  no_applyrule(step,5,29).
	  no_applyelserule(step,5).
hedge_state(step,11).
    no_applyrule(step,11,4).
	  no_applyrule(step,11,22).
	  no_applyrule(step,11,28).
	  no_applyrule(step,11,20).
	  no_applyrule(step,11,29).
	  no_applyelserule(step,11).
hedge_state(step,3).
    no_applyrule(step,3,4).
	  no_applyrule(step,3,22).
	  no_applyrule(step,3,28).
	  no_applyrule(step,3,20).
	  no_applyrule(step,3,29).
	  no_applyelserule(step,3).
hedge_state(step,8).
    no_applyrule(step,8,4).
	  no_applyrule(step,8,22).
	  no_applyrule(step,8,28).
	  no_applyrule(step,8,20).
	  no_applyrule(step,8,29).
	  no_applyelserule(step,8).
hedge_state(step,16).
    no_applyrule(step,16,4).
	  no_applyrule(step,16,22).
	  no_applyrule(step,16,28).
	  no_applyrule(step,16,20).
	  no_applyrule(step,16,29).
	  no_applyelserule(step,16).
hedge_state(step,2).
    no_applyrule(step,2,4).
	  no_applyrule(step,2,22).
	  no_applyrule(step,2,28).
	  no_applyrule(step,2,20).
	  no_applyrule(step,2,29).
	  no_applyelserule(step,2).
hedge_state(step,9).
    no_applyrule(step,9,4).
	  no_applyrule(step,9,22).
	  no_applyrule(step,9,28).
	  no_applyrule(step,9,20).
	  no_applyrule(step,9,29).
	  no_applyelserule(step,9).
hedge_state(step,17).
    no_applyrule(step,17,4).
	  no_applyrule(step,17,22).
	  no_applyrule(step,17,28).
	  no_applyrule(step,17,20).
	  no_applyrule(step,17,29).
	  no_applyelserule(step,17).
hedge_state(step,1).
    no_applyrule(step,1,4).
	  no_applyrule(step,1,22).
	  no_applyrule(step,1,28).
	  no_applyrule(step,1,20).
	  no_applyrule(step,1,29).
	  no_applyelserule(step,1).
hedge_state(step,0).
    no_applyrule(step,0,4).
	  no_applyrule(step,0,22).
	  no_applyrule(step,0,28).
	  no_applyrule(step,0,20).
	  no_applyelserule(step,0).
hedge_state(step,38).
    no_applyrule(step,38,4).
	  no_applyrule(step,38,22).
	  no_applyrule(step,38,28).
	  no_applyrule(step,38,20).
	  no_applyrule(step,38,29).
	  no_applyelserule(step,38).
no_elserule(step,38).
	% %
hedge_state(step,15).
    no_applyrule(step,15,4).
	  no_applyrule(step,15,22).
	  no_applyrule(step,15,28).
	  no_applyrule(step,15,20).
	  no_applyrule(step,15,29).
	  no_applyelserule(step,15).
hedge_state(step,12).
    no_applyrule(step,12,4).
	  no_applyrule(step,12,22).
	  no_applyrule(step,12,28).
	  no_applyrule(step,12,20).
	  no_applyrule(step,12,29).
	  no_applyelserule(step,12).
hedge_state(step,18).
    no_applyrule(step,18,4).
	  no_applyrule(step,18,22).
	  no_applyrule(step,18,28).
	  no_applyrule(step,18,20).
	  no_applyrule(step,18,29).
	  no_applyelserule(step,18).
hedge_state(step,24).
    no_applyrule(step,24,22).
	  no_applyrule(step,24,28).
	  no_applyrule(step,24,20).
	  no_applyrule(step,24,29).
	  no_applyelserule(step,24).
no_elserule(step,24).
	% %
hedge_state(step,26).
    no_applyrule(step,26,4).
	  no_applyrule(step,26,22).
	  no_applyrule(step,26,28).
	  no_applyrule(step,26,20).
	  no_applyrule(step,26,29).
	  no_applyelserule(step,26).
no_elserule(step,26).
	% %
hedge_state(step,37).
    no_applyrule(step,37,4).
	  no_applyrule(step,37,22).
	  no_applyrule(step,37,28).
	  no_applyrule(step,37,20).
	  no_applyrule(step,37,29).
	  no_applyelserule(step,37).
no_elserule(step,37).
	% %
hedge_state(step,14).
    no_applyrule(step,14,29).
	  no_applyelserule(step,14).
no_elserule(step,14).
	% %
applyrule(step,21,4,21).
applyrule(step,21,20,21).
applyrule(step,35,4,35).
applyrule(step,13,4,13).
applyrule(step,13,20,21).
applyrule(step,6,22,26).
applyrule(step,6,28,37).
applyrule(step,0,29,38).
applyrule(step,24,4,24).
applyrule(step,14,4,14).
applyrule(step,14,22,24).
applyrule(step,14,28,35).
applyrule(step,14,20,14).
elserule(step,7,11).
elserule(step,19,5).
elserule(step,5,5).
elserule(step,3,5).
elserule(step,16,5).
elserule(step,8,11).
elserule(step,2,8).
elserule(step,9,5).
elserule(step,17,5).
elserule(step,15,5).
elserule(step,18,5).
treerule(step,21,22).
treerule(step,35,28).
treerule(step,19,20).
treerule(step,5,4).
treerule(step,16,4).
treerule(step,17,4).
treerule(step,15,4).
treerule(step,18,4).
treerule(step,24,28).
treerule(step,26,29).
treerule(step,37,29).
treerule(step,14,4).
elserule(step,10,14).
elserule(step,11,14).
elserule(step,0,5).
elserule(step,1,6).
elserule(step,12,15).
internalrule(step,7,'glossary',name,10).
internalrule(step,3,'default',namespace,9).
internalrule(step,16,'u',char,17).
internalrule(step,2,'default',namespace,7).
internalrule(step,9,'role',name,12).
internalrule(step,17,'t',char,18).
internalrule(step,15,'a',char,16).
internalrule(step,18,'o',char,19).
internalrule(step,10,'x',var,13).
internalrule(step,0,'att',type,3).
internalrule(step,0,'elem',type,2).
internalrule(step,0,'doc',type,1).
no_elserule(step,4).
no_elserule(step,22).
no_elserule(step,28).
no_elserule(step,20).
no_elserule(step,29).
no_elserule(step,21).
no_elserule(step,35).
no_elserule(step,13).
no_elserule(step,6).
no_elserule(step,38).
no_elserule(step,24).
no_elserule(step,26).
no_elserule(step,37).
no_elserule(step,14).
no_treerule(step,4).
no_treerule(step,22).
no_treerule(step,28).
no_treerule(step,20).
no_treerule(step,29).
no_treerule(step,7).
no_treerule(step,10).
no_treerule(step,13).
no_treerule(step,6).
no_treerule(step,11).
no_treerule(step,3).
no_treerule(step,8).
no_treerule(step,2).
no_treerule(step,9).
no_treerule(step,1).
no_treerule(step,0).
no_treerule(step,38).
no_treerule(step,12).
label(step,'o',char,7).
no_internalrule(step,4,'o',char).
no_internalrule(step,22,'o',char).
no_internalrule(step,28,'o',char).
no_internalrule(step,20,'o',char).
no_internalrule(step,29,'o',char).
no_internalrule(step,7,'o',char).
no_internalrule(step,10,'o',char).
no_internalrule(step,21,'o',char).
no_internalrule(step,35,'o',char).
no_internalrule(step,19,'o',char).
no_internalrule(step,13,'o',char).
no_internalrule(step,6,'o',char).
no_internalrule(step,5,'o',char).
no_internalrule(step,11,'o',char).
no_internalrule(step,3,'o',char).
no_internalrule(step,8,'o',char).
no_internalrule(step,16,'o',char).
no_internalrule(step,2,'o',char).
no_internalrule(step,9,'o',char).
no_internalrule(step,17,'o',char).
no_internalrule(step,1,'o',char).
no_internalrule(step,0,'o',char).
no_internalrule(step,38,'o',char).
no_internalrule(step,15,'o',char).
no_internalrule(step,12,'o',char).
no_internalrule(step,24,'o',char).
no_internalrule(step,26,'o',char).
no_internalrule(step,37,'o',char).
no_internalrule(step,14,'o',char).
label(step,'att',type,2).
no_internalrule(step,4,'att',type).
no_internalrule(step,22,'att',type).
no_internalrule(step,28,'att',type).
no_internalrule(step,20,'att',type).
no_internalrule(step,29,'att',type).
no_internalrule(step,7,'att',type).
no_internalrule(step,10,'att',type).
no_internalrule(step,21,'att',type).
no_internalrule(step,35,'att',type).
no_internalrule(step,19,'att',type).
no_internalrule(step,13,'att',type).
no_internalrule(step,6,'att',type).
no_internalrule(step,5,'att',type).
no_internalrule(step,11,'att',type).
no_internalrule(step,3,'att',type).
no_internalrule(step,8,'att',type).
no_internalrule(step,16,'att',type).
no_internalrule(step,2,'att',type).
no_internalrule(step,9,'att',type).
no_internalrule(step,17,'att',type).
no_internalrule(step,1,'att',type).
no_internalrule(step,38,'att',type).
no_internalrule(step,15,'att',type).
no_internalrule(step,12,'att',type).
no_internalrule(step,18,'att',type).
no_internalrule(step,24,'att',type).
no_internalrule(step,26,'att',type).
no_internalrule(step,37,'att',type).
no_internalrule(step,14,'att',type).
label(step,'u',char,10).
no_internalrule(step,4,'u',char).
no_internalrule(step,22,'u',char).
no_internalrule(step,28,'u',char).
no_internalrule(step,20,'u',char).
no_internalrule(step,29,'u',char).
no_internalrule(step,7,'u',char).
no_internalrule(step,10,'u',char).
no_internalrule(step,21,'u',char).
no_internalrule(step,35,'u',char).
no_internalrule(step,19,'u',char).
no_internalrule(step,13,'u',char).
no_internalrule(step,6,'u',char).
no_internalrule(step,5,'u',char).
no_internalrule(step,11,'u',char).
no_internalrule(step,3,'u',char).
no_internalrule(step,8,'u',char).
no_internalrule(step,2,'u',char).
no_internalrule(step,9,'u',char).
no_internalrule(step,17,'u',char).
no_internalrule(step,1,'u',char).
no_internalrule(step,0,'u',char).
no_internalrule(step,38,'u',char).
no_internalrule(step,15,'u',char).
no_internalrule(step,12,'u',char).
no_internalrule(step,18,'u',char).
no_internalrule(step,24,'u',char).
no_internalrule(step,26,'u',char).
no_internalrule(step,37,'u',char).
no_internalrule(step,14,'u',char).
label(step,'a',char,1).
no_internalrule(step,4,'a',char).
no_internalrule(step,22,'a',char).
no_internalrule(step,28,'a',char).
no_internalrule(step,20,'a',char).
no_internalrule(step,29,'a',char).
no_internalrule(step,7,'a',char).
no_internalrule(step,10,'a',char).
no_internalrule(step,21,'a',char).
no_internalrule(step,35,'a',char).
no_internalrule(step,19,'a',char).
no_internalrule(step,13,'a',char).
no_internalrule(step,6,'a',char).
no_internalrule(step,5,'a',char).
no_internalrule(step,11,'a',char).
no_internalrule(step,3,'a',char).
no_internalrule(step,8,'a',char).
no_internalrule(step,16,'a',char).
no_internalrule(step,2,'a',char).
no_internalrule(step,9,'a',char).
no_internalrule(step,17,'a',char).
no_internalrule(step,1,'a',char).
no_internalrule(step,0,'a',char).
no_internalrule(step,38,'a',char).
no_internalrule(step,12,'a',char).
no_internalrule(step,18,'a',char).
no_internalrule(step,24,'a',char).
no_internalrule(step,26,'a',char).
no_internalrule(step,37,'a',char).
no_internalrule(step,14,'a',char).
label(step,'t',char,9).
no_internalrule(step,4,'t',char).
no_internalrule(step,22,'t',char).
no_internalrule(step,28,'t',char).
no_internalrule(step,20,'t',char).
no_internalrule(step,29,'t',char).
no_internalrule(step,7,'t',char).
no_internalrule(step,10,'t',char).
no_internalrule(step,21,'t',char).
no_internalrule(step,35,'t',char).
no_internalrule(step,19,'t',char).
no_internalrule(step,13,'t',char).
no_internalrule(step,6,'t',char).
no_internalrule(step,5,'t',char).
no_internalrule(step,11,'t',char).
no_internalrule(step,3,'t',char).
no_internalrule(step,8,'t',char).
no_internalrule(step,16,'t',char).
no_internalrule(step,2,'t',char).
no_internalrule(step,9,'t',char).
no_internalrule(step,1,'t',char).
no_internalrule(step,0,'t',char).
no_internalrule(step,38,'t',char).
no_internalrule(step,15,'t',char).
no_internalrule(step,12,'t',char).
no_internalrule(step,18,'t',char).
no_internalrule(step,24,'t',char).
no_internalrule(step,26,'t',char).
no_internalrule(step,37,'t',char).
no_internalrule(step,14,'t',char).
label(step,'x',var,11).
no_internalrule(step,4,'x',var).
no_internalrule(step,22,'x',var).
no_internalrule(step,28,'x',var).
no_internalrule(step,20,'x',var).
no_internalrule(step,29,'x',var).
no_internalrule(step,7,'x',var).
no_internalrule(step,21,'x',var).
no_internalrule(step,35,'x',var).
no_internalrule(step,19,'x',var).
no_internalrule(step,13,'x',var).
no_internalrule(step,6,'x',var).
no_internalrule(step,5,'x',var).
no_internalrule(step,11,'x',var).
no_internalrule(step,3,'x',var).
no_internalrule(step,8,'x',var).
no_internalrule(step,16,'x',var).
no_internalrule(step,2,'x',var).
no_internalrule(step,9,'x',var).
no_internalrule(step,17,'x',var).
no_internalrule(step,1,'x',var).
no_internalrule(step,0,'x',var).
no_internalrule(step,38,'x',var).
no_internalrule(step,15,'x',var).
no_internalrule(step,12,'x',var).
no_internalrule(step,18,'x',var).
no_internalrule(step,24,'x',var).
no_internalrule(step,26,'x',var).
no_internalrule(step,37,'x',var).
no_internalrule(step,14,'x',var).
label(step,'x',negvar,18).
no_internalrule(step,4,'x',negvar).
no_internalrule(step,22,'x',negvar).
no_internalrule(step,28,'x',negvar).
no_internalrule(step,20,'x',negvar).
no_internalrule(step,29,'x',negvar).
no_internalrule(step,7,'x',negvar).
no_internalrule(step,10,'x',negvar).
no_internalrule(step,21,'x',negvar).
no_internalrule(step,35,'x',negvar).
no_internalrule(step,19,'x',negvar).
no_internalrule(step,13,'x',negvar).
no_internalrule(step,6,'x',negvar).
no_internalrule(step,5,'x',negvar).
no_internalrule(step,11,'x',negvar).
no_internalrule(step,3,'x',negvar).
no_internalrule(step,8,'x',negvar).
no_internalrule(step,16,'x',negvar).
no_internalrule(step,2,'x',negvar).
no_internalrule(step,9,'x',negvar).
no_internalrule(step,17,'x',negvar).
no_internalrule(step,1,'x',negvar).
no_internalrule(step,0,'x',negvar).
no_internalrule(step,38,'x',negvar).
no_internalrule(step,15,'x',negvar).
no_internalrule(step,12,'x',negvar).
no_internalrule(step,18,'x',negvar).
no_internalrule(step,24,'x',negvar).
no_internalrule(step,26,'x',negvar).
no_internalrule(step,37,'x',negvar).
no_internalrule(step,14,'x',negvar).
label(step,'text',type,17).
no_internalrule(step,4,'text',type).
no_internalrule(step,22,'text',type).
no_internalrule(step,28,'text',type).
no_internalrule(step,20,'text',type).
no_internalrule(step,29,'text',type).
no_internalrule(step,7,'text',type).
no_internalrule(step,10,'text',type).
no_internalrule(step,21,'text',type).
no_internalrule(step,35,'text',type).
no_internalrule(step,19,'text',type).
no_internalrule(step,13,'text',type).
no_internalrule(step,6,'text',type).
no_internalrule(step,5,'text',type).
no_internalrule(step,11,'text',type).
no_internalrule(step,3,'text',type).
no_internalrule(step,8,'text',type).
no_internalrule(step,16,'text',type).
no_internalrule(step,2,'text',type).
no_internalrule(step,9,'text',type).
no_internalrule(step,17,'text',type).
no_internalrule(step,1,'text',type).
no_internalrule(step,0,'text',type).
no_internalrule(step,38,'text',type).
no_internalrule(step,15,'text',type).
no_internalrule(step,12,'text',type).
no_internalrule(step,18,'text',type).
no_internalrule(step,24,'text',type).
no_internalrule(step,26,'text',type).
no_internalrule(step,37,'text',type).
no_internalrule(step,14,'text',type).
label(step,'comment',type,14).
no_internalrule(step,4,'comment',type).
no_internalrule(step,22,'comment',type).
no_internalrule(step,28,'comment',type).
no_internalrule(step,20,'comment',type).
no_internalrule(step,29,'comment',type).
no_internalrule(step,7,'comment',type).
no_internalrule(step,10,'comment',type).
no_internalrule(step,21,'comment',type).
no_internalrule(step,35,'comment',type).
no_internalrule(step,19,'comment',type).
no_internalrule(step,13,'comment',type).
no_internalrule(step,6,'comment',type).
no_internalrule(step,5,'comment',type).
no_internalrule(step,11,'comment',type).
no_internalrule(step,3,'comment',type).
no_internalrule(step,8,'comment',type).
no_internalrule(step,16,'comment',type).
no_internalrule(step,2,'comment',type).
no_internalrule(step,9,'comment',type).
no_internalrule(step,17,'comment',type).
no_internalrule(step,1,'comment',type).
no_internalrule(step,0,'comment',type).
no_internalrule(step,38,'comment',type).
no_internalrule(step,15,'comment',type).
no_internalrule(step,12,'comment',type).
no_internalrule(step,18,'comment',type).
no_internalrule(step,24,'comment',type).
no_internalrule(step,26,'comment',type).
no_internalrule(step,37,'comment',type).
no_internalrule(step,14,'comment',type).
label(step,'default',namespace,3).
no_internalrule(step,4,'default',namespace).
no_internalrule(step,22,'default',namespace).
no_internalrule(step,28,'default',namespace).
no_internalrule(step,20,'default',namespace).
no_internalrule(step,29,'default',namespace).
no_internalrule(step,7,'default',namespace).
no_internalrule(step,10,'default',namespace).
no_internalrule(step,21,'default',namespace).
no_internalrule(step,35,'default',namespace).
no_internalrule(step,19,'default',namespace).
no_internalrule(step,13,'default',namespace).
no_internalrule(step,6,'default',namespace).
no_internalrule(step,5,'default',namespace).
no_internalrule(step,11,'default',namespace).
no_internalrule(step,8,'default',namespace).
no_internalrule(step,16,'default',namespace).
no_internalrule(step,9,'default',namespace).
no_internalrule(step,17,'default',namespace).
no_internalrule(step,1,'default',namespace).
no_internalrule(step,0,'default',namespace).
no_internalrule(step,38,'default',namespace).
no_internalrule(step,15,'default',namespace).
no_internalrule(step,12,'default',namespace).
no_internalrule(step,18,'default',namespace).
no_internalrule(step,24,'default',namespace).
no_internalrule(step,26,'default',namespace).
no_internalrule(step,37,'default',namespace).
no_internalrule(step,14,'default',namespace).
label(step,'elem',type,5).
no_internalrule(step,4,'elem',type).
no_internalrule(step,22,'elem',type).
no_internalrule(step,28,'elem',type).
no_internalrule(step,20,'elem',type).
no_internalrule(step,29,'elem',type).
no_internalrule(step,7,'elem',type).
no_internalrule(step,10,'elem',type).
no_internalrule(step,21,'elem',type).
no_internalrule(step,35,'elem',type).
no_internalrule(step,19,'elem',type).
no_internalrule(step,13,'elem',type).
no_internalrule(step,6,'elem',type).
no_internalrule(step,5,'elem',type).
no_internalrule(step,11,'elem',type).
no_internalrule(step,3,'elem',type).
no_internalrule(step,8,'elem',type).
no_internalrule(step,16,'elem',type).
no_internalrule(step,2,'elem',type).
no_internalrule(step,9,'elem',type).
no_internalrule(step,17,'elem',type).
no_internalrule(step,1,'elem',type).
no_internalrule(step,38,'elem',type).
no_internalrule(step,15,'elem',type).
no_internalrule(step,12,'elem',type).
no_internalrule(step,18,'elem',type).
no_internalrule(step,24,'elem',type).
no_internalrule(step,26,'elem',type).
no_internalrule(step,37,'elem',type).
no_internalrule(step,14,'elem',type).
label(step,'glossary',name,6).
no_internalrule(step,4,'glossary',name).
no_internalrule(step,22,'glossary',name).
no_internalrule(step,28,'glossary',name).
no_internalrule(step,20,'glossary',name).
no_internalrule(step,29,'glossary',name).
no_internalrule(step,10,'glossary',name).
no_internalrule(step,21,'glossary',name).
no_internalrule(step,35,'glossary',name).
no_internalrule(step,19,'glossary',name).
no_internalrule(step,13,'glossary',name).
no_internalrule(step,6,'glossary',name).
no_internalrule(step,5,'glossary',name).
no_internalrule(step,11,'glossary',name).
no_internalrule(step,3,'glossary',name).
no_internalrule(step,8,'glossary',name).
no_internalrule(step,16,'glossary',name).
no_internalrule(step,2,'glossary',name).
no_internalrule(step,9,'glossary',name).
no_internalrule(step,17,'glossary',name).
no_internalrule(step,1,'glossary',name).
no_internalrule(step,0,'glossary',name).
no_internalrule(step,38,'glossary',name).
no_internalrule(step,15,'glossary',name).
no_internalrule(step,12,'glossary',name).
no_internalrule(step,18,'glossary',name).
no_internalrule(step,24,'glossary',name).
no_internalrule(step,26,'glossary',name).
no_internalrule(step,37,'glossary',name).
no_internalrule(step,14,'glossary',name).
label(step,'role',name,8).
no_internalrule(step,4,'role',name).
no_internalrule(step,22,'role',name).
no_internalrule(step,28,'role',name).
no_internalrule(step,20,'role',name).
no_internalrule(step,29,'role',name).
no_internalrule(step,7,'role',name).
no_internalrule(step,10,'role',name).
no_internalrule(step,21,'role',name).
no_internalrule(step,35,'role',name).
no_internalrule(step,19,'role',name).
no_internalrule(step,13,'role',name).
no_internalrule(step,6,'role',name).
no_internalrule(step,5,'role',name).
no_internalrule(step,11,'role',name).
no_internalrule(step,3,'role',name).
no_internalrule(step,8,'role',name).
no_internalrule(step,16,'role',name).
no_internalrule(step,2,'role',name).
no_internalrule(step,17,'role',name).
no_internalrule(step,1,'role',name).
no_internalrule(step,0,'role',name).
no_internalrule(step,38,'role',name).
no_internalrule(step,15,'role',name).
no_internalrule(step,12,'role',name).
no_internalrule(step,18,'role',name).
no_internalrule(step,24,'role',name).
no_internalrule(step,26,'role',name).
no_internalrule(step,37,'role',name).
no_internalrule(step,14,'role',name).
label(step,'doc',type,4).
no_internalrule(step,4,'doc',type).
no_internalrule(step,22,'doc',type).
no_internalrule(step,28,'doc',type).
no_internalrule(step,20,'doc',type).
no_internalrule(step,29,'doc',type).
no_internalrule(step,7,'doc',type).
no_internalrule(step,10,'doc',type).
no_internalrule(step,21,'doc',type).
no_internalrule(step,35,'doc',type).
no_internalrule(step,19,'doc',type).
no_internalrule(step,13,'doc',type).
no_internalrule(step,6,'doc',type).
no_internalrule(step,5,'doc',type).
no_internalrule(step,11,'doc',type).
no_internalrule(step,3,'doc',type).
no_internalrule(step,8,'doc',type).
no_internalrule(step,16,'doc',type).
no_internalrule(step,2,'doc',type).
no_internalrule(step,9,'doc',type).
no_internalrule(step,17,'doc',type).
no_internalrule(step,1,'doc',type).
no_internalrule(step,38,'doc',type).
no_internalrule(step,15,'doc',type).
no_internalrule(step,12,'doc',type).
no_internalrule(step,18,'doc',type).
no_internalrule(step,24,'doc',type).
no_internalrule(step,26,'doc',type).
no_internalrule(step,37,'doc',type).
no_internalrule(step,14,'doc',type).
max_label_id(step,18).
    %%% different origins %%%%%%%
    
