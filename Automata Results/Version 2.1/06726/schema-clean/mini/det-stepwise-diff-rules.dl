diff_final(A,X,X1) :- final(A,X), nonfinal(A,X1), hedge_state(A,X1).
diff_type(A,X,X1)  :- hedge_state(A,X), tree_state(A,X1).
diff_sym(A,X1,X)   :- diff(A,X,X1).
diff_else(A,X,X1)  :- else(A,X), nonelse(A,X1), tree_state(A,X1).

internal(A,X,L,Type,Y)  :- internalrule(A,X,L,Type,Y).
internal(A,X,L,Type,Y)  :- no_internalrule(A,X,L,Type), elserule(A,X,Y).
internal(A,X,L,Type,Y)  :- no_internalrule(A,X,L,Type), typedelserule(A,X,Type,Y).
no_internal(A,X,L,Type) :- hedge_state(A,X), no_internalrule(A,X,L,Type),
                           no_elserule(A,X), no_typedelserule(A,X,Type).

apply(A,X,Y,Z)  :- applyrule(A,X,Y,Z).
apply(A,X,Y,Z)  :- applyelse(A,X,Y,Z).

applyelse(A,X,Y,Z)  :- applyelserule(A,X,Z), else(A,Y), no_applyrule(A,X,Y).
no_apply(A,X,Y)     :- no_applyrule(A,X,Y),
		       no_applyelserule_with(A,X,Y), tree_state(A,Y).
no_applyelserule_with(A,X,Y) :- no_applyelserule(A,X), tree_state(A,Y). 
no_applyelserule_with(A,X,Y) :- applyelserule(A,X,Z),
			        nonelse(A,Y), tree_state(Y).
no_applyelserule_with(A,X,Y) :- applyelserule(A,X,Z),
			        applyrule(A,X,Y,Z1).

type_lab(A,T) :- typedeleserule(A,X,T,Y).
type_lab(A,T) :- no_typedeleserule(A,X,T).
typedelserule(A,X,T,Y) :- elserule(A,X,Y), type_lab(A,T).

diff_typedelse(A,X,T,X1) :- typedelserule(A,X,T,Y), no_typedelserule(A,X1,T).
% self_diff_typedelse(X)   :- diff_typedelse(A,X,T,X).
diff(A,X,X1) :- diff_typedelse(A,X,T,X1).
diff(A,X,X1) :- diff_typedelserule(A,X,T,X1).

diff(A,X,X1)      :- diff_tree(A,X,X1).
% self_diff_tree(X) :- diff_tree(A,X,X).
diff(A,X,X1)      :- diff_type(A,X,X1).
% self_diff_type(X) :- diff_type(A,X,X).
diff(A,X,X1)      :- diff_final(A,X,X1).
% self_diff_final(X):- diff_final(A,X,X).
diff(A,X,X1)      :- diff_else(A,X,X1).
% self_diff_else(X) :- diff_else(A,X,X).

diff(A,X,X1) :- diff_treerule(A,X,X1).
diff(A,X,X1) :- diff_no_treerule(A,X,X1).
diff(A,X,X1) :- diff_apply(A,X,X1).
diff(A,X,X1) :- diff_no_apply(A,X,X1).
diff(A,X,X1) :- diff_internal(A,X,X1).

diff(A,X,X1) :- diff_no_internal(A,X,X1).
diff(A,X,X1) :- diff_nonelse(A,X,X1).
diff(A,X,X1) :- diff_origin(A,X,X1).
diff(A,X,X1) :- diff_sym(A,X,X1).
diff(A,X,X1) :- diff_elserule(A,X,X1).
diff(A,X,X1) :- diff_no_elserule(A,X,X1).
diff(A,X,X1) :- diff_no_typedelserule(A,X,X1).

diff_apply_from1(A,X,X1,Z,Z1)  :- apply(A,X,Y,Z), apply(A,X1,Y,Z1), diff(A,Z,Z1).
diff_apply_from2(A,Y,Y1,Z,Z1)  :- apply(A,X,Y,Z), apply(A,X,Y1,Z1), diff(A,Z,Z1).
% self_debug_diff_apply_from1(X,Y,Z,Z1) :- apply(A,X,Y,Z), apply(A,X,Y,Z1), diff(A,Z,Z1).
% self_debug_diff_apply_from2(Y,X,Z,Z1) :- apply(A,X,Y,Z), apply(A,X,Y,Z1), diff(A,Z,Z1).
diff_apply(A,X,X1)             :- diff_apply_from1(A,X,X1,Z,Z1).
diff_apply(A,X,X1)             :- diff_apply_from2(A,X,X1,Z,Z1).

diff_no_apply_from1(A,X,X1,Y,Z):- apply(A,X,Y,Z), no_apply(A,X1,Y).
diff_no_apply_from2(A,Y,Y1,X,Z):- apply(A,X,Y,Z), no_apply(A,X,Y1).
% self_debug_diff_no_apply_from1(X,Y,Z) :- apply(A,X,Y,Z), no_apply(A,X,Y).
% self_debug_diff_no_apply_from2(Y,X,Z) :- apply(A,X,Y,Z), no_apply(A,X,Y).
diff_no_apply(A,X,X1)          :- diff_no_apply_from1(A,X,X1,Y,Z).
diff_no_apply(A,X,X1)          :- diff_no_apply_from2(A,X,X1,Y,Z).

diff_treerule(A,X,X1)     :- treerule(A,X,Y), treerule(A,X1,Y1), diff(A,Y,Y1).
% self_debug_diff_treerule(X,Y,Y1) :- treerule(A,X,Y), treerule(A,X,Y1), diff(A,Y,Y1).
diff_no_treerule(A,X,X1)  :- treerule(A,X,Y), no_treerule(A,X1), hedge_state(A,X1).
% self_diff_no_treerule(X)  :-  treerule(A,X,Y), no_treerule(A,X), hedge_state(A,X).

diff_internal(A,X,X1) :- diff_internal_from(A,X,X1,L,Type,Y,Y1).
diff_internal_from(A,X,X1,L,Type,Y,Y1)
                      :- internal(A,X,L,Type,Y),
                         internal(A,X1,L,Type,Y1), diff(A,Y,Y1).
% self_debug_diff_internal_from(A,X,L,Type,Y,Y1):-diff_internal_from(A,X,X,L,Type,Y,Y1).
			 
diff_no_internal(A,X,X1) :- no_internal(A,X,L,Type), internal(A,X1,L,Type,Y1).
% self_debug_diff_no_internal(X,L,Type,Y1) :- no_internal(A,X,L,Type), internal(A,X,L,Type,Y1).

diff_elserule(A,X,X1) :- elserule(A,X,Y), elserule(A,X1,Y1), diff(A,Y,Y1).
% self_debug_diff_elserule(X,Y,Y1) :- elserule(A,X,Y), elserule(A,X,Y1), diff(A,Y,Y1).
diff_typedelserule(A,X,T,X1) :- typedelserule(A,X,T,Y), typedelserule(A,X1,T,Y1), diff(A,Y,Y1).
% self_diff_typedelserule(X)   :- typedelserule(A,X,T,Y), typedelserule(A,X,T,Y1),  diff(A,Y,Y1).
% self_debug_diff_typedelserule(X,Y,Y1) :- typedelserule(A,X,T,Y), typedelserule(A,X,T,Y1), diff(A,Y,Y1).

diff_no_elserule(A,X,X1) :- elserule(A,X,Y), no_elserule(A,X1).
diff_no_typedelserule(A,X,X1) :- typedelserule(A,X,T,Y), no_elserule(A,X1),no_typedelserule(A,X1,T).

% self_diff_no_elserule(X) :- diff_no_elserule(A,X,X).

% don't fusion states with different schema origins
%    (Joachim: should first diff_orig rule be switched on or off?)

% diff_origin(A,X1,X) :- origin_of(A,X1,Y1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).
% self_diff_origin(X) :- origin_of(A,XY1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).
% self_debug_diff_origin(X,Y1,Z1,Y,Z) :- origin_of(A,X,Y1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).

% for minimization replace rule(A,X,Y,Z) by rule(A,min(X),min(Y),min(Z)) 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule
% negative information (NEGATIVE=true)
%   no_applyrule
%   no_elserule
%   no_treerule
%   no_internalrule
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(step,'schema-clean[det_Ttyped[schema-clean[pc[step[(stepwise[06726:d1e9]+stepwise[06726:d1e106])]]]]]').
xpath(step,'//doc:table|//doc:informaltable').
var(step,'x',1).
no_typedelserule(step,31,type).
no_typedelserule(step,33,type).
no_typedelserule(step,28,type).
no_typedelserule(step,3,type).
no_typedelserule(step,15,type).
no_typedelserule(step,24,type).
no_typedelserule(step,14,type).
no_typedelserule(step,7,type).
no_typedelserule(step,68,type).
no_typedelserule(step,13,type).
no_typedelserule(step,11,type).
no_typedelserule(step,22,type).
no_typedelserule(step,51,type).
no_typedelserule(step,58,type).
no_typedelserule(step,16,type).
no_typedelserule(step,20,type).
no_typedelserule(step,40,type).
no_typedelserule(step,12,type).
no_typedelserule(step,37,type).
no_typedelserule(step,18,type).
no_typedelserule(step,61,type).
no_typedelserule(step,31,name).
no_typedelserule(step,33,name).
no_typedelserule(step,28,name).
no_typedelserule(step,3,name).
no_typedelserule(step,15,name).
no_typedelserule(step,24,name).
no_typedelserule(step,14,name).
no_typedelserule(step,7,name).
no_typedelserule(step,68,name).
no_typedelserule(step,13,name).
no_typedelserule(step,11,name).
no_typedelserule(step,22,name).
no_typedelserule(step,51,name).
no_typedelserule(step,58,name).
no_typedelserule(step,16,name).
no_typedelserule(step,20,name).
no_typedelserule(step,40,name).
no_typedelserule(step,12,name).
no_typedelserule(step,37,name).
no_typedelserule(step,18,name).
no_typedelserule(step,61,name).
no_typedelserule(step,31,var).
no_typedelserule(step,33,var).
no_typedelserule(step,28,var).
no_typedelserule(step,3,var).
no_typedelserule(step,15,var).
no_typedelserule(step,24,var).
no_typedelserule(step,14,var).
no_typedelserule(step,7,var).
no_typedelserule(step,68,var).
no_typedelserule(step,13,var).
no_typedelserule(step,11,var).
no_typedelserule(step,22,var).
no_typedelserule(step,51,var).
no_typedelserule(step,58,var).
no_typedelserule(step,16,var).
no_typedelserule(step,20,var).
no_typedelserule(step,40,var).
no_typedelserule(step,12,var).
no_typedelserule(step,37,var).
no_typedelserule(step,18,var).
no_typedelserule(step,61,var).
no_typedelserule(step,31,negvar).
no_typedelserule(step,33,negvar).
no_typedelserule(step,28,negvar).
no_typedelserule(step,3,negvar).
no_typedelserule(step,15,negvar).
no_typedelserule(step,24,negvar).
no_typedelserule(step,14,negvar).
no_typedelserule(step,7,negvar).
no_typedelserule(step,68,negvar).
no_typedelserule(step,13,negvar).
no_typedelserule(step,11,negvar).
no_typedelserule(step,22,negvar).
no_typedelserule(step,51,negvar).
no_typedelserule(step,58,negvar).
no_typedelserule(step,16,negvar).
no_typedelserule(step,20,negvar).
no_typedelserule(step,40,negvar).
no_typedelserule(step,12,negvar).
no_typedelserule(step,37,negvar).
no_typedelserule(step,18,negvar).
no_typedelserule(step,61,negvar).
no_typedelserule(step,31,namespace).
no_typedelserule(step,33,namespace).
no_typedelserule(step,28,namespace).
no_typedelserule(step,3,namespace).
no_typedelserule(step,15,namespace).
no_typedelserule(step,24,namespace).
no_typedelserule(step,14,namespace).
no_typedelserule(step,7,namespace).
no_typedelserule(step,68,namespace).
no_typedelserule(step,13,namespace).
no_typedelserule(step,11,namespace).
no_typedelserule(step,22,namespace).
no_typedelserule(step,51,namespace).
no_typedelserule(step,58,namespace).
no_typedelserule(step,16,namespace).
no_typedelserule(step,20,namespace).
no_typedelserule(step,40,namespace).
no_typedelserule(step,12,namespace).
no_typedelserule(step,37,namespace).
no_typedelserule(step,18,namespace).
no_typedelserule(step,61,namespace).
state(step,31).
state(step,33).
state(step,28).
state(step,3).
state(step,15).
state(step,24).
state(step,14).
state(step,10).
state(step,7).
state(step,68).
state(step,6).
state(step,4).
state(step,13).
state(step,5).
state(step,11).
state(step,22).
state(step,51).
state(step,58).
state(step,8).
state(step,16).
state(step,2).
state(step,9).
state(step,20).
state(step,1).
state(step,0).
state(step,40).
state(step,12).
state(step,37).
state(step,18).
state(step,61).
start_tree(step,0).
initial(step,0).
not_sink(step,31).
not_sink(step,33).
not_sink(step,28).
not_sink(step,3).
not_sink(step,15).
not_sink(step,24).
not_sink(step,14).
not_sink(step,10).
not_sink(step,7).
not_sink(step,68).
not_sink(step,6).
not_sink(step,4).
not_sink(step,13).
not_sink(step,5).
not_sink(step,11).
not_sink(step,22).
not_sink(step,51).
not_sink(step,58).
not_sink(step,8).
not_sink(step,16).
not_sink(step,2).
not_sink(step,9).
not_sink(step,20).
not_sink(step,1).
not_sink(step,0).
not_sink(step,40).
not_sink(step,12).
not_sink(step,37).
not_sink(step,18).
not_sink(step,61).
else(step,31).
else(step,33).
else(step,28).
else(step,3).
else(step,15).
else(step,24).
else(step,14).
nonelse(step,10).
nonelse(step,7).
nonelse(step,68).
nonelse(step,6).
nonelse(step,4).
nonelse(step,13).
nonelse(step,5).
nonelse(step,11).
nonelse(step,22).
nonelse(step,51).
nonelse(step,58).
nonelse(step,8).
nonelse(step,16).
nonelse(step,2).
nonelse(step,9).
nonelse(step,20).
nonelse(step,1).
nonelse(step,0).
nonelse(step,40).
nonelse(step,12).
nonelse(step,37).
nonelse(step,18).
nonelse(step,61).
final(step,68).
final(step,51).
nonfinal(step,31).
nonfinal(step,33).
nonfinal(step,28).
nonfinal(step,3).
nonfinal(step,15).
nonfinal(step,24).
nonfinal(step,14).
nonfinal(step,10).
nonfinal(step,7).
nonfinal(step,6).
nonfinal(step,4).
nonfinal(step,13).
nonfinal(step,5).
nonfinal(step,11).
nonfinal(step,22).
nonfinal(step,58).
nonfinal(step,8).
nonfinal(step,16).
nonfinal(step,2).
nonfinal(step,9).
nonfinal(step,20).
nonfinal(step,1).
nonfinal(step,0).
nonfinal(step,40).
nonfinal(step,12).
nonfinal(step,37).
nonfinal(step,18).
nonfinal(step,61).
copy_of(step,31,31).
copy_of(step,33,33).
copy_of(step,28,28).
copy_of(step,3,3).
copy_of(step,15,15).
copy_of(step,24,24).
copy_of(step,14,14).
tree_state(step,31).
tree_istate(step,31,0).
 tree_state(step,33).
tree_istate(step,33,0).
 tree_state(step,28).
tree_istate(step,28,0).
 tree_state(step,3).
tree_istate(step,3,0).
 tree_state(step,15).
tree_istate(step,15,0).
 tree_state(step,24).
tree_istate(step,24,0).
 tree_state(step,14).
tree_istate(step,14,0).
 hedge_state(step,10).
    no_applyrule(step,10,31).
	  no_applyrule(step,10,33).
	  no_applyrule(step,10,28).
	  no_applyrule(step,10,3).
	  no_applyrule(step,10,15).
	  no_applyrule(step,10,24).
	  no_applyrule(step,10,14).
	  no_applyelserule(step,10).
hedge_state(step,7).
    no_applyrule(step,7,33).
	  no_applyrule(step,7,28).
	  no_applyrule(step,7,3).
	  no_applyelserule(step,7).
no_elserule(step,7).
	% %
hedge_state(step,68).
    no_applyrule(step,68,31).
	  no_applyrule(step,68,33).
	  no_applyrule(step,68,28).
	  no_applyrule(step,68,3).
	  no_applyrule(step,68,15).
	  no_applyrule(step,68,24).
	  no_applyrule(step,68,14).
	  no_applyelserule(step,68).
no_elserule(step,68).
	% %
hedge_state(step,6).
    no_applyrule(step,6,31).
	  no_applyrule(step,6,33).
	  no_applyrule(step,6,28).
	  no_applyrule(step,6,3).
	  no_applyrule(step,6,15).
	  no_applyrule(step,6,24).
	  no_applyrule(step,6,14).
	  no_applyelserule(step,6).
hedge_state(step,4).
    no_applyrule(step,4,31).
	  no_applyrule(step,4,33).
	  no_applyrule(step,4,28).
	  no_applyrule(step,4,3).
	  no_applyrule(step,4,15).
	  no_applyrule(step,4,24).
	  no_applyrule(step,4,14).
	  no_applyelserule(step,4).
hedge_state(step,13).
    no_applyrule(step,13,31).
	  no_applyrule(step,13,33).
	  no_applyrule(step,13,28).
	  no_applyrule(step,13,15).
	  no_applyrule(step,13,24).
	  no_applyrule(step,13,14).
	  no_applyelserule(step,13).
no_elserule(step,13).
	% %
hedge_state(step,5).
    no_applyrule(step,5,31).
	  no_applyrule(step,5,33).
	  no_applyrule(step,5,28).
	  no_applyrule(step,5,3).
	  no_applyrule(step,5,15).
	  no_applyrule(step,5,24).
	  no_applyrule(step,5,14).
	  no_applyelserule(step,5).
hedge_state(step,11).
    no_applyrule(step,11,31).
	  no_applyrule(step,11,33).
	  no_applyrule(step,11,28).
	  no_applyrule(step,11,15).
	  no_applyrule(step,11,24).
	  no_applyrule(step,11,14).
	  no_applyelserule(step,11).
no_elserule(step,11).
	% %
hedge_state(step,22).
    no_applyrule(step,22,31).
	  no_applyrule(step,22,33).
	  no_applyrule(step,22,28).
	  no_applyrule(step,22,3).
	  no_applyrule(step,22,15).
	  no_applyrule(step,22,24).
	  no_applyrule(step,22,14).
	  no_applyelserule(step,22).
no_elserule(step,22).
	% %
hedge_state(step,51).
    no_applyrule(step,51,31).
	  no_applyrule(step,51,33).
	  no_applyrule(step,51,28).
	  no_applyrule(step,51,3).
	  no_applyrule(step,51,15).
	  no_applyrule(step,51,24).
	  no_applyrule(step,51,14).
	  no_applyelserule(step,51).
no_elserule(step,51).
	% %
hedge_state(step,58).
    no_applyrule(step,58,31).
	  no_applyrule(step,58,33).
	  no_applyrule(step,58,28).
	  no_applyrule(step,58,15).
	  no_applyrule(step,58,24).
	  no_applyrule(step,58,14).
	  no_applyelserule(step,58).
no_elserule(step,58).
	% %
hedge_state(step,8).
    no_applyrule(step,8,31).
	  no_applyrule(step,8,33).
	  no_applyrule(step,8,28).
	  no_applyrule(step,8,3).
	  no_applyrule(step,8,15).
	  no_applyrule(step,8,24).
	  no_applyrule(step,8,14).
	  no_applyelserule(step,8).
hedge_state(step,16).
    no_applyrule(step,16,31).
	  no_applyrule(step,16,33).
	  no_applyrule(step,16,28).
	  no_applyrule(step,16,15).
	  no_applyrule(step,16,24).
	  no_applyrule(step,16,14).
	  no_applyelserule(step,16).
no_elserule(step,16).
	% %
hedge_state(step,2).
    no_applyrule(step,2,31).
	  no_applyrule(step,2,33).
	  no_applyrule(step,2,28).
	  no_applyrule(step,2,3).
	  no_applyrule(step,2,15).
	  no_applyrule(step,2,24).
	  no_applyrule(step,2,14).
	  no_applyelserule(step,2).
hedge_state(step,9).
    no_applyrule(step,9,31).
	  no_applyrule(step,9,33).
	  no_applyrule(step,9,28).
	  no_applyrule(step,9,3).
	  no_applyrule(step,9,15).
	  no_applyrule(step,9,24).
	  no_applyrule(step,9,14).
	  no_applyelserule(step,9).
hedge_state(step,20).
    no_applyrule(step,20,31).
	  no_applyrule(step,20,33).
	  no_applyrule(step,20,28).
	  no_applyrule(step,20,15).
	  no_applyrule(step,20,24).
	  no_applyrule(step,20,14).
	  no_applyelserule(step,20).
no_elserule(step,20).
	% %
hedge_state(step,1).
    no_applyrule(step,1,31).
	  no_applyrule(step,1,33).
	  no_applyrule(step,1,28).
	  no_applyrule(step,1,3).
	  no_applyrule(step,1,15).
	  no_applyrule(step,1,24).
	  no_applyrule(step,1,14).
	  no_applyelserule(step,1).
hedge_state(step,0).
    no_applyrule(step,0,31).
	  no_applyrule(step,0,3).
	  no_applyrule(step,0,15).
	  no_applyrule(step,0,24).
	  no_applyrule(step,0,14).
	  no_applyelserule(step,0).
hedge_state(step,40).
    no_applyrule(step,40,31).
	  no_applyrule(step,40,33).
	  no_applyrule(step,40,28).
	  no_applyrule(step,40,3).
	  no_applyrule(step,40,15).
	  no_applyrule(step,40,24).
	  no_applyrule(step,40,14).
	  no_applyelserule(step,40).
no_elserule(step,40).
	% %
hedge_state(step,12).
    no_applyrule(step,12,33).
	  no_applyrule(step,12,28).
	  no_applyelserule(step,12).
no_elserule(step,12).
	% %
hedge_state(step,37).
    no_applyrule(step,37,31).
	  no_applyrule(step,37,33).
	  no_applyrule(step,37,28).
	  no_applyrule(step,37,15).
	  no_applyrule(step,37,24).
	  no_applyrule(step,37,14).
	  no_applyelserule(step,37).
no_elserule(step,37).
	% %
hedge_state(step,18).
    no_applyrule(step,18,31).
	  no_applyrule(step,18,33).
	  no_applyrule(step,18,28).
	  no_applyrule(step,18,3).
	  no_applyrule(step,18,15).
	  no_applyrule(step,18,24).
	  no_applyrule(step,18,14).
	  no_applyelserule(step,18).
no_elserule(step,18).
	% %
hedge_state(step,61).
    no_applyrule(step,61,31).
	  no_applyrule(step,61,33).
	  no_applyrule(step,61,28).
	  no_applyrule(step,61,3).
	  no_applyrule(step,61,15).
	  no_applyrule(step,61,24).
	  no_applyrule(step,61,14).
	  no_applyelserule(step,61).
no_elserule(step,61).
	% %
applyrule(step,7,31,61).
applyrule(step,7,15,22).
applyrule(step,7,24,40).
applyrule(step,7,14,18).
applyrule(step,13,3,13).
applyrule(step,11,3,11).
applyrule(step,58,3,58).
applyrule(step,16,3,16).
applyrule(step,20,3,20).
applyrule(step,0,33,68).
applyrule(step,0,28,51).
applyrule(step,12,31,58).
applyrule(step,12,3,12).
applyrule(step,12,15,20).
applyrule(step,12,24,37).
applyrule(step,12,14,16).
applyrule(step,37,3,37).
elserule(step,6,10).
elserule(step,4,4).
elserule(step,5,10).
elserule(step,1,6).
treerule(step,4,3).
treerule(step,13,15).
treerule(step,11,14).
treerule(step,22,33).
treerule(step,58,31).
treerule(step,16,24).
treerule(step,20,31).
treerule(step,40,28).
treerule(step,12,3).
treerule(step,37,24).
treerule(step,18,28).
treerule(step,61,33).
elserule(step,10,12).
elserule(step,8,12).
elserule(step,2,7).
elserule(step,9,12).
elserule(step,0,4).
internalrule(step,5,'informaltable',name,8).
internalrule(step,5,'table',name,9).
internalrule(step,1,'doc',namespace,5).
internalrule(step,8,'x',var,11).
internalrule(step,9,'x',var,13).
internalrule(step,0,'elem',type,1).
internalrule(step,0,'doc',type,2).
no_elserule(step,31).
no_elserule(step,33).
no_elserule(step,28).
no_elserule(step,3).
no_elserule(step,15).
no_elserule(step,24).
no_elserule(step,14).
no_elserule(step,7).
no_elserule(step,68).
no_elserule(step,13).
no_elserule(step,11).
no_elserule(step,22).
no_elserule(step,51).
no_elserule(step,58).
no_elserule(step,16).
no_elserule(step,20).
no_elserule(step,40).
no_elserule(step,12).
no_elserule(step,37).
no_elserule(step,18).
no_elserule(step,61).
no_treerule(step,31).
no_treerule(step,33).
no_treerule(step,28).
no_treerule(step,3).
no_treerule(step,15).
no_treerule(step,24).
no_treerule(step,14).
no_treerule(step,10).
no_treerule(step,7).
no_treerule(step,68).
no_treerule(step,6).
no_treerule(step,5).
no_treerule(step,51).
no_treerule(step,8).
no_treerule(step,2).
no_treerule(step,9).
no_treerule(step,1).
no_treerule(step,0).
label(step,'att',type,8).
no_internalrule(step,31,'att',type).
no_internalrule(step,33,'att',type).
no_internalrule(step,28,'att',type).
no_internalrule(step,3,'att',type).
no_internalrule(step,15,'att',type).
no_internalrule(step,24,'att',type).
no_internalrule(step,14,'att',type).
no_internalrule(step,10,'att',type).
no_internalrule(step,7,'att',type).
no_internalrule(step,68,'att',type).
no_internalrule(step,6,'att',type).
no_internalrule(step,4,'att',type).
no_internalrule(step,13,'att',type).
no_internalrule(step,5,'att',type).
no_internalrule(step,11,'att',type).
no_internalrule(step,22,'att',type).
no_internalrule(step,51,'att',type).
no_internalrule(step,58,'att',type).
no_internalrule(step,8,'att',type).
no_internalrule(step,16,'att',type).
no_internalrule(step,2,'att',type).
no_internalrule(step,9,'att',type).
no_internalrule(step,20,'att',type).
no_internalrule(step,1,'att',type).
no_internalrule(step,0,'att',type).
no_internalrule(step,40,'att',type).
no_internalrule(step,12,'att',type).
no_internalrule(step,37,'att',type).
no_internalrule(step,18,'att',type).
no_internalrule(step,61,'att',type).
label(step,'informaltable',name,4).
no_internalrule(step,31,'informaltable',name).
no_internalrule(step,33,'informaltable',name).
no_internalrule(step,28,'informaltable',name).
no_internalrule(step,3,'informaltable',name).
no_internalrule(step,15,'informaltable',name).
no_internalrule(step,24,'informaltable',name).
no_internalrule(step,14,'informaltable',name).
no_internalrule(step,10,'informaltable',name).
no_internalrule(step,7,'informaltable',name).
no_internalrule(step,68,'informaltable',name).
no_internalrule(step,6,'informaltable',name).
no_internalrule(step,4,'informaltable',name).
no_internalrule(step,13,'informaltable',name).
no_internalrule(step,11,'informaltable',name).
no_internalrule(step,22,'informaltable',name).
no_internalrule(step,51,'informaltable',name).
no_internalrule(step,58,'informaltable',name).
no_internalrule(step,8,'informaltable',name).
no_internalrule(step,16,'informaltable',name).
no_internalrule(step,2,'informaltable',name).
no_internalrule(step,9,'informaltable',name).
no_internalrule(step,20,'informaltable',name).
no_internalrule(step,1,'informaltable',name).
no_internalrule(step,0,'informaltable',name).
no_internalrule(step,40,'informaltable',name).
no_internalrule(step,12,'informaltable',name).
no_internalrule(step,37,'informaltable',name).
no_internalrule(step,18,'informaltable',name).
no_internalrule(step,61,'informaltable',name).
label(step,'table',name,5).
no_internalrule(step,31,'table',name).
no_internalrule(step,33,'table',name).
no_internalrule(step,28,'table',name).
no_internalrule(step,3,'table',name).
no_internalrule(step,15,'table',name).
no_internalrule(step,24,'table',name).
no_internalrule(step,14,'table',name).
no_internalrule(step,10,'table',name).
no_internalrule(step,7,'table',name).
no_internalrule(step,68,'table',name).
no_internalrule(step,6,'table',name).
no_internalrule(step,4,'table',name).
no_internalrule(step,13,'table',name).
no_internalrule(step,11,'table',name).
no_internalrule(step,22,'table',name).
no_internalrule(step,51,'table',name).
no_internalrule(step,58,'table',name).
no_internalrule(step,8,'table',name).
no_internalrule(step,16,'table',name).
no_internalrule(step,2,'table',name).
no_internalrule(step,9,'table',name).
no_internalrule(step,20,'table',name).
no_internalrule(step,1,'table',name).
no_internalrule(step,0,'table',name).
no_internalrule(step,40,'table',name).
no_internalrule(step,12,'table',name).
no_internalrule(step,37,'table',name).
no_internalrule(step,18,'table',name).
no_internalrule(step,61,'table',name).
label(step,'x',var,6).
no_internalrule(step,31,'x',var).
no_internalrule(step,33,'x',var).
no_internalrule(step,28,'x',var).
no_internalrule(step,3,'x',var).
no_internalrule(step,15,'x',var).
no_internalrule(step,24,'x',var).
no_internalrule(step,14,'x',var).
no_internalrule(step,10,'x',var).
no_internalrule(step,7,'x',var).
no_internalrule(step,68,'x',var).
no_internalrule(step,6,'x',var).
no_internalrule(step,4,'x',var).
no_internalrule(step,13,'x',var).
no_internalrule(step,5,'x',var).
no_internalrule(step,11,'x',var).
no_internalrule(step,22,'x',var).
no_internalrule(step,51,'x',var).
no_internalrule(step,58,'x',var).
no_internalrule(step,16,'x',var).
no_internalrule(step,2,'x',var).
no_internalrule(step,20,'x',var).
no_internalrule(step,1,'x',var).
no_internalrule(step,0,'x',var).
no_internalrule(step,40,'x',var).
no_internalrule(step,12,'x',var).
no_internalrule(step,37,'x',var).
no_internalrule(step,18,'x',var).
no_internalrule(step,61,'x',var).
label(step,'x',negvar,13).
no_internalrule(step,31,'x',negvar).
no_internalrule(step,33,'x',negvar).
no_internalrule(step,28,'x',negvar).
no_internalrule(step,3,'x',negvar).
no_internalrule(step,15,'x',negvar).
no_internalrule(step,24,'x',negvar).
no_internalrule(step,14,'x',negvar).
no_internalrule(step,10,'x',negvar).
no_internalrule(step,7,'x',negvar).
no_internalrule(step,68,'x',negvar).
no_internalrule(step,6,'x',negvar).
no_internalrule(step,4,'x',negvar).
no_internalrule(step,13,'x',negvar).
no_internalrule(step,5,'x',negvar).
no_internalrule(step,11,'x',negvar).
no_internalrule(step,22,'x',negvar).
no_internalrule(step,51,'x',negvar).
no_internalrule(step,58,'x',negvar).
no_internalrule(step,8,'x',negvar).
no_internalrule(step,16,'x',negvar).
no_internalrule(step,2,'x',negvar).
no_internalrule(step,9,'x',negvar).
no_internalrule(step,20,'x',negvar).
no_internalrule(step,1,'x',negvar).
no_internalrule(step,0,'x',negvar).
no_internalrule(step,40,'x',negvar).
no_internalrule(step,12,'x',negvar).
no_internalrule(step,37,'x',negvar).
no_internalrule(step,18,'x',negvar).
no_internalrule(step,61,'x',negvar).
label(step,'text',type,12).
no_internalrule(step,31,'text',type).
no_internalrule(step,33,'text',type).
no_internalrule(step,28,'text',type).
no_internalrule(step,3,'text',type).
no_internalrule(step,15,'text',type).
no_internalrule(step,24,'text',type).
no_internalrule(step,14,'text',type).
no_internalrule(step,10,'text',type).
no_internalrule(step,7,'text',type).
no_internalrule(step,68,'text',type).
no_internalrule(step,6,'text',type).
no_internalrule(step,4,'text',type).
no_internalrule(step,13,'text',type).
no_internalrule(step,5,'text',type).
no_internalrule(step,11,'text',type).
no_internalrule(step,22,'text',type).
no_internalrule(step,51,'text',type).
no_internalrule(step,58,'text',type).
no_internalrule(step,8,'text',type).
no_internalrule(step,16,'text',type).
no_internalrule(step,2,'text',type).
no_internalrule(step,9,'text',type).
no_internalrule(step,20,'text',type).
no_internalrule(step,1,'text',type).
no_internalrule(step,0,'text',type).
no_internalrule(step,40,'text',type).
no_internalrule(step,12,'text',type).
no_internalrule(step,37,'text',type).
no_internalrule(step,18,'text',type).
no_internalrule(step,61,'text',type).
label(step,'comment',type,9).
no_internalrule(step,31,'comment',type).
no_internalrule(step,33,'comment',type).
no_internalrule(step,28,'comment',type).
no_internalrule(step,3,'comment',type).
no_internalrule(step,15,'comment',type).
no_internalrule(step,24,'comment',type).
no_internalrule(step,14,'comment',type).
no_internalrule(step,10,'comment',type).
no_internalrule(step,7,'comment',type).
no_internalrule(step,68,'comment',type).
no_internalrule(step,6,'comment',type).
no_internalrule(step,4,'comment',type).
no_internalrule(step,13,'comment',type).
no_internalrule(step,5,'comment',type).
no_internalrule(step,11,'comment',type).
no_internalrule(step,22,'comment',type).
no_internalrule(step,51,'comment',type).
no_internalrule(step,58,'comment',type).
no_internalrule(step,8,'comment',type).
no_internalrule(step,16,'comment',type).
no_internalrule(step,2,'comment',type).
no_internalrule(step,9,'comment',type).
no_internalrule(step,20,'comment',type).
no_internalrule(step,1,'comment',type).
no_internalrule(step,0,'comment',type).
no_internalrule(step,40,'comment',type).
no_internalrule(step,12,'comment',type).
no_internalrule(step,37,'comment',type).
no_internalrule(step,18,'comment',type).
no_internalrule(step,61,'comment',type).
label(step,'elem',type,3).
no_internalrule(step,31,'elem',type).
no_internalrule(step,33,'elem',type).
no_internalrule(step,28,'elem',type).
no_internalrule(step,3,'elem',type).
no_internalrule(step,15,'elem',type).
no_internalrule(step,24,'elem',type).
no_internalrule(step,14,'elem',type).
no_internalrule(step,10,'elem',type).
no_internalrule(step,7,'elem',type).
no_internalrule(step,68,'elem',type).
no_internalrule(step,6,'elem',type).
no_internalrule(step,4,'elem',type).
no_internalrule(step,13,'elem',type).
no_internalrule(step,5,'elem',type).
no_internalrule(step,11,'elem',type).
no_internalrule(step,22,'elem',type).
no_internalrule(step,51,'elem',type).
no_internalrule(step,58,'elem',type).
no_internalrule(step,8,'elem',type).
no_internalrule(step,16,'elem',type).
no_internalrule(step,2,'elem',type).
no_internalrule(step,9,'elem',type).
no_internalrule(step,20,'elem',type).
no_internalrule(step,1,'elem',type).
no_internalrule(step,40,'elem',type).
no_internalrule(step,12,'elem',type).
no_internalrule(step,37,'elem',type).
no_internalrule(step,18,'elem',type).
no_internalrule(step,61,'elem',type).
label(step,'doc',namespace,1).
no_internalrule(step,31,'doc',namespace).
no_internalrule(step,33,'doc',namespace).
no_internalrule(step,28,'doc',namespace).
no_internalrule(step,3,'doc',namespace).
no_internalrule(step,15,'doc',namespace).
no_internalrule(step,24,'doc',namespace).
no_internalrule(step,14,'doc',namespace).
no_internalrule(step,10,'doc',namespace).
no_internalrule(step,7,'doc',namespace).
no_internalrule(step,68,'doc',namespace).
no_internalrule(step,6,'doc',namespace).
no_internalrule(step,4,'doc',namespace).
no_internalrule(step,13,'doc',namespace).
no_internalrule(step,5,'doc',namespace).
no_internalrule(step,11,'doc',namespace).
no_internalrule(step,22,'doc',namespace).
no_internalrule(step,51,'doc',namespace).
no_internalrule(step,58,'doc',namespace).
no_internalrule(step,8,'doc',namespace).
no_internalrule(step,16,'doc',namespace).
no_internalrule(step,2,'doc',namespace).
no_internalrule(step,9,'doc',namespace).
no_internalrule(step,20,'doc',namespace).
no_internalrule(step,0,'doc',namespace).
no_internalrule(step,40,'doc',namespace).
no_internalrule(step,12,'doc',namespace).
no_internalrule(step,37,'doc',namespace).
no_internalrule(step,18,'doc',namespace).
no_internalrule(step,61,'doc',namespace).
label(step,'doc',type,2).
no_internalrule(step,31,'doc',type).
no_internalrule(step,33,'doc',type).
no_internalrule(step,28,'doc',type).
no_internalrule(step,3,'doc',type).
no_internalrule(step,15,'doc',type).
no_internalrule(step,24,'doc',type).
no_internalrule(step,14,'doc',type).
no_internalrule(step,10,'doc',type).
no_internalrule(step,7,'doc',type).
no_internalrule(step,68,'doc',type).
no_internalrule(step,6,'doc',type).
no_internalrule(step,4,'doc',type).
no_internalrule(step,13,'doc',type).
no_internalrule(step,5,'doc',type).
no_internalrule(step,11,'doc',type).
no_internalrule(step,22,'doc',type).
no_internalrule(step,51,'doc',type).
no_internalrule(step,58,'doc',type).
no_internalrule(step,8,'doc',type).
no_internalrule(step,16,'doc',type).
no_internalrule(step,2,'doc',type).
no_internalrule(step,9,'doc',type).
no_internalrule(step,20,'doc',type).
no_internalrule(step,1,'doc',type).
no_internalrule(step,40,'doc',type).
no_internalrule(step,12,'doc',type).
no_internalrule(step,37,'doc',type).
no_internalrule(step,18,'doc',type).
no_internalrule(step,61,'doc',type).
max_label_id(step,13).
    %%% different origins %%%%%%%
    
