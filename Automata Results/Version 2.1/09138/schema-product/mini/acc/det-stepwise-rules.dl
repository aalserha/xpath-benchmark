%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Accessible states of stepwise hedge automata A
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% whether variable is allowed or not

kind(0).
kind(1).

%% whether we started a tree or a hedge

start(t).
start(h).

%% types different from type var


nonvar(type).
nonvar(char).
nonvar(negvar).
nonvar(name).
nonvar(namespace).

%% starting trees and hedges

acc(t,0,A,X) :- start_tree(A,X).
acc(h,0,A,X) :- initial(A,X).

%% ending trees

acc_tree_state(K,A,Z) :- acc(t,K,A,X), treerule(A,X,Z).
acc_tree_state(K,A,Y) :- acc_tree_state(K,A,X), is(A,X,Y).

%% internal rules

internalrule_all(A,X,L,T,Y) :- internalrule(A,X,L,T,Y).
internalrule_all(A,X,L,T,Y) :- elserule(A,X,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,L,T,Y) :- typedelserule(A,X,T,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- elserule(A,X,Y), typedelserule(A,L,T,Z).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- typedelserule(A,X,T,Y).

acc(S,K,A,Y) :- acc(S,K,A,X), internalrule_all(A,X,L,T  ,Y), nonvar(T).
acc(S,1,A,Y) :- acc(S,0,A,X), internalrule_all(A,X,L,var,Y).

%% epsilon rules

acc(S,K,A,Y) :- acc(S,K,A,X), epsilonrule(A,X,Y), kind(K).

%% apply rules

applyrule_all(A,X,Z,Y) :- applyrule(A,X,Z,Y).
applyrule_all(A,X,Z,Y) :- applyelserule(A,X,Y), no_applyrule(A,X,Z).

acc(S,K,A,Y) :- acc(S,K,A,X), acc_tree_state(0,A,Z), applyrule_all(A,X,Z,Y).
acc(S,1,A,Y) :- acc(S,0,A,X), acc_tree_state(1,A,Z), applyrule_all(A,X,Z,Y).

%% compatibility with previous predicates

acc_h(A,X) :- acc(h,1,A,X).
acc_t(A,X) :- acc(t,1,A,X).




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% label states where XML labels end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

internal_or_else(A,X,Y) :- internalrule(A,X,L,T,Y).
internal_or_else(A,X,Y) :- elserule(A,X,Y).
internal_or_else(A,X,Y) :- typedelserule(A,X,T,Y).

label_state(A,U) :- initial(A,X), internalrule(A,X,'elem',type,Y),internal_or_else(A,Y,Z),
		    internal_or_else(A,Z,U).
label_state(A,U) :- initial(A,X), internalrule(A,X,'att',type,Y), internal_or_else(A,Y,Z),
                    internal_or_else(A,Z,U).
label_state(A,Y) :- initial(A,X), internalrule(A,X,'text',type,Y). 
label_state(A,Y) :- initial(A,X), internalrule(A,X,'comment',type,Y). 
label_state(A,Y) :- initial(A,X), internalrule(A,X,'doc',type,Y). 

acc_label(A,X,X) :- label_state(A,X).
acc_label(A,X,Z) :- acc_label(A,X,Y), internalrule(A,Y,L,T,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), elserule(A,Y,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), typedelserule(A,Y,T,Z).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(A,Z), applyrule(A,X1,Z,Y).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(A,Z), applyelserule(A,X1,Y), no_applyrule(A,X1,Z).

acc_label_tree(A,X,Z) :- acc_label(A,X,Y), treerule(A,Y,Z).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% types of label states
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

elem(A,NS,N,U)   :- initial(A,X), internalrule(A,X,'elem',type,NS),internal_or_else(A,NS,N),
		    internal_or_else(A,N,U).
att(A,NS,N,U)    :- initial(A,X), internalrule(A,X,'att',type,NS), internal_or_else(A,NS,N),
                    internal_or_else(A,N,U).
text(A,Y)	 :- initial(A,X), internalrule(A,X,'text',type,Y). 
comment(A,Y)     :- initial(A,X), internalrule(A,X,'comment',type,Y). 
doc(A,Y)         :- initial(A,X), internalrule(A,X,'doc',type,Y).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule
% negative information (NEGATIVE=true)
%   no_applyrule
%   no_elserule
%   no_treerule
%   no_internalrule
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(stepwise,'mini(det_Ttyped[stepwise[]])').
var(stepwise,'x',1).
no_typedelserule(stepwise,3,type).
no_typedelserule(stepwise,1,type).
no_typedelserule(stepwise,29,type).
no_typedelserule(stepwise,38,type).
no_typedelserule(stepwise,5,type).
no_typedelserule(stepwise,41,type).
no_typedelserule(stepwise,28,type).
no_typedelserule(stepwise,2,type).
no_typedelserule(stepwise,4,type).
no_typedelserule(stepwise,13,type).
no_typedelserule(stepwise,26,type).
no_typedelserule(stepwise,9,type).
no_typedelserule(stepwise,19,type).
no_typedelserule(stepwise,39,type).
no_typedelserule(stepwise,12,type).
no_typedelserule(stepwise,67,type).
no_typedelserule(stepwise,27,type).
no_typedelserule(stepwise,0,type).
no_typedelserule(stepwise,20,type).
no_typedelserule(stepwise,8,type).
no_typedelserule(stepwise,25,type).
no_typedelserule(stepwise,55,type).
no_typedelserule(stepwise,10,type).
no_typedelserule(stepwise,6,type).
no_typedelserule(stepwise,18,type).
no_typedelserule(stepwise,7,type).
no_typedelserule(stepwise,11,type).
no_typedelserule(stepwise,3,name).
no_typedelserule(stepwise,1,name).
no_typedelserule(stepwise,29,name).
no_typedelserule(stepwise,38,name).
no_typedelserule(stepwise,5,name).
no_typedelserule(stepwise,41,name).
no_typedelserule(stepwise,28,name).
no_typedelserule(stepwise,2,name).
no_typedelserule(stepwise,4,name).
no_typedelserule(stepwise,13,name).
no_typedelserule(stepwise,26,name).
no_typedelserule(stepwise,9,name).
no_typedelserule(stepwise,19,name).
no_typedelserule(stepwise,39,name).
no_typedelserule(stepwise,12,name).
no_typedelserule(stepwise,67,name).
no_typedelserule(stepwise,27,name).
no_typedelserule(stepwise,0,name).
no_typedelserule(stepwise,20,name).
no_typedelserule(stepwise,25,name).
no_typedelserule(stepwise,55,name).
no_typedelserule(stepwise,18,name).
no_typedelserule(stepwise,11,name).
no_typedelserule(stepwise,3,namespace).
no_typedelserule(stepwise,1,namespace).
no_typedelserule(stepwise,29,namespace).
no_typedelserule(stepwise,38,namespace).
no_typedelserule(stepwise,5,namespace).
no_typedelserule(stepwise,41,namespace).
no_typedelserule(stepwise,28,namespace).
no_typedelserule(stepwise,13,namespace).
no_typedelserule(stepwise,26,namespace).
no_typedelserule(stepwise,9,namespace).
no_typedelserule(stepwise,19,namespace).
no_typedelserule(stepwise,39,namespace).
no_typedelserule(stepwise,12,namespace).
no_typedelserule(stepwise,67,namespace).
no_typedelserule(stepwise,27,namespace).
no_typedelserule(stepwise,0,namespace).
no_typedelserule(stepwise,20,namespace).
no_typedelserule(stepwise,8,namespace).
no_typedelserule(stepwise,25,namespace).
no_typedelserule(stepwise,55,namespace).
no_typedelserule(stepwise,10,namespace).
no_typedelserule(stepwise,6,namespace).
no_typedelserule(stepwise,18,namespace).
no_typedelserule(stepwise,7,namespace).
no_typedelserule(stepwise,11,namespace).
no_typedelserule(stepwise,3,var).
no_typedelserule(stepwise,1,var).
no_typedelserule(stepwise,29,var).
no_typedelserule(stepwise,38,var).
no_typedelserule(stepwise,5,var).
no_typedelserule(stepwise,41,var).
no_typedelserule(stepwise,28,var).
no_typedelserule(stepwise,2,var).
no_typedelserule(stepwise,4,var).
no_typedelserule(stepwise,13,var).
no_typedelserule(stepwise,26,var).
no_typedelserule(stepwise,9,var).
no_typedelserule(stepwise,19,var).
no_typedelserule(stepwise,39,var).
no_typedelserule(stepwise,12,var).
no_typedelserule(stepwise,67,var).
no_typedelserule(stepwise,27,var).
no_typedelserule(stepwise,0,var).
no_typedelserule(stepwise,20,var).
no_typedelserule(stepwise,8,var).
no_typedelserule(stepwise,25,var).
no_typedelserule(stepwise,55,var).
no_typedelserule(stepwise,10,var).
no_typedelserule(stepwise,6,var).
no_typedelserule(stepwise,18,var).
no_typedelserule(stepwise,7,var).
no_typedelserule(stepwise,11,var).
no_typedelserule(stepwise,3,negvar).
no_typedelserule(stepwise,1,negvar).
no_typedelserule(stepwise,29,negvar).
no_typedelserule(stepwise,38,negvar).
no_typedelserule(stepwise,5,negvar).
no_typedelserule(stepwise,41,negvar).
no_typedelserule(stepwise,28,negvar).
no_typedelserule(stepwise,2,negvar).
no_typedelserule(stepwise,4,negvar).
no_typedelserule(stepwise,13,negvar).
no_typedelserule(stepwise,26,negvar).
no_typedelserule(stepwise,9,negvar).
no_typedelserule(stepwise,19,negvar).
no_typedelserule(stepwise,39,negvar).
no_typedelserule(stepwise,12,negvar).
no_typedelserule(stepwise,67,negvar).
no_typedelserule(stepwise,27,negvar).
no_typedelserule(stepwise,0,negvar).
no_typedelserule(stepwise,20,negvar).
no_typedelserule(stepwise,8,negvar).
no_typedelserule(stepwise,25,negvar).
no_typedelserule(stepwise,55,negvar).
no_typedelserule(stepwise,10,negvar).
no_typedelserule(stepwise,6,negvar).
no_typedelserule(stepwise,18,negvar).
no_typedelserule(stepwise,7,negvar).
no_typedelserule(stepwise,11,negvar).
no_typedelserule(stepwise,3,char).
no_typedelserule(stepwise,1,char).
no_typedelserule(stepwise,29,char).
no_typedelserule(stepwise,38,char).
no_typedelserule(stepwise,41,char).
no_typedelserule(stepwise,28,char).
no_typedelserule(stepwise,2,char).
no_typedelserule(stepwise,4,char).
no_typedelserule(stepwise,13,char).
no_typedelserule(stepwise,26,char).
no_typedelserule(stepwise,9,char).
no_typedelserule(stepwise,19,char).
no_typedelserule(stepwise,39,char).
no_typedelserule(stepwise,12,char).
no_typedelserule(stepwise,67,char).
no_typedelserule(stepwise,27,char).
no_typedelserule(stepwise,0,char).
no_typedelserule(stepwise,20,char).
no_typedelserule(stepwise,8,char).
no_typedelserule(stepwise,55,char).
no_typedelserule(stepwise,10,char).
no_typedelserule(stepwise,6,char).
no_typedelserule(stepwise,18,char).
no_typedelserule(stepwise,7,char).
no_typedelserule(stepwise,11,char).
state(stepwise,3).
state(stepwise,1).
state(stepwise,29).
state(stepwise,38).
state(stepwise,5).
state(stepwise,41).
state(stepwise,28).
state(stepwise,2).
state(stepwise,4).
state(stepwise,13).
state(stepwise,26).
state(stepwise,9).
state(stepwise,19).
state(stepwise,39).
state(stepwise,12).
state(stepwise,67).
state(stepwise,27).
state(stepwise,0).
state(stepwise,20).
state(stepwise,8).
state(stepwise,25).
state(stepwise,55).
state(stepwise,10).
state(stepwise,6).
state(stepwise,18).
state(stepwise,7).
state(stepwise,11).
start_tree(stepwise,0).
initial(stepwise,0).
origin_of(stepwise,3,'152','70','78','181','199','0').
origin_of(stepwise,1,'94','0').
origin_of(stepwise,29,'182','0').
origin_of(stepwise,38,'15','1').
origin_of(stepwise,5,'92','0').
origin_of(stepwise,41,'36','114','0').
origin_of(stepwise,28,'10','1').
origin_of(stepwise,2,'176','103','34','51','126','91','200','115','29','40','177','0').
origin_of(stepwise,4,'88','0').
origin_of(stepwise,13,'77','180','89','69','198','151','0').
origin_of(stepwise,26,'13','1').
origin_of(stepwise,9,'61','169','190','118','131','41','153','121','161','56','100','128','26','81','141','0').
origin_of(stepwise,19,'53','185','155','65','163','171','123','58','46','192','93','130','43','28','120','63','143','197','83','133','102','0').
origin_of(stepwise,39,'139','0').
origin_of(stepwise,12,'77','22','180','89','69','198','151','0').
origin_of(stepwise,67,'137','0').
origin_of(stepwise,27,'101','82','42','27','52','154','122','57','170','119','184','45','142','62','191','129','196','90','162','132','64','0').
origin_of(stepwise,0,'138','165','84','66','79','59','76','108','96','158','60','167','75','160','109','39','71','150','166','99','168','116','38','21','140','68','0').
origin_of(stepwise,20,'183','0').
origin_of(stepwise,8,'44','95','117','189','104','105','0').
origin_of(stepwise,25,'97','0').
origin_of(stepwise,55,'5','1').
origin_of(stepwise,10,'87','0').
origin_of(stepwise,6,'44','95','117','189','105','72','104','0').
origin_of(stepwise,18,'98','0').
origin_of(stepwise,7,'44','95','117','195','112','104','105','127','37','189','0').
origin_of(stepwise,11,'14','1').
not_sink(stepwise,3).
not_sink(stepwise,1).
not_sink(stepwise,29).
not_sink(stepwise,38).
not_sink(stepwise,5).
not_sink(stepwise,41).
not_sink(stepwise,28).
not_sink(stepwise,2).
not_sink(stepwise,4).
not_sink(stepwise,13).
not_sink(stepwise,26).
not_sink(stepwise,9).
not_sink(stepwise,19).
not_sink(stepwise,39).
not_sink(stepwise,12).
not_sink(stepwise,67).
not_sink(stepwise,27).
not_sink(stepwise,0).
not_sink(stepwise,20).
not_sink(stepwise,8).
not_sink(stepwise,25).
not_sink(stepwise,55).
not_sink(stepwise,10).
not_sink(stepwise,6).
not_sink(stepwise,18).
not_sink(stepwise,7).
not_sink(stepwise,11).
nonelse(stepwise,3).
nonelse(stepwise,1).
nonelse(stepwise,29).
nonelse(stepwise,38).
nonelse(stepwise,5).
nonelse(stepwise,41).
nonelse(stepwise,28).
nonelse(stepwise,2).
nonelse(stepwise,4).
nonelse(stepwise,13).
nonelse(stepwise,26).
nonelse(stepwise,9).
nonelse(stepwise,19).
nonelse(stepwise,39).
nonelse(stepwise,12).
nonelse(stepwise,67).
nonelse(stepwise,27).
nonelse(stepwise,0).
nonelse(stepwise,20).
nonelse(stepwise,8).
nonelse(stepwise,25).
nonelse(stepwise,55).
nonelse(stepwise,10).
nonelse(stepwise,6).
nonelse(stepwise,18).
nonelse(stepwise,7).
nonelse(stepwise,11).
final(stepwise,39).
final(stepwise,67).
nonfinal(stepwise,3).
nonfinal(stepwise,1).
nonfinal(stepwise,29).
nonfinal(stepwise,38).
nonfinal(stepwise,5).
nonfinal(stepwise,41).
nonfinal(stepwise,28).
nonfinal(stepwise,2).
nonfinal(stepwise,4).
nonfinal(stepwise,13).
nonfinal(stepwise,26).
nonfinal(stepwise,9).
nonfinal(stepwise,19).
nonfinal(stepwise,12).
nonfinal(stepwise,27).
nonfinal(stepwise,0).
nonfinal(stepwise,20).
nonfinal(stepwise,8).
nonfinal(stepwise,25).
nonfinal(stepwise,55).
nonfinal(stepwise,10).
nonfinal(stepwise,6).
nonfinal(stepwise,18).
nonfinal(stepwise,7).
nonfinal(stepwise,11).
tree_state(stepwise,38).
tree_istate(stepwise,38,0).
 tree_state(stepwise,28).
tree_istate(stepwise,28,0).
 tree_state(stepwise,26).
tree_istate(stepwise,26,0).
 tree_state(stepwise,55).
tree_istate(stepwise,55,0).
 tree_state(stepwise,11).
tree_istate(stepwise,11,0).
 hedge_state(stepwise,3).
    no_applyrule(stepwise,3,38).
	  no_applyrule(stepwise,3,28).
	  no_applyrule(stepwise,3,26).
	  no_applyrule(stepwise,3,55).
	  no_applyrule(stepwise,3,11).
	  no_applyelserule(stepwise,3).
no_elserule(stepwise,3).
	% %
hedge_state(stepwise,1).
    no_applyrule(stepwise,1,38).
	  no_applyrule(stepwise,1,28).
	  no_applyrule(stepwise,1,26).
	  no_applyrule(stepwise,1,55).
	  no_applyrule(stepwise,1,11).
	  no_applyelserule(stepwise,1).
no_elserule(stepwise,1).
	% %
hedge_state(stepwise,29).
    no_applyrule(stepwise,29,38).
	  no_applyrule(stepwise,29,28).
	  no_applyrule(stepwise,29,55).
	  no_applyelserule(stepwise,29).
no_elserule(stepwise,29).
	% %
hedge_state(stepwise,5).
    no_applyrule(stepwise,5,38).
	  no_applyrule(stepwise,5,28).
	  no_applyrule(stepwise,5,26).
	  no_applyrule(stepwise,5,55).
	  no_applyrule(stepwise,5,11).
	  no_applyelserule(stepwise,5).
hedge_state(stepwise,41).
    no_applyrule(stepwise,41,38).
	  no_applyrule(stepwise,41,28).
	  no_applyrule(stepwise,41,26).
	  no_applyrule(stepwise,41,55).
	  no_applyrule(stepwise,41,11).
	  no_applyelserule(stepwise,41).
no_elserule(stepwise,41).
	% %
hedge_state(stepwise,2).
    no_applyrule(stepwise,2,38).
	  no_applyrule(stepwise,2,28).
	  no_applyrule(stepwise,2,26).
	  no_applyrule(stepwise,2,55).
	  no_applyrule(stepwise,2,11).
	  no_applyelserule(stepwise,2).
hedge_state(stepwise,4).
    no_applyrule(stepwise,4,38).
	  no_applyrule(stepwise,4,28).
	  no_applyrule(stepwise,4,26).
	  no_applyrule(stepwise,4,55).
	  no_applyrule(stepwise,4,11).
	  no_applyelserule(stepwise,4).
hedge_state(stepwise,13).
    no_applyrule(stepwise,13,38).
	  no_applyrule(stepwise,13,28).
	  no_applyrule(stepwise,13,26).
	  no_applyrule(stepwise,13,55).
	  no_applyrule(stepwise,13,11).
	  no_applyelserule(stepwise,13).
no_elserule(stepwise,13).
	% %
hedge_state(stepwise,9).
    no_applyrule(stepwise,9,38).
	  no_applyrule(stepwise,9,26).
	  no_applyrule(stepwise,9,55).
	  no_applyrule(stepwise,9,11).
	  no_applyelserule(stepwise,9).
no_elserule(stepwise,9).
	% %
hedge_state(stepwise,19).
    no_applyrule(stepwise,19,55).
	  no_applyelserule(stepwise,19).
no_elserule(stepwise,19).
	% %
hedge_state(stepwise,39).
    no_applyrule(stepwise,39,38).
	  no_applyrule(stepwise,39,28).
	  no_applyrule(stepwise,39,55).
	  no_applyrule(stepwise,39,11).
	  no_applyelserule(stepwise,39).
no_elserule(stepwise,39).
	% %
hedge_state(stepwise,12).
    no_applyrule(stepwise,12,38).
	  no_applyrule(stepwise,12,28).
	  no_applyrule(stepwise,12,26).
	  no_applyrule(stepwise,12,55).
	  no_applyrule(stepwise,12,11).
	  no_applyelserule(stepwise,12).
no_elserule(stepwise,12).
	% %
hedge_state(stepwise,67).
    no_applyrule(stepwise,67,38).
	  no_applyrule(stepwise,67,28).
	  no_applyrule(stepwise,67,26).
	  no_applyrule(stepwise,67,55).
	  no_applyrule(stepwise,67,11).
	  no_applyelserule(stepwise,67).
no_elserule(stepwise,67).
	% %
hedge_state(stepwise,27).
    no_applyrule(stepwise,27,38).
	  no_applyrule(stepwise,27,55).
	  no_applyelserule(stepwise,27).
no_elserule(stepwise,27).
	% %
hedge_state(stepwise,0).
    no_applyrule(stepwise,0,38).
	  no_applyrule(stepwise,0,26).
	  no_applyrule(stepwise,0,11).
	  no_applyelserule(stepwise,0).
no_elserule(stepwise,0).
	% %
hedge_state(stepwise,20).
    no_applyrule(stepwise,20,28).
	  no_applyrule(stepwise,20,55).
	  no_applyelserule(stepwise,20).
no_elserule(stepwise,20).
	% %
hedge_state(stepwise,8).
    no_applyrule(stepwise,8,38).
	  no_applyrule(stepwise,8,28).
	  no_applyrule(stepwise,8,26).
	  no_applyrule(stepwise,8,55).
	  no_applyrule(stepwise,8,11).
	  no_applyelserule(stepwise,8).
hedge_state(stepwise,25).
    no_applyrule(stepwise,25,38).
	  no_applyrule(stepwise,25,28).
	  no_applyrule(stepwise,25,26).
	  no_applyrule(stepwise,25,55).
	  no_applyrule(stepwise,25,11).
	  no_applyelserule(stepwise,25).
hedge_state(stepwise,10).
    no_applyrule(stepwise,10,38).
	  no_applyrule(stepwise,10,28).
	  no_applyrule(stepwise,10,26).
	  no_applyrule(stepwise,10,55).
	  no_applyrule(stepwise,10,11).
	  no_applyelserule(stepwise,10).
hedge_state(stepwise,6).
    no_applyrule(stepwise,6,38).
	  no_applyrule(stepwise,6,28).
	  no_applyrule(stepwise,6,26).
	  no_applyrule(stepwise,6,55).
	  no_applyrule(stepwise,6,11).
	  no_applyelserule(stepwise,6).
hedge_state(stepwise,18).
    no_applyrule(stepwise,18,38).
	  no_applyrule(stepwise,18,28).
	  no_applyrule(stepwise,18,26).
	  no_applyrule(stepwise,18,55).
	  no_applyrule(stepwise,18,11).
	  no_applyelserule(stepwise,18).
no_elserule(stepwise,18).
	% %
hedge_state(stepwise,7).
    no_applyrule(stepwise,7,38).
	  no_applyrule(stepwise,7,28).
	  no_applyrule(stepwise,7,26).
	  no_applyrule(stepwise,7,55).
	  no_applyrule(stepwise,7,11).
	  no_applyelserule(stepwise,7).
applyrule(stepwise,39,26,39).
applyrule(stepwise,19,28,29).
typedelserule(stepwise,4,namespace,10).
internalrule(stepwise,6,'ref',name,12).
applyrule(stepwise,27,28,29).
applyrule(stepwise,9,28,41).
typedelserule(stepwise,7,name,13).
treerule(stepwise,19,26).
internalrule(stepwise,2,'rng',namespace,6).
applyrule(stepwise,0,55,67).
internalrule(stepwise,1,'x',negvar,5).
internalrule(stepwise,2,'tei',namespace,7).
internalrule(stepwise,0,'comment',type,1).
treerule(stepwise,25,38).
applyrule(stepwise,0,28,39).
internalrule(stepwise,0,'text',type,1).
internalrule(stepwise,12,'x',var,20).
applyrule(stepwise,20,11,29).
treerule(stepwise,41,55).
internalrule(stepwise,7,'dataRef',name,12).
internalrule(stepwise,0,'doc',type,3).
applyrule(stepwise,29,11,29).
typedelserule(stepwise,5,char,5).
internalrule(stepwise,3,'x',negvar,9).
typedelserule(stepwise,10,name,18).
treerule(stepwise,5,11).
treerule(stepwise,20,28).
typedelserule(stepwise,2,namespace,8).
internalrule(stepwise,18,'x',negvar,25).
applyrule(stepwise,29,26,29).
internalrule(stepwise,13,'x',negvar,19).
typedelserule(stepwise,25,char,25).
applyrule(stepwise,19,26,27).
internalrule(stepwise,7,'macroRef',name,12).
internalrule(stepwise,0,'att',type,4).
applyrule(stepwise,27,26,27).
applyrule(stepwise,20,38,20).
internalrule(stepwise,12,'x',negvar,19).
applyrule(stepwise,20,26,29).
treerule(stepwise,29,28).
typedelserule(stepwise,6,name,13).
applyrule(stepwise,27,11,27).
typedelserule(stepwise,8,name,13).
applyrule(stepwise,19,11,27).
internalrule(stepwise,7,'elementRef',name,12).
treerule(stepwise,27,26).
applyrule(stepwise,19,38,19).
internalrule(stepwise,7,'classRef',name,12).
internalrule(stepwise,0,'elem',type,2).
no_elserule(stepwise,3).
no_elserule(stepwise,1).
no_elserule(stepwise,29).
no_elserule(stepwise,38).
no_elserule(stepwise,41).
no_elserule(stepwise,28).
no_elserule(stepwise,13).
no_elserule(stepwise,26).
no_elserule(stepwise,9).
no_elserule(stepwise,19).
no_elserule(stepwise,39).
no_elserule(stepwise,12).
no_elserule(stepwise,67).
no_elserule(stepwise,27).
no_elserule(stepwise,0).
no_elserule(stepwise,20).
no_elserule(stepwise,55).
no_elserule(stepwise,18).
no_elserule(stepwise,11).
no_treerule(stepwise,3).
no_treerule(stepwise,1).
no_treerule(stepwise,38).
no_treerule(stepwise,28).
no_treerule(stepwise,2).
no_treerule(stepwise,4).
no_treerule(stepwise,13).
no_treerule(stepwise,26).
no_treerule(stepwise,9).
no_treerule(stepwise,39).
no_treerule(stepwise,12).
no_treerule(stepwise,67).
no_treerule(stepwise,0).
no_treerule(stepwise,8).
no_treerule(stepwise,55).
no_treerule(stepwise,10).
no_treerule(stepwise,6).
no_treerule(stepwise,18).
no_treerule(stepwise,7).
no_treerule(stepwise,11).
label(stepwise,'comment',type,3).
no_internalrule(stepwise,3,'comment',type).
no_internalrule(stepwise,1,'comment',type).
no_internalrule(stepwise,29,'comment',type).
no_internalrule(stepwise,38,'comment',type).
no_internalrule(stepwise,5,'comment',type).
no_internalrule(stepwise,41,'comment',type).
no_internalrule(stepwise,28,'comment',type).
no_internalrule(stepwise,2,'comment',type).
no_internalrule(stepwise,4,'comment',type).
no_internalrule(stepwise,13,'comment',type).
no_internalrule(stepwise,26,'comment',type).
no_internalrule(stepwise,9,'comment',type).
no_internalrule(stepwise,19,'comment',type).
no_internalrule(stepwise,39,'comment',type).
no_internalrule(stepwise,12,'comment',type).
no_internalrule(stepwise,67,'comment',type).
no_internalrule(stepwise,27,'comment',type).
no_internalrule(stepwise,20,'comment',type).
no_internalrule(stepwise,8,'comment',type).
no_internalrule(stepwise,25,'comment',type).
no_internalrule(stepwise,55,'comment',type).
no_internalrule(stepwise,10,'comment',type).
no_internalrule(stepwise,6,'comment',type).
no_internalrule(stepwise,18,'comment',type).
no_internalrule(stepwise,7,'comment',type).
no_internalrule(stepwise,11,'comment',type).
label(stepwise,'att',type,1).
no_internalrule(stepwise,3,'att',type).
no_internalrule(stepwise,1,'att',type).
no_internalrule(stepwise,29,'att',type).
no_internalrule(stepwise,38,'att',type).
no_internalrule(stepwise,5,'att',type).
no_internalrule(stepwise,41,'att',type).
no_internalrule(stepwise,28,'att',type).
no_internalrule(stepwise,2,'att',type).
no_internalrule(stepwise,4,'att',type).
no_internalrule(stepwise,13,'att',type).
no_internalrule(stepwise,26,'att',type).
no_internalrule(stepwise,9,'att',type).
no_internalrule(stepwise,19,'att',type).
no_internalrule(stepwise,39,'att',type).
no_internalrule(stepwise,12,'att',type).
no_internalrule(stepwise,67,'att',type).
no_internalrule(stepwise,27,'att',type).
no_internalrule(stepwise,20,'att',type).
no_internalrule(stepwise,8,'att',type).
no_internalrule(stepwise,25,'att',type).
no_internalrule(stepwise,55,'att',type).
no_internalrule(stepwise,10,'att',type).
no_internalrule(stepwise,6,'att',type).
no_internalrule(stepwise,18,'att',type).
no_internalrule(stepwise,7,'att',type).
no_internalrule(stepwise,11,'att',type).
label(stepwise,'elem',type,7).
no_internalrule(stepwise,3,'elem',type).
no_internalrule(stepwise,1,'elem',type).
no_internalrule(stepwise,29,'elem',type).
no_internalrule(stepwise,38,'elem',type).
no_internalrule(stepwise,5,'elem',type).
no_internalrule(stepwise,41,'elem',type).
no_internalrule(stepwise,28,'elem',type).
no_internalrule(stepwise,2,'elem',type).
no_internalrule(stepwise,4,'elem',type).
no_internalrule(stepwise,13,'elem',type).
no_internalrule(stepwise,26,'elem',type).
no_internalrule(stepwise,9,'elem',type).
no_internalrule(stepwise,19,'elem',type).
no_internalrule(stepwise,39,'elem',type).
no_internalrule(stepwise,12,'elem',type).
no_internalrule(stepwise,67,'elem',type).
no_internalrule(stepwise,27,'elem',type).
no_internalrule(stepwise,20,'elem',type).
no_internalrule(stepwise,8,'elem',type).
no_internalrule(stepwise,25,'elem',type).
no_internalrule(stepwise,55,'elem',type).
no_internalrule(stepwise,10,'elem',type).
no_internalrule(stepwise,6,'elem',type).
no_internalrule(stepwise,18,'elem',type).
no_internalrule(stepwise,7,'elem',type).
no_internalrule(stepwise,11,'elem',type).
label(stepwise,'macroRef',name,8).
no_internalrule(stepwise,3,'macroRef',name).
no_internalrule(stepwise,1,'macroRef',name).
no_internalrule(stepwise,29,'macroRef',name).
no_internalrule(stepwise,38,'macroRef',name).
no_internalrule(stepwise,5,'macroRef',name).
no_internalrule(stepwise,41,'macroRef',name).
no_internalrule(stepwise,28,'macroRef',name).
no_internalrule(stepwise,2,'macroRef',name).
no_internalrule(stepwise,4,'macroRef',name).
no_internalrule(stepwise,13,'macroRef',name).
no_internalrule(stepwise,26,'macroRef',name).
no_internalrule(stepwise,9,'macroRef',name).
no_internalrule(stepwise,19,'macroRef',name).
no_internalrule(stepwise,39,'macroRef',name).
no_internalrule(stepwise,12,'macroRef',name).
no_internalrule(stepwise,67,'macroRef',name).
no_internalrule(stepwise,27,'macroRef',name).
no_internalrule(stepwise,0,'macroRef',name).
no_internalrule(stepwise,20,'macroRef',name).
no_internalrule(stepwise,8,'macroRef',name).
no_internalrule(stepwise,25,'macroRef',name).
no_internalrule(stepwise,55,'macroRef',name).
no_internalrule(stepwise,10,'macroRef',name).
no_internalrule(stepwise,6,'macroRef',name).
no_internalrule(stepwise,18,'macroRef',name).
no_internalrule(stepwise,11,'macroRef',name).
label(stepwise,'ref',name,9).
no_internalrule(stepwise,3,'ref',name).
no_internalrule(stepwise,1,'ref',name).
no_internalrule(stepwise,29,'ref',name).
no_internalrule(stepwise,38,'ref',name).
no_internalrule(stepwise,5,'ref',name).
no_internalrule(stepwise,41,'ref',name).
no_internalrule(stepwise,28,'ref',name).
no_internalrule(stepwise,2,'ref',name).
no_internalrule(stepwise,4,'ref',name).
no_internalrule(stepwise,13,'ref',name).
no_internalrule(stepwise,26,'ref',name).
no_internalrule(stepwise,9,'ref',name).
no_internalrule(stepwise,19,'ref',name).
no_internalrule(stepwise,39,'ref',name).
no_internalrule(stepwise,12,'ref',name).
no_internalrule(stepwise,67,'ref',name).
no_internalrule(stepwise,27,'ref',name).
no_internalrule(stepwise,0,'ref',name).
no_internalrule(stepwise,20,'ref',name).
no_internalrule(stepwise,8,'ref',name).
no_internalrule(stepwise,25,'ref',name).
no_internalrule(stepwise,55,'ref',name).
no_internalrule(stepwise,10,'ref',name).
no_internalrule(stepwise,18,'ref',name).
no_internalrule(stepwise,7,'ref',name).
no_internalrule(stepwise,11,'ref',name).
label(stepwise,'rng',namespace,10).
no_internalrule(stepwise,3,'rng',namespace).
no_internalrule(stepwise,1,'rng',namespace).
no_internalrule(stepwise,29,'rng',namespace).
no_internalrule(stepwise,38,'rng',namespace).
no_internalrule(stepwise,5,'rng',namespace).
no_internalrule(stepwise,41,'rng',namespace).
no_internalrule(stepwise,28,'rng',namespace).
no_internalrule(stepwise,4,'rng',namespace).
no_internalrule(stepwise,13,'rng',namespace).
no_internalrule(stepwise,26,'rng',namespace).
no_internalrule(stepwise,9,'rng',namespace).
no_internalrule(stepwise,19,'rng',namespace).
no_internalrule(stepwise,39,'rng',namespace).
no_internalrule(stepwise,12,'rng',namespace).
no_internalrule(stepwise,67,'rng',namespace).
no_internalrule(stepwise,27,'rng',namespace).
no_internalrule(stepwise,0,'rng',namespace).
no_internalrule(stepwise,20,'rng',namespace).
no_internalrule(stepwise,8,'rng',namespace).
no_internalrule(stepwise,25,'rng',namespace).
no_internalrule(stepwise,55,'rng',namespace).
no_internalrule(stepwise,10,'rng',namespace).
no_internalrule(stepwise,6,'rng',namespace).
no_internalrule(stepwise,18,'rng',namespace).
no_internalrule(stepwise,7,'rng',namespace).
no_internalrule(stepwise,11,'rng',namespace).
label(stepwise,'x',var,14).
no_internalrule(stepwise,3,'x',var).
no_internalrule(stepwise,1,'x',var).
no_internalrule(stepwise,29,'x',var).
no_internalrule(stepwise,38,'x',var).
no_internalrule(stepwise,5,'x',var).
no_internalrule(stepwise,41,'x',var).
no_internalrule(stepwise,28,'x',var).
no_internalrule(stepwise,2,'x',var).
no_internalrule(stepwise,4,'x',var).
no_internalrule(stepwise,13,'x',var).
no_internalrule(stepwise,26,'x',var).
no_internalrule(stepwise,9,'x',var).
no_internalrule(stepwise,19,'x',var).
no_internalrule(stepwise,39,'x',var).
no_internalrule(stepwise,67,'x',var).
no_internalrule(stepwise,27,'x',var).
no_internalrule(stepwise,0,'x',var).
no_internalrule(stepwise,20,'x',var).
no_internalrule(stepwise,8,'x',var).
no_internalrule(stepwise,25,'x',var).
no_internalrule(stepwise,55,'x',var).
no_internalrule(stepwise,10,'x',var).
no_internalrule(stepwise,6,'x',var).
no_internalrule(stepwise,18,'x',var).
no_internalrule(stepwise,7,'x',var).
no_internalrule(stepwise,11,'x',var).
label(stepwise,'tei',namespace,11).
no_internalrule(stepwise,3,'tei',namespace).
no_internalrule(stepwise,1,'tei',namespace).
no_internalrule(stepwise,29,'tei',namespace).
no_internalrule(stepwise,38,'tei',namespace).
no_internalrule(stepwise,5,'tei',namespace).
no_internalrule(stepwise,41,'tei',namespace).
no_internalrule(stepwise,28,'tei',namespace).
no_internalrule(stepwise,4,'tei',namespace).
no_internalrule(stepwise,13,'tei',namespace).
no_internalrule(stepwise,26,'tei',namespace).
no_internalrule(stepwise,9,'tei',namespace).
no_internalrule(stepwise,19,'tei',namespace).
no_internalrule(stepwise,39,'tei',namespace).
no_internalrule(stepwise,12,'tei',namespace).
no_internalrule(stepwise,67,'tei',namespace).
no_internalrule(stepwise,27,'tei',namespace).
no_internalrule(stepwise,0,'tei',namespace).
no_internalrule(stepwise,20,'tei',namespace).
no_internalrule(stepwise,8,'tei',namespace).
no_internalrule(stepwise,25,'tei',namespace).
no_internalrule(stepwise,55,'tei',namespace).
no_internalrule(stepwise,10,'tei',namespace).
no_internalrule(stepwise,6,'tei',namespace).
no_internalrule(stepwise,18,'tei',namespace).
no_internalrule(stepwise,7,'tei',namespace).
no_internalrule(stepwise,11,'tei',namespace).
label(stepwise,'elementRef',name,6).
no_internalrule(stepwise,3,'elementRef',name).
no_internalrule(stepwise,1,'elementRef',name).
no_internalrule(stepwise,29,'elementRef',name).
no_internalrule(stepwise,38,'elementRef',name).
no_internalrule(stepwise,5,'elementRef',name).
no_internalrule(stepwise,41,'elementRef',name).
no_internalrule(stepwise,28,'elementRef',name).
no_internalrule(stepwise,2,'elementRef',name).
no_internalrule(stepwise,4,'elementRef',name).
no_internalrule(stepwise,13,'elementRef',name).
no_internalrule(stepwise,26,'elementRef',name).
no_internalrule(stepwise,9,'elementRef',name).
no_internalrule(stepwise,19,'elementRef',name).
no_internalrule(stepwise,39,'elementRef',name).
no_internalrule(stepwise,12,'elementRef',name).
no_internalrule(stepwise,67,'elementRef',name).
no_internalrule(stepwise,27,'elementRef',name).
no_internalrule(stepwise,0,'elementRef',name).
no_internalrule(stepwise,20,'elementRef',name).
no_internalrule(stepwise,8,'elementRef',name).
no_internalrule(stepwise,25,'elementRef',name).
no_internalrule(stepwise,55,'elementRef',name).
no_internalrule(stepwise,10,'elementRef',name).
no_internalrule(stepwise,6,'elementRef',name).
no_internalrule(stepwise,18,'elementRef',name).
no_internalrule(stepwise,11,'elementRef',name).
label(stepwise,'text',type,12).
no_internalrule(stepwise,3,'text',type).
no_internalrule(stepwise,1,'text',type).
no_internalrule(stepwise,29,'text',type).
no_internalrule(stepwise,38,'text',type).
no_internalrule(stepwise,5,'text',type).
no_internalrule(stepwise,41,'text',type).
no_internalrule(stepwise,28,'text',type).
no_internalrule(stepwise,2,'text',type).
no_internalrule(stepwise,4,'text',type).
no_internalrule(stepwise,13,'text',type).
no_internalrule(stepwise,26,'text',type).
no_internalrule(stepwise,9,'text',type).
no_internalrule(stepwise,19,'text',type).
no_internalrule(stepwise,39,'text',type).
no_internalrule(stepwise,12,'text',type).
no_internalrule(stepwise,67,'text',type).
no_internalrule(stepwise,27,'text',type).
no_internalrule(stepwise,20,'text',type).
no_internalrule(stepwise,8,'text',type).
no_internalrule(stepwise,25,'text',type).
no_internalrule(stepwise,55,'text',type).
no_internalrule(stepwise,10,'text',type).
no_internalrule(stepwise,6,'text',type).
no_internalrule(stepwise,18,'text',type).
no_internalrule(stepwise,7,'text',type).
no_internalrule(stepwise,11,'text',type).
label(stepwise,'dataRef',name,4).
no_internalrule(stepwise,3,'dataRef',name).
no_internalrule(stepwise,1,'dataRef',name).
no_internalrule(stepwise,29,'dataRef',name).
no_internalrule(stepwise,38,'dataRef',name).
no_internalrule(stepwise,5,'dataRef',name).
no_internalrule(stepwise,41,'dataRef',name).
no_internalrule(stepwise,28,'dataRef',name).
no_internalrule(stepwise,2,'dataRef',name).
no_internalrule(stepwise,4,'dataRef',name).
no_internalrule(stepwise,13,'dataRef',name).
no_internalrule(stepwise,26,'dataRef',name).
no_internalrule(stepwise,9,'dataRef',name).
no_internalrule(stepwise,19,'dataRef',name).
no_internalrule(stepwise,39,'dataRef',name).
no_internalrule(stepwise,12,'dataRef',name).
no_internalrule(stepwise,67,'dataRef',name).
no_internalrule(stepwise,27,'dataRef',name).
no_internalrule(stepwise,0,'dataRef',name).
no_internalrule(stepwise,20,'dataRef',name).
no_internalrule(stepwise,8,'dataRef',name).
no_internalrule(stepwise,25,'dataRef',name).
no_internalrule(stepwise,55,'dataRef',name).
no_internalrule(stepwise,10,'dataRef',name).
no_internalrule(stepwise,6,'dataRef',name).
no_internalrule(stepwise,18,'dataRef',name).
no_internalrule(stepwise,11,'dataRef',name).
label(stepwise,'doc',type,5).
no_internalrule(stepwise,3,'doc',type).
no_internalrule(stepwise,1,'doc',type).
no_internalrule(stepwise,29,'doc',type).
no_internalrule(stepwise,38,'doc',type).
no_internalrule(stepwise,5,'doc',type).
no_internalrule(stepwise,41,'doc',type).
no_internalrule(stepwise,28,'doc',type).
no_internalrule(stepwise,2,'doc',type).
no_internalrule(stepwise,4,'doc',type).
no_internalrule(stepwise,13,'doc',type).
no_internalrule(stepwise,26,'doc',type).
no_internalrule(stepwise,9,'doc',type).
no_internalrule(stepwise,19,'doc',type).
no_internalrule(stepwise,39,'doc',type).
no_internalrule(stepwise,12,'doc',type).
no_internalrule(stepwise,67,'doc',type).
no_internalrule(stepwise,27,'doc',type).
no_internalrule(stepwise,20,'doc',type).
no_internalrule(stepwise,8,'doc',type).
no_internalrule(stepwise,25,'doc',type).
no_internalrule(stepwise,55,'doc',type).
no_internalrule(stepwise,10,'doc',type).
no_internalrule(stepwise,6,'doc',type).
no_internalrule(stepwise,18,'doc',type).
no_internalrule(stepwise,7,'doc',type).
no_internalrule(stepwise,11,'doc',type).
label(stepwise,'x',negvar,13).
no_internalrule(stepwise,29,'x',negvar).
no_internalrule(stepwise,38,'x',negvar).
no_internalrule(stepwise,5,'x',negvar).
no_internalrule(stepwise,41,'x',negvar).
no_internalrule(stepwise,28,'x',negvar).
no_internalrule(stepwise,2,'x',negvar).
no_internalrule(stepwise,4,'x',negvar).
no_internalrule(stepwise,26,'x',negvar).
no_internalrule(stepwise,9,'x',negvar).
no_internalrule(stepwise,19,'x',negvar).
no_internalrule(stepwise,39,'x',negvar).
no_internalrule(stepwise,67,'x',negvar).
no_internalrule(stepwise,27,'x',negvar).
no_internalrule(stepwise,0,'x',negvar).
no_internalrule(stepwise,20,'x',negvar).
no_internalrule(stepwise,8,'x',negvar).
no_internalrule(stepwise,25,'x',negvar).
no_internalrule(stepwise,55,'x',negvar).
no_internalrule(stepwise,10,'x',negvar).
no_internalrule(stepwise,6,'x',negvar).
no_internalrule(stepwise,7,'x',negvar).
no_internalrule(stepwise,11,'x',negvar).
label(stepwise,'classRef',name,2).
no_internalrule(stepwise,3,'classRef',name).
no_internalrule(stepwise,1,'classRef',name).
no_internalrule(stepwise,29,'classRef',name).
no_internalrule(stepwise,38,'classRef',name).
no_internalrule(stepwise,5,'classRef',name).
no_internalrule(stepwise,41,'classRef',name).
no_internalrule(stepwise,28,'classRef',name).
no_internalrule(stepwise,2,'classRef',name).
no_internalrule(stepwise,4,'classRef',name).
no_internalrule(stepwise,13,'classRef',name).
no_internalrule(stepwise,26,'classRef',name).
no_internalrule(stepwise,9,'classRef',name).
no_internalrule(stepwise,19,'classRef',name).
no_internalrule(stepwise,39,'classRef',name).
no_internalrule(stepwise,12,'classRef',name).
no_internalrule(stepwise,67,'classRef',name).
no_internalrule(stepwise,27,'classRef',name).
no_internalrule(stepwise,0,'classRef',name).
no_internalrule(stepwise,20,'classRef',name).
no_internalrule(stepwise,8,'classRef',name).
no_internalrule(stepwise,25,'classRef',name).
no_internalrule(stepwise,55,'classRef',name).
no_internalrule(stepwise,10,'classRef',name).
no_internalrule(stepwise,6,'classRef',name).
no_internalrule(stepwise,18,'classRef',name).
no_internalrule(stepwise,11,'classRef',name).
max_label_id(stepwise,14).
    %%% different origins %%%%%%%
    
