diff_final(A,X,X1) :- final(A,X), nonfinal(A,X1), hedge_state(A,X1).
diff_type(A,X,X1)  :- hedge_state(A,X), tree_state(A,X1).
diff_sym(A,X1,X)   :- diff(A,X,X1).
diff_else(A,X,X1)  :- else(A,X), nonelse(A,X1), tree_state(A,X1).

internal(A,X,L,Type,Y)  :- internalrule(A,X,L,Type,Y).
internal(A,X,L,Type,Y)  :- no_internalrule(A,X,L,Type), elserule(A,X,Y).
internal(A,X,L,Type,Y)  :- no_internalrule(A,X,L,Type), typedelserule(A,X,Type,Y).
no_internal(A,X,L,Type) :- hedge_state(A,X), no_internalrule(A,X,L,Type),
                           no_elserule(A,X), no_typedelserule(A,X,Type).

apply(A,X,Y,Z)  :- applyrule(A,X,Y,Z).
apply(A,X,Y,Z)  :- applyelse(A,X,Y,Z).

applyelse(A,X,Y,Z)  :- applyelserule(A,X,Z), else(A,Y), no_applyrule(A,X,Y).
no_apply(A,X,Y)     :- no_applyrule(A,X,Y),
		       no_applyelserule_with(A,X,Y), tree_state(A,Y).
no_applyelserule_with(A,X,Y) :- no_applyelserule(A,X), tree_state(A,Y). 
no_applyelserule_with(A,X,Y) :- applyelserule(A,X,Z),
			        nonelse(A,Y), tree_state(Y).
no_applyelserule_with(A,X,Y) :- applyelserule(A,X,Z),
			        applyrule(A,X,Y,Z1).

type_lab(A,T) :- typedeleserule(A,X,T,Y).
type_lab(A,T) :- no_typedeleserule(A,X,T).
typedelserule(A,X,T,Y) :- elserule(A,X,Y), type_lab(A,T).

diff_typedelse(A,X,T,X1) :- typedelserule(A,X,T,Y), no_typedelserule(A,X1,T).
% self_diff_typedelse(X)   :- diff_typedelse(A,X,T,X).
diff(A,X,X1) :- diff_typedelse(A,X,T,X1).
diff(A,X,X1) :- diff_typedelserule(A,X,T,X1).

diff(A,X,X1)      :- diff_tree(A,X,X1).
% self_diff_tree(X) :- diff_tree(A,X,X).
diff(A,X,X1)      :- diff_type(A,X,X1).
% self_diff_type(X) :- diff_type(A,X,X).
diff(A,X,X1)      :- diff_final(A,X,X1).
% self_diff_final(X):- diff_final(A,X,X).
diff(A,X,X1)      :- diff_else(A,X,X1).
% self_diff_else(X) :- diff_else(A,X,X).

diff(A,X,X1) :- diff_treerule(A,X,X1).
diff(A,X,X1) :- diff_no_treerule(A,X,X1).
diff(A,X,X1) :- diff_apply(A,X,X1).
diff(A,X,X1) :- diff_no_apply(A,X,X1).
diff(A,X,X1) :- diff_internal(A,X,X1).

diff(A,X,X1) :- diff_no_internal(A,X,X1).
diff(A,X,X1) :- diff_nonelse(A,X,X1).
diff(A,X,X1) :- diff_origin(A,X,X1).
diff(A,X,X1) :- diff_sym(A,X,X1).
diff(A,X,X1) :- diff_elserule(A,X,X1).
diff(A,X,X1) :- diff_no_elserule(A,X,X1).
diff(A,X,X1) :- diff_no_typedelserule(A,X,X1).

diff_apply_from1(A,X,X1,Z,Z1)  :- apply(A,X,Y,Z), apply(A,X1,Y,Z1), diff(A,Z,Z1).
diff_apply_from2(A,Y,Y1,Z,Z1)  :- apply(A,X,Y,Z), apply(A,X,Y1,Z1), diff(A,Z,Z1).
% self_debug_diff_apply_from1(X,Y,Z,Z1) :- apply(A,X,Y,Z), apply(A,X,Y,Z1), diff(A,Z,Z1).
% self_debug_diff_apply_from2(Y,X,Z,Z1) :- apply(A,X,Y,Z), apply(A,X,Y,Z1), diff(A,Z,Z1).
diff_apply(A,X,X1)             :- diff_apply_from1(A,X,X1,Z,Z1).
diff_apply(A,X,X1)             :- diff_apply_from2(A,X,X1,Z,Z1).

diff_no_apply_from1(A,X,X1,Y,Z):- apply(A,X,Y,Z), no_apply(A,X1,Y).
diff_no_apply_from2(A,Y,Y1,X,Z):- apply(A,X,Y,Z), no_apply(A,X,Y1).
% self_debug_diff_no_apply_from1(X,Y,Z) :- apply(A,X,Y,Z), no_apply(A,X,Y).
% self_debug_diff_no_apply_from2(Y,X,Z) :- apply(A,X,Y,Z), no_apply(A,X,Y).
diff_no_apply(A,X,X1)          :- diff_no_apply_from1(A,X,X1,Y,Z).
diff_no_apply(A,X,X1)          :- diff_no_apply_from2(A,X,X1,Y,Z).

diff_treerule(A,X,X1)     :- treerule(A,X,Y), treerule(A,X1,Y1), diff(A,Y,Y1).
% self_debug_diff_treerule(X,Y,Y1) :- treerule(A,X,Y), treerule(A,X,Y1), diff(A,Y,Y1).
diff_no_treerule(A,X,X1)  :- treerule(A,X,Y), no_treerule(A,X1), hedge_state(A,X1).
% self_diff_no_treerule(X)  :-  treerule(A,X,Y), no_treerule(A,X), hedge_state(A,X).

diff_internal(A,X,X1) :- diff_internal_from(A,X,X1,L,Type,Y,Y1).
diff_internal_from(A,X,X1,L,Type,Y,Y1)
                      :- internal(A,X,L,Type,Y),
                         internal(A,X1,L,Type,Y1), diff(A,Y,Y1).
% self_debug_diff_internal_from(A,X,L,Type,Y,Y1):-diff_internal_from(A,X,X,L,Type,Y,Y1).
			 
diff_no_internal(A,X,X1) :- no_internal(A,X,L,Type), internal(A,X1,L,Type,Y1).
% self_debug_diff_no_internal(X,L,Type,Y1) :- no_internal(A,X,L,Type), internal(A,X,L,Type,Y1).

diff_elserule(A,X,X1) :- elserule(A,X,Y), elserule(A,X1,Y1), diff(A,Y,Y1).
% self_debug_diff_elserule(X,Y,Y1) :- elserule(A,X,Y), elserule(A,X,Y1), diff(A,Y,Y1).
diff_typedelserule(A,X,T,X1) :- typedelserule(A,X,T,Y), typedelserule(A,X1,T,Y1), diff(A,Y,Y1).
% self_diff_typedelserule(X)   :- typedelserule(A,X,T,Y), typedelserule(A,X,T,Y1),  diff(A,Y,Y1).
% self_debug_diff_typedelserule(X,Y,Y1) :- typedelserule(A,X,T,Y), typedelserule(A,X,T,Y1), diff(A,Y,Y1).

diff_no_elserule(A,X,X1) :- elserule(A,X,Y), no_elserule(A,X1).
diff_no_typedelserule(A,X,X1) :- typedelserule(A,X,T,Y), no_elserule(A,X1),no_typedelserule(A,X1,T).

% self_diff_no_elserule(X) :- diff_no_elserule(A,X,X).

% don't fusion states with different schema origins
%    (Joachim: should first diff_orig rule be switched on or off?)

% diff_origin(A,X1,X) :- origin_of(A,X1,Y1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).
% self_diff_origin(X) :- origin_of(A,XY1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).
% self_debug_diff_origin(X,Y1,Z1,Y,Z) :- origin_of(A,X,Y1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).

% for minimization replace rule(A,X,Y,Z) by rule(A,min(X),min(Y),min(Z)) 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule
% negative information (NEGATIVE=true)
%   no_applyrule
%   no_elserule
%   no_treerule
%   no_internalrule
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(step,'stepwise[]').
var(step,'x',1).
no_typedelserule(step,1,namespace).
no_typedelserule(step,2,namespace).
no_typedelserule(step,3,namespace).
no_typedelserule(step,4,namespace).
no_typedelserule(step,5,namespace).
no_typedelserule(step,6,namespace).
no_typedelserule(step,7,namespace).
no_typedelserule(step,8,namespace).
no_typedelserule(step,9,namespace).
no_typedelserule(step,10,namespace).
no_typedelserule(step,11,namespace).
no_typedelserule(step,12,namespace).
no_typedelserule(step,13,namespace).
no_typedelserule(step,14,namespace).
no_typedelserule(step,17,namespace).
no_typedelserule(step,18,namespace).
no_typedelserule(step,19,namespace).
no_typedelserule(step,22,namespace).
no_typedelserule(step,23,namespace).
no_typedelserule(step,24,namespace).
no_typedelserule(step,27,namespace).
no_typedelserule(step,28,namespace).
no_typedelserule(step,29,namespace).
no_typedelserule(step,31,namespace).
no_typedelserule(step,33,namespace).
no_typedelserule(step,36,namespace).
no_typedelserule(step,37,namespace).
no_typedelserule(step,38,namespace).
no_typedelserule(step,39,namespace).
no_typedelserule(step,40,namespace).
no_typedelserule(step,41,namespace).
no_typedelserule(step,1,type).
no_typedelserule(step,2,type).
no_typedelserule(step,3,type).
no_typedelserule(step,4,type).
no_typedelserule(step,5,type).
no_typedelserule(step,6,type).
no_typedelserule(step,7,type).
no_typedelserule(step,8,type).
no_typedelserule(step,9,type).
no_typedelserule(step,10,type).
no_typedelserule(step,11,type).
no_typedelserule(step,12,type).
no_typedelserule(step,13,type).
no_typedelserule(step,14,type).
no_typedelserule(step,17,type).
no_typedelserule(step,18,type).
no_typedelserule(step,19,type).
no_typedelserule(step,22,type).
no_typedelserule(step,23,type).
no_typedelserule(step,24,type).
no_typedelserule(step,27,type).
no_typedelserule(step,28,type).
no_typedelserule(step,29,type).
no_typedelserule(step,31,type).
no_typedelserule(step,33,type).
no_typedelserule(step,36,type).
no_typedelserule(step,37,type).
no_typedelserule(step,38,type).
no_typedelserule(step,39,type).
no_typedelserule(step,40,type).
no_typedelserule(step,41,type).
no_typedelserule(step,1,name).
no_typedelserule(step,2,name).
no_typedelserule(step,3,name).
no_typedelserule(step,4,name).
no_typedelserule(step,5,name).
no_typedelserule(step,6,name).
no_typedelserule(step,7,name).
no_typedelserule(step,8,name).
no_typedelserule(step,9,name).
no_typedelserule(step,10,name).
no_typedelserule(step,11,name).
no_typedelserule(step,12,name).
no_typedelserule(step,13,name).
no_typedelserule(step,14,name).
no_typedelserule(step,17,name).
no_typedelserule(step,18,name).
no_typedelserule(step,19,name).
no_typedelserule(step,22,name).
no_typedelserule(step,23,name).
no_typedelserule(step,24,name).
no_typedelserule(step,27,name).
no_typedelserule(step,28,name).
no_typedelserule(step,29,name).
no_typedelserule(step,31,name).
no_typedelserule(step,33,name).
no_typedelserule(step,36,name).
no_typedelserule(step,37,name).
no_typedelserule(step,38,name).
no_typedelserule(step,39,name).
no_typedelserule(step,40,name).
no_typedelserule(step,41,name).
no_typedelserule(step,1,var).
no_typedelserule(step,2,var).
no_typedelserule(step,3,var).
no_typedelserule(step,4,var).
no_typedelserule(step,5,var).
no_typedelserule(step,6,var).
no_typedelserule(step,7,var).
no_typedelserule(step,8,var).
no_typedelserule(step,9,var).
no_typedelserule(step,10,var).
no_typedelserule(step,11,var).
no_typedelserule(step,12,var).
no_typedelserule(step,13,var).
no_typedelserule(step,14,var).
no_typedelserule(step,17,var).
no_typedelserule(step,18,var).
no_typedelserule(step,19,var).
no_typedelserule(step,22,var).
no_typedelserule(step,23,var).
no_typedelserule(step,24,var).
no_typedelserule(step,27,var).
no_typedelserule(step,28,var).
no_typedelserule(step,29,var).
no_typedelserule(step,31,var).
no_typedelserule(step,33,var).
no_typedelserule(step,36,var).
no_typedelserule(step,37,var).
no_typedelserule(step,38,var).
no_typedelserule(step,39,var).
no_typedelserule(step,40,var).
no_typedelserule(step,41,var).
state(step,1).
state(step,2).
state(step,3).
state(step,4).
state(step,5).
state(step,6).
state(step,7).
state(step,8).
state(step,9).
state(step,10).
state(step,11).
state(step,12).
state(step,13).
state(step,14).
state(step,15).
state(step,16).
state(step,17).
state(step,18).
state(step,19).
state(step,20).
state(step,21).
state(step,22).
state(step,23).
state(step,24).
state(step,25).
state(step,26).
state(step,27).
state(step,28).
state(step,29).
state(step,30).
state(step,31).
state(step,32).
state(step,33).
state(step,34).
state(step,35).
state(step,36).
state(step,37).
state(step,38).
state(step,39).
state(step,40).
state(step,41).
start_tree(step,35).
initial(step,35).
origin_of(step,1,'7','3').
origin_of(step,2,'6','1').
origin_of(step,3,'4','1').
origin_of(step,4,'13','1').
origin_of(step,5,'11','1').
origin_of(step,6,'8','1').
origin_of(step,7,'14','1').
origin_of(step,8,'25','4').
origin_of(step,9,'9','2').
origin_of(step,10,'17','4').
origin_of(step,11,'12','4').
origin_of(step,12,'18','4').
origin_of(step,13,'24','4').
origin_of(step,14,'26','4').
origin_of(step,15,'7','1').
origin_of(step,16,'10','0').
origin_of(step,17,'31','1').
origin_of(step,18,'21','1').
origin_of(step,19,'32','1').
origin_of(step,20,'6','0').
origin_of(step,21,'4','0').
origin_of(step,22,'13','0').
origin_of(step,23,'19','1').
origin_of(step,24,'33','1').
origin_of(step,25,'5','0').
origin_of(step,26,'11','0').
origin_of(step,27,'22','1').
origin_of(step,28,'28','1').
origin_of(step,29,'16','1').
origin_of(step,30,'3','0').
origin_of(step,31,'8','0').
origin_of(step,32,'2','0').
origin_of(step,33,'20','1').
origin_of(step,34,'1','0').
origin_of(step,35,'0','0').
origin_of(step,36,'23','1').
origin_of(step,37,'15','1').
origin_of(step,38,'29','1').
origin_of(step,39,'30','1').
origin_of(step,40,'27','1').
origin_of(step,41,'14','0').
sink(step,1).
sink(step,2).
sink(step,3).
sink(step,4).
sink(step,5).
sink(step,6).
sink(step,7).
not_sink(step,8).
not_sink(step,9).
not_sink(step,10).
not_sink(step,11).
not_sink(step,12).
not_sink(step,13).
not_sink(step,14).
not_sink(step,15).
not_sink(step,16).
not_sink(step,17).
not_sink(step,18).
not_sink(step,19).
not_sink(step,20).
not_sink(step,21).
not_sink(step,22).
not_sink(step,23).
not_sink(step,24).
not_sink(step,25).
not_sink(step,26).
not_sink(step,27).
not_sink(step,28).
not_sink(step,29).
not_sink(step,30).
not_sink(step,31).
not_sink(step,32).
not_sink(step,33).
not_sink(step,34).
not_sink(step,35).
not_sink(step,36).
not_sink(step,37).
not_sink(step,38).
not_sink(step,39).
not_sink(step,40).
not_sink(step,41).
else(step,8).
else(step,9).
else(step,10).
else(step,11).
else(step,12).
else(step,13).
else(step,14).
nonelse(step,1).
nonelse(step,2).
nonelse(step,3).
nonelse(step,4).
nonelse(step,5).
nonelse(step,6).
nonelse(step,7).
nonelse(step,15).
nonelse(step,16).
nonelse(step,17).
nonelse(step,18).
nonelse(step,19).
nonelse(step,20).
nonelse(step,21).
nonelse(step,22).
nonelse(step,23).
nonelse(step,24).
nonelse(step,25).
nonelse(step,26).
nonelse(step,27).
nonelse(step,28).
nonelse(step,29).
nonelse(step,30).
nonelse(step,31).
nonelse(step,32).
nonelse(step,33).
nonelse(step,34).
nonelse(step,35).
nonelse(step,36).
nonelse(step,37).
nonelse(step,38).
nonelse(step,39).
nonelse(step,40).
nonelse(step,41).
final(step,24).
nonfinal(step,1).
nonfinal(step,2).
nonfinal(step,3).
nonfinal(step,4).
nonfinal(step,5).
nonfinal(step,6).
nonfinal(step,7).
nonfinal(step,8).
nonfinal(step,9).
nonfinal(step,10).
nonfinal(step,11).
nonfinal(step,12).
nonfinal(step,13).
nonfinal(step,14).
nonfinal(step,15).
nonfinal(step,16).
nonfinal(step,17).
nonfinal(step,18).
nonfinal(step,19).
nonfinal(step,20).
nonfinal(step,21).
nonfinal(step,22).
nonfinal(step,23).
nonfinal(step,25).
nonfinal(step,26).
nonfinal(step,27).
nonfinal(step,28).
nonfinal(step,29).
nonfinal(step,30).
nonfinal(step,31).
nonfinal(step,32).
nonfinal(step,33).
nonfinal(step,34).
nonfinal(step,35).
nonfinal(step,36).
nonfinal(step,37).
nonfinal(step,38).
nonfinal(step,39).
nonfinal(step,40).
nonfinal(step,41).
copy_of(step,8,8).
copy_of(step,9,9).
copy_of(step,10,10).
copy_of(step,11,11).
copy_of(step,12,12).
copy_of(step,13,13).
copy_of(step,14,14).
tree_state(step,8).
tree_istate(step,8,35).
 tree_state(step,9).
tree_istate(step,9,35).
 tree_state(step,10).
tree_istate(step,10,35).
 tree_state(step,11).
tree_istate(step,11,35).
 tree_state(step,12).
tree_istate(step,12,35).
 tree_state(step,13).
tree_istate(step,13,35).
 tree_state(step,14).
tree_istate(step,14,35).
 hedge_state(step,1).
    no_applyrule(step,1,8).
	  no_applyrule(step,1,9).
	  no_applyrule(step,1,10).
	  no_applyrule(step,1,11).
	  no_applyrule(step,1,12).
	  no_applyrule(step,1,13).
	  no_applyrule(step,1,14).
	  no_applyelserule(step,1).
no_elserule(step,1).
	% %
hedge_state(step,2).
    no_applyrule(step,2,8).
	  no_applyrule(step,2,9).
	  no_applyrule(step,2,10).
	  no_applyrule(step,2,11).
	  no_applyrule(step,2,12).
	  no_applyrule(step,2,13).
	  no_applyrule(step,2,14).
	  no_applyelserule(step,2).
no_elserule(step,2).
	% %
hedge_state(step,3).
    no_applyrule(step,3,8).
	  no_applyrule(step,3,9).
	  no_applyrule(step,3,10).
	  no_applyrule(step,3,11).
	  no_applyrule(step,3,12).
	  no_applyrule(step,3,13).
	  no_applyrule(step,3,14).
	  no_applyelserule(step,3).
no_elserule(step,3).
	% %
hedge_state(step,4).
    no_applyrule(step,4,8).
	  no_applyrule(step,4,9).
	  no_applyrule(step,4,10).
	  no_applyrule(step,4,11).
	  no_applyrule(step,4,12).
	  no_applyrule(step,4,13).
	  no_applyrule(step,4,14).
	  no_applyelserule(step,4).
no_elserule(step,4).
	% %
hedge_state(step,5).
    no_applyrule(step,5,8).
	  no_applyrule(step,5,9).
	  no_applyrule(step,5,10).
	  no_applyrule(step,5,11).
	  no_applyrule(step,5,12).
	  no_applyrule(step,5,13).
	  no_applyrule(step,5,14).
	  no_applyelserule(step,5).
no_elserule(step,5).
	% %
hedge_state(step,6).
    no_applyrule(step,6,8).
	  no_applyrule(step,6,9).
	  no_applyrule(step,6,10).
	  no_applyrule(step,6,11).
	  no_applyrule(step,6,12).
	  no_applyrule(step,6,13).
	  no_applyrule(step,6,14).
	  no_applyelserule(step,6).
no_elserule(step,6).
	% %
hedge_state(step,7).
    no_applyrule(step,7,8).
	  no_applyrule(step,7,9).
	  no_applyrule(step,7,10).
	  no_applyrule(step,7,11).
	  no_applyrule(step,7,12).
	  no_applyrule(step,7,13).
	  no_applyrule(step,7,14).
	  no_applyelserule(step,7).
no_elserule(step,7).
	% %
hedge_state(step,15).
    no_applyrule(step,15,8).
	  no_applyrule(step,15,9).
	  no_applyrule(step,15,10).
	  no_applyrule(step,15,11).
	  no_applyrule(step,15,12).
	  no_applyrule(step,15,13).
	  no_applyrule(step,15,14).
	  no_applyelserule(step,15).
hedge_state(step,16).
    no_applyrule(step,16,8).
	  no_applyrule(step,16,9).
	  no_applyrule(step,16,10).
	  no_applyrule(step,16,11).
	  no_applyrule(step,16,12).
	  no_applyrule(step,16,13).
	  no_applyrule(step,16,14).
	  no_applyelserule(step,16).
hedge_state(step,17).
    no_applyrule(step,17,8).
	  no_applyrule(step,17,10).
	  no_applyrule(step,17,11).
	  no_applyrule(step,17,12).
	  no_applyrule(step,17,13).
	  no_applyrule(step,17,14).
	  no_applyelserule(step,17).
no_elserule(step,17).
	% %
hedge_state(step,18).
    no_applyrule(step,18,8).
	  no_applyrule(step,18,9).
	  no_applyrule(step,18,10).
	  no_applyrule(step,18,11).
	  no_applyrule(step,18,12).
	  no_applyrule(step,18,13).
	  no_applyrule(step,18,14).
	  no_applyelserule(step,18).
no_elserule(step,18).
	% %
hedge_state(step,19).
    no_applyrule(step,19,8).
	  no_applyrule(step,19,9).
	  no_applyrule(step,19,10).
	  no_applyrule(step,19,11).
	  no_applyrule(step,19,12).
	  no_applyrule(step,19,13).
	  no_applyrule(step,19,14).
	  no_applyelserule(step,19).
no_elserule(step,19).
	% %
hedge_state(step,20).
    no_applyrule(step,20,8).
	  no_applyrule(step,20,9).
	  no_applyrule(step,20,10).
	  no_applyrule(step,20,11).
	  no_applyrule(step,20,12).
	  no_applyrule(step,20,13).
	  no_applyrule(step,20,14).
	  no_applyelserule(step,20).
hedge_state(step,21).
    no_applyrule(step,21,8).
	  no_applyrule(step,21,10).
	  no_applyrule(step,21,11).
	  no_applyrule(step,21,12).
	  no_applyrule(step,21,13).
	  no_applyrule(step,21,14).
	  no_applyelserule(step,21).
hedge_state(step,22).
    no_applyrule(step,22,14).
	  no_applyelserule(step,22).
no_elserule(step,22).
	% %
hedge_state(step,23).
    no_applyrule(step,23,8).
	  no_applyrule(step,23,10).
	  no_applyrule(step,23,11).
	  no_applyrule(step,23,12).
	  no_applyrule(step,23,13).
	  no_applyrule(step,23,14).
	  no_applyelserule(step,23).
no_elserule(step,23).
	% %
hedge_state(step,24).
    no_applyrule(step,24,8).
	  no_applyrule(step,24,9).
	  no_applyrule(step,24,10).
	  no_applyrule(step,24,11).
	  no_applyrule(step,24,12).
	  no_applyrule(step,24,13).
	  no_applyrule(step,24,14).
	  no_applyelserule(step,24).
no_elserule(step,24).
	% %
hedge_state(step,25).
    no_applyrule(step,25,8).
	  no_applyrule(step,25,9).
	  no_applyrule(step,25,10).
	  no_applyrule(step,25,11).
	  no_applyrule(step,25,12).
	  no_applyrule(step,25,13).
	  no_applyrule(step,25,14).
	  no_applyelserule(step,25).
hedge_state(step,26).
    no_applyrule(step,26,8).
	  no_applyrule(step,26,9).
	  no_applyrule(step,26,10).
	  no_applyrule(step,26,11).
	  no_applyrule(step,26,12).
	  no_applyrule(step,26,13).
	  no_applyrule(step,26,14).
	  no_applyelserule(step,26).
hedge_state(step,27).
    no_applyrule(step,27,8).
	  no_applyrule(step,27,10).
	  no_applyrule(step,27,11).
	  no_applyrule(step,27,12).
	  no_applyrule(step,27,13).
	  no_applyrule(step,27,14).
	  no_applyelserule(step,27).
no_elserule(step,27).
	% %
hedge_state(step,28).
    no_applyrule(step,28,8).
	  no_applyrule(step,28,10).
	  no_applyrule(step,28,11).
	  no_applyrule(step,28,12).
	  no_applyrule(step,28,13).
	  no_applyrule(step,28,14).
	  no_applyelserule(step,28).
no_elserule(step,28).
	% %
hedge_state(step,29).
    no_applyrule(step,29,8).
	  no_applyrule(step,29,10).
	  no_applyrule(step,29,11).
	  no_applyrule(step,29,12).
	  no_applyrule(step,29,13).
	  no_applyrule(step,29,14).
	  no_applyelserule(step,29).
no_elserule(step,29).
	% %
hedge_state(step,30).
    no_applyrule(step,30,8).
	  no_applyrule(step,30,9).
	  no_applyrule(step,30,10).
	  no_applyrule(step,30,11).
	  no_applyrule(step,30,12).
	  no_applyrule(step,30,13).
	  no_applyrule(step,30,14).
	  no_applyelserule(step,30).
hedge_state(step,31).
    no_applyrule(step,31,11).
	  no_applyrule(step,31,12).
	  no_applyrule(step,31,14).
	  no_applyelserule(step,31).
no_elserule(step,31).
	% %
hedge_state(step,32).
    no_applyrule(step,32,8).
	  no_applyrule(step,32,9).
	  no_applyrule(step,32,10).
	  no_applyrule(step,32,11).
	  no_applyrule(step,32,12).
	  no_applyrule(step,32,13).
	  no_applyrule(step,32,14).
	  no_applyelserule(step,32).
hedge_state(step,33).
    no_applyrule(step,33,8).
	  no_applyrule(step,33,10).
	  no_applyrule(step,33,11).
	  no_applyrule(step,33,12).
	  no_applyrule(step,33,13).
	  no_applyrule(step,33,14).
	  no_applyelserule(step,33).
no_elserule(step,33).
	% %
hedge_state(step,34).
    no_applyrule(step,34,8).
	  no_applyrule(step,34,9).
	  no_applyrule(step,34,10).
	  no_applyrule(step,34,11).
	  no_applyrule(step,34,12).
	  no_applyrule(step,34,13).
	  no_applyrule(step,34,14).
	  no_applyelserule(step,34).
hedge_state(step,35).
    no_applyrule(step,35,8).
	  no_applyrule(step,35,10).
	  no_applyrule(step,35,11).
	  no_applyrule(step,35,12).
	  no_applyrule(step,35,13).
	  no_applyelserule(step,35).
hedge_state(step,36).
    no_applyrule(step,36,8).
	  no_applyrule(step,36,10).
	  no_applyrule(step,36,11).
	  no_applyrule(step,36,12).
	  no_applyrule(step,36,13).
	  no_applyrule(step,36,14).
	  no_applyelserule(step,36).
no_elserule(step,36).
	% %
hedge_state(step,37).
    no_applyrule(step,37,8).
	  no_applyrule(step,37,10).
	  no_applyrule(step,37,11).
	  no_applyrule(step,37,12).
	  no_applyrule(step,37,13).
	  no_applyrule(step,37,14).
	  no_applyelserule(step,37).
no_elserule(step,37).
	% %
hedge_state(step,38).
    no_applyrule(step,38,8).
	  no_applyrule(step,38,9).
	  no_applyrule(step,38,10).
	  no_applyrule(step,38,11).
	  no_applyrule(step,38,12).
	  no_applyrule(step,38,13).
	  no_applyrule(step,38,14).
	  no_applyelserule(step,38).
no_elserule(step,38).
	% %
hedge_state(step,39).
    no_applyrule(step,39,8).
	  no_applyrule(step,39,10).
	  no_applyrule(step,39,11).
	  no_applyrule(step,39,12).
	  no_applyrule(step,39,13).
	  no_applyrule(step,39,14).
	  no_applyelserule(step,39).
no_elserule(step,39).
	% %
hedge_state(step,40).
    no_applyrule(step,40,8).
	  no_applyrule(step,40,10).
	  no_applyrule(step,40,11).
	  no_applyrule(step,40,12).
	  no_applyrule(step,40,13).
	  no_applyrule(step,40,14).
	  no_applyelserule(step,40).
no_elserule(step,40).
	% %
hedge_state(step,41).
    no_applyrule(step,41,14).
	  no_applyelserule(step,41).
no_elserule(step,41).
	% %
applyrule(step,17,9,17).
applyrule(step,21,9,21).
applyrule(step,23,9,23).
applyrule(step,22,8,39).
applyrule(step,22,9,22).
applyrule(step,22,10,23).
applyrule(step,22,11,37).
applyrule(step,22,12,27).
applyrule(step,22,13,40).
applyrule(step,27,9,27).
applyrule(step,28,9,28).
applyrule(step,29,9,29).
applyrule(step,31,8,19).
applyrule(step,31,9,21).
applyrule(step,31,10,18).
applyrule(step,31,13,38).
applyrule(step,33,9,33).
applyrule(step,35,9,21).
applyrule(step,35,14,24).
applyrule(step,36,9,36).
applyrule(step,37,9,37).
applyrule(step,39,9,39).
applyrule(step,40,9,40).
applyrule(step,41,8,17).
applyrule(step,41,9,41).
applyrule(step,41,10,33).
applyrule(step,41,11,29).
applyrule(step,41,12,36).
applyrule(step,41,13,28).
elserule(step,15,15).
elserule(step,16,22).
elserule(step,20,26).
elserule(step,21,21).
elserule(step,25,26).
elserule(step,26,41).
elserule(step,30,31).
elserule(step,32,21).
elserule(step,34,20).
elserule(step,35,21).
treerule(step,15,11).
treerule(step,17,8).
treerule(step,18,14).
treerule(step,19,14).
treerule(step,21,9).
treerule(step,22,9).
treerule(step,23,13).
treerule(step,27,10).
treerule(step,28,8).
treerule(step,29,12).
treerule(step,33,8).
treerule(step,36,12).
treerule(step,37,10).
treerule(step,38,14).
treerule(step,39,13).
treerule(step,40,13).
treerule(step,41,9).
internalrule(step,15,'x',var,1).
internalrule(step,16,'x',var,4).
internalrule(step,20,'x',var,5).
internalrule(step,21,'x',var,3).
internalrule(step,25,'x',var,5).
internalrule(step,26,'x',var,7).
internalrule(step,30,'x',var,6).
internalrule(step,34,'x',var,2).
internalrule(step,35,'x',var,3).
internalrule(step,25,'refentry',name,16).
internalrule(step,34,'default',namespace,25).
internalrule(step,35,'text',type,32).
internalrule(step,35,'elem',type,34).
internalrule(step,35,'doc',type,30).
internalrule(step,32,'x',var,15).
no_elserule(step,1).
no_elserule(step,2).
no_elserule(step,3).
no_elserule(step,4).
no_elserule(step,5).
no_elserule(step,6).
no_elserule(step,7).
no_elserule(step,8).
no_elserule(step,9).
no_elserule(step,10).
no_elserule(step,11).
no_elserule(step,12).
no_elserule(step,13).
no_elserule(step,14).
no_elserule(step,17).
no_elserule(step,18).
no_elserule(step,19).
no_elserule(step,22).
no_elserule(step,23).
no_elserule(step,24).
no_elserule(step,27).
no_elserule(step,28).
no_elserule(step,29).
no_elserule(step,31).
no_elserule(step,33).
no_elserule(step,36).
no_elserule(step,37).
no_elserule(step,38).
no_elserule(step,39).
no_elserule(step,40).
no_elserule(step,41).
no_treerule(step,1).
no_treerule(step,2).
no_treerule(step,3).
no_treerule(step,4).
no_treerule(step,5).
no_treerule(step,6).
no_treerule(step,7).
no_treerule(step,8).
no_treerule(step,9).
no_treerule(step,10).
no_treerule(step,11).
no_treerule(step,12).
no_treerule(step,13).
no_treerule(step,14).
no_treerule(step,16).
no_treerule(step,20).
no_treerule(step,24).
no_treerule(step,25).
no_treerule(step,26).
no_treerule(step,30).
no_treerule(step,31).
no_treerule(step,32).
no_treerule(step,34).
no_treerule(step,35).
label(step,'default',namespace,1).
no_internalrule(step,1,'default',namespace).
no_internalrule(step,2,'default',namespace).
no_internalrule(step,3,'default',namespace).
no_internalrule(step,4,'default',namespace).
no_internalrule(step,5,'default',namespace).
no_internalrule(step,6,'default',namespace).
no_internalrule(step,7,'default',namespace).
no_internalrule(step,8,'default',namespace).
no_internalrule(step,9,'default',namespace).
no_internalrule(step,10,'default',namespace).
no_internalrule(step,11,'default',namespace).
no_internalrule(step,12,'default',namespace).
no_internalrule(step,13,'default',namespace).
no_internalrule(step,14,'default',namespace).
no_internalrule(step,15,'default',namespace).
no_internalrule(step,16,'default',namespace).
no_internalrule(step,17,'default',namespace).
no_internalrule(step,18,'default',namespace).
no_internalrule(step,19,'default',namespace).
no_internalrule(step,20,'default',namespace).
no_internalrule(step,21,'default',namespace).
no_internalrule(step,22,'default',namespace).
no_internalrule(step,23,'default',namespace).
no_internalrule(step,24,'default',namespace).
no_internalrule(step,25,'default',namespace).
no_internalrule(step,26,'default',namespace).
no_internalrule(step,27,'default',namespace).
no_internalrule(step,28,'default',namespace).
no_internalrule(step,29,'default',namespace).
no_internalrule(step,30,'default',namespace).
no_internalrule(step,31,'default',namespace).
no_internalrule(step,32,'default',namespace).
no_internalrule(step,33,'default',namespace).
no_internalrule(step,35,'default',namespace).
no_internalrule(step,36,'default',namespace).
no_internalrule(step,37,'default',namespace).
no_internalrule(step,38,'default',namespace).
no_internalrule(step,39,'default',namespace).
no_internalrule(step,40,'default',namespace).
no_internalrule(step,41,'default',namespace).
label(step,'doc',type,2).
no_internalrule(step,1,'doc',type).
no_internalrule(step,2,'doc',type).
no_internalrule(step,3,'doc',type).
no_internalrule(step,4,'doc',type).
no_internalrule(step,5,'doc',type).
no_internalrule(step,6,'doc',type).
no_internalrule(step,7,'doc',type).
no_internalrule(step,8,'doc',type).
no_internalrule(step,9,'doc',type).
no_internalrule(step,10,'doc',type).
no_internalrule(step,11,'doc',type).
no_internalrule(step,12,'doc',type).
no_internalrule(step,13,'doc',type).
no_internalrule(step,14,'doc',type).
no_internalrule(step,15,'doc',type).
no_internalrule(step,16,'doc',type).
no_internalrule(step,17,'doc',type).
no_internalrule(step,18,'doc',type).
no_internalrule(step,19,'doc',type).
no_internalrule(step,20,'doc',type).
no_internalrule(step,21,'doc',type).
no_internalrule(step,22,'doc',type).
no_internalrule(step,23,'doc',type).
no_internalrule(step,24,'doc',type).
no_internalrule(step,25,'doc',type).
no_internalrule(step,26,'doc',type).
no_internalrule(step,27,'doc',type).
no_internalrule(step,28,'doc',type).
no_internalrule(step,29,'doc',type).
no_internalrule(step,30,'doc',type).
no_internalrule(step,31,'doc',type).
no_internalrule(step,32,'doc',type).
no_internalrule(step,33,'doc',type).
no_internalrule(step,34,'doc',type).
no_internalrule(step,36,'doc',type).
no_internalrule(step,37,'doc',type).
no_internalrule(step,38,'doc',type).
no_internalrule(step,39,'doc',type).
no_internalrule(step,40,'doc',type).
no_internalrule(step,41,'doc',type).
label(step,'elem',type,3).
no_internalrule(step,1,'elem',type).
no_internalrule(step,2,'elem',type).
no_internalrule(step,3,'elem',type).
no_internalrule(step,4,'elem',type).
no_internalrule(step,5,'elem',type).
no_internalrule(step,6,'elem',type).
no_internalrule(step,7,'elem',type).
no_internalrule(step,8,'elem',type).
no_internalrule(step,9,'elem',type).
no_internalrule(step,10,'elem',type).
no_internalrule(step,11,'elem',type).
no_internalrule(step,12,'elem',type).
no_internalrule(step,13,'elem',type).
no_internalrule(step,14,'elem',type).
no_internalrule(step,15,'elem',type).
no_internalrule(step,16,'elem',type).
no_internalrule(step,17,'elem',type).
no_internalrule(step,18,'elem',type).
no_internalrule(step,19,'elem',type).
no_internalrule(step,20,'elem',type).
no_internalrule(step,21,'elem',type).
no_internalrule(step,22,'elem',type).
no_internalrule(step,23,'elem',type).
no_internalrule(step,24,'elem',type).
no_internalrule(step,25,'elem',type).
no_internalrule(step,26,'elem',type).
no_internalrule(step,27,'elem',type).
no_internalrule(step,28,'elem',type).
no_internalrule(step,29,'elem',type).
no_internalrule(step,30,'elem',type).
no_internalrule(step,31,'elem',type).
no_internalrule(step,32,'elem',type).
no_internalrule(step,33,'elem',type).
no_internalrule(step,34,'elem',type).
no_internalrule(step,36,'elem',type).
no_internalrule(step,37,'elem',type).
no_internalrule(step,38,'elem',type).
no_internalrule(step,39,'elem',type).
no_internalrule(step,40,'elem',type).
no_internalrule(step,41,'elem',type).
label(step,'refentry',name,4).
no_internalrule(step,1,'refentry',name).
no_internalrule(step,2,'refentry',name).
no_internalrule(step,3,'refentry',name).
no_internalrule(step,4,'refentry',name).
no_internalrule(step,5,'refentry',name).
no_internalrule(step,6,'refentry',name).
no_internalrule(step,7,'refentry',name).
no_internalrule(step,8,'refentry',name).
no_internalrule(step,9,'refentry',name).
no_internalrule(step,10,'refentry',name).
no_internalrule(step,11,'refentry',name).
no_internalrule(step,12,'refentry',name).
no_internalrule(step,13,'refentry',name).
no_internalrule(step,14,'refentry',name).
no_internalrule(step,15,'refentry',name).
no_internalrule(step,16,'refentry',name).
no_internalrule(step,17,'refentry',name).
no_internalrule(step,18,'refentry',name).
no_internalrule(step,19,'refentry',name).
no_internalrule(step,20,'refentry',name).
no_internalrule(step,21,'refentry',name).
no_internalrule(step,22,'refentry',name).
no_internalrule(step,23,'refentry',name).
no_internalrule(step,24,'refentry',name).
no_internalrule(step,26,'refentry',name).
no_internalrule(step,27,'refentry',name).
no_internalrule(step,28,'refentry',name).
no_internalrule(step,29,'refentry',name).
no_internalrule(step,30,'refentry',name).
no_internalrule(step,31,'refentry',name).
no_internalrule(step,32,'refentry',name).
no_internalrule(step,33,'refentry',name).
no_internalrule(step,34,'refentry',name).
no_internalrule(step,35,'refentry',name).
no_internalrule(step,36,'refentry',name).
no_internalrule(step,37,'refentry',name).
no_internalrule(step,38,'refentry',name).
no_internalrule(step,39,'refentry',name).
no_internalrule(step,40,'refentry',name).
no_internalrule(step,41,'refentry',name).
label(step,'text',type,5).
no_internalrule(step,1,'text',type).
no_internalrule(step,2,'text',type).
no_internalrule(step,3,'text',type).
no_internalrule(step,4,'text',type).
no_internalrule(step,5,'text',type).
no_internalrule(step,6,'text',type).
no_internalrule(step,7,'text',type).
no_internalrule(step,8,'text',type).
no_internalrule(step,9,'text',type).
no_internalrule(step,10,'text',type).
no_internalrule(step,11,'text',type).
no_internalrule(step,12,'text',type).
no_internalrule(step,13,'text',type).
no_internalrule(step,14,'text',type).
no_internalrule(step,15,'text',type).
no_internalrule(step,16,'text',type).
no_internalrule(step,17,'text',type).
no_internalrule(step,18,'text',type).
no_internalrule(step,19,'text',type).
no_internalrule(step,20,'text',type).
no_internalrule(step,21,'text',type).
no_internalrule(step,22,'text',type).
no_internalrule(step,23,'text',type).
no_internalrule(step,24,'text',type).
no_internalrule(step,25,'text',type).
no_internalrule(step,26,'text',type).
no_internalrule(step,27,'text',type).
no_internalrule(step,28,'text',type).
no_internalrule(step,29,'text',type).
no_internalrule(step,30,'text',type).
no_internalrule(step,31,'text',type).
no_internalrule(step,32,'text',type).
no_internalrule(step,33,'text',type).
no_internalrule(step,34,'text',type).
no_internalrule(step,36,'text',type).
no_internalrule(step,37,'text',type).
no_internalrule(step,38,'text',type).
no_internalrule(step,39,'text',type).
no_internalrule(step,40,'text',type).
no_internalrule(step,41,'text',type).
label(step,'x',var,6).
no_internalrule(step,1,'x',var).
no_internalrule(step,2,'x',var).
no_internalrule(step,3,'x',var).
no_internalrule(step,4,'x',var).
no_internalrule(step,5,'x',var).
no_internalrule(step,6,'x',var).
no_internalrule(step,7,'x',var).
no_internalrule(step,8,'x',var).
no_internalrule(step,9,'x',var).
no_internalrule(step,10,'x',var).
no_internalrule(step,11,'x',var).
no_internalrule(step,12,'x',var).
no_internalrule(step,13,'x',var).
no_internalrule(step,14,'x',var).
no_internalrule(step,17,'x',var).
no_internalrule(step,18,'x',var).
no_internalrule(step,19,'x',var).
no_internalrule(step,22,'x',var).
no_internalrule(step,23,'x',var).
no_internalrule(step,24,'x',var).
no_internalrule(step,27,'x',var).
no_internalrule(step,28,'x',var).
no_internalrule(step,29,'x',var).
no_internalrule(step,31,'x',var).
no_internalrule(step,33,'x',var).
no_internalrule(step,36,'x',var).
no_internalrule(step,37,'x',var).
no_internalrule(step,38,'x',var).
no_internalrule(step,39,'x',var).
no_internalrule(step,40,'x',var).
no_internalrule(step,41,'x',var).
max_label_id(step,6).
    %%% different origins %%%%%%%
    diff_orig(step,1,0).
	  diff_orig(step,2,0).
	  diff_orig(step,2,1).
	  diff_orig(step,3,0).
	  diff_orig(step,3,1).
	  diff_orig(step,3,2).
	  diff_orig(step,4,0).
	  diff_orig(step,4,1).
	  diff_orig(step,4,2).
	  diff_orig(step,4,3).
	  
