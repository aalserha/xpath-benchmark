diff_final(A,X,X1) :- final(A,X), nonfinal(A,X1), hedge_state(A,X1).
diff_type(A,X,X1)  :- hedge_state(A,X), tree_state(A,X1).
diff_sym(A,X1,X)   :- diff(A,X,X1).
diff_else(A,X,X1)  :- else(A,X), nonelse(A,X1), tree_state(A,X1).

internal(A,X,L,Type,Y)  :- internalrule(A,X,L,Type,Y).
internal(A,X,L,Type,Y)  :- no_internalrule(A,X,L,Type), elserule(A,X,Y).
internal(A,X,L,Type,Y)  :- no_internalrule(A,X,L,Type), typedelserule(A,X,Type,Y).
no_internal(A,X,L,Type) :- hedge_state(A,X), no_internalrule(A,X,L,Type),
                           no_elserule(A,X), no_typedelserule(A,X,Type).

apply(A,X,Y,Z)  :- applyrule(A,X,Y,Z).
apply(A,X,Y,Z)  :- applyelse(A,X,Y,Z).

applyelse(A,X,Y,Z)  :- applyelserule(A,X,Z), else(A,Y), no_applyrule(A,X,Y).
no_apply(A,X,Y)     :- no_applyrule(A,X,Y),
		       no_applyelserule_with(A,X,Y), tree_state(A,Y).
no_applyelserule_with(A,X,Y) :- no_applyelserule(A,X), tree_state(A,Y). 
no_applyelserule_with(A,X,Y) :- applyelserule(A,X,Z),
			        nonelse(A,Y), tree_state(Y).
no_applyelserule_with(A,X,Y) :- applyelserule(A,X,Z),
			        applyrule(A,X,Y,Z1).

type_lab(A,T) :- typedeleserule(A,X,T,Y).
type_lab(A,T) :- no_typedeleserule(A,X,T).
typedelserule(A,X,T,Y) :- elserule(A,X,Y), type_lab(A,T).

diff_typedelse(A,X,T,X1) :- typedelserule(A,X,T,Y), no_typedelserule(A,X1,T).
% self_diff_typedelse(X)   :- diff_typedelse(A,X,T,X).
diff(A,X,X1) :- diff_typedelse(A,X,T,X1).
diff(A,X,X1) :- diff_typedelserule(A,X,T,X1).

diff(A,X,X1)      :- diff_tree(A,X,X1).
% self_diff_tree(X) :- diff_tree(A,X,X).
diff(A,X,X1)      :- diff_type(A,X,X1).
% self_diff_type(X) :- diff_type(A,X,X).
diff(A,X,X1)      :- diff_final(A,X,X1).
% self_diff_final(X):- diff_final(A,X,X).
diff(A,X,X1)      :- diff_else(A,X,X1).
% self_diff_else(X) :- diff_else(A,X,X).

diff(A,X,X1) :- diff_treerule(A,X,X1).
diff(A,X,X1) :- diff_no_treerule(A,X,X1).
diff(A,X,X1) :- diff_apply(A,X,X1).
diff(A,X,X1) :- diff_no_apply(A,X,X1).
diff(A,X,X1) :- diff_internal(A,X,X1).

diff(A,X,X1) :- diff_no_internal(A,X,X1).
diff(A,X,X1) :- diff_nonelse(A,X,X1).
diff(A,X,X1) :- diff_origin(A,X,X1).
diff(A,X,X1) :- diff_sym(A,X,X1).
diff(A,X,X1) :- diff_elserule(A,X,X1).
diff(A,X,X1) :- diff_no_elserule(A,X,X1).
diff(A,X,X1) :- diff_no_typedelserule(A,X,X1).

diff_apply_from1(A,X,X1,Z,Z1)  :- apply(A,X,Y,Z), apply(A,X1,Y,Z1), diff(A,Z,Z1).
diff_apply_from2(A,Y,Y1,Z,Z1)  :- apply(A,X,Y,Z), apply(A,X,Y1,Z1), diff(A,Z,Z1).
% self_debug_diff_apply_from1(X,Y,Z,Z1) :- apply(A,X,Y,Z), apply(A,X,Y,Z1), diff(A,Z,Z1).
% self_debug_diff_apply_from2(Y,X,Z,Z1) :- apply(A,X,Y,Z), apply(A,X,Y,Z1), diff(A,Z,Z1).
diff_apply(A,X,X1)             :- diff_apply_from1(A,X,X1,Z,Z1).
diff_apply(A,X,X1)             :- diff_apply_from2(A,X,X1,Z,Z1).

diff_no_apply_from1(A,X,X1,Y,Z):- apply(A,X,Y,Z), no_apply(A,X1,Y).
diff_no_apply_from2(A,Y,Y1,X,Z):- apply(A,X,Y,Z), no_apply(A,X,Y1).
% self_debug_diff_no_apply_from1(X,Y,Z) :- apply(A,X,Y,Z), no_apply(A,X,Y).
% self_debug_diff_no_apply_from2(Y,X,Z) :- apply(A,X,Y,Z), no_apply(A,X,Y).
diff_no_apply(A,X,X1)          :- diff_no_apply_from1(A,X,X1,Y,Z).
diff_no_apply(A,X,X1)          :- diff_no_apply_from2(A,X,X1,Y,Z).

diff_treerule(A,X,X1)     :- treerule(A,X,Y), treerule(A,X1,Y1), diff(A,Y,Y1).
% self_debug_diff_treerule(X,Y,Y1) :- treerule(A,X,Y), treerule(A,X,Y1), diff(A,Y,Y1).
diff_no_treerule(A,X,X1)  :- treerule(A,X,Y), no_treerule(A,X1), hedge_state(A,X1).
% self_diff_no_treerule(X)  :-  treerule(A,X,Y), no_treerule(A,X), hedge_state(A,X).

diff_internal(A,X,X1) :- diff_internal_from(A,X,X1,L,Type,Y,Y1).
diff_internal_from(A,X,X1,L,Type,Y,Y1)
                      :- internal(A,X,L,Type,Y),
                         internal(A,X1,L,Type,Y1), diff(A,Y,Y1).
% self_debug_diff_internal_from(A,X,L,Type,Y,Y1):-diff_internal_from(A,X,X,L,Type,Y,Y1).
			 
diff_no_internal(A,X,X1) :- no_internal(A,X,L,Type), internal(A,X1,L,Type,Y1).
% self_debug_diff_no_internal(X,L,Type,Y1) :- no_internal(A,X,L,Type), internal(A,X,L,Type,Y1).

diff_elserule(A,X,X1) :- elserule(A,X,Y), elserule(A,X1,Y1), diff(A,Y,Y1).
% self_debug_diff_elserule(X,Y,Y1) :- elserule(A,X,Y), elserule(A,X,Y1), diff(A,Y,Y1).
diff_typedelserule(A,X,T,X1) :- typedelserule(A,X,T,Y), typedelserule(A,X1,T,Y1), diff(A,Y,Y1).
% self_diff_typedelserule(X)   :- typedelserule(A,X,T,Y), typedelserule(A,X,T,Y1),  diff(A,Y,Y1).
% self_debug_diff_typedelserule(X,Y,Y1) :- typedelserule(A,X,T,Y), typedelserule(A,X,T,Y1), diff(A,Y,Y1).

diff_no_elserule(A,X,X1) :- elserule(A,X,Y), no_elserule(A,X1).
diff_no_typedelserule(A,X,X1) :- typedelserule(A,X,T,Y), no_elserule(A,X1),no_typedelserule(A,X1,T).

% self_diff_no_elserule(X) :- diff_no_elserule(A,X,X).

% don't fusion states with different schema origins
%    (Joachim: should first diff_orig rule be switched on or off?)

% diff_origin(A,X1,X) :- origin_of(A,X1,Y1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).
% self_diff_origin(X) :- origin_of(A,XY1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).
% self_debug_diff_origin(X,Y1,Z1,Y,Z) :- origin_of(A,X,Y1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).

% for minimization replace rule(A,X,Y,Z) by rule(A,min(X),min(Y),min(Z)) 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule
% negative information (NEGATIVE=true)
%   no_applyrule
%   no_elserule
%   no_treerule
%   no_internalrule
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(step,'schema-clean[det_Ttyped[schema-clean[pc[step[(μx_d1.(&amp;lt;elem⋅tei⋅dataRef⋅x⋅T⋅&amp;lt;att⋅default⋅name⋅_⋅T&amp;gt;⋅T&amp;gt;⋅T_bl_Z+_bl_Z&amp;lt;(elem⋅_⋅__bl_Z+_bl_Zdoc)⋅_⋅T⋅x_d1&amp;gt;⋅T))]]]]]').
xpath(step,'.//tei:dataRef[@name]').
var(step,'x',1).
no_typedelserule(step,21,type).
no_typedelserule(step,4,type).
no_typedelserule(step,16,type).
no_typedelserule(step,14,type).
no_typedelserule(step,6,type).
no_typedelserule(step,13,type).
no_typedelserule(step,19,type).
no_typedelserule(step,15,type).
no_typedelserule(step,29,type).
no_typedelserule(step,18,type).
no_typedelserule(step,26,type).
no_typedelserule(step,21,name).
no_typedelserule(step,4,name).
no_typedelserule(step,16,name).
no_typedelserule(step,14,name).
no_typedelserule(step,6,name).
no_typedelserule(step,13,name).
no_typedelserule(step,19,name).
no_typedelserule(step,15,name).
no_typedelserule(step,29,name).
no_typedelserule(step,18,name).
no_typedelserule(step,26,name).
no_typedelserule(step,21,var).
no_typedelserule(step,4,var).
no_typedelserule(step,16,var).
no_typedelserule(step,14,var).
no_typedelserule(step,6,var).
no_typedelserule(step,13,var).
no_typedelserule(step,19,var).
no_typedelserule(step,15,var).
no_typedelserule(step,29,var).
no_typedelserule(step,18,var).
no_typedelserule(step,26,var).
no_typedelserule(step,21,negvar).
no_typedelserule(step,4,negvar).
no_typedelserule(step,16,negvar).
no_typedelserule(step,14,negvar).
no_typedelserule(step,6,negvar).
no_typedelserule(step,13,negvar).
no_typedelserule(step,19,negvar).
no_typedelserule(step,15,negvar).
no_typedelserule(step,29,negvar).
no_typedelserule(step,18,negvar).
no_typedelserule(step,26,negvar).
no_typedelserule(step,21,namespace).
no_typedelserule(step,4,namespace).
no_typedelserule(step,16,namespace).
no_typedelserule(step,14,namespace).
no_typedelserule(step,6,namespace).
no_typedelserule(step,13,namespace).
no_typedelserule(step,19,namespace).
no_typedelserule(step,15,namespace).
no_typedelserule(step,29,namespace).
no_typedelserule(step,18,namespace).
no_typedelserule(step,26,namespace).
state(step,21).
state(step,4).
state(step,16).
state(step,14).
state(step,7).
state(step,10).
state(step,6).
state(step,13).
state(step,19).
state(step,11).
state(step,5).
state(step,3).
state(step,8).
state(step,2).
state(step,9).
state(step,1).
state(step,0).
state(step,15).
state(step,29).
state(step,12).
state(step,18).
state(step,26).
start_tree(step,0).
initial(step,0).
not_sink(step,21).
not_sink(step,4).
not_sink(step,16).
not_sink(step,14).
not_sink(step,7).
not_sink(step,10).
not_sink(step,6).
not_sink(step,13).
not_sink(step,19).
not_sink(step,11).
not_sink(step,5).
not_sink(step,3).
not_sink(step,8).
not_sink(step,2).
not_sink(step,9).
not_sink(step,1).
not_sink(step,0).
not_sink(step,15).
not_sink(step,29).
not_sink(step,12).
not_sink(step,18).
not_sink(step,26).
else(step,21).
else(step,4).
else(step,16).
else(step,14).
nonelse(step,7).
nonelse(step,10).
nonelse(step,6).
nonelse(step,13).
nonelse(step,19).
nonelse(step,11).
nonelse(step,5).
nonelse(step,3).
nonelse(step,8).
nonelse(step,2).
nonelse(step,9).
nonelse(step,1).
nonelse(step,0).
nonelse(step,15).
nonelse(step,29).
nonelse(step,12).
nonelse(step,18).
nonelse(step,26).
final(step,19).
final(step,29).
nonfinal(step,21).
nonfinal(step,4).
nonfinal(step,16).
nonfinal(step,14).
nonfinal(step,7).
nonfinal(step,10).
nonfinal(step,6).
nonfinal(step,13).
nonfinal(step,11).
nonfinal(step,5).
nonfinal(step,3).
nonfinal(step,8).
nonfinal(step,2).
nonfinal(step,9).
nonfinal(step,1).
nonfinal(step,0).
nonfinal(step,15).
nonfinal(step,12).
nonfinal(step,18).
nonfinal(step,26).
copy_of(step,21,21).
copy_of(step,4,4).
copy_of(step,16,16).
copy_of(step,14,14).
tree_state(step,21).
tree_istate(step,21,0).
 tree_state(step,4).
tree_istate(step,4,0).
 tree_state(step,16).
tree_istate(step,16,0).
 tree_state(step,14).
tree_istate(step,14,0).
 hedge_state(step,7).
    no_applyrule(step,7,21).
	  no_applyrule(step,7,4).
	  no_applyrule(step,7,16).
	  no_applyrule(step,7,14).
	  no_applyelserule(step,7).
hedge_state(step,10).
    no_applyrule(step,10,21).
	  no_applyrule(step,10,4).
	  no_applyrule(step,10,16).
	  no_applyrule(step,10,14).
	  no_applyelserule(step,10).
hedge_state(step,6).
    no_applyelserule(step,6).
no_elserule(step,6).
	% %
hedge_state(step,13).
    no_applyrule(step,13,21).
	  no_applyrule(step,13,16).
	  no_applyelserule(step,13).
no_elserule(step,13).
	% %
hedge_state(step,19).
    no_applyrule(step,19,21).
	  no_applyrule(step,19,16).
	  no_applyrule(step,19,14).
	  no_applyelserule(step,19).
no_elserule(step,19).
	% %
hedge_state(step,11).
    no_applyrule(step,11,21).
	  no_applyrule(step,11,4).
	  no_applyrule(step,11,16).
	  no_applyrule(step,11,14).
	  no_applyelserule(step,11).
hedge_state(step,5).
    no_applyrule(step,5,21).
	  no_applyrule(step,5,4).
	  no_applyrule(step,5,16).
	  no_applyrule(step,5,14).
	  no_applyelserule(step,5).
hedge_state(step,3).
    no_applyrule(step,3,21).
	  no_applyrule(step,3,4).
	  no_applyrule(step,3,16).
	  no_applyrule(step,3,14).
	  no_applyelserule(step,3).
hedge_state(step,8).
    no_applyrule(step,8,21).
	  no_applyrule(step,8,4).
	  no_applyrule(step,8,16).
	  no_applyrule(step,8,14).
	  no_applyelserule(step,8).
hedge_state(step,2).
    no_applyrule(step,2,21).
	  no_applyrule(step,2,4).
	  no_applyrule(step,2,16).
	  no_applyrule(step,2,14).
	  no_applyelserule(step,2).
hedge_state(step,9).
    no_applyrule(step,9,21).
	  no_applyrule(step,9,4).
	  no_applyrule(step,9,16).
	  no_applyrule(step,9,14).
	  no_applyelserule(step,9).
hedge_state(step,1).
    no_applyrule(step,1,21).
	  no_applyrule(step,1,4).
	  no_applyrule(step,1,16).
	  no_applyrule(step,1,14).
	  no_applyelserule(step,1).
hedge_state(step,0).
    no_applyrule(step,0,4).
	  no_applyrule(step,0,14).
	  no_applyelserule(step,0).
hedge_state(step,15).
    no_applyrule(step,15,21).
	  no_applyrule(step,15,16).
	  no_applyelserule(step,15).
no_elserule(step,15).
	% %
hedge_state(step,29).
    no_applyrule(step,29,21).
	  no_applyrule(step,29,16).
	  no_applyrule(step,29,14).
	  no_applyelserule(step,29).
no_elserule(step,29).
	% %
hedge_state(step,12).
    no_applyrule(step,12,21).
	  no_applyrule(step,12,4).
	  no_applyrule(step,12,16).
	  no_applyrule(step,12,14).
	  no_applyelserule(step,12).
hedge_state(step,18).
    no_applyrule(step,18,21).
	  no_applyrule(step,18,16).
	  no_applyrule(step,18,14).
	  no_applyelserule(step,18).
no_elserule(step,18).
	% %
hedge_state(step,26).
    no_applyrule(step,26,21).
	  no_applyrule(step,26,16).
	  no_applyrule(step,26,14).
	  no_applyelserule(step,26).
no_elserule(step,26).
	% %
applyrule(step,6,21,26).
applyrule(step,6,16,18).
applyrule(step,6,4,6).
applyrule(step,6,14,6).
applyrule(step,13,4,13).
applyrule(step,13,14,15).
applyrule(step,19,4,19).
applyrule(step,0,21,29).
applyrule(step,0,16,19).
applyrule(step,15,4,15).
applyrule(step,15,14,15).
applyrule(step,29,4,29).
applyrule(step,18,4,18).
applyrule(step,26,4,26).
elserule(step,7,5).
elserule(step,5,5).
elserule(step,3,9).
elserule(step,8,1).
elserule(step,2,5).
elserule(step,9,1).
elserule(step,12,12).
treerule(step,6,4).
treerule(step,5,4).
treerule(step,15,16).
treerule(step,12,14).
treerule(step,26,21).
treerule(step,18,21).
elserule(step,10,12).
elserule(step,11,6).
elserule(step,0,5).
elserule(step,1,6).
internalrule(step,7,'name',name,10).
internalrule(step,3,'tei',namespace,8).
internalrule(step,8,'dataRef',name,11).
internalrule(step,2,'default',namespace,7).
internalrule(step,11,'x',var,13).
internalrule(step,0,'att',type,2).
internalrule(step,0,'elem',type,3).
internalrule(step,0,'doc',type,1).
no_elserule(step,21).
no_elserule(step,4).
no_elserule(step,16).
no_elserule(step,14).
no_elserule(step,6).
no_elserule(step,13).
no_elserule(step,19).
no_elserule(step,15).
no_elserule(step,29).
no_elserule(step,18).
no_elserule(step,26).
no_treerule(step,21).
no_treerule(step,4).
no_treerule(step,16).
no_treerule(step,14).
no_treerule(step,7).
no_treerule(step,10).
no_treerule(step,13).
no_treerule(step,19).
no_treerule(step,11).
no_treerule(step,3).
no_treerule(step,8).
no_treerule(step,2).
no_treerule(step,9).
no_treerule(step,1).
no_treerule(step,0).
no_treerule(step,29).
label(step,'att',type,1).
no_internalrule(step,21,'att',type).
no_internalrule(step,4,'att',type).
no_internalrule(step,16,'att',type).
no_internalrule(step,14,'att',type).
no_internalrule(step,7,'att',type).
no_internalrule(step,10,'att',type).
no_internalrule(step,6,'att',type).
no_internalrule(step,13,'att',type).
no_internalrule(step,19,'att',type).
no_internalrule(step,11,'att',type).
no_internalrule(step,5,'att',type).
no_internalrule(step,3,'att',type).
no_internalrule(step,8,'att',type).
no_internalrule(step,2,'att',type).
no_internalrule(step,9,'att',type).
no_internalrule(step,1,'att',type).
no_internalrule(step,15,'att',type).
no_internalrule(step,29,'att',type).
no_internalrule(step,12,'att',type).
no_internalrule(step,18,'att',type).
no_internalrule(step,26,'att',type).
label(step,'dataRef',name,2).
no_internalrule(step,21,'dataRef',name).
no_internalrule(step,4,'dataRef',name).
no_internalrule(step,16,'dataRef',name).
no_internalrule(step,14,'dataRef',name).
no_internalrule(step,7,'dataRef',name).
no_internalrule(step,10,'dataRef',name).
no_internalrule(step,6,'dataRef',name).
no_internalrule(step,13,'dataRef',name).
no_internalrule(step,19,'dataRef',name).
no_internalrule(step,11,'dataRef',name).
no_internalrule(step,5,'dataRef',name).
no_internalrule(step,3,'dataRef',name).
no_internalrule(step,2,'dataRef',name).
no_internalrule(step,9,'dataRef',name).
no_internalrule(step,1,'dataRef',name).
no_internalrule(step,0,'dataRef',name).
no_internalrule(step,15,'dataRef',name).
no_internalrule(step,29,'dataRef',name).
no_internalrule(step,12,'dataRef',name).
no_internalrule(step,18,'dataRef',name).
no_internalrule(step,26,'dataRef',name).
label(step,'name',name,6).
no_internalrule(step,21,'name',name).
no_internalrule(step,4,'name',name).
no_internalrule(step,16,'name',name).
no_internalrule(step,14,'name',name).
no_internalrule(step,10,'name',name).
no_internalrule(step,6,'name',name).
no_internalrule(step,13,'name',name).
no_internalrule(step,19,'name',name).
no_internalrule(step,11,'name',name).
no_internalrule(step,5,'name',name).
no_internalrule(step,3,'name',name).
no_internalrule(step,8,'name',name).
no_internalrule(step,2,'name',name).
no_internalrule(step,9,'name',name).
no_internalrule(step,1,'name',name).
no_internalrule(step,0,'name',name).
no_internalrule(step,15,'name',name).
no_internalrule(step,29,'name',name).
no_internalrule(step,12,'name',name).
no_internalrule(step,18,'name',name).
no_internalrule(step,26,'name',name).
label(step,'x',var,8).
no_internalrule(step,21,'x',var).
no_internalrule(step,4,'x',var).
no_internalrule(step,16,'x',var).
no_internalrule(step,14,'x',var).
no_internalrule(step,7,'x',var).
no_internalrule(step,10,'x',var).
no_internalrule(step,6,'x',var).
no_internalrule(step,13,'x',var).
no_internalrule(step,19,'x',var).
no_internalrule(step,5,'x',var).
no_internalrule(step,3,'x',var).
no_internalrule(step,8,'x',var).
no_internalrule(step,2,'x',var).
no_internalrule(step,9,'x',var).
no_internalrule(step,1,'x',var).
no_internalrule(step,0,'x',var).
no_internalrule(step,15,'x',var).
no_internalrule(step,29,'x',var).
no_internalrule(step,12,'x',var).
no_internalrule(step,18,'x',var).
no_internalrule(step,26,'x',var).
label(step,'x',negvar,15).
no_internalrule(step,21,'x',negvar).
no_internalrule(step,4,'x',negvar).
no_internalrule(step,16,'x',negvar).
no_internalrule(step,14,'x',negvar).
no_internalrule(step,7,'x',negvar).
no_internalrule(step,10,'x',negvar).
no_internalrule(step,6,'x',negvar).
no_internalrule(step,13,'x',negvar).
no_internalrule(step,19,'x',negvar).
no_internalrule(step,11,'x',negvar).
no_internalrule(step,5,'x',negvar).
no_internalrule(step,3,'x',negvar).
no_internalrule(step,8,'x',negvar).
no_internalrule(step,2,'x',negvar).
no_internalrule(step,9,'x',negvar).
no_internalrule(step,1,'x',negvar).
no_internalrule(step,0,'x',negvar).
no_internalrule(step,15,'x',negvar).
no_internalrule(step,29,'x',negvar).
no_internalrule(step,12,'x',negvar).
no_internalrule(step,18,'x',negvar).
no_internalrule(step,26,'x',negvar).
label(step,'tei',namespace,7).
no_internalrule(step,21,'tei',namespace).
no_internalrule(step,4,'tei',namespace).
no_internalrule(step,16,'tei',namespace).
no_internalrule(step,14,'tei',namespace).
no_internalrule(step,7,'tei',namespace).
no_internalrule(step,10,'tei',namespace).
no_internalrule(step,6,'tei',namespace).
no_internalrule(step,13,'tei',namespace).
no_internalrule(step,19,'tei',namespace).
no_internalrule(step,11,'tei',namespace).
no_internalrule(step,5,'tei',namespace).
no_internalrule(step,8,'tei',namespace).
no_internalrule(step,2,'tei',namespace).
no_internalrule(step,9,'tei',namespace).
no_internalrule(step,1,'tei',namespace).
no_internalrule(step,0,'tei',namespace).
no_internalrule(step,15,'tei',namespace).
no_internalrule(step,29,'tei',namespace).
no_internalrule(step,12,'tei',namespace).
no_internalrule(step,18,'tei',namespace).
no_internalrule(step,26,'tei',namespace).
label(step,'text',type,14).
no_internalrule(step,21,'text',type).
no_internalrule(step,4,'text',type).
no_internalrule(step,16,'text',type).
no_internalrule(step,14,'text',type).
no_internalrule(step,7,'text',type).
no_internalrule(step,10,'text',type).
no_internalrule(step,6,'text',type).
no_internalrule(step,13,'text',type).
no_internalrule(step,19,'text',type).
no_internalrule(step,11,'text',type).
no_internalrule(step,5,'text',type).
no_internalrule(step,3,'text',type).
no_internalrule(step,8,'text',type).
no_internalrule(step,2,'text',type).
no_internalrule(step,9,'text',type).
no_internalrule(step,1,'text',type).
no_internalrule(step,0,'text',type).
no_internalrule(step,15,'text',type).
no_internalrule(step,29,'text',type).
no_internalrule(step,12,'text',type).
no_internalrule(step,18,'text',type).
no_internalrule(step,26,'text',type).
label(step,'comment',type,11).
no_internalrule(step,21,'comment',type).
no_internalrule(step,4,'comment',type).
no_internalrule(step,16,'comment',type).
no_internalrule(step,14,'comment',type).
no_internalrule(step,7,'comment',type).
no_internalrule(step,10,'comment',type).
no_internalrule(step,6,'comment',type).
no_internalrule(step,13,'comment',type).
no_internalrule(step,19,'comment',type).
no_internalrule(step,11,'comment',type).
no_internalrule(step,5,'comment',type).
no_internalrule(step,3,'comment',type).
no_internalrule(step,8,'comment',type).
no_internalrule(step,2,'comment',type).
no_internalrule(step,9,'comment',type).
no_internalrule(step,1,'comment',type).
no_internalrule(step,0,'comment',type).
no_internalrule(step,15,'comment',type).
no_internalrule(step,29,'comment',type).
no_internalrule(step,12,'comment',type).
no_internalrule(step,18,'comment',type).
no_internalrule(step,26,'comment',type).
label(step,'default',namespace,3).
no_internalrule(step,21,'default',namespace).
no_internalrule(step,4,'default',namespace).
no_internalrule(step,16,'default',namespace).
no_internalrule(step,14,'default',namespace).
no_internalrule(step,7,'default',namespace).
no_internalrule(step,10,'default',namespace).
no_internalrule(step,6,'default',namespace).
no_internalrule(step,13,'default',namespace).
no_internalrule(step,19,'default',namespace).
no_internalrule(step,11,'default',namespace).
no_internalrule(step,5,'default',namespace).
no_internalrule(step,3,'default',namespace).
no_internalrule(step,8,'default',namespace).
no_internalrule(step,9,'default',namespace).
no_internalrule(step,1,'default',namespace).
no_internalrule(step,0,'default',namespace).
no_internalrule(step,15,'default',namespace).
no_internalrule(step,29,'default',namespace).
no_internalrule(step,12,'default',namespace).
no_internalrule(step,18,'default',namespace).
no_internalrule(step,26,'default',namespace).
label(step,'elem',type,5).
no_internalrule(step,21,'elem',type).
no_internalrule(step,4,'elem',type).
no_internalrule(step,16,'elem',type).
no_internalrule(step,14,'elem',type).
no_internalrule(step,7,'elem',type).
no_internalrule(step,10,'elem',type).
no_internalrule(step,6,'elem',type).
no_internalrule(step,13,'elem',type).
no_internalrule(step,19,'elem',type).
no_internalrule(step,11,'elem',type).
no_internalrule(step,5,'elem',type).
no_internalrule(step,3,'elem',type).
no_internalrule(step,8,'elem',type).
no_internalrule(step,2,'elem',type).
no_internalrule(step,9,'elem',type).
no_internalrule(step,1,'elem',type).
no_internalrule(step,15,'elem',type).
no_internalrule(step,29,'elem',type).
no_internalrule(step,12,'elem',type).
no_internalrule(step,18,'elem',type).
no_internalrule(step,26,'elem',type).
label(step,'doc',type,4).
no_internalrule(step,21,'doc',type).
no_internalrule(step,4,'doc',type).
no_internalrule(step,16,'doc',type).
no_internalrule(step,14,'doc',type).
no_internalrule(step,7,'doc',type).
no_internalrule(step,10,'doc',type).
no_internalrule(step,6,'doc',type).
no_internalrule(step,13,'doc',type).
no_internalrule(step,19,'doc',type).
no_internalrule(step,11,'doc',type).
no_internalrule(step,5,'doc',type).
no_internalrule(step,3,'doc',type).
no_internalrule(step,8,'doc',type).
no_internalrule(step,2,'doc',type).
no_internalrule(step,9,'doc',type).
no_internalrule(step,1,'doc',type).
no_internalrule(step,15,'doc',type).
no_internalrule(step,29,'doc',type).
no_internalrule(step,12,'doc',type).
no_internalrule(step,18,'doc',type).
no_internalrule(step,26,'doc',type).
max_label_id(step,15).
    %%% different origins %%%%%%%
    
