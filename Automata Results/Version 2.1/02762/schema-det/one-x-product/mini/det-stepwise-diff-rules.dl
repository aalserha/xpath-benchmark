diff_final(A,X,X1) :- final(A,X), nonfinal(A,X1), hedge_state(A,X1).
diff_type(A,X,X1)  :- hedge_state(A,X), tree_state(A,X1).
diff_sym(A,X1,X)   :- diff(A,X,X1).
diff_else(A,X,X1)  :- else(A,X), nonelse(A,X1), tree_state(A,X1).

internal(A,X,L,Type,Y)  :- internalrule(A,X,L,Type,Y).
internal(A,X,L,Type,Y)  :- no_internalrule(A,X,L,Type), elserule(A,X,Y).
internal(A,X,L,Type,Y)  :- no_internalrule(A,X,L,Type), typedelserule(A,X,Type,Y).
no_internal(A,X,L,Type) :- hedge_state(A,X), no_internalrule(A,X,L,Type),
                           no_elserule(A,X), no_typedelserule(A,X,Type).

apply(A,X,Y,Z)  :- applyrule(A,X,Y,Z).
apply(A,X,Y,Z)  :- applyelse(A,X,Y,Z).

applyelse(A,X,Y,Z)  :- applyelserule(A,X,Z), else(A,Y), no_applyrule(A,X,Y).
no_apply(A,X,Y)     :- no_applyrule(A,X,Y),
		       no_applyelserule_with(A,X,Y), tree_state(A,Y).
no_applyelserule_with(A,X,Y) :- no_applyelserule(A,X), tree_state(A,Y). 
no_applyelserule_with(A,X,Y) :- applyelserule(A,X,Z),
			        nonelse(A,Y), tree_state(Y).
no_applyelserule_with(A,X,Y) :- applyelserule(A,X,Z),
			        applyrule(A,X,Y,Z1).

type_lab(A,T) :- typedeleserule(A,X,T,Y).
type_lab(A,T) :- no_typedeleserule(A,X,T).
typedelserule(A,X,T,Y) :- elserule(A,X,Y), type_lab(A,T).

diff_typedelse(A,X,T,X1) :- typedelserule(A,X,T,Y), no_typedelserule(A,X1,T).
% self_diff_typedelse(X)   :- diff_typedelse(A,X,T,X).
diff(A,X,X1) :- diff_typedelse(A,X,T,X1).
diff(A,X,X1) :- diff_typedelserule(A,X,T,X1).

diff(A,X,X1)      :- diff_tree(A,X,X1).
% self_diff_tree(X) :- diff_tree(A,X,X).
diff(A,X,X1)      :- diff_type(A,X,X1).
% self_diff_type(X) :- diff_type(A,X,X).
diff(A,X,X1)      :- diff_final(A,X,X1).
% self_diff_final(X):- diff_final(A,X,X).
diff(A,X,X1)      :- diff_else(A,X,X1).
% self_diff_else(X) :- diff_else(A,X,X).

diff(A,X,X1) :- diff_treerule(A,X,X1).
diff(A,X,X1) :- diff_no_treerule(A,X,X1).
diff(A,X,X1) :- diff_apply(A,X,X1).
diff(A,X,X1) :- diff_no_apply(A,X,X1).
diff(A,X,X1) :- diff_internal(A,X,X1).

diff(A,X,X1) :- diff_no_internal(A,X,X1).
diff(A,X,X1) :- diff_nonelse(A,X,X1).
diff(A,X,X1) :- diff_origin(A,X,X1).
diff(A,X,X1) :- diff_sym(A,X,X1).
diff(A,X,X1) :- diff_elserule(A,X,X1).
diff(A,X,X1) :- diff_no_elserule(A,X,X1).
diff(A,X,X1) :- diff_no_typedelserule(A,X,X1).

diff_apply_from1(A,X,X1,Z,Z1)  :- apply(A,X,Y,Z), apply(A,X1,Y,Z1), diff(A,Z,Z1).
diff_apply_from2(A,Y,Y1,Z,Z1)  :- apply(A,X,Y,Z), apply(A,X,Y1,Z1), diff(A,Z,Z1).
% self_debug_diff_apply_from1(X,Y,Z,Z1) :- apply(A,X,Y,Z), apply(A,X,Y,Z1), diff(A,Z,Z1).
% self_debug_diff_apply_from2(Y,X,Z,Z1) :- apply(A,X,Y,Z), apply(A,X,Y,Z1), diff(A,Z,Z1).
diff_apply(A,X,X1)             :- diff_apply_from1(A,X,X1,Z,Z1).
diff_apply(A,X,X1)             :- diff_apply_from2(A,X,X1,Z,Z1).

diff_no_apply_from1(A,X,X1,Y,Z):- apply(A,X,Y,Z), no_apply(A,X1,Y).
diff_no_apply_from2(A,Y,Y1,X,Z):- apply(A,X,Y,Z), no_apply(A,X,Y1).
% self_debug_diff_no_apply_from1(X,Y,Z) :- apply(A,X,Y,Z), no_apply(A,X,Y).
% self_debug_diff_no_apply_from2(Y,X,Z) :- apply(A,X,Y,Z), no_apply(A,X,Y).
diff_no_apply(A,X,X1)          :- diff_no_apply_from1(A,X,X1,Y,Z).
diff_no_apply(A,X,X1)          :- diff_no_apply_from2(A,X,X1,Y,Z).

diff_treerule(A,X,X1)     :- treerule(A,X,Y), treerule(A,X1,Y1), diff(A,Y,Y1).
% self_debug_diff_treerule(X,Y,Y1) :- treerule(A,X,Y), treerule(A,X,Y1), diff(A,Y,Y1).
diff_no_treerule(A,X,X1)  :- treerule(A,X,Y), no_treerule(A,X1), hedge_state(A,X1).
% self_diff_no_treerule(X)  :-  treerule(A,X,Y), no_treerule(A,X), hedge_state(A,X).

diff_internal(A,X,X1) :- diff_internal_from(A,X,X1,L,Type,Y,Y1).
diff_internal_from(A,X,X1,L,Type,Y,Y1)
                      :- internal(A,X,L,Type,Y),
                         internal(A,X1,L,Type,Y1), diff(A,Y,Y1).
% self_debug_diff_internal_from(A,X,L,Type,Y,Y1):-diff_internal_from(A,X,X,L,Type,Y,Y1).
			 
diff_no_internal(A,X,X1) :- no_internal(A,X,L,Type), internal(A,X1,L,Type,Y1).
% self_debug_diff_no_internal(X,L,Type,Y1) :- no_internal(A,X,L,Type), internal(A,X,L,Type,Y1).

diff_elserule(A,X,X1) :- elserule(A,X,Y), elserule(A,X1,Y1), diff(A,Y,Y1).
% self_debug_diff_elserule(X,Y,Y1) :- elserule(A,X,Y), elserule(A,X,Y1), diff(A,Y,Y1).
diff_typedelserule(A,X,T,X1) :- typedelserule(A,X,T,Y), typedelserule(A,X1,T,Y1), diff(A,Y,Y1).
% self_diff_typedelserule(X)   :- typedelserule(A,X,T,Y), typedelserule(A,X,T,Y1),  diff(A,Y,Y1).
% self_debug_diff_typedelserule(X,Y,Y1) :- typedelserule(A,X,T,Y), typedelserule(A,X,T,Y1), diff(A,Y,Y1).

diff_no_elserule(A,X,X1) :- elserule(A,X,Y), no_elserule(A,X1).
diff_no_typedelserule(A,X,X1) :- typedelserule(A,X,T,Y), no_elserule(A,X1),no_typedelserule(A,X1,T).

% self_diff_no_elserule(X) :- diff_no_elserule(A,X,X).

% don't fusion states with different schema origins
%    (Joachim: should first diff_orig rule be switched on or off?)

% diff_origin(A,X1,X) :- origin_of(A,X1,Y1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).
% self_diff_origin(X) :- origin_of(A,XY1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).
% self_debug_diff_origin(X,Y1,Z1,Y,Z) :- origin_of(A,X,Y1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).

% for minimization replace rule(A,X,Y,Z) by rule(A,min(X),min(Y),min(Z)) 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule
% negative information (NEGATIVE=true)
%   no_applyrule
%   no_elserule
%   no_treerule
%   no_internalrule
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(step,'stepwise[]').
var(step,'x',1).
no_typedelserule(step,1,name).
no_typedelserule(step,2,name).
no_typedelserule(step,3,name).
no_typedelserule(step,4,name).
no_typedelserule(step,5,name).
no_typedelserule(step,6,name).
no_typedelserule(step,7,name).
no_typedelserule(step,8,name).
no_typedelserule(step,9,name).
no_typedelserule(step,10,name).
no_typedelserule(step,12,name).
no_typedelserule(step,13,name).
no_typedelserule(step,14,name).
no_typedelserule(step,15,name).
no_typedelserule(step,17,name).
no_typedelserule(step,18,name).
no_typedelserule(step,22,name).
no_typedelserule(step,24,name).
no_typedelserule(step,25,name).
no_typedelserule(step,28,name).
no_typedelserule(step,29,name).
no_typedelserule(step,30,name).
no_typedelserule(step,31,name).
no_typedelserule(step,32,name).
no_typedelserule(step,33,name).
no_typedelserule(step,1,namespace).
no_typedelserule(step,2,namespace).
no_typedelserule(step,3,namespace).
no_typedelserule(step,4,namespace).
no_typedelserule(step,5,namespace).
no_typedelserule(step,6,namespace).
no_typedelserule(step,7,namespace).
no_typedelserule(step,8,namespace).
no_typedelserule(step,9,namespace).
no_typedelserule(step,10,namespace).
no_typedelserule(step,12,namespace).
no_typedelserule(step,13,namespace).
no_typedelserule(step,14,namespace).
no_typedelserule(step,15,namespace).
no_typedelserule(step,17,namespace).
no_typedelserule(step,18,namespace).
no_typedelserule(step,22,namespace).
no_typedelserule(step,24,namespace).
no_typedelserule(step,25,namespace).
no_typedelserule(step,28,namespace).
no_typedelserule(step,29,namespace).
no_typedelserule(step,30,namespace).
no_typedelserule(step,31,namespace).
no_typedelserule(step,32,namespace).
no_typedelserule(step,33,namespace).
no_typedelserule(step,1,type).
no_typedelserule(step,2,type).
no_typedelserule(step,3,type).
no_typedelserule(step,4,type).
no_typedelserule(step,5,type).
no_typedelserule(step,6,type).
no_typedelserule(step,7,type).
no_typedelserule(step,8,type).
no_typedelserule(step,9,type).
no_typedelserule(step,10,type).
no_typedelserule(step,12,type).
no_typedelserule(step,13,type).
no_typedelserule(step,14,type).
no_typedelserule(step,15,type).
no_typedelserule(step,17,type).
no_typedelserule(step,18,type).
no_typedelserule(step,22,type).
no_typedelserule(step,24,type).
no_typedelserule(step,25,type).
no_typedelserule(step,28,type).
no_typedelserule(step,29,type).
no_typedelserule(step,30,type).
no_typedelserule(step,31,type).
no_typedelserule(step,32,type).
no_typedelserule(step,33,type).
no_typedelserule(step,1,var).
no_typedelserule(step,2,var).
no_typedelserule(step,3,var).
no_typedelserule(step,4,var).
no_typedelserule(step,5,var).
no_typedelserule(step,6,var).
no_typedelserule(step,7,var).
no_typedelserule(step,8,var).
no_typedelserule(step,9,var).
no_typedelserule(step,10,var).
no_typedelserule(step,12,var).
no_typedelserule(step,13,var).
no_typedelserule(step,14,var).
no_typedelserule(step,15,var).
no_typedelserule(step,17,var).
no_typedelserule(step,18,var).
no_typedelserule(step,22,var).
no_typedelserule(step,24,var).
no_typedelserule(step,25,var).
no_typedelserule(step,28,var).
no_typedelserule(step,29,var).
no_typedelserule(step,30,var).
no_typedelserule(step,31,var).
no_typedelserule(step,32,var).
no_typedelserule(step,33,var).
state(step,1).
state(step,2).
state(step,3).
state(step,4).
state(step,5).
state(step,6).
state(step,7).
state(step,8).
state(step,9).
state(step,10).
state(step,11).
state(step,12).
state(step,13).
state(step,14).
state(step,15).
state(step,16).
state(step,17).
state(step,18).
state(step,19).
state(step,20).
state(step,21).
state(step,22).
state(step,23).
state(step,24).
state(step,25).
state(step,26).
state(step,27).
state(step,28).
state(step,29).
state(step,30).
state(step,31).
state(step,32).
state(step,33).
start_tree(step,27).
initial(step,27).
origin_of(step,1,'6','1').
origin_of(step,2,'4','1').
origin_of(step,3,'3','1').
origin_of(step,4,'9','1').
origin_of(step,5,'1','1').
origin_of(step,6,'11','2').
origin_of(step,7,'16','4').
origin_of(step,8,'17','4').
origin_of(step,9,'12','4').
origin_of(step,10,'24','4').
origin_of(step,11,'7','0').
origin_of(step,12,'10','1').
origin_of(step,13,'21','1').
origin_of(step,14,'13','1').
origin_of(step,15,'19','1').
origin_of(step,16,'6','0').
origin_of(step,17,'4','0').
origin_of(step,18,'22','1').
origin_of(step,19,'5','0').
origin_of(step,20,'8','0').
origin_of(step,21,'3','0').
origin_of(step,22,'25','1').
origin_of(step,23,'2','0').
origin_of(step,24,'20','1').
origin_of(step,25,'9','0').
origin_of(step,26,'1','0').
origin_of(step,27,'0','0').
origin_of(step,28,'23','1').
origin_of(step,29,'15','1').
origin_of(step,30,'27','1').
origin_of(step,31,'26','1').
origin_of(step,32,'18','1').
origin_of(step,33,'14','1').
sink(step,1).
sink(step,2).
sink(step,3).
sink(step,4).
sink(step,5).
not_sink(step,6).
not_sink(step,7).
not_sink(step,8).
not_sink(step,9).
not_sink(step,10).
not_sink(step,11).
not_sink(step,12).
not_sink(step,13).
not_sink(step,14).
not_sink(step,15).
not_sink(step,16).
not_sink(step,17).
not_sink(step,18).
not_sink(step,19).
not_sink(step,20).
not_sink(step,21).
not_sink(step,22).
not_sink(step,23).
not_sink(step,24).
not_sink(step,25).
not_sink(step,26).
not_sink(step,27).
not_sink(step,28).
not_sink(step,29).
not_sink(step,30).
not_sink(step,31).
not_sink(step,32).
not_sink(step,33).
else(step,6).
else(step,7).
else(step,8).
else(step,9).
else(step,10).
nonelse(step,1).
nonelse(step,2).
nonelse(step,3).
nonelse(step,4).
nonelse(step,5).
nonelse(step,11).
nonelse(step,12).
nonelse(step,13).
nonelse(step,14).
nonelse(step,15).
nonelse(step,16).
nonelse(step,17).
nonelse(step,18).
nonelse(step,19).
nonelse(step,20).
nonelse(step,21).
nonelse(step,22).
nonelse(step,23).
nonelse(step,24).
nonelse(step,25).
nonelse(step,26).
nonelse(step,27).
nonelse(step,28).
nonelse(step,29).
nonelse(step,30).
nonelse(step,31).
nonelse(step,32).
nonelse(step,33).
final(step,18).
final(step,24).
final(step,29).
final(step,31).
nonfinal(step,1).
nonfinal(step,2).
nonfinal(step,3).
nonfinal(step,4).
nonfinal(step,5).
nonfinal(step,6).
nonfinal(step,7).
nonfinal(step,8).
nonfinal(step,9).
nonfinal(step,10).
nonfinal(step,11).
nonfinal(step,12).
nonfinal(step,13).
nonfinal(step,14).
nonfinal(step,15).
nonfinal(step,16).
nonfinal(step,17).
nonfinal(step,19).
nonfinal(step,20).
nonfinal(step,21).
nonfinal(step,22).
nonfinal(step,23).
nonfinal(step,25).
nonfinal(step,26).
nonfinal(step,27).
nonfinal(step,28).
nonfinal(step,30).
nonfinal(step,32).
nonfinal(step,33).
copy_of(step,6,6).
copy_of(step,7,7).
copy_of(step,8,8).
copy_of(step,9,9).
copy_of(step,10,10).
tree_state(step,6).
tree_istate(step,6,27).
 tree_state(step,7).
tree_istate(step,7,27).
 tree_state(step,8).
tree_istate(step,8,27).
 tree_state(step,9).
tree_istate(step,9,27).
 tree_state(step,10).
tree_istate(step,10,27).
 hedge_state(step,1).
    no_applyrule(step,1,6).
	  no_applyrule(step,1,7).
	  no_applyrule(step,1,8).
	  no_applyrule(step,1,9).
	  no_applyrule(step,1,10).
	  no_applyelserule(step,1).
no_elserule(step,1).
	% %
hedge_state(step,2).
    no_applyrule(step,2,6).
	  no_applyrule(step,2,7).
	  no_applyrule(step,2,8).
	  no_applyrule(step,2,9).
	  no_applyrule(step,2,10).
	  no_applyelserule(step,2).
no_elserule(step,2).
	% %
hedge_state(step,3).
    no_applyrule(step,3,6).
	  no_applyrule(step,3,7).
	  no_applyrule(step,3,8).
	  no_applyrule(step,3,9).
	  no_applyrule(step,3,10).
	  no_applyelserule(step,3).
no_elserule(step,3).
	% %
hedge_state(step,4).
    no_applyrule(step,4,6).
	  no_applyrule(step,4,7).
	  no_applyrule(step,4,8).
	  no_applyrule(step,4,9).
	  no_applyrule(step,4,10).
	  no_applyelserule(step,4).
no_elserule(step,4).
	% %
hedge_state(step,5).
    no_applyrule(step,5,6).
	  no_applyrule(step,5,7).
	  no_applyrule(step,5,8).
	  no_applyrule(step,5,9).
	  no_applyrule(step,5,10).
	  no_applyelserule(step,5).
no_elserule(step,5).
	% %
hedge_state(step,11).
    no_applyrule(step,11,6).
	  no_applyrule(step,11,7).
	  no_applyrule(step,11,8).
	  no_applyrule(step,11,9).
	  no_applyrule(step,11,10).
	  no_applyelserule(step,11).
hedge_state(step,12).
    no_applyrule(step,12,7).
	  no_applyrule(step,12,8).
	  no_applyrule(step,12,9).
	  no_applyrule(step,12,10).
	  no_applyelserule(step,12).
no_elserule(step,12).
	% %
hedge_state(step,13).
    no_applyrule(step,13,7).
	  no_applyrule(step,13,8).
	  no_applyrule(step,13,9).
	  no_applyrule(step,13,10).
	  no_applyelserule(step,13).
no_elserule(step,13).
	% %
hedge_state(step,14).
    no_applyrule(step,14,7).
	  no_applyrule(step,14,8).
	  no_applyrule(step,14,9).
	  no_applyrule(step,14,10).
	  no_applyelserule(step,14).
no_elserule(step,14).
	% %
hedge_state(step,15).
    no_applyrule(step,15,7).
	  no_applyrule(step,15,8).
	  no_applyrule(step,15,9).
	  no_applyrule(step,15,10).
	  no_applyelserule(step,15).
no_elserule(step,15).
	% %
hedge_state(step,16).
    no_applyrule(step,16,6).
	  no_applyrule(step,16,7).
	  no_applyrule(step,16,8).
	  no_applyrule(step,16,9).
	  no_applyrule(step,16,10).
	  no_applyelserule(step,16).
hedge_state(step,17).
    no_applyelserule(step,17).
no_elserule(step,17).
	% %
hedge_state(step,18).
    no_applyrule(step,18,7).
	  no_applyrule(step,18,8).
	  no_applyrule(step,18,9).
	  no_applyrule(step,18,10).
	  no_applyelserule(step,18).
no_elserule(step,18).
	% %
hedge_state(step,19).
    no_applyrule(step,19,6).
	  no_applyrule(step,19,7).
	  no_applyrule(step,19,8).
	  no_applyrule(step,19,9).
	  no_applyrule(step,19,10).
	  no_applyelserule(step,19).
hedge_state(step,20).
    no_applyrule(step,20,6).
	  no_applyrule(step,20,7).
	  no_applyrule(step,20,8).
	  no_applyrule(step,20,9).
	  no_applyrule(step,20,10).
	  no_applyelserule(step,20).
hedge_state(step,21).
    no_applyrule(step,21,7).
	  no_applyrule(step,21,8).
	  no_applyrule(step,21,9).
	  no_applyrule(step,21,10).
	  no_applyelserule(step,21).
hedge_state(step,22).
    no_applyrule(step,22,7).
	  no_applyrule(step,22,8).
	  no_applyrule(step,22,9).
	  no_applyrule(step,22,10).
	  no_applyelserule(step,22).
no_elserule(step,22).
	% %
hedge_state(step,23).
    no_applyrule(step,23,6).
	  no_applyrule(step,23,7).
	  no_applyrule(step,23,8).
	  no_applyrule(step,23,9).
	  no_applyrule(step,23,10).
	  no_applyelserule(step,23).
hedge_state(step,24).
    no_applyrule(step,24,7).
	  no_applyrule(step,24,8).
	  no_applyrule(step,24,9).
	  no_applyrule(step,24,10).
	  no_applyelserule(step,24).
no_elserule(step,24).
	% %
hedge_state(step,25).
    no_applyelserule(step,25).
no_elserule(step,25).
	% %
hedge_state(step,26).
    no_applyrule(step,26,6).
	  no_applyrule(step,26,7).
	  no_applyrule(step,26,8).
	  no_applyrule(step,26,9).
	  no_applyrule(step,26,10).
	  no_applyelserule(step,26).
hedge_state(step,27).
    no_applyelserule(step,27).
hedge_state(step,28).
    no_applyrule(step,28,7).
	  no_applyrule(step,28,8).
	  no_applyrule(step,28,9).
	  no_applyrule(step,28,10).
	  no_applyelserule(step,28).
no_elserule(step,28).
	% %
hedge_state(step,29).
    no_applyrule(step,29,7).
	  no_applyrule(step,29,8).
	  no_applyrule(step,29,9).
	  no_applyrule(step,29,10).
	  no_applyelserule(step,29).
no_elserule(step,29).
	% %
hedge_state(step,30).
    no_applyrule(step,30,7).
	  no_applyrule(step,30,8).
	  no_applyrule(step,30,9).
	  no_applyrule(step,30,10).
	  no_applyelserule(step,30).
no_elserule(step,30).
	% %
hedge_state(step,31).
    no_applyrule(step,31,7).
	  no_applyrule(step,31,8).
	  no_applyrule(step,31,9).
	  no_applyrule(step,31,10).
	  no_applyelserule(step,31).
no_elserule(step,31).
	% %
hedge_state(step,32).
    no_applyrule(step,32,7).
	  no_applyrule(step,32,8).
	  no_applyrule(step,32,9).
	  no_applyrule(step,32,10).
	  no_applyelserule(step,32).
no_elserule(step,32).
	% %
hedge_state(step,33).
    no_applyrule(step,33,7).
	  no_applyrule(step,33,8).
	  no_applyrule(step,33,9).
	  no_applyrule(step,33,10).
	  no_applyelserule(step,33).
no_elserule(step,33).
	% %
applyrule(step,12,6,12).
applyrule(step,13,6,13).
applyrule(step,14,6,14).
applyrule(step,15,6,15).
applyrule(step,17,6,17).
applyrule(step,17,7,32).
applyrule(step,17,8,13).
applyrule(step,17,9,14).
applyrule(step,17,10,22).
applyrule(step,18,6,18).
applyrule(step,21,6,21).
applyrule(step,22,6,22).
applyrule(step,24,6,24).
applyrule(step,25,6,25).
applyrule(step,25,7,15).
applyrule(step,25,8,28).
applyrule(step,25,9,33).
applyrule(step,25,10,30).
applyrule(step,27,6,21).
applyrule(step,27,7,24).
applyrule(step,27,8,18).
applyrule(step,27,9,29).
applyrule(step,27,10,31).
applyrule(step,28,6,28).
applyrule(step,29,6,29).
applyrule(step,30,6,30).
applyrule(step,31,6,31).
applyrule(step,32,6,32).
applyrule(step,33,6,33).
elserule(step,11,25).
elserule(step,16,26).
elserule(step,19,26).
elserule(step,21,21).
elserule(step,20,17).
elserule(step,23,16).
elserule(step,27,21).
elserule(step,26,17).
treerule(step,12,9).
treerule(step,13,10).
treerule(step,14,7).
treerule(step,15,7).
treerule(step,17,6).
treerule(step,21,6).
treerule(step,22,10).
treerule(step,25,6).
treerule(step,28,10).
treerule(step,30,10).
treerule(step,32,7).
treerule(step,33,8).
internalrule(step,11,'x',var,4).
internalrule(step,16,'x',var,5).
internalrule(step,19,'x',var,5).
internalrule(step,21,'x',var,3).
internalrule(step,23,'x',var,1).
internalrule(step,27,'x',var,3).
internalrule(step,26,'x',var,2).
internalrule(step,19,'author',name,20).
internalrule(step,19,'authorgroup',name,11).
internalrule(step,23,'default',namespace,19).
internalrule(step,27,'elem',type,23).
internalrule(step,27,'doc',type,26).
internalrule(step,20,'x',var,12).
no_elserule(step,1).
no_elserule(step,2).
no_elserule(step,3).
no_elserule(step,4).
no_elserule(step,5).
no_elserule(step,6).
no_elserule(step,7).
no_elserule(step,8).
no_elserule(step,9).
no_elserule(step,10).
no_elserule(step,12).
no_elserule(step,13).
no_elserule(step,14).
no_elserule(step,15).
no_elserule(step,17).
no_elserule(step,18).
no_elserule(step,22).
no_elserule(step,24).
no_elserule(step,25).
no_elserule(step,28).
no_elserule(step,29).
no_elserule(step,30).
no_elserule(step,31).
no_elserule(step,32).
no_elserule(step,33).
no_treerule(step,1).
no_treerule(step,2).
no_treerule(step,3).
no_treerule(step,4).
no_treerule(step,5).
no_treerule(step,6).
no_treerule(step,7).
no_treerule(step,8).
no_treerule(step,9).
no_treerule(step,10).
no_treerule(step,11).
no_treerule(step,16).
no_treerule(step,18).
no_treerule(step,19).
no_treerule(step,20).
no_treerule(step,23).
no_treerule(step,24).
no_treerule(step,26).
no_treerule(step,27).
no_treerule(step,29).
no_treerule(step,31).
label(step,'authorgroup',name,1).
no_internalrule(step,1,'authorgroup',name).
no_internalrule(step,2,'authorgroup',name).
no_internalrule(step,3,'authorgroup',name).
no_internalrule(step,4,'authorgroup',name).
no_internalrule(step,5,'authorgroup',name).
no_internalrule(step,6,'authorgroup',name).
no_internalrule(step,7,'authorgroup',name).
no_internalrule(step,8,'authorgroup',name).
no_internalrule(step,9,'authorgroup',name).
no_internalrule(step,10,'authorgroup',name).
no_internalrule(step,11,'authorgroup',name).
no_internalrule(step,12,'authorgroup',name).
no_internalrule(step,13,'authorgroup',name).
no_internalrule(step,14,'authorgroup',name).
no_internalrule(step,15,'authorgroup',name).
no_internalrule(step,16,'authorgroup',name).
no_internalrule(step,17,'authorgroup',name).
no_internalrule(step,18,'authorgroup',name).
no_internalrule(step,20,'authorgroup',name).
no_internalrule(step,21,'authorgroup',name).
no_internalrule(step,22,'authorgroup',name).
no_internalrule(step,23,'authorgroup',name).
no_internalrule(step,24,'authorgroup',name).
no_internalrule(step,25,'authorgroup',name).
no_internalrule(step,26,'authorgroup',name).
no_internalrule(step,27,'authorgroup',name).
no_internalrule(step,28,'authorgroup',name).
no_internalrule(step,29,'authorgroup',name).
no_internalrule(step,30,'authorgroup',name).
no_internalrule(step,31,'authorgroup',name).
no_internalrule(step,32,'authorgroup',name).
no_internalrule(step,33,'authorgroup',name).
label(step,'author',name,2).
no_internalrule(step,1,'author',name).
no_internalrule(step,2,'author',name).
no_internalrule(step,3,'author',name).
no_internalrule(step,4,'author',name).
no_internalrule(step,5,'author',name).
no_internalrule(step,6,'author',name).
no_internalrule(step,7,'author',name).
no_internalrule(step,8,'author',name).
no_internalrule(step,9,'author',name).
no_internalrule(step,10,'author',name).
no_internalrule(step,11,'author',name).
no_internalrule(step,12,'author',name).
no_internalrule(step,13,'author',name).
no_internalrule(step,14,'author',name).
no_internalrule(step,15,'author',name).
no_internalrule(step,16,'author',name).
no_internalrule(step,17,'author',name).
no_internalrule(step,18,'author',name).
no_internalrule(step,20,'author',name).
no_internalrule(step,21,'author',name).
no_internalrule(step,22,'author',name).
no_internalrule(step,23,'author',name).
no_internalrule(step,24,'author',name).
no_internalrule(step,25,'author',name).
no_internalrule(step,26,'author',name).
no_internalrule(step,27,'author',name).
no_internalrule(step,28,'author',name).
no_internalrule(step,29,'author',name).
no_internalrule(step,30,'author',name).
no_internalrule(step,31,'author',name).
no_internalrule(step,32,'author',name).
no_internalrule(step,33,'author',name).
label(step,'default',namespace,3).
no_internalrule(step,1,'default',namespace).
no_internalrule(step,2,'default',namespace).
no_internalrule(step,3,'default',namespace).
no_internalrule(step,4,'default',namespace).
no_internalrule(step,5,'default',namespace).
no_internalrule(step,6,'default',namespace).
no_internalrule(step,7,'default',namespace).
no_internalrule(step,8,'default',namespace).
no_internalrule(step,9,'default',namespace).
no_internalrule(step,10,'default',namespace).
no_internalrule(step,11,'default',namespace).
no_internalrule(step,12,'default',namespace).
no_internalrule(step,13,'default',namespace).
no_internalrule(step,14,'default',namespace).
no_internalrule(step,15,'default',namespace).
no_internalrule(step,16,'default',namespace).
no_internalrule(step,17,'default',namespace).
no_internalrule(step,18,'default',namespace).
no_internalrule(step,19,'default',namespace).
no_internalrule(step,20,'default',namespace).
no_internalrule(step,21,'default',namespace).
no_internalrule(step,22,'default',namespace).
no_internalrule(step,24,'default',namespace).
no_internalrule(step,25,'default',namespace).
no_internalrule(step,26,'default',namespace).
no_internalrule(step,27,'default',namespace).
no_internalrule(step,28,'default',namespace).
no_internalrule(step,29,'default',namespace).
no_internalrule(step,30,'default',namespace).
no_internalrule(step,31,'default',namespace).
no_internalrule(step,32,'default',namespace).
no_internalrule(step,33,'default',namespace).
label(step,'doc',type,4).
no_internalrule(step,1,'doc',type).
no_internalrule(step,2,'doc',type).
no_internalrule(step,3,'doc',type).
no_internalrule(step,4,'doc',type).
no_internalrule(step,5,'doc',type).
no_internalrule(step,6,'doc',type).
no_internalrule(step,7,'doc',type).
no_internalrule(step,8,'doc',type).
no_internalrule(step,9,'doc',type).
no_internalrule(step,10,'doc',type).
no_internalrule(step,11,'doc',type).
no_internalrule(step,12,'doc',type).
no_internalrule(step,13,'doc',type).
no_internalrule(step,14,'doc',type).
no_internalrule(step,15,'doc',type).
no_internalrule(step,16,'doc',type).
no_internalrule(step,17,'doc',type).
no_internalrule(step,18,'doc',type).
no_internalrule(step,19,'doc',type).
no_internalrule(step,20,'doc',type).
no_internalrule(step,21,'doc',type).
no_internalrule(step,22,'doc',type).
no_internalrule(step,23,'doc',type).
no_internalrule(step,24,'doc',type).
no_internalrule(step,25,'doc',type).
no_internalrule(step,26,'doc',type).
no_internalrule(step,28,'doc',type).
no_internalrule(step,29,'doc',type).
no_internalrule(step,30,'doc',type).
no_internalrule(step,31,'doc',type).
no_internalrule(step,32,'doc',type).
no_internalrule(step,33,'doc',type).
label(step,'elem',type,5).
no_internalrule(step,1,'elem',type).
no_internalrule(step,2,'elem',type).
no_internalrule(step,3,'elem',type).
no_internalrule(step,4,'elem',type).
no_internalrule(step,5,'elem',type).
no_internalrule(step,6,'elem',type).
no_internalrule(step,7,'elem',type).
no_internalrule(step,8,'elem',type).
no_internalrule(step,9,'elem',type).
no_internalrule(step,10,'elem',type).
no_internalrule(step,11,'elem',type).
no_internalrule(step,12,'elem',type).
no_internalrule(step,13,'elem',type).
no_internalrule(step,14,'elem',type).
no_internalrule(step,15,'elem',type).
no_internalrule(step,16,'elem',type).
no_internalrule(step,17,'elem',type).
no_internalrule(step,18,'elem',type).
no_internalrule(step,19,'elem',type).
no_internalrule(step,20,'elem',type).
no_internalrule(step,21,'elem',type).
no_internalrule(step,22,'elem',type).
no_internalrule(step,23,'elem',type).
no_internalrule(step,24,'elem',type).
no_internalrule(step,25,'elem',type).
no_internalrule(step,26,'elem',type).
no_internalrule(step,28,'elem',type).
no_internalrule(step,29,'elem',type).
no_internalrule(step,30,'elem',type).
no_internalrule(step,31,'elem',type).
no_internalrule(step,32,'elem',type).
no_internalrule(step,33,'elem',type).
label(step,'x',var,6).
no_internalrule(step,1,'x',var).
no_internalrule(step,2,'x',var).
no_internalrule(step,3,'x',var).
no_internalrule(step,4,'x',var).
no_internalrule(step,5,'x',var).
no_internalrule(step,6,'x',var).
no_internalrule(step,7,'x',var).
no_internalrule(step,8,'x',var).
no_internalrule(step,9,'x',var).
no_internalrule(step,10,'x',var).
no_internalrule(step,12,'x',var).
no_internalrule(step,13,'x',var).
no_internalrule(step,14,'x',var).
no_internalrule(step,15,'x',var).
no_internalrule(step,17,'x',var).
no_internalrule(step,18,'x',var).
no_internalrule(step,22,'x',var).
no_internalrule(step,24,'x',var).
no_internalrule(step,25,'x',var).
no_internalrule(step,28,'x',var).
no_internalrule(step,29,'x',var).
no_internalrule(step,30,'x',var).
no_internalrule(step,31,'x',var).
no_internalrule(step,32,'x',var).
no_internalrule(step,33,'x',var).
max_label_id(step,6).
    %%% different origins %%%%%%%
    diff_orig(step,1,0).
	  diff_orig(step,2,0).
	  diff_orig(step,2,1).
	  diff_orig(step,4,0).
	  diff_orig(step,4,1).
	  diff_orig(step,4,2).
	  
