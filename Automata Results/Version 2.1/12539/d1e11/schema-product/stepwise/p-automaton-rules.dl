%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  intersection of stepwise hedge automata A and B
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% variables X,Y for states,
%%           L,T for labels and their types
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% hedge states
p_state(X,Y)   :- p_start_tree(X,Y).
p_state(X,Y)   :- p_initial(X,Y).
p_state(X1,Y1) :- p_elserule(X,Y,X1,Y1).
p_state(X1,Y1) :- p_typedelserule(X,Y,T,X1,Y1).
p_state(X1,Y1) :- p_typedelseArule(X,Y,T,X1,Y1).
p_state(X1,Y1) :- p_typedelseBrule(X,Y,T,X1,Y1).
p_state(X3,Y3) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyelserule(X1,Y1,X3,Y3).
p_state(X3,Y3) :- p_applyAelserule(X1,Y1,X3,Y3).
p_state(X3,Y3) :- p_applyBelserule(X1,Y1,X3,Y3).
p_state(X1,Y1) :- p_internalrule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_internalArule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_internalBrule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_epsilonArule(X,Y,X1,Y1).
p_state(X1,Y1) :- p_epsilonBrule(X,Y,X1,Y1).
p_state(IX,IY) :- p_tree_istate(X2,Y2,IX,IY).

%%% tree states that are copies of hedge states
p_copy_of(X1,X2,Z1,Z2) :- p_state(X1,X2), copy_of(A,X1,Z1), state(A,Z1),
                          copy_of(B,X2,Z2), state(B,Z2), inter(A,B,C).

%%% tree states and istates of tree states
p_tree_state(X2,Y2)        :- p_tree_istate(X2,Y2,IX,IY).
p_tree_istate(X2,Y2,IX,IY) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyelserule(X1,Y1,X3,Y3), 
                               tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).

p_tree_state(X2,Y2) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyelserule(X1,Y1,X3,Y3), tree_state(A,X2), tree_state(B,Y2), inter(A,B,C).  

p_tree_state(Z1,Z2) :- p_copy_of(X1,X2,Z1,Z2).
p_tree_state(U,V)   :- p_treerule(X,Y,U,V).

%%% distinguished states
p_initial(X,Y)    :- initial(A,X), initial(B,Y), inter(A,B,C).
p_else(X,Y)       :- p_tree_state(X,Y), else(A,X), else(B,Y),  inter(A,B,C).
p_elseA(X,Y)      :- else(A,X), p_tree_state(X,Y), inter(A,B,C).
p_elseB(X,Y)      :- p_tree_state(X,Y), else(B,Y), inter(A,B,C).
p_final(X,Y)      :- final(A,X), final(B,Y), p_state(X,Y), inter(A,B,C).
p_finalA(X,Y)      :- final(A,X), p_state(X,Y), inter(A,B,C).
p_finalB(X,Y)      :- final(B,Y), p_state(X,Y), inter(A,B,C).
p_start_tree(X,Y) :- start_tree(A,X), start_tree(B,Y), inter(A,B,C).
p_sinkA(X,Y)       :- sink(A,X), state(B,Y), inter(A,B,C).
p_sinkB(X,Y)       :- state(A,X), sink(B,Y), inter(A,B,C).
p_sink(X,Y)        :- p_sinkA(X,Y).
p_sink(X,Y)        :- p_sinkB(X,Y).

%% origins
%  p_originA_of(X,Y,X1,Y2)   :- origin_of(A,X,X1,X2), state(B,Y), inter(A,B,C).

%%% internalrules

internalfromelse(B,Y,L,T,Y1) :- elserule(B,Y,Y1), no_internalrule(B,Y,L,T).
internalfromelse(B,Y,L,T,Y1) :- typedelserule(B,Y,T,Y1), no_internalrule(B,Y,L,T).
internalfromelse(B,Y,L,T,Y1) :- elserule(B,Y,Y1), not_in_signature(B,L,T).
internalfromelse(B,Y,L,T,Y1) :- typedelserule(B,Y,T,Y1), not_in_signature(B,L,T).


% internal / else
p_internalArule(X,Y,L,T,X1,Y1) :- internalrule(A,X,L,T,X1), internalfromelse(B,Y,L,T,Y1),
                                  p_state(X,Y), inter(A,B,C).
% else / internal
p_internalBrule(X,Y,L,T,X1,Y1) :- internalfromelse(A,X,L,T,X1), internalrule(B,Y,L,T,Y1),
			       	  p_state(X,Y), inter(A,B,C).

% internal / internal
p_internalrule(X,Y,L,T,X1,Y1) :- internalrule(A,X,L,T,X1), internalrule(B,Y,L,T,Y1),
			      	 p_state(X,Y), inter(A,B,C).

%%% else rules typed and untyped

% else / else
p_elserule(X,Y,X1,Y1) :- elserule(A,X,X1), elserule(B,Y,Y1), p_state(X,Y), inter(A,B,C).

% typed_else / typed_else
p_typedelserule(X,Y,T,X1,Y1) :- typedelserule(A,X,T,X1), typedelserule(B,Y,T,Y1),
			     p_state(X,Y), inter(A,B,C).
% typed_else / else
p_typedelseArule(X,Y,T,X1,Y1) :- typedelserule(A,X,T,X1), elserule(B,Y,Y1),
			      p_state(X,Y), inter(A,B,C).

% else / typed_else
p_typedelseBrule(X,Y,T,X1,Y1) :- elserule(A,X,X1), typedelserule(B,Y,T,Y1),
			      p_state(X,Y), inter(A,B,C).


%%% applyrules

applyfromelse(B,Y1,Y2,Y3) :- else(B,Y2), no_applyrule(B,Y1,Y2), applyelserule(B,Y1,Y3).

% apply / apply
p_applyrule(X1,Y1,X2,Y2,X3,Y3) :- applyrule(A,X1,X2,X3), applyrule(B,Y1,Y2,Y3),
			          p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
% apply / applyelse
p_applyArule(X1,Y1,X2,Y2,X3,Y3) :- applyrule(A,X1,X2,X3), applyfromelse(B,Y1,Y2,Y3),
				p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
p_applyAelserule(X1,Y1,X2,else,X3,Y3) :- applyrule(A,X1,X2,X3), applyelserule(B,Y1,Y3),
				      p_state(X1,Y1), else(B,Y2),
				      p_tree_state(X2,Y2), inter(A,B,C).
% applyelse / apply
p_applyBrule(X1,Y1,X2,Y2,X3,Y3) :- applyfromelse(A,X1,X2,X3), applyrule(B,Y1,Y2,Y3),
				p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
p_applyBelserule(X1,Y1,else,Y2,X3,Y3) :- applyelserule(A,X1,X3), applyrule(B,Y1,Y2,Y3),
				      p_state(X1,Y1), else(A,Y1),
				      p_tree_state(X2,Y2), inter(A,B,C).
% applyelse / applyelse
p_applyelserule(X1,Y1,X3,Y3) :- applyelserule(A,X1,X3), applyelserule(B,Y1,Y3),
			     else(A,X2), no_applyrule(A,X1,X2),
			     else(B,Y2), no_applyrule(B,Y1,Y2),
			     p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).

% else states
p_else(X,Y) :-  else(A,X), else(B,Y), inter(A,B,C).

% tree / tree
p_treerule(X,Y,X1,Y1) :- treerule(A,X,X1), treerule(B,Y,Y1), p_state(X,Y), inter(A,B,C). 

% epsilon / skip
p_epsilonArule(X,Y,X1,Y) :- epsilonrule(A,X,X1), p_state(X,Y), inter(A,B,C).

% skip / epsilon
p_epsilonBrule(X,Y,X,Y1) :- epsilonrule(B,Y,Y1), p_state(X,Y), inter(A,B,C).

% difference of signature
no_internalrule(A,X,L,T) :- not_in_signature(A,L,T), state(A,X), inter(A,B,C).
no_internalrule(B,Y,L,T) :- not_in_signature(B,L,T), state(B,Y), inter(A,B,C).

% title
p_title(X,Y) :- title(A,X), title(B,Y), inter(A,B,C).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% co-accessibility
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p_state(co_acc,X,Y) :- p_final(X,Y).
p_state(co_acc,X,Y) :- p_treerule(X,Y,X1,Y1), p_tree_state(co_acc,X1,Y1).
p_state(co_acc,X,Y) :- p_applyelserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).

p_tree_state(co_acc,X2,Y2) :- p_applyelserule(co_acc,X,Y,X1,Y1),
			      p_state(co_acc,X1,Y1), p_else(X2,Y2).

p_state(co_acc,X1,Y1) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_state(co_acc,X1,Y1)      :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_state(co_acc,X1,Y1)      :- p_applyrule(X1,Y1,X2,Y2,X3,Y3),  p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3),  p_state(co_acc,X3,Y3).
p_state(co_acc,X,Y)   :- p_internalrule(X,Y,L,T,X1,Y1),  p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_internalArule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_internalBrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_epsilonArule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_epsilonBrule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_elserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_typedelseArule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_typedelseBrule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_typedelserule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).

p_initial(co_acc,X,Y) :- p_state(co_acc,X,Y), p_initial(X,Y).
p_final(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_final(X,Y).
p_finalA(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_finalA(X,Y).
p_finalB(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_finalB(X,Y).
p_sink(co_acc,X,Y)    :- p_state(co_acc,X,Y), p_sink(X,Y).
p_sinkA(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_sinkA(X,Y).
p_else(co_acc,X,Y)    :- p_tree_state(co_acc,X,Y), p_else(X,Y).
p_elseA(co_acc,X,Y)   :- p_tree_state(co_acc,X,Y), p_elseA(X,Y).
p_elseB(co_acc,X,Y)   :- p_tree_state(co_acc,X,Y), p_elseB(X,Y).
p_start_tree(co_acc,X,Y) :- p_state(co_acc,X,Y), p_start_tree(X,Y).

p_copy_of(co_acc,X,Y,X1,Y1)     :- p_copy_of(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_tree_istate(co_acc,X,Y,X1,Y1) :- p_tree_istate(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_internalrule(co_acc,X,Y,L,T,X1,Y1)  :- p_internalrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_internalArule(co_acc,X,Y,L,T,X1,Y1) :- p_internalArule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_internalBrule(co_acc,X,Y,L,T,X1,Y1) :- p_internalBrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_epsilonArule(co_acc,X,Y,X1,Y1) :- p_epsilonArule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_epsilonBrule(co_acc,X,Y,X1,Y1) :- p_epsilonBrule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).

p_treerule(co_acc,X,Y,X1,Y1) :- p_treerule(X,Y,X1,Y1), p_tree_state(co_acc,X1,Y1).	
p_elserule(co_acc,X,Y,X1,Y1) :- p_elserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelserule(co_acc,X,Y,T,X1,Y1) :- p_typedelserule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelseArule(co_acc,X,Y,T,X1,Y1) :- p_typedelseArule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelseBrule(co_acc,X,Y,T,X1,Y1) :- p_typedelseBrule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_applyrule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyArule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyArule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyBrule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyBrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).

p_applyelserule(co_acc,X,Y,X2,Y2) :- p_applyelserule(X,Y,X2,Y2), p_state(co_acc,X2,Y2).
p_applyAelserule(co_acc,X,Y,X1,Y1,X2,Y2) :- p_applyAelserule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyelseBrule(co_acc,X,Y,X1,Y1,X2,Y2) :- p_applyelseBrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).


n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalrule(X1,Y1,L,T,X,Y).

n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalBrule(X1,Y1,L,T,X,Y).

n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalArule(X1,Y1,L,T,X,Y).

p_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- n_internalrule(co_acc,X1,Y1,L,T,X,Y).
p_internalArule(co_acc,X1,Y1,L,T,X,Y) :- n_internalArule(co_acc,X1,Y1,L,T,X,Y).
p_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- n_internalBrule(co_acc,X1,Y1,L,T,X,Y).

needed_p_state(co_acc,X,Y)            :- n_internalrule(co_acc,X1,Y1,L,T,X,Y). 
needed_p_stateA(co_acc,X,Y)           :- n_internalArule(co_acc,X1,Y1,L,T,X,Y).
needed_p_state (co_acc,X,Y)           :- n_internalBrule(co_acc,X1,Y1,L,T,X,Y).

p_originA(X,U,Y,Z) :- p_state(X,U),      origin_of(A,X,Y,Z), inter(A,B,C).
p_originA(X,U,Y,Z) :- p_tree_state(X,U), origin_of(A,X,Y,Z), inter(A,B,C).
p_labelA(L,T,Id)   :- label(A,L,T,Id), inter(A,B,C).
p_labelB(L,T,Id)   :- label(B,L,T,Id), inter(A,B,C).
p_max_label_idA(N)   :- max_label_id(A,N), inter(A,B,C).
p_max_label_idB(N)   :- max_label_id(B,N), inter(A,B,C).

p_originA(co_acc,X,U,Y,Z) :- p_originA(X,U,Y,Z), p_state(co_acc,X,U).
p_originA(co_acc,X,U,Y,Z) :- p_originA(X,U,Y,Z), p_tree_state(co_acc,X,U).
p_labelA(co_acc,L,T,Id)   :- p_labelA(L,T,Id).
p_labelB(co_acc,L,T,Id)   :- p_labelB(L,T,Id).
p_max_label_idA(co_acc,N)   :- p_max_label_idA(N).
p_max_label_idB(co_acc,N)   :- p_max_label_idB(N).


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXPRESSION=d1e11 AUTOMATON=././acc-clean/stepwise
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule

% negative information only for elserules and applyelserules (NEGATIVE=false)
%   no_applyrule
%   no_internalrule
  
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(auto1,'step[&lt;doc⋅_⋅T&gt;]').
expression(auto1,'d1e11').
no_typedelserule(auto1,2,type).
no_typedelserule(auto1,3,type).
no_typedelserule(auto1,6,type).
no_typedelserule(auto1,9,type).
no_typedelserule(auto1,11,type).
state(auto1,2).
state(auto1,3).
state(auto1,4).
state(auto1,5).
state(auto1,6).
state(auto1,7).
state(auto1,9).
state(auto1,11).
start_tree(auto1,3).
start_tree(auto1,5).
initial(auto1,9).
not_sink(auto1,2).
not_sink(auto1,3).
not_sink(auto1,4).
not_sink(auto1,5).
not_sink(auto1,6).
not_sink(auto1,7).
not_sink(auto1,9).
not_sink(auto1,11).
else(auto1,6).
nonelse(auto1,2).
nonelse(auto1,3).
nonelse(auto1,4).
nonelse(auto1,5).
nonelse(auto1,7).
nonelse(auto1,9).
nonelse(auto1,11).
final(auto1,11).
nonfinal(auto1,2).
nonfinal(auto1,3).
nonfinal(auto1,4).
nonfinal(auto1,5).
nonfinal(auto1,6).
nonfinal(auto1,7).
nonfinal(auto1,9).
copy_of(auto1,2,2).
copy_of(auto1,3,2).
copy_of(auto1,4,2).
copy_of(auto1,5,6).
copy_of(auto1,6,6).
copy_of(auto1,7,2).
tree_state(auto1,2).
tree_istate(auto1,2,3).
 tree_state(auto1,6).
tree_istate(auto1,6,5).
 hedge_state(auto1,3).
    hedge_state(auto1,4).
    hedge_state(auto1,5).
    hedge_state(auto1,7).
    hedge_state(auto1,9).
    hedge_state(auto1,11).
    applyrule(auto1,9,2,11).
internalrule(auto1,3,'doc',type,4).
applyelserule(auto1,7,7).
no_applyrule(auto1,7,6).
no_applyrule(auto1,7,2).
elserule(auto1,7,7).
applyelserule(auto1,5,5).
no_applyrule(auto1,5,6).
no_applyrule(auto1,5,2).
elserule(auto1,5,5).
treerule(auto1,5,6).
elserule(auto1,4,7).
treerule(auto1,7,2).
label(auto1,'doc',type,1).
no_internalrule(auto1,4,'doc',type).
no_internalrule(auto1,5,'doc',type).
no_internalrule(auto1,7,'doc',type).
max_label_id(auto1,1).
    %%% different origins %%%%%%%
    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXPRESSION=and[xml-document][one-x] AUTOMATON=det-stepwise
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule

% negative information only for elserules and applyelserules (NEGATIVE=false)
%   no_applyrule
%   no_internalrule
  
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(auto2,'det[stepwise[]]').
var(auto2,'x',1).
no_typedelserule(auto2,23,type).
no_typedelserule(auto2,15,type).
no_typedelserule(auto2,4,type).
no_typedelserule(auto2,6,type).
no_typedelserule(auto2,17,type).
no_typedelserule(auto2,21,type).
no_typedelserule(auto2,10,type).
no_typedelserule(auto2,27,type).
no_typedelserule(auto2,5,type).
no_typedelserule(auto2,20,type).
no_typedelserule(auto2,28,type).
no_typedelserule(auto2,29,type).
no_typedelserule(auto2,9,type).
no_typedelserule(auto2,12,type).
no_typedelserule(auto2,24,type).
no_typedelserule(auto2,7,type).
no_typedelserule(auto2,2,type).
no_typedelserule(auto2,18,type).
no_typedelserule(auto2,0,type).
no_typedelserule(auto2,26,type).
no_typedelserule(auto2,11,type).
no_typedelserule(auto2,22,type).
no_typedelserule(auto2,14,type).
no_typedelserule(auto2,16,type).
no_typedelserule(auto2,8,type).
no_typedelserule(auto2,13,type).
no_typedelserule(auto2,25,type).
no_typedelserule(auto2,19,type).
no_typedelserule(auto2,1,type).
no_typedelserule(auto2,3,type).
no_typedelserule(auto2,23,negvar).
no_typedelserule(auto2,15,negvar).
no_typedelserule(auto2,4,negvar).
no_typedelserule(auto2,6,negvar).
no_typedelserule(auto2,17,negvar).
no_typedelserule(auto2,21,negvar).
no_typedelserule(auto2,10,negvar).
no_typedelserule(auto2,27,negvar).
no_typedelserule(auto2,5,negvar).
no_typedelserule(auto2,20,negvar).
no_typedelserule(auto2,28,negvar).
no_typedelserule(auto2,29,negvar).
no_typedelserule(auto2,9,negvar).
no_typedelserule(auto2,12,negvar).
no_typedelserule(auto2,24,negvar).
no_typedelserule(auto2,7,negvar).
no_typedelserule(auto2,2,negvar).
no_typedelserule(auto2,18,negvar).
no_typedelserule(auto2,0,negvar).
no_typedelserule(auto2,26,negvar).
no_typedelserule(auto2,11,negvar).
no_typedelserule(auto2,22,negvar).
no_typedelserule(auto2,14,negvar).
no_typedelserule(auto2,16,negvar).
no_typedelserule(auto2,8,negvar).
no_typedelserule(auto2,13,negvar).
no_typedelserule(auto2,25,negvar).
no_typedelserule(auto2,19,negvar).
no_typedelserule(auto2,1,negvar).
no_typedelserule(auto2,3,negvar).
no_typedelserule(auto2,23,var).
no_typedelserule(auto2,15,var).
no_typedelserule(auto2,4,var).
no_typedelserule(auto2,6,var).
no_typedelserule(auto2,17,var).
no_typedelserule(auto2,21,var).
no_typedelserule(auto2,10,var).
no_typedelserule(auto2,27,var).
no_typedelserule(auto2,5,var).
no_typedelserule(auto2,20,var).
no_typedelserule(auto2,28,var).
no_typedelserule(auto2,29,var).
no_typedelserule(auto2,9,var).
no_typedelserule(auto2,12,var).
no_typedelserule(auto2,24,var).
no_typedelserule(auto2,7,var).
no_typedelserule(auto2,2,var).
no_typedelserule(auto2,18,var).
no_typedelserule(auto2,0,var).
no_typedelserule(auto2,26,var).
no_typedelserule(auto2,11,var).
no_typedelserule(auto2,22,var).
no_typedelserule(auto2,14,var).
no_typedelserule(auto2,16,var).
no_typedelserule(auto2,8,var).
no_typedelserule(auto2,13,var).
no_typedelserule(auto2,25,var).
no_typedelserule(auto2,19,var).
no_typedelserule(auto2,1,var).
no_typedelserule(auto2,3,var).
no_typedelserule(auto2,23,char).
no_typedelserule(auto2,15,char).
no_typedelserule(auto2,4,char).
no_typedelserule(auto2,6,char).
no_typedelserule(auto2,21,char).
no_typedelserule(auto2,10,char).
no_typedelserule(auto2,27,char).
no_typedelserule(auto2,5,char).
no_typedelserule(auto2,20,char).
no_typedelserule(auto2,28,char).
no_typedelserule(auto2,29,char).
no_typedelserule(auto2,9,char).
no_typedelserule(auto2,12,char).
no_typedelserule(auto2,24,char).
no_typedelserule(auto2,2,char).
no_typedelserule(auto2,0,char).
no_typedelserule(auto2,26,char).
no_typedelserule(auto2,11,char).
no_typedelserule(auto2,22,char).
no_typedelserule(auto2,14,char).
no_typedelserule(auto2,16,char).
no_typedelserule(auto2,13,char).
no_typedelserule(auto2,25,char).
no_typedelserule(auto2,19,char).
no_typedelserule(auto2,1,char).
no_typedelserule(auto2,3,char).
no_typedelserule(auto2,23,namespace).
no_typedelserule(auto2,15,namespace).
no_typedelserule(auto2,6,namespace).
no_typedelserule(auto2,17,namespace).
no_typedelserule(auto2,21,namespace).
no_typedelserule(auto2,10,namespace).
no_typedelserule(auto2,27,namespace).
no_typedelserule(auto2,5,namespace).
no_typedelserule(auto2,20,namespace).
no_typedelserule(auto2,28,namespace).
no_typedelserule(auto2,29,namespace).
no_typedelserule(auto2,9,namespace).
no_typedelserule(auto2,12,namespace).
no_typedelserule(auto2,24,namespace).
no_typedelserule(auto2,7,namespace).
no_typedelserule(auto2,2,namespace).
no_typedelserule(auto2,18,namespace).
no_typedelserule(auto2,0,namespace).
no_typedelserule(auto2,26,namespace).
no_typedelserule(auto2,11,namespace).
no_typedelserule(auto2,22,namespace).
no_typedelserule(auto2,14,namespace).
no_typedelserule(auto2,16,namespace).
no_typedelserule(auto2,8,namespace).
no_typedelserule(auto2,13,namespace).
no_typedelserule(auto2,25,namespace).
no_typedelserule(auto2,19,namespace).
no_typedelserule(auto2,1,namespace).
no_typedelserule(auto2,23,name).
no_typedelserule(auto2,15,name).
no_typedelserule(auto2,4,name).
no_typedelserule(auto2,6,name).
no_typedelserule(auto2,17,name).
no_typedelserule(auto2,21,name).
no_typedelserule(auto2,27,name).
no_typedelserule(auto2,5,name).
no_typedelserule(auto2,20,name).
no_typedelserule(auto2,28,name).
no_typedelserule(auto2,29,name).
no_typedelserule(auto2,12,name).
no_typedelserule(auto2,24,name).
no_typedelserule(auto2,7,name).
no_typedelserule(auto2,2,name).
no_typedelserule(auto2,18,name).
no_typedelserule(auto2,0,name).
no_typedelserule(auto2,26,name).
no_typedelserule(auto2,11,name).
no_typedelserule(auto2,22,name).
no_typedelserule(auto2,14,name).
no_typedelserule(auto2,16,name).
no_typedelserule(auto2,8,name).
no_typedelserule(auto2,13,name).
no_typedelserule(auto2,25,name).
no_typedelserule(auto2,19,name).
no_typedelserule(auto2,1,name).
no_typedelserule(auto2,3,name).
state(auto2,23).
state(auto2,15).
state(auto2,4).
state(auto2,6).
state(auto2,17).
state(auto2,21).
state(auto2,10).
state(auto2,27).
state(auto2,5).
state(auto2,20).
state(auto2,28).
state(auto2,29).
state(auto2,9).
state(auto2,12).
state(auto2,24).
state(auto2,7).
state(auto2,2).
state(auto2,18).
state(auto2,0).
state(auto2,26).
state(auto2,11).
state(auto2,22).
state(auto2,14).
state(auto2,16).
state(auto2,8).
state(auto2,13).
state(auto2,25).
state(auto2,19).
state(auto2,1).
state(auto2,3).
start_tree(auto2,0).
initial(auto2,0).
origin_of(auto2,23,'22','4').
origin_of(auto2,15,'17','1').
origin_of(auto2,4,'5','0').
origin_of(auto2,6,'6','0').
origin_of(auto2,17,'18','1').
origin_of(auto2,21,'19','2').
origin_of(auto2,10,'10','0').
origin_of(auto2,27,'26','0').
origin_of(auto2,5,'6','1').
origin_of(auto2,20,'20','1').
origin_of(auto2,28,'28','4').
origin_of(auto2,29,'29','1').
origin_of(auto2,9,'8','0').
origin_of(auto2,12,'11','2').
origin_of(auto2,24,'22','2').
origin_of(auto2,7,'7','1').
origin_of(auto2,2,'2','0').
origin_of(auto2,18,'18','0').
origin_of(auto2,0,'0','0').
origin_of(auto2,26,'25','1').
origin_of(auto2,11,'11','4').
origin_of(auto2,22,'20','0').
origin_of(auto2,14,'16','0').
origin_of(auto2,16,'17','0').
origin_of(auto2,8,'7','0').
origin_of(auto2,13,'13','0').
origin_of(auto2,25,'26','1').
origin_of(auto2,19,'19','4').
origin_of(auto2,1,'1','0').
origin_of(auto2,3,'3','0').
not_sink(auto2,23).
not_sink(auto2,15).
not_sink(auto2,4).
not_sink(auto2,6).
not_sink(auto2,17).
not_sink(auto2,21).
not_sink(auto2,10).
not_sink(auto2,27).
not_sink(auto2,5).
not_sink(auto2,20).
not_sink(auto2,28).
not_sink(auto2,29).
not_sink(auto2,9).
not_sink(auto2,12).
not_sink(auto2,24).
not_sink(auto2,7).
not_sink(auto2,2).
not_sink(auto2,18).
not_sink(auto2,0).
not_sink(auto2,26).
not_sink(auto2,11).
not_sink(auto2,22).
not_sink(auto2,14).
not_sink(auto2,16).
not_sink(auto2,8).
not_sink(auto2,13).
not_sink(auto2,25).
not_sink(auto2,19).
not_sink(auto2,1).
not_sink(auto2,3).
nonelse(auto2,23).
nonelse(auto2,15).
nonelse(auto2,4).
nonelse(auto2,6).
nonelse(auto2,17).
nonelse(auto2,21).
nonelse(auto2,10).
nonelse(auto2,27).
nonelse(auto2,5).
nonelse(auto2,20).
nonelse(auto2,28).
nonelse(auto2,29).
nonelse(auto2,9).
nonelse(auto2,12).
nonelse(auto2,24).
nonelse(auto2,7).
nonelse(auto2,2).
nonelse(auto2,18).
nonelse(auto2,0).
nonelse(auto2,26).
nonelse(auto2,11).
nonelse(auto2,22).
nonelse(auto2,14).
nonelse(auto2,16).
nonelse(auto2,8).
nonelse(auto2,13).
nonelse(auto2,25).
nonelse(auto2,19).
nonelse(auto2,1).
nonelse(auto2,3).
final(auto2,29).
final(auto2,25).
nonfinal(auto2,23).
nonfinal(auto2,15).
nonfinal(auto2,4).
nonfinal(auto2,6).
nonfinal(auto2,17).
nonfinal(auto2,21).
nonfinal(auto2,10).
nonfinal(auto2,27).
nonfinal(auto2,5).
nonfinal(auto2,20).
nonfinal(auto2,28).
nonfinal(auto2,9).
nonfinal(auto2,12).
nonfinal(auto2,24).
nonfinal(auto2,7).
nonfinal(auto2,2).
nonfinal(auto2,18).
nonfinal(auto2,0).
nonfinal(auto2,26).
nonfinal(auto2,11).
nonfinal(auto2,22).
nonfinal(auto2,14).
nonfinal(auto2,16).
nonfinal(auto2,8).
nonfinal(auto2,13).
nonfinal(auto2,19).
nonfinal(auto2,1).
nonfinal(auto2,3).
tree_state(auto2,23).
tree_istate(auto2,23,0).
 tree_state(auto2,21).
tree_istate(auto2,21,0).
 tree_state(auto2,28).
tree_istate(auto2,28,0).
 tree_state(auto2,12).
tree_istate(auto2,12,0).
 tree_state(auto2,24).
tree_istate(auto2,24,0).
 tree_state(auto2,11).
tree_istate(auto2,11,0).
 tree_state(auto2,19).
tree_istate(auto2,19,0).
 hedge_state(auto2,15).
    hedge_state(auto2,4).
    hedge_state(auto2,6).
    hedge_state(auto2,17).
    hedge_state(auto2,10).
    hedge_state(auto2,27).
    hedge_state(auto2,5).
    hedge_state(auto2,20).
    hedge_state(auto2,29).
    hedge_state(auto2,9).
    hedge_state(auto2,7).
    hedge_state(auto2,2).
    hedge_state(auto2,18).
    hedge_state(auto2,0).
    hedge_state(auto2,26).
    hedge_state(auto2,22).
    hedge_state(auto2,14).
    hedge_state(auto2,16).
    hedge_state(auto2,8).
    hedge_state(auto2,13).
    hedge_state(auto2,25).
    hedge_state(auto2,1).
    hedge_state(auto2,3).
    applyrule(auto2,22,11,20).
internalrule(auto2,0,'elem',type,3).
applyrule(auto2,16,24,16).
internalrule(auto2,0,'doc',type,1).
applyrule(auto2,16,21,22).
applyrule(auto2,27,21,27).
applyrule(auto2,16,12,22).
typedelserule(auto2,8,char,8).
typedelserule(auto2,4,namespace,10).
applyrule(auto2,25,21,25).
typedelserule(auto2,17,char,17).
typedelserule(auto2,18,char,18).
typedelserule(auto2,7,char,7).
typedelserule(auto2,10,name,14).
internalrule(auto2,2,'x',negvar,8).
applyrule(auto2,20,12,20).
typedelserule(auto2,9,name,13).
treerule(auto2,16,21).
applyrule(auto2,15,24,15).
applyrule(auto2,0,19,25).
internalrule(auto2,14,'x',negvar,18).
typedelserule(auto2,3,namespace,9).
treerule(auto2,8,12).
internalrule(auto2,1,'x',negvar,6).
applyrule(auto2,22,21,22).
treerule(auto2,22,21).
treerule(auto2,18,24).
applyrule(auto2,27,19,25).
internalrule(auto2,0,'text',type,2).
internalrule(auto2,1,'x',var,5).
applyrule(auto2,6,19,26).
treerule(auto2,7,11).
applyrule(auto2,22,19,20).
treerule(auto2,20,19).
treerule(auto2,17,23).
applyrule(auto2,0,28,29).
internalrule(auto2,14,'x',var,17).
internalrule(auto2,2,'x',var,7).
internalrule(auto2,0,'comment',type,2).
applyrule(auto2,16,19,20).
applyrule(auto2,5,21,26).
applyrule(auto2,15,21,20).
internalrule(auto2,0,'att',type,4).
treerule(auto2,26,28).
treerule(auto2,15,19).
internalrule(auto2,13,'x',negvar,16).
applyrule(auto2,16,11,20).
internalrule(auto2,13,'x',var,15).
applyrule(auto2,15,12,20).
applyrule(auto2,0,21,27).
applyrule(auto2,16,23,15).
applyrule(auto2,22,12,22).
applyrule(auto2,20,21,20).
label(auto2,'text',type,5).
no_internalrule(auto2,4,'text',type).
no_internalrule(auto2,17,'text',type).
no_internalrule(auto2,10,'text',type).
no_internalrule(auto2,9,'text',type).
no_internalrule(auto2,7,'text',type).
no_internalrule(auto2,18,'text',type).
no_internalrule(auto2,8,'text',type).
no_internalrule(auto2,3,'text',type).
label(auto2,'doc',type,3).
no_internalrule(auto2,4,'doc',type).
no_internalrule(auto2,17,'doc',type).
no_internalrule(auto2,10,'doc',type).
no_internalrule(auto2,9,'doc',type).
no_internalrule(auto2,7,'doc',type).
no_internalrule(auto2,18,'doc',type).
no_internalrule(auto2,8,'doc',type).
no_internalrule(auto2,3,'doc',type).
label(auto2,'x',negvar,6).
no_internalrule(auto2,4,'x',negvar).
no_internalrule(auto2,17,'x',negvar).
no_internalrule(auto2,10,'x',negvar).
no_internalrule(auto2,9,'x',negvar).
no_internalrule(auto2,7,'x',negvar).
no_internalrule(auto2,18,'x',negvar).
no_internalrule(auto2,8,'x',negvar).
no_internalrule(auto2,3,'x',negvar).
label(auto2,'x',var,7).
no_internalrule(auto2,4,'x',var).
no_internalrule(auto2,17,'x',var).
no_internalrule(auto2,10,'x',var).
no_internalrule(auto2,9,'x',var).
no_internalrule(auto2,7,'x',var).
no_internalrule(auto2,18,'x',var).
no_internalrule(auto2,8,'x',var).
no_internalrule(auto2,3,'x',var).
label(auto2,'comment',type,2).
no_internalrule(auto2,4,'comment',type).
no_internalrule(auto2,17,'comment',type).
no_internalrule(auto2,10,'comment',type).
no_internalrule(auto2,9,'comment',type).
no_internalrule(auto2,7,'comment',type).
no_internalrule(auto2,18,'comment',type).
no_internalrule(auto2,8,'comment',type).
no_internalrule(auto2,3,'comment',type).
label(auto2,'att',type,1).
no_internalrule(auto2,4,'att',type).
no_internalrule(auto2,17,'att',type).
no_internalrule(auto2,10,'att',type).
no_internalrule(auto2,9,'att',type).
no_internalrule(auto2,7,'att',type).
no_internalrule(auto2,18,'att',type).
no_internalrule(auto2,8,'att',type).
no_internalrule(auto2,3,'att',type).
label(auto2,'elem',type,4).
no_internalrule(auto2,4,'elem',type).
no_internalrule(auto2,17,'elem',type).
no_internalrule(auto2,10,'elem',type).
no_internalrule(auto2,9,'elem',type).
no_internalrule(auto2,7,'elem',type).
no_internalrule(auto2,18,'elem',type).
no_internalrule(auto2,8,'elem',type).
no_internalrule(auto2,3,'elem',type).
max_label_id(auto2,7).
    %%% different origins %%%%%%%
    diff_orig(auto2,1,0).
	  diff_orig(auto2,2,0).
	  diff_orig(auto2,2,1).
	  diff_orig(auto2,4,0).
	  diff_orig(auto2,4,1).
	  diff_orig(auto2,4,2).
	  %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% not in signature

      %%%%% difference of signatures %%%%%%%%%%%
      
    
not_in_signature(auto1,'text',type).
not_in_signature(auto1,'x',negvar).
not_in_signature(auto1,'x',var).
not_in_signature(auto1,'comment',type).
not_in_signature(auto1,'att',type).
not_in_signature(auto1,'elem',type).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% intersection auto=auto1.auto2
inter(auto1,auto2,auto).
