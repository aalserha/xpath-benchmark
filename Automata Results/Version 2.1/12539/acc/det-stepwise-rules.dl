%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Accessible states of stepwise hedge automata A
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% whether variable is allowed or not

kind(0).
kind(1).

%% whether we started a tree or a hedge

start(t).
start(h).

%% types different from type var


nonvar(type).
nonvar(char).
nonvar(negvar).
nonvar(name).
nonvar(namespace).

%% starting trees and hedges

acc(t,0,A,X) :- start_tree(A,X).
acc(h,0,A,X) :- initial(A,X).

%% ending trees

acc_tree_state(K,A,Z) :- acc(t,K,A,X), treerule(A,X,Z).
acc_tree_state(K,A,Y) :- acc_tree_state(K,A,X), is(A,X,Y).

%% internal rules

internalrule_all(A,X,L,T,Y) :- internalrule(A,X,L,T,Y).
internalrule_all(A,X,L,T,Y) :- elserule(A,X,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,L,T,Y) :- typedelserule(A,X,T,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- elserule(A,X,Y), typedelserule(A,L,T,Z).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- typedelserule(A,X,T,Y).

acc(S,K,A,Y) :- acc(S,K,A,X), internalrule_all(A,X,L,T  ,Y), nonvar(T).
acc(S,1,A,Y) :- acc(S,0,A,X), internalrule_all(A,X,L,var,Y).

%% epsilon rules

acc(S,K,A,Y) :- acc(S,K,A,X), epsilonrule(A,X,Y), kind(K).

%% apply rules

applyrule_all(A,X,Z,Y) :- applyrule(A,X,Z,Y).
applyrule_all(A,X,Z,Y) :- applyelserule(A,X,Y), no_applyrule(A,X,Z).

acc(S,K,A,Y) :- acc(S,K,A,X), acc_tree_state(0,A,Z), applyrule_all(A,X,Z,Y).
acc(S,1,A,Y) :- acc(S,0,A,X), acc_tree_state(1,A,Z), applyrule_all(A,X,Z,Y).

%% compatibility with previous predicates

acc_h(A,X) :- acc(h,1,A,X).
acc_t(A,X) :- acc(t,1,A,X).




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% label states where XML labels end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

internal_or_else(A,X,Y) :- internalrule(A,X,L,T,Y).
internal_or_else(A,X,Y) :- elserule(A,X,Y).
internal_or_else(A,X,Y) :- typedelserule(A,X,T,Y).

label_state(A,U) :- initial(A,X), internalrule(A,X,'elem',type,Y),internal_or_else(A,Y,Z),
		    internal_or_else(A,Z,U).
label_state(A,U) :- initial(A,X), internalrule(A,X,'att',type,Y), internal_or_else(A,Y,Z),
                    internal_or_else(A,Z,U).
label_state(A,Y) :- initial(A,X), internalrule(A,X,'text',type,Y). 
label_state(A,Y) :- initial(A,X), internalrule(A,X,'comment',type,Y). 
label_state(A,Y) :- initial(A,X), internalrule(A,X,'doc',type,Y). 

acc_label(A,X,X) :- label_state(A,X).
acc_label(A,X,Z) :- acc_label(A,X,Y), internalrule(A,Y,L,T,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), elserule(A,Y,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), typedelserule(A,Y,T,Z).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(A,Z), applyrule(A,X1,Z,Y).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(A,Z), applyelserule(A,X1,Y), no_applyrule(A,X1,Z).

acc_label_tree(A,X,Z) :- acc_label(A,X,Y), treerule(A,Y,Z).


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule
% negative information (NEGATIVE=true)
%   no_applyrule
%   no_elserule
%   no_treerule
%   no_internalrule
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(stepwise,'mini(det_and[xml-document][one-x][schema-clean[pc[step[(stepwise[12539:d1e9]+stepwise[12539:d1e106])]]]])').
xpath(stepwise,'//tei:elementSpec|//tei:classSpec[@type=_pos_Zatts_pos_Z]').
var(stepwise,'x',1).
no_typedelserule(stepwise,16,var).
no_typedelserule(stepwise,15,var).
no_typedelserule(stepwise,14,var).
no_typedelserule(stepwise,30,var).
no_typedelserule(stepwise,19,var).
no_typedelserule(stepwise,18,var).
no_typedelserule(stepwise,21,var).
no_typedelserule(stepwise,24,var).
no_typedelserule(stepwise,22,var).
no_typedelserule(stepwise,8,var).
no_typedelserule(stepwise,27,var).
no_typedelserule(stepwise,16,namespace).
no_typedelserule(stepwise,15,namespace).
no_typedelserule(stepwise,14,namespace).
no_typedelserule(stepwise,30,namespace).
no_typedelserule(stepwise,19,namespace).
no_typedelserule(stepwise,18,namespace).
no_typedelserule(stepwise,21,namespace).
no_typedelserule(stepwise,24,namespace).
no_typedelserule(stepwise,22,namespace).
no_typedelserule(stepwise,8,namespace).
no_typedelserule(stepwise,27,namespace).
no_typedelserule(stepwise,16,char).
no_typedelserule(stepwise,15,char).
no_typedelserule(stepwise,14,char).
no_typedelserule(stepwise,30,char).
no_typedelserule(stepwise,19,char).
no_typedelserule(stepwise,18,char).
no_typedelserule(stepwise,21,char).
no_typedelserule(stepwise,24,char).
no_typedelserule(stepwise,22,char).
no_typedelserule(stepwise,8,char).
no_typedelserule(stepwise,27,char).
no_typedelserule(stepwise,16,type).
no_typedelserule(stepwise,15,type).
no_typedelserule(stepwise,14,type).
no_typedelserule(stepwise,30,type).
no_typedelserule(stepwise,19,type).
no_typedelserule(stepwise,18,type).
no_typedelserule(stepwise,21,type).
no_typedelserule(stepwise,24,type).
no_typedelserule(stepwise,22,type).
no_typedelserule(stepwise,8,type).
no_typedelserule(stepwise,27,type).
no_typedelserule(stepwise,16,name).
no_typedelserule(stepwise,15,name).
no_typedelserule(stepwise,14,name).
no_typedelserule(stepwise,30,name).
no_typedelserule(stepwise,19,name).
no_typedelserule(stepwise,18,name).
no_typedelserule(stepwise,21,name).
no_typedelserule(stepwise,24,name).
no_typedelserule(stepwise,22,name).
no_typedelserule(stepwise,8,name).
no_typedelserule(stepwise,27,name).
no_typedelserule(stepwise,16,negvar).
no_typedelserule(stepwise,15,negvar).
no_typedelserule(stepwise,14,negvar).
no_typedelserule(stepwise,30,negvar).
no_typedelserule(stepwise,19,negvar).
no_typedelserule(stepwise,18,negvar).
no_typedelserule(stepwise,21,negvar).
no_typedelserule(stepwise,24,negvar).
no_typedelserule(stepwise,22,negvar).
no_typedelserule(stepwise,8,negvar).
no_typedelserule(stepwise,27,negvar).
state(stepwise,20).
state(stepwise,5).
state(stepwise,16).
state(stepwise,15).
state(stepwise,17).
state(stepwise,14).
state(stepwise,4).
state(stepwise,9).
state(stepwise,30).
state(stepwise,13).
state(stepwise,26).
state(stepwise,7).
state(stepwise,19).
state(stepwise,23).
state(stepwise,2).
state(stepwise,6).
state(stepwise,12).
state(stepwise,18).
state(stepwise,21).
state(stepwise,3).
state(stepwise,24).
state(stepwise,22).
state(stepwise,1).
state(stepwise,8).
state(stepwise,11).
state(stepwise,10).
state(stepwise,0).
state(stepwise,27).
start_tree(stepwise,0).
initial(stepwise,0).
origin_of(stepwise,20,'18','51','17','18').
origin_of(stepwise,5,'18','49','10').
origin_of(stepwise,16,'18','31','69','27','16','62','22','25','58','56','15','20').
origin_of(stepwise,15,'18','31','69','27','62','22','25','58','56','16','15','20','22').
origin_of(stepwise,17,'18','68','17','18').
origin_of(stepwise,14,'18','31','69','27','73','62','65','22','25','58','56','15','20').
origin_of(stepwise,4,'18','10','14','27','26','7','29','18','8','25','2','17').
origin_of(stepwise,9,'18','55','14').
origin_of(stepwise,30,'6','37','23','24').
origin_of(stepwise,13,'18','66','17','18').
origin_of(stepwise,26,'18','60','17','18').
origin_of(stepwise,7,'18','33','71','9').
origin_of(stepwise,19,'6','5','19').
origin_of(stepwise,23,'18','76','17','18').
origin_of(stepwise,2,'18','52','17','63','19','3').
origin_of(stepwise,6,'18','33','23','57','71','9').
origin_of(stepwise,12,'18','72','28','13').
origin_of(stepwise,18,'6','23','12','24','28','19','21','11').
origin_of(stepwise,21,'18','14','12','26').
origin_of(stepwise,3,'18','48','15','1').
origin_of(stepwise,24,'6','7','28').
origin_of(stepwise,22,'31','69','27','22','25','18','29','58','56','62','32','20').
origin_of(stepwise,1,'18','64','4').
origin_of(stepwise,8,'9','44','43','18','42','8','13','5','6').
origin_of(stepwise,11,'18','72','28','20','13').
origin_of(stepwise,10,'18','72','53','28','13').
origin_of(stepwise,0,'18','21','59','24','35','36','78','79','61','67','0').
origin_of(stepwise,27,'18','34','29').
not_sink(stepwise,20).
not_sink(stepwise,5).
not_sink(stepwise,16).
not_sink(stepwise,15).
not_sink(stepwise,17).
not_sink(stepwise,14).
not_sink(stepwise,4).
not_sink(stepwise,9).
not_sink(stepwise,30).
not_sink(stepwise,13).
not_sink(stepwise,26).
not_sink(stepwise,7).
not_sink(stepwise,19).
not_sink(stepwise,23).
not_sink(stepwise,2).
not_sink(stepwise,6).
not_sink(stepwise,12).
not_sink(stepwise,18).
not_sink(stepwise,21).
not_sink(stepwise,3).
not_sink(stepwise,24).
not_sink(stepwise,22).
not_sink(stepwise,1).
not_sink(stepwise,8).
not_sink(stepwise,11).
not_sink(stepwise,10).
not_sink(stepwise,0).
not_sink(stepwise,27).
else(stepwise,30).
else(stepwise,19).
else(stepwise,18).
else(stepwise,24).
nonelse(stepwise,20).
nonelse(stepwise,5).
nonelse(stepwise,16).
nonelse(stepwise,15).
nonelse(stepwise,17).
nonelse(stepwise,14).
nonelse(stepwise,4).
nonelse(stepwise,9).
nonelse(stepwise,13).
nonelse(stepwise,26).
nonelse(stepwise,7).
nonelse(stepwise,23).
nonelse(stepwise,2).
nonelse(stepwise,6).
nonelse(stepwise,12).
nonelse(stepwise,21).
nonelse(stepwise,3).
nonelse(stepwise,22).
nonelse(stepwise,1).
nonelse(stepwise,8).
nonelse(stepwise,11).
nonelse(stepwise,10).
nonelse(stepwise,0).
nonelse(stepwise,27).
final(stepwise,27).
nonfinal(stepwise,20).
nonfinal(stepwise,5).
nonfinal(stepwise,16).
nonfinal(stepwise,15).
nonfinal(stepwise,17).
nonfinal(stepwise,14).
nonfinal(stepwise,4).
nonfinal(stepwise,9).
nonfinal(stepwise,30).
nonfinal(stepwise,13).
nonfinal(stepwise,26).
nonfinal(stepwise,7).
nonfinal(stepwise,19).
nonfinal(stepwise,23).
nonfinal(stepwise,2).
nonfinal(stepwise,6).
nonfinal(stepwise,12).
nonfinal(stepwise,18).
nonfinal(stepwise,21).
nonfinal(stepwise,3).
nonfinal(stepwise,24).
nonfinal(stepwise,22).
nonfinal(stepwise,1).
nonfinal(stepwise,8).
nonfinal(stepwise,11).
nonfinal(stepwise,10).
nonfinal(stepwise,0).
tree_state(stepwise,30).
tree_istate(stepwise,30,0).
 tree_state(stepwise,19).
tree_istate(stepwise,19,0).
 tree_state(stepwise,18).
tree_istate(stepwise,18,0).
 tree_state(stepwise,24).
tree_istate(stepwise,24,0).
 hedge_state(stepwise,20).
    no_applyrule(stepwise,20,30).
	  no_applyrule(stepwise,20,19).
	  no_applyrule(stepwise,20,18).
	  no_applyrule(stepwise,20,24).
	  no_applyelserule(stepwise,20).
hedge_state(stepwise,5).
    no_applyrule(stepwise,5,30).
	  no_applyrule(stepwise,5,19).
	  no_applyrule(stepwise,5,18).
	  no_applyrule(stepwise,5,24).
	  no_applyelserule(stepwise,5).
hedge_state(stepwise,16).
    no_applyrule(stepwise,16,19).
	  no_applyrule(stepwise,16,24).
	  no_applyelserule(stepwise,16).
no_elserule(stepwise,16).
	% %
hedge_state(stepwise,15).
    no_applyrule(stepwise,15,24).
	  no_applyelserule(stepwise,15).
no_elserule(stepwise,15).
	% %
hedge_state(stepwise,17).
    no_applyrule(stepwise,17,30).
	  no_applyrule(stepwise,17,19).
	  no_applyrule(stepwise,17,18).
	  no_applyrule(stepwise,17,24).
	  no_applyelserule(stepwise,17).
hedge_state(stepwise,14).
    no_applyrule(stepwise,14,19).
	  no_applyrule(stepwise,14,24).
	  no_applyelserule(stepwise,14).
no_elserule(stepwise,14).
	% %
hedge_state(stepwise,4).
    no_applyrule(stepwise,4,30).
	  no_applyrule(stepwise,4,24).
	  no_applyelserule(stepwise,4).
hedge_state(stepwise,9).
    no_applyrule(stepwise,9,30).
	  no_applyrule(stepwise,9,19).
	  no_applyrule(stepwise,9,18).
	  no_applyrule(stepwise,9,24).
	  no_applyelserule(stepwise,9).
hedge_state(stepwise,13).
    no_applyrule(stepwise,13,30).
	  no_applyrule(stepwise,13,19).
	  no_applyrule(stepwise,13,18).
	  no_applyrule(stepwise,13,24).
	  no_applyelserule(stepwise,13).
hedge_state(stepwise,26).
    no_applyrule(stepwise,26,30).
	  no_applyrule(stepwise,26,19).
	  no_applyrule(stepwise,26,18).
	  no_applyrule(stepwise,26,24).
	  no_applyelserule(stepwise,26).
hedge_state(stepwise,7).
    no_applyrule(stepwise,7,30).
	  no_applyrule(stepwise,7,19).
	  no_applyrule(stepwise,7,18).
	  no_applyrule(stepwise,7,24).
	  no_applyelserule(stepwise,7).
hedge_state(stepwise,23).
    no_applyrule(stepwise,23,30).
	  no_applyrule(stepwise,23,19).
	  no_applyrule(stepwise,23,18).
	  no_applyrule(stepwise,23,24).
	  no_applyelserule(stepwise,23).
hedge_state(stepwise,2).
    no_applyrule(stepwise,2,30).
	  no_applyrule(stepwise,2,19).
	  no_applyrule(stepwise,2,18).
	  no_applyrule(stepwise,2,24).
	  no_applyelserule(stepwise,2).
hedge_state(stepwise,6).
    no_applyrule(stepwise,6,30).
	  no_applyrule(stepwise,6,19).
	  no_applyrule(stepwise,6,18).
	  no_applyrule(stepwise,6,24).
	  no_applyelserule(stepwise,6).
hedge_state(stepwise,12).
    no_applyrule(stepwise,12,30).
	  no_applyrule(stepwise,12,19).
	  no_applyrule(stepwise,12,18).
	  no_applyrule(stepwise,12,24).
	  no_applyelserule(stepwise,12).
hedge_state(stepwise,21).
    no_applyrule(stepwise,21,30).
	  no_applyrule(stepwise,21,19).
	  no_applyrule(stepwise,21,18).
	  no_applyrule(stepwise,21,24).
	  no_applyelserule(stepwise,21).
no_elserule(stepwise,21).
	% %
hedge_state(stepwise,3).
    no_applyrule(stepwise,3,30).
	  no_applyrule(stepwise,3,19).
	  no_applyrule(stepwise,3,18).
	  no_applyrule(stepwise,3,24).
	  no_applyelserule(stepwise,3).
hedge_state(stepwise,22).
    no_applyrule(stepwise,22,30).
	  no_applyrule(stepwise,22,19).
	  no_applyrule(stepwise,22,24).
	  no_applyelserule(stepwise,22).
no_elserule(stepwise,22).
	% %
hedge_state(stepwise,1).
    no_applyrule(stepwise,1,30).
	  no_applyrule(stepwise,1,19).
	  no_applyrule(stepwise,1,18).
	  no_applyrule(stepwise,1,24).
	  no_applyelserule(stepwise,1).
hedge_state(stepwise,8).
    no_applyrule(stepwise,8,30).
	  no_applyrule(stepwise,8,24).
	  no_applyelserule(stepwise,8).
no_elserule(stepwise,8).
	% %
hedge_state(stepwise,11).
    no_applyrule(stepwise,11,30).
	  no_applyrule(stepwise,11,19).
	  no_applyrule(stepwise,11,18).
	  no_applyrule(stepwise,11,24).
	  no_applyelserule(stepwise,11).
hedge_state(stepwise,10).
    no_applyrule(stepwise,10,30).
	  no_applyrule(stepwise,10,19).
	  no_applyrule(stepwise,10,18).
	  no_applyrule(stepwise,10,24).
	  no_applyelserule(stepwise,10).
hedge_state(stepwise,0).
    no_applyrule(stepwise,0,30).
	  no_applyelserule(stepwise,0).
hedge_state(stepwise,27).
    no_applyrule(stepwise,27,30).
	  no_applyrule(stepwise,27,19).
	  no_applyrule(stepwise,27,18).
	  no_applyrule(stepwise,27,24).
	  no_applyelserule(stepwise,27).
no_elserule(stepwise,27).
	% %
applyrule(stepwise,15,19,22).
internalrule(stepwise,11,'x',var,16).
applyrule(stepwise,0,19,4).
elserule(stepwise,0,4).
applyrule(stepwise,22,18,22).
elserule(stepwise,3,8).
internalrule(stepwise,6,'classSpec',name,10).
applyrule(stepwise,0,24,27).
treerule(stepwise,20,18).
treerule(stepwise,16,19).
internalrule(stepwise,13,'a',char,17).
elserule(stepwise,13,4).
elserule(stepwise,26,4).
applyrule(stepwise,14,30,16).
treerule(stepwise,14,18).
internalrule(stepwise,1,'default',namespace,5).
elserule(stepwise,5,4).
applyrule(stepwise,8,18,4).
applyrule(stepwise,15,18,15).
elserule(stepwise,9,13).
internalrule(stepwise,5,'type',name,9).
treerule(stepwise,26,30).
internalrule(stepwise,10,'x',var,14).
treerule(stepwise,15,18).
internalrule(stepwise,0,'doc',type,3).
treerule(stepwise,13,18).
treerule(stepwise,17,18).
applyrule(stepwise,16,18,16).
treerule(stepwise,23,18).
elserule(stepwise,23,4).
treerule(stepwise,21,24).
applyrule(stepwise,8,19,21).
applyrule(stepwise,14,18,14).
internalrule(stepwise,17,'t',char,20).
treerule(stepwise,4,18).
elserule(stepwise,11,15).
internalrule(stepwise,6,'elementSpec',name,11).
applyrule(stepwise,4,19,4).
internalrule(stepwise,0,'att',type,1).
elserule(stepwise,1,4).
elserule(stepwise,2,7).
applyrule(stepwise,16,30,16).
internalrule(stepwise,23,'s',char,26).
elserule(stepwise,20,4).
elserule(stepwise,17,4).
elserule(stepwise,10,15).
elserule(stepwise,6,12).
internalrule(stepwise,2,'tei',namespace,6).
elserule(stepwise,7,12).
internalrule(stepwise,20,'t',char,23).
elserule(stepwise,4,4).
applyrule(stepwise,4,18,4).
applyrule(stepwise,15,30,15).
elserule(stepwise,12,15).
treerule(stepwise,22,19).
internalrule(stepwise,0,'elem',type,2).
applyrule(stepwise,0,18,4).
no_elserule(stepwise,16).
no_elserule(stepwise,15).
no_elserule(stepwise,14).
no_elserule(stepwise,30).
no_elserule(stepwise,19).
no_elserule(stepwise,18).
no_elserule(stepwise,21).
no_elserule(stepwise,24).
no_elserule(stepwise,22).
no_elserule(stepwise,8).
no_elserule(stepwise,27).
no_treerule(stepwise,5).
no_treerule(stepwise,9).
no_treerule(stepwise,30).
no_treerule(stepwise,7).
no_treerule(stepwise,19).
no_treerule(stepwise,2).
no_treerule(stepwise,6).
no_treerule(stepwise,12).
no_treerule(stepwise,18).
no_treerule(stepwise,3).
no_treerule(stepwise,24).
no_treerule(stepwise,1).
no_treerule(stepwise,8).
no_treerule(stepwise,11).
no_treerule(stepwise,10).
no_treerule(stepwise,0).
no_treerule(stepwise,27).
label(stepwise,'x',var,12).
no_internalrule(stepwise,20,'x',var).
no_internalrule(stepwise,5,'x',var).
no_internalrule(stepwise,16,'x',var).
no_internalrule(stepwise,15,'x',var).
no_internalrule(stepwise,17,'x',var).
no_internalrule(stepwise,14,'x',var).
no_internalrule(stepwise,4,'x',var).
no_internalrule(stepwise,9,'x',var).
no_internalrule(stepwise,30,'x',var).
no_internalrule(stepwise,13,'x',var).
no_internalrule(stepwise,26,'x',var).
no_internalrule(stepwise,7,'x',var).
no_internalrule(stepwise,19,'x',var).
no_internalrule(stepwise,23,'x',var).
no_internalrule(stepwise,2,'x',var).
no_internalrule(stepwise,6,'x',var).
no_internalrule(stepwise,12,'x',var).
no_internalrule(stepwise,18,'x',var).
no_internalrule(stepwise,21,'x',var).
no_internalrule(stepwise,3,'x',var).
no_internalrule(stepwise,24,'x',var).
no_internalrule(stepwise,22,'x',var).
no_internalrule(stepwise,1,'x',var).
no_internalrule(stepwise,8,'x',var).
no_internalrule(stepwise,0,'x',var).
no_internalrule(stepwise,27,'x',var).
label(stepwise,'tei',namespace,10).
no_internalrule(stepwise,20,'tei',namespace).
no_internalrule(stepwise,5,'tei',namespace).
no_internalrule(stepwise,16,'tei',namespace).
no_internalrule(stepwise,15,'tei',namespace).
no_internalrule(stepwise,17,'tei',namespace).
no_internalrule(stepwise,14,'tei',namespace).
no_internalrule(stepwise,4,'tei',namespace).
no_internalrule(stepwise,9,'tei',namespace).
no_internalrule(stepwise,30,'tei',namespace).
no_internalrule(stepwise,13,'tei',namespace).
no_internalrule(stepwise,26,'tei',namespace).
no_internalrule(stepwise,7,'tei',namespace).
no_internalrule(stepwise,19,'tei',namespace).
no_internalrule(stepwise,23,'tei',namespace).
no_internalrule(stepwise,6,'tei',namespace).
no_internalrule(stepwise,12,'tei',namespace).
no_internalrule(stepwise,18,'tei',namespace).
no_internalrule(stepwise,21,'tei',namespace).
no_internalrule(stepwise,3,'tei',namespace).
no_internalrule(stepwise,24,'tei',namespace).
no_internalrule(stepwise,22,'tei',namespace).
no_internalrule(stepwise,1,'tei',namespace).
no_internalrule(stepwise,8,'tei',namespace).
no_internalrule(stepwise,11,'tei',namespace).
no_internalrule(stepwise,10,'tei',namespace).
no_internalrule(stepwise,0,'tei',namespace).
no_internalrule(stepwise,27,'tei',namespace).
label(stepwise,'t',char,9).
no_internalrule(stepwise,5,'t',char).
no_internalrule(stepwise,16,'t',char).
no_internalrule(stepwise,15,'t',char).
no_internalrule(stepwise,14,'t',char).
no_internalrule(stepwise,4,'t',char).
no_internalrule(stepwise,9,'t',char).
no_internalrule(stepwise,30,'t',char).
no_internalrule(stepwise,13,'t',char).
no_internalrule(stepwise,26,'t',char).
no_internalrule(stepwise,7,'t',char).
no_internalrule(stepwise,19,'t',char).
no_internalrule(stepwise,23,'t',char).
no_internalrule(stepwise,2,'t',char).
no_internalrule(stepwise,6,'t',char).
no_internalrule(stepwise,12,'t',char).
no_internalrule(stepwise,18,'t',char).
no_internalrule(stepwise,21,'t',char).
no_internalrule(stepwise,3,'t',char).
no_internalrule(stepwise,24,'t',char).
no_internalrule(stepwise,22,'t',char).
no_internalrule(stepwise,1,'t',char).
no_internalrule(stepwise,8,'t',char).
no_internalrule(stepwise,11,'t',char).
no_internalrule(stepwise,10,'t',char).
no_internalrule(stepwise,0,'t',char).
no_internalrule(stepwise,27,'t',char).
label(stepwise,'s',char,8).
no_internalrule(stepwise,20,'s',char).
no_internalrule(stepwise,5,'s',char).
no_internalrule(stepwise,16,'s',char).
no_internalrule(stepwise,15,'s',char).
no_internalrule(stepwise,17,'s',char).
no_internalrule(stepwise,14,'s',char).
no_internalrule(stepwise,4,'s',char).
no_internalrule(stepwise,9,'s',char).
no_internalrule(stepwise,30,'s',char).
no_internalrule(stepwise,13,'s',char).
no_internalrule(stepwise,26,'s',char).
no_internalrule(stepwise,7,'s',char).
no_internalrule(stepwise,19,'s',char).
no_internalrule(stepwise,2,'s',char).
no_internalrule(stepwise,6,'s',char).
no_internalrule(stepwise,12,'s',char).
no_internalrule(stepwise,18,'s',char).
no_internalrule(stepwise,21,'s',char).
no_internalrule(stepwise,3,'s',char).
no_internalrule(stepwise,24,'s',char).
no_internalrule(stepwise,22,'s',char).
no_internalrule(stepwise,1,'s',char).
no_internalrule(stepwise,8,'s',char).
no_internalrule(stepwise,11,'s',char).
no_internalrule(stepwise,10,'s',char).
no_internalrule(stepwise,0,'s',char).
no_internalrule(stepwise,27,'s',char).
label(stepwise,'doc',type,5).
no_internalrule(stepwise,20,'doc',type).
no_internalrule(stepwise,5,'doc',type).
no_internalrule(stepwise,16,'doc',type).
no_internalrule(stepwise,15,'doc',type).
no_internalrule(stepwise,17,'doc',type).
no_internalrule(stepwise,14,'doc',type).
no_internalrule(stepwise,4,'doc',type).
no_internalrule(stepwise,9,'doc',type).
no_internalrule(stepwise,30,'doc',type).
no_internalrule(stepwise,13,'doc',type).
no_internalrule(stepwise,26,'doc',type).
no_internalrule(stepwise,7,'doc',type).
no_internalrule(stepwise,19,'doc',type).
no_internalrule(stepwise,23,'doc',type).
no_internalrule(stepwise,2,'doc',type).
no_internalrule(stepwise,6,'doc',type).
no_internalrule(stepwise,12,'doc',type).
no_internalrule(stepwise,18,'doc',type).
no_internalrule(stepwise,21,'doc',type).
no_internalrule(stepwise,3,'doc',type).
no_internalrule(stepwise,24,'doc',type).
no_internalrule(stepwise,22,'doc',type).
no_internalrule(stepwise,1,'doc',type).
no_internalrule(stepwise,8,'doc',type).
no_internalrule(stepwise,11,'doc',type).
no_internalrule(stepwise,10,'doc',type).
no_internalrule(stepwise,27,'doc',type).
label(stepwise,'type',name,11).
no_internalrule(stepwise,20,'type',name).
no_internalrule(stepwise,16,'type',name).
no_internalrule(stepwise,15,'type',name).
no_internalrule(stepwise,17,'type',name).
no_internalrule(stepwise,14,'type',name).
no_internalrule(stepwise,4,'type',name).
no_internalrule(stepwise,9,'type',name).
no_internalrule(stepwise,30,'type',name).
no_internalrule(stepwise,13,'type',name).
no_internalrule(stepwise,26,'type',name).
no_internalrule(stepwise,7,'type',name).
no_internalrule(stepwise,19,'type',name).
no_internalrule(stepwise,23,'type',name).
no_internalrule(stepwise,2,'type',name).
no_internalrule(stepwise,6,'type',name).
no_internalrule(stepwise,12,'type',name).
no_internalrule(stepwise,18,'type',name).
no_internalrule(stepwise,21,'type',name).
no_internalrule(stepwise,3,'type',name).
no_internalrule(stepwise,24,'type',name).
no_internalrule(stepwise,22,'type',name).
no_internalrule(stepwise,1,'type',name).
no_internalrule(stepwise,8,'type',name).
no_internalrule(stepwise,11,'type',name).
no_internalrule(stepwise,10,'type',name).
no_internalrule(stepwise,0,'type',name).
no_internalrule(stepwise,27,'type',name).
label(stepwise,'elem',type,7).
no_internalrule(stepwise,20,'elem',type).
no_internalrule(stepwise,5,'elem',type).
no_internalrule(stepwise,16,'elem',type).
no_internalrule(stepwise,15,'elem',type).
no_internalrule(stepwise,17,'elem',type).
no_internalrule(stepwise,14,'elem',type).
no_internalrule(stepwise,4,'elem',type).
no_internalrule(stepwise,9,'elem',type).
no_internalrule(stepwise,30,'elem',type).
no_internalrule(stepwise,13,'elem',type).
no_internalrule(stepwise,26,'elem',type).
no_internalrule(stepwise,7,'elem',type).
no_internalrule(stepwise,19,'elem',type).
no_internalrule(stepwise,23,'elem',type).
no_internalrule(stepwise,2,'elem',type).
no_internalrule(stepwise,6,'elem',type).
no_internalrule(stepwise,12,'elem',type).
no_internalrule(stepwise,18,'elem',type).
no_internalrule(stepwise,21,'elem',type).
no_internalrule(stepwise,3,'elem',type).
no_internalrule(stepwise,24,'elem',type).
no_internalrule(stepwise,22,'elem',type).
no_internalrule(stepwise,1,'elem',type).
no_internalrule(stepwise,8,'elem',type).
no_internalrule(stepwise,11,'elem',type).
no_internalrule(stepwise,10,'elem',type).
no_internalrule(stepwise,27,'elem',type).
label(stepwise,'elementSpec',name,6).
no_internalrule(stepwise,20,'elementSpec',name).
no_internalrule(stepwise,5,'elementSpec',name).
no_internalrule(stepwise,16,'elementSpec',name).
no_internalrule(stepwise,15,'elementSpec',name).
no_internalrule(stepwise,17,'elementSpec',name).
no_internalrule(stepwise,14,'elementSpec',name).
no_internalrule(stepwise,4,'elementSpec',name).
no_internalrule(stepwise,9,'elementSpec',name).
no_internalrule(stepwise,30,'elementSpec',name).
no_internalrule(stepwise,13,'elementSpec',name).
no_internalrule(stepwise,26,'elementSpec',name).
no_internalrule(stepwise,7,'elementSpec',name).
no_internalrule(stepwise,19,'elementSpec',name).
no_internalrule(stepwise,23,'elementSpec',name).
no_internalrule(stepwise,2,'elementSpec',name).
no_internalrule(stepwise,12,'elementSpec',name).
no_internalrule(stepwise,18,'elementSpec',name).
no_internalrule(stepwise,21,'elementSpec',name).
no_internalrule(stepwise,3,'elementSpec',name).
no_internalrule(stepwise,24,'elementSpec',name).
no_internalrule(stepwise,22,'elementSpec',name).
no_internalrule(stepwise,1,'elementSpec',name).
no_internalrule(stepwise,8,'elementSpec',name).
no_internalrule(stepwise,11,'elementSpec',name).
no_internalrule(stepwise,10,'elementSpec',name).
no_internalrule(stepwise,0,'elementSpec',name).
no_internalrule(stepwise,27,'elementSpec',name).
label(stepwise,'text',type,18).
no_internalrule(stepwise,20,'text',type).
no_internalrule(stepwise,5,'text',type).
no_internalrule(stepwise,16,'text',type).
no_internalrule(stepwise,15,'text',type).
no_internalrule(stepwise,17,'text',type).
no_internalrule(stepwise,14,'text',type).
no_internalrule(stepwise,4,'text',type).
no_internalrule(stepwise,9,'text',type).
no_internalrule(stepwise,30,'text',type).
no_internalrule(stepwise,13,'text',type).
no_internalrule(stepwise,26,'text',type).
no_internalrule(stepwise,7,'text',type).
no_internalrule(stepwise,19,'text',type).
no_internalrule(stepwise,23,'text',type).
no_internalrule(stepwise,2,'text',type).
no_internalrule(stepwise,6,'text',type).
no_internalrule(stepwise,12,'text',type).
no_internalrule(stepwise,18,'text',type).
no_internalrule(stepwise,21,'text',type).
no_internalrule(stepwise,3,'text',type).
no_internalrule(stepwise,24,'text',type).
no_internalrule(stepwise,22,'text',type).
no_internalrule(stepwise,1,'text',type).
no_internalrule(stepwise,8,'text',type).
no_internalrule(stepwise,11,'text',type).
no_internalrule(stepwise,10,'text',type).
no_internalrule(stepwise,0,'text',type).
no_internalrule(stepwise,27,'text',type).
label(stepwise,'comment',type,15).
no_internalrule(stepwise,20,'comment',type).
no_internalrule(stepwise,5,'comment',type).
no_internalrule(stepwise,16,'comment',type).
no_internalrule(stepwise,15,'comment',type).
no_internalrule(stepwise,17,'comment',type).
no_internalrule(stepwise,14,'comment',type).
no_internalrule(stepwise,4,'comment',type).
no_internalrule(stepwise,9,'comment',type).
no_internalrule(stepwise,30,'comment',type).
no_internalrule(stepwise,13,'comment',type).
no_internalrule(stepwise,26,'comment',type).
no_internalrule(stepwise,7,'comment',type).
no_internalrule(stepwise,19,'comment',type).
no_internalrule(stepwise,23,'comment',type).
no_internalrule(stepwise,2,'comment',type).
no_internalrule(stepwise,6,'comment',type).
no_internalrule(stepwise,12,'comment',type).
no_internalrule(stepwise,18,'comment',type).
no_internalrule(stepwise,21,'comment',type).
no_internalrule(stepwise,3,'comment',type).
no_internalrule(stepwise,24,'comment',type).
no_internalrule(stepwise,22,'comment',type).
no_internalrule(stepwise,1,'comment',type).
no_internalrule(stepwise,8,'comment',type).
no_internalrule(stepwise,11,'comment',type).
no_internalrule(stepwise,10,'comment',type).
no_internalrule(stepwise,0,'comment',type).
no_internalrule(stepwise,27,'comment',type).
label(stepwise,'classSpec',name,3).
no_internalrule(stepwise,20,'classSpec',name).
no_internalrule(stepwise,5,'classSpec',name).
no_internalrule(stepwise,16,'classSpec',name).
no_internalrule(stepwise,15,'classSpec',name).
no_internalrule(stepwise,17,'classSpec',name).
no_internalrule(stepwise,14,'classSpec',name).
no_internalrule(stepwise,4,'classSpec',name).
no_internalrule(stepwise,9,'classSpec',name).
no_internalrule(stepwise,30,'classSpec',name).
no_internalrule(stepwise,13,'classSpec',name).
no_internalrule(stepwise,26,'classSpec',name).
no_internalrule(stepwise,7,'classSpec',name).
no_internalrule(stepwise,19,'classSpec',name).
no_internalrule(stepwise,23,'classSpec',name).
no_internalrule(stepwise,2,'classSpec',name).
no_internalrule(stepwise,12,'classSpec',name).
no_internalrule(stepwise,18,'classSpec',name).
no_internalrule(stepwise,21,'classSpec',name).
no_internalrule(stepwise,3,'classSpec',name).
no_internalrule(stepwise,24,'classSpec',name).
no_internalrule(stepwise,22,'classSpec',name).
no_internalrule(stepwise,1,'classSpec',name).
no_internalrule(stepwise,8,'classSpec',name).
no_internalrule(stepwise,11,'classSpec',name).
no_internalrule(stepwise,10,'classSpec',name).
no_internalrule(stepwise,0,'classSpec',name).
no_internalrule(stepwise,27,'classSpec',name).
label(stepwise,'x',negvar,19).
no_internalrule(stepwise,20,'x',negvar).
no_internalrule(stepwise,5,'x',negvar).
no_internalrule(stepwise,16,'x',negvar).
no_internalrule(stepwise,15,'x',negvar).
no_internalrule(stepwise,17,'x',negvar).
no_internalrule(stepwise,14,'x',negvar).
no_internalrule(stepwise,4,'x',negvar).
no_internalrule(stepwise,9,'x',negvar).
no_internalrule(stepwise,30,'x',negvar).
no_internalrule(stepwise,13,'x',negvar).
no_internalrule(stepwise,26,'x',negvar).
no_internalrule(stepwise,7,'x',negvar).
no_internalrule(stepwise,19,'x',negvar).
no_internalrule(stepwise,23,'x',negvar).
no_internalrule(stepwise,2,'x',negvar).
no_internalrule(stepwise,6,'x',negvar).
no_internalrule(stepwise,12,'x',negvar).
no_internalrule(stepwise,18,'x',negvar).
no_internalrule(stepwise,21,'x',negvar).
no_internalrule(stepwise,3,'x',negvar).
no_internalrule(stepwise,24,'x',negvar).
no_internalrule(stepwise,22,'x',negvar).
no_internalrule(stepwise,1,'x',negvar).
no_internalrule(stepwise,8,'x',negvar).
no_internalrule(stepwise,11,'x',negvar).
no_internalrule(stepwise,10,'x',negvar).
no_internalrule(stepwise,0,'x',negvar).
no_internalrule(stepwise,27,'x',negvar).
label(stepwise,'a',char,1).
no_internalrule(stepwise,20,'a',char).
no_internalrule(stepwise,5,'a',char).
no_internalrule(stepwise,16,'a',char).
no_internalrule(stepwise,15,'a',char).
no_internalrule(stepwise,17,'a',char).
no_internalrule(stepwise,14,'a',char).
no_internalrule(stepwise,4,'a',char).
no_internalrule(stepwise,9,'a',char).
no_internalrule(stepwise,30,'a',char).
no_internalrule(stepwise,26,'a',char).
no_internalrule(stepwise,7,'a',char).
no_internalrule(stepwise,19,'a',char).
no_internalrule(stepwise,23,'a',char).
no_internalrule(stepwise,2,'a',char).
no_internalrule(stepwise,6,'a',char).
no_internalrule(stepwise,12,'a',char).
no_internalrule(stepwise,18,'a',char).
no_internalrule(stepwise,21,'a',char).
no_internalrule(stepwise,3,'a',char).
no_internalrule(stepwise,24,'a',char).
no_internalrule(stepwise,22,'a',char).
no_internalrule(stepwise,1,'a',char).
no_internalrule(stepwise,8,'a',char).
no_internalrule(stepwise,11,'a',char).
no_internalrule(stepwise,10,'a',char).
no_internalrule(stepwise,0,'a',char).
no_internalrule(stepwise,27,'a',char).
label(stepwise,'default',namespace,4).
no_internalrule(stepwise,20,'default',namespace).
no_internalrule(stepwise,5,'default',namespace).
no_internalrule(stepwise,16,'default',namespace).
no_internalrule(stepwise,15,'default',namespace).
no_internalrule(stepwise,17,'default',namespace).
no_internalrule(stepwise,14,'default',namespace).
no_internalrule(stepwise,4,'default',namespace).
no_internalrule(stepwise,9,'default',namespace).
no_internalrule(stepwise,30,'default',namespace).
no_internalrule(stepwise,13,'default',namespace).
no_internalrule(stepwise,26,'default',namespace).
no_internalrule(stepwise,7,'default',namespace).
no_internalrule(stepwise,19,'default',namespace).
no_internalrule(stepwise,23,'default',namespace).
no_internalrule(stepwise,2,'default',namespace).
no_internalrule(stepwise,6,'default',namespace).
no_internalrule(stepwise,12,'default',namespace).
no_internalrule(stepwise,18,'default',namespace).
no_internalrule(stepwise,21,'default',namespace).
no_internalrule(stepwise,3,'default',namespace).
no_internalrule(stepwise,24,'default',namespace).
no_internalrule(stepwise,22,'default',namespace).
no_internalrule(stepwise,8,'default',namespace).
no_internalrule(stepwise,11,'default',namespace).
no_internalrule(stepwise,10,'default',namespace).
no_internalrule(stepwise,0,'default',namespace).
no_internalrule(stepwise,27,'default',namespace).
label(stepwise,'att',type,2).
no_internalrule(stepwise,20,'att',type).
no_internalrule(stepwise,5,'att',type).
no_internalrule(stepwise,16,'att',type).
no_internalrule(stepwise,15,'att',type).
no_internalrule(stepwise,17,'att',type).
no_internalrule(stepwise,14,'att',type).
no_internalrule(stepwise,4,'att',type).
no_internalrule(stepwise,9,'att',type).
no_internalrule(stepwise,30,'att',type).
no_internalrule(stepwise,13,'att',type).
no_internalrule(stepwise,26,'att',type).
no_internalrule(stepwise,7,'att',type).
no_internalrule(stepwise,19,'att',type).
no_internalrule(stepwise,23,'att',type).
no_internalrule(stepwise,2,'att',type).
no_internalrule(stepwise,6,'att',type).
no_internalrule(stepwise,12,'att',type).
no_internalrule(stepwise,18,'att',type).
no_internalrule(stepwise,21,'att',type).
no_internalrule(stepwise,3,'att',type).
no_internalrule(stepwise,24,'att',type).
no_internalrule(stepwise,22,'att',type).
no_internalrule(stepwise,1,'att',type).
no_internalrule(stepwise,8,'att',type).
no_internalrule(stepwise,11,'att',type).
no_internalrule(stepwise,10,'att',type).
no_internalrule(stepwise,27,'att',type).
max_label_id(stepwise,19).
    %%% different origins %%%%%%%
    
