%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% User input are subsets Q of states of A for which to compute:
%%
%%    safe_sel^A(Q) and safe_ref^A(Q)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% These subsets Q are specified by facts of the form:
%%
%%    safe_sel_of(A,Q,X) and safe_rej_of(A,Q,X)
%%
%% which specify the elements X of the subsets Q. The
%% elements X in safe_sel^A(Q) and safe_ref^A(Q) are specified
%% by facts of the form:
%%
%%    safe_sel(A,Q,X) and safe_rej(A,Q,X) 
%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% the default subset provided is the subset of final states

safe_sel_of(A,final,X) :- final(A,X).
safe_rej_of(A,final,X) :- final(A,X).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% inverse delta accessibility
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

invacc_delta(A,Q,X) :- safe_rej_of(A,Q,X). 
invacc_delta(A,Q,X) :- step_nonvar(A,X,Y), invacc_delta(A,Q,Y).
invacc_delta(A,Q,X) :- step_var(A,X,Y), invacc_delta(A,Q,Y).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% inverse delta accessibility without variables
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

invacc_delta_nonvar(A,Q,X) :- hedge_state(A,X), safe_sel_of(A,Q,_),
                              not safe_sel_of(A,Q,X). 
invacc_delta_nonvar(A,Q,X) :- step_nonvar(A,X,Y),
                              invacc_delta_nonvar(A,Q,Y).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% safety for selection and selection
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


safe_rej(A,Q,X) :- hedge_state(A,X), safe_rej_of(A,Q,_),
		   not invacc_delta(A,Q,X).

safe_sel(A,Q,X) :- hedge_state(A,X), safe_sel_of(A,Q,_),
		   not invacc_delta_nonvar(A,Q,X).




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% delta steps
%% - with and without variables
%% - hedge, final, and nonfinal state
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% start datalog
% parse file ../FXP/../automata-store/lick/forward/version-0.6/07113/./step/det-stepwise-rules.dl
% process 2935 clauses
% computing fixpoint...
% done.
% facts matching pattern nonfinal(X0, X1):
  nonfinal(stepwise, 7).
  nonfinal(stepwise, 65).
  nonfinal(stepwise, 55).
  nonfinal(stepwise, 74).
  nonfinal(stepwise, 56).
  nonfinal(stepwise, 21).
  nonfinal(stepwise, 68).
  nonfinal(stepwise, 52).
  nonfinal(stepwise, 5).
  nonfinal(stepwise, 36).
  nonfinal(stepwise, 59).
  nonfinal(stepwise, 34).
  nonfinal(stepwise, 2).
  nonfinal(stepwise, 70).
  nonfinal(stepwise, 96).
  nonfinal(stepwise, 20).
  nonfinal(stepwise, 1).
  nonfinal(stepwise, 0).
  nonfinal(stepwise, 38).
  nonfinal(stepwise, 54).
  nonfinal(stepwise, 73).
  nonfinal(stepwise, 60).
  nonfinal(stepwise, 92).
  nonfinal(stepwise, 50).
  nonfinal(stepwise, 69).
  nonfinal(stepwise, 67).
  nonfinal(stepwise, 63).
  nonfinal(stepwise, 64).
  nonfinal(stepwise, 53).
  nonfinal(stepwise, 35).
  nonfinal(stepwise, 4).
  nonfinal(stepwise, 6).
  nonfinal(stepwise, 51).
  nonfinal(stepwise, 58).
  nonfinal(stepwise, 88).
  nonfinal(stepwise, 62).
  nonfinal(stepwise, 82).
  nonfinal(stepwise, 72).
  nonfinal(stepwise, 3).
  nonfinal(stepwise, 77).
  nonfinal(stepwise, 79).
  nonfinal(stepwise, 66).
  nonfinal(stepwise, 57).
  nonfinal(stepwise, 15).
  nonfinal(stepwise, 29).
  nonfinal(stepwise, 37).
  nonfinal(stepwise, 61).
% facts matching pattern final(X0, X1):
  final(stepwise, 227).
% facts matching pattern hedge_state(X0, X1):
  hedge_state(stepwise, 7).
  hedge_state(stepwise, 65).
  hedge_state(stepwise, 55).
  hedge_state(stepwise, 74).
  hedge_state(stepwise, 56).
  hedge_state(stepwise, 21).
  hedge_state(stepwise, 68).
  hedge_state(stepwise, 52).
  hedge_state(stepwise, 5).
  hedge_state(stepwise, 36).
  hedge_state(stepwise, 59).
  hedge_state(stepwise, 34).
  hedge_state(stepwise, 2).
  hedge_state(stepwise, 70).
  hedge_state(stepwise, 227).
  hedge_state(stepwise, 96).
  hedge_state(stepwise, 20).
  hedge_state(stepwise, 1).
  hedge_state(stepwise, 0).
  hedge_state(stepwise, 54).
  hedge_state(stepwise, 60).
  hedge_state(stepwise, 92).
  hedge_state(stepwise, 50).
  hedge_state(stepwise, 69).
  hedge_state(stepwise, 67).
  hedge_state(stepwise, 63).
  hedge_state(stepwise, 64).
  hedge_state(stepwise, 53).
  hedge_state(stepwise, 35).
  hedge_state(stepwise, 4).
  hedge_state(stepwise, 6).
  hedge_state(stepwise, 51).
  hedge_state(stepwise, 58).
  hedge_state(stepwise, 88).
  hedge_state(stepwise, 62).
  hedge_state(stepwise, 82).
  hedge_state(stepwise, 72).
  hedge_state(stepwise, 3).
  hedge_state(stepwise, 77).
  hedge_state(stepwise, 79).
  hedge_state(stepwise, 66).
  hedge_state(stepwise, 57).
  hedge_state(stepwise, 15).
  hedge_state(stepwise, 29).
  hedge_state(stepwise, 61).
% facts matching pattern step_var(X0, X1, X2):
  step_var(stepwise, 55, 3).
  step_var(stepwise, 65, 3).
  step_var(stepwise, 7, 21).
  step_var(stepwise, 56, 3).
  step_var(stepwise, 74, 3).
  step_var(stepwise, 21, 21).
  step_var(stepwise, 52, 3).
  step_var(stepwise, 68, 3).
  step_var(stepwise, 5, 3).
  step_var(stepwise, 59, 3).
  step_var(stepwise, 36, 3).
  step_var(stepwise, 34, 3).
  step_var(stepwise, 70, 3).
  step_var(stepwise, 2, 3).
  step_var(stepwise, 227, 227).
  step_var(stepwise, 96, 3).
  step_var(stepwise, 20, 3).
  step_var(stepwise, 54, 3).
  step_var(stepwise, 1, 3).
  step_var(stepwise, 0, 53).
  step_var(stepwise, 0, 3).
  step_var(stepwise, 60, 3).
  step_var(stepwise, 92, 3).
  step_var(stepwise, 69, 3).
  step_var(stepwise, 50, 3).
  step_var(stepwise, 63, 3).
  step_var(stepwise, 67, 3).
  step_var(stepwise, 64, 3).
  step_var(stepwise, 53, 53).
  step_var(stepwise, 53, 227).
  step_var(stepwise, 35, 3).
  step_var(stepwise, 6, 20).
  step_var(stepwise, 4, 3).
  step_var(stepwise, 51, 3).
  step_var(stepwise, 82, 3).
  step_var(stepwise, 62, 3).
  step_var(stepwise, 88, 3).
  step_var(stepwise, 58, 3).
  step_var(stepwise, 72, 3).
  step_var(stepwise, 3, 3).
  step_var(stepwise, 77, 3).
  step_var(stepwise, 66, 3).
  step_var(stepwise, 79, 3).
  step_var(stepwise, 57, 3).
  step_var(stepwise, 15, 29).
  step_var(stepwise, 29, 21).
  step_var(stepwise, 29, 29).
  step_var(stepwise, 61, 3).
% facts matching pattern step_nonvar(X0, X1, X2):
  step_nonvar(stepwise, 7, 3).
  step_nonvar(stepwise, 65, 68).
  step_nonvar(stepwise, 65, 3).
  step_nonvar(stepwise, 55, 58).
  step_nonvar(stepwise, 55, 3).
  step_nonvar(stepwise, 56, 59).
  step_nonvar(stepwise, 56, 3).
  step_nonvar(stepwise, 21, 21).
  step_nonvar(stepwise, 74, 3).
  step_nonvar(stepwise, 74, 79).
  step_nonvar(stepwise, 52, 56).
  step_nonvar(stepwise, 52, 3).
  step_nonvar(stepwise, 68, 3).
  step_nonvar(stepwise, 68, 69).
  step_nonvar(stepwise, 5, 7).
  step_nonvar(stepwise, 5, 3).
  step_nonvar(stepwise, 5, 15).
  step_nonvar(stepwise, 59, 62).
  step_nonvar(stepwise, 59, 3).
  step_nonvar(stepwise, 36, 52).
  step_nonvar(stepwise, 36, 3).
  step_nonvar(stepwise, 34, 3).
  step_nonvar(stepwise, 34, 50).
  step_nonvar(stepwise, 70, 74).
  step_nonvar(stepwise, 70, 3).
  step_nonvar(stepwise, 2, 5).
  step_nonvar(stepwise, 2, 3).
  step_nonvar(stepwise, 227, 227).
  step_nonvar(stepwise, 96, 3).
  step_nonvar(stepwise, 20, 35).
  step_nonvar(stepwise, 20, 36).
  step_nonvar(stepwise, 20, 3).
  step_nonvar(stepwise, 20, 34).
  step_nonvar(stepwise, 1, 4).
  step_nonvar(stepwise, 1, 3).
  step_nonvar(stepwise, 54, 3).
  step_nonvar(stepwise, 54, 57).
  step_nonvar(stepwise, 0, 53).
  step_nonvar(stepwise, 0, 3).
  step_nonvar(stepwise, 0, 2).
  step_nonvar(stepwise, 0, 1).
  step_nonvar(stepwise, 60, 63).
  step_nonvar(stepwise, 60, 3).
  step_nonvar(stepwise, 92, 3).
  step_nonvar(stepwise, 92, 96).
  step_nonvar(stepwise, 50, 3).
  step_nonvar(stepwise, 50, 54).
  step_nonvar(stepwise, 69, 3).
  step_nonvar(stepwise, 69, 72).
  step_nonvar(stepwise, 63, 3).
  step_nonvar(stepwise, 63, 66).
  step_nonvar(stepwise, 67, 3).
  step_nonvar(stepwise, 67, 70).
  step_nonvar(stepwise, 64, 67).
  step_nonvar(stepwise, 64, 3).
  step_nonvar(stepwise, 53, 53).
  step_nonvar(stepwise, 35, 51).
  step_nonvar(stepwise, 35, 3).
  step_nonvar(stepwise, 6, 20).
  step_nonvar(stepwise, 4, 6).
  step_nonvar(stepwise, 4, 3).
  step_nonvar(stepwise, 51, 55).
  step_nonvar(stepwise, 51, 3).
  step_nonvar(stepwise, 62, 65).
  step_nonvar(stepwise, 62, 3).
  step_nonvar(stepwise, 58, 3).
  step_nonvar(stepwise, 58, 61).
  step_nonvar(stepwise, 82, 88).
  step_nonvar(stepwise, 82, 3).
  step_nonvar(stepwise, 88, 3).
  step_nonvar(stepwise, 88, 92).
  step_nonvar(stepwise, 72, 3).
  step_nonvar(stepwise, 72, 77).
  step_nonvar(stepwise, 3, 3).
  step_nonvar(stepwise, 77, 82).
  step_nonvar(stepwise, 77, 3).
  step_nonvar(stepwise, 66, 3).
  step_nonvar(stepwise, 66, 69).
  step_nonvar(stepwise, 79, 3).
  step_nonvar(stepwise, 79, 66).
  step_nonvar(stepwise, 15, 3).
  step_nonvar(stepwise, 57, 3).
  step_nonvar(stepwise, 57, 60).
  step_nonvar(stepwise, 29, 21).
  step_nonvar(stepwise, 29, 29).
  step_nonvar(stepwise, 61, 64).
  step_nonvar(stepwise, 61, 3).
% max_heap_size: 1006592; minor_collections: 13; major collections: 2

