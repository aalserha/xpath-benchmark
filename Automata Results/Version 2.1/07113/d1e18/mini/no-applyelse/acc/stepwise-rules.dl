%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Accessible states of stepwise hedge automata A
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% whether variable is allowed or not

kind(0).
kind(1).

%% whether we started a tree or a hedge

start(t).
start(h).

%% types different from type var


nonvar(type).
nonvar(char).
nonvar(negvar).
nonvar(name).
nonvar(namespace).

%% starting trees and hedges

acc(t,0,A,X) :- start_tree(A,X).
acc(h,0,A,X) :- initial(A,X).

%% ending trees

acc_tree_state(K,A,Z) :- acc(t,K,A,X), treerule(A,X,Z).
acc_tree_state(K,A,Y) :- acc_tree_state(K,A,X), is(A,X,Y).

%% internal rules

internalrule_all(A,X,L,T,Y) :- internalrule(A,X,L,T,Y).
internalrule_all(A,X,L,T,Y) :- elserule(A,X,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,L,T,Y) :- typedelserule(A,X,T,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- elserule(A,X,Y), typedelserule(A,L,T,Z).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- typedelserule(A,X,T,Y).

acc(S,K,A,Y) :- acc(S,K,A,X), internalrule_all(A,X,L,T  ,Y), nonvar(T).
acc(S,1,A,Y) :- acc(S,0,A,X), internalrule_all(A,X,L,var,Y).

%% epsilon rules

acc(S,K,A,Y) :- acc(S,K,A,X), epsilonrule(A,X,Y), kind(K).

%% apply rules

applyrule_all(A,X,Z,Y) :- applyrule(A,X,Z,Y).
applyrule_all(A,X,Z,Y) :- applyelserule(A,X,Y), no_applyrule(A,X,Z).

acc(S,K,A,Y) :- acc(S,K,A,X), acc_tree_state(0,A,Z), applyrule_all(A,X,Z,Y).
acc(S,1,A,Y) :- acc(S,0,A,X), acc_tree_state(1,A,Z), applyrule_all(A,X,Z,Y).

%% compatibility with previous predicates

acc_h(A,X) :- acc(h,K,A,X).
acc_t(A,X) :- acc_tree_state(K,A,X).
acc_t(A,X) :- acc(t,K,A,X).




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% label states where XML labels end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

internal_or_else(A,X,Y) :- internalrule(A,X,L,T,Y).
internal_or_else(A,X,Y) :- elserule(A,X,Y).
internal_or_else(A,X,Y) :- typedelserule(A,X,T,Y).

label_state(A,U) :- start_tree(A,X), internalrule(A,X,'elem',type,Y),internal_or_else(A,Y,Z),
		    internal_or_else(A,Z,U).
label_state(A,U) :- start_tree(A,X), internalrule(A,X,'att',type,Y), internal_or_else(A,Y,Z),
                    internal_or_else(A,Z,U).
label_state(A,Y) :- start_tree(A,X), internalrule(A,X,'text',type,Y). 
label_state(A,Y) :- start_tree(A,X), internalrule(A,X,'comment',type,Y). 
label_state(A,Y) :- start_tree(A,X), internalrule(A,X,'doc',type,Y). 

%% the booleans K are not yet used everywhere to ensure one-x

acc_label(A,X,X) :- label_state(A,X).
acc_label(A,X,Z) :- acc_label(A,X,Y), internalrule(A,Y,L,T,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), elserule(A,Y,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), typedelserule(A,Y,T,Z).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(K,A,Z), applyrule(A,X1,Z,Y).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(K,A,Z), applyelserule(A,X1,Y), no_applyrule(A,X1,Z).

acc_label_tree(A,X,Z) :- acc_label(A,X,Y), treerule(A,Y,Z).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% types of label states
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

elem(A,NS,N,U)   :- start_tree(A,X), internalrule(A,X,'elem',type,NS),internal_or_else(A,NS,N),
		    internal_or_else(A,N,U).
att(A,NS,N,U)    :- start_tree(A,X), internalrule(A,X,'att',type,NS), internal_or_else(A,NS,N),
                    internal_or_else(A,N,U).
text(A,Y)	 :- start_tree(A,X), internalrule(A,X,'text',type,Y). 
comment(A,Y)     :- start_tree(A,X), internalrule(A,X,'comment',type,Y). 
doc(A,Y)         :- start_tree(A,X), internalrule(A,X,'doc',type,Y).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule
% negative information (NEGATIVE=true)
%   no_applyrule
%   no_elserule
%   no_treerule
%   no_internalrule
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(stepwise,'step[&lt;T&gt;⋅T]').
expression(stepwise,'d1e18').
state(stepwise,2).
state(stepwise,5).
state(stepwise,6).
state(stepwise,8).
start_tree(stepwise,5).
initial(stepwise,2).
not_sink(stepwise,2).
not_sink(stepwise,5).
not_sink(stepwise,6).
not_sink(stepwise,8).
nonelse(stepwise,2).
nonelse(stepwise,5).
nonelse(stepwise,6).
nonelse(stepwise,8).
final(stepwise,8).
nonfinal(stepwise,2).
nonfinal(stepwise,5).
nonfinal(stepwise,6).
copy_of(stepwise,5,6).
copy_of(stepwise,6,6).
tree_state(stepwise,6).
tree_istate(stepwise,6,5).
 hedge_state(stepwise,2).
    no_applyelserule(stepwise,2).
no_elserule(stepwise,2).
	% %
hedge_state(stepwise,5).
    no_applyelserule(stepwise,5).
hedge_state(stepwise,8).
    no_applyelserule(stepwise,8).
applyrule(stepwise,8,6,8).
elserule(stepwise,8,8).
applyrule(stepwise,5,6,5).
elserule(stepwise,5,5).
treerule(stepwise,5,6).
applyrule(stepwise,2,6,8).
no_elserule(stepwise,2).
no_elserule(stepwise,6).
no_treerule(stepwise,2).
no_treerule(stepwise,6).
no_treerule(stepwise,8).
max_label_id(stepwise,
	  0
	).
    %%% different origins %%%%%%%
    
