%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Accessible states of stepwise hedge automata A
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% whether variable is allowed or not

kind(0).
kind(1).

%% whether we started a tree or a hedge

start(t).
start(h).

%% types different from type var


nonvar(type).
nonvar(char).
nonvar(negvar).
nonvar(name).
nonvar(namespace).

%% starting trees and hedges

acc(t,0,A,X) :- start_tree(A,X).
acc(h,0,A,X) :- initial(A,X).

%% ending trees

acc_tree_state(K,A,Z) :- acc(t,K,A,X), treerule(A,X,Z).
acc_tree_state(K,A,Y) :- acc_tree_state(K,A,X), is(A,X,Y).

%% internal rules

internalrule_all(A,X,L,T,Y) :- internalrule(A,X,L,T,Y).
internalrule_all(A,X,L,T,Y) :- elserule(A,X,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,L,T,Y) :- typedelserule(A,X,T,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- elserule(A,X,Y), typedelserule(A,L,T,Z).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- typedelserule(A,X,T,Y).

acc(S,K,A,Y) :- acc(S,K,A,X), internalrule_all(A,X,L,T  ,Y), nonvar(T).
acc(S,1,A,Y) :- acc(S,0,A,X), internalrule_all(A,X,L,var,Y).

%% epsilon rules

acc(S,K,A,Y) :- acc(S,K,A,X), epsilonrule(A,X,Y), kind(K).

%% apply rules

applyrule_all(A,X,Z,Y) :- applyrule(A,X,Z,Y).
applyrule_all(A,X,Z,Y) :- applyelserule(A,X,Y), no_applyrule(A,X,Z).

acc(S,K,A,Y) :- acc(S,K,A,X), acc_tree_state(0,A,Z), applyrule_all(A,X,Z,Y).
acc(S,1,A,Y) :- acc(S,0,A,X), acc_tree_state(1,A,Z), applyrule_all(A,X,Z,Y).

%% compatibility with previous predicates

acc_h(A,X) :- acc(h,K,A,X).
acc_t(A,X) :- acc_tree_state(K,A,X).
acc_t(A,X) :- acc(t,K,A,X).




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% label states where XML labels end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

internal_or_else(A,X,Y) :- internalrule(A,X,L,T,Y).
internal_or_else(A,X,Y) :- elserule(A,X,Y).
internal_or_else(A,X,Y) :- typedelserule(A,X,T,Y).

label_state(A,U) :- start_tree(A,X), internalrule(A,X,'elem',type,Y),internal_or_else(A,Y,Z),
		    internal_or_else(A,Z,U).
label_state(A,U) :- start_tree(A,X), internalrule(A,X,'att',type,Y), internal_or_else(A,Y,Z),
                    internal_or_else(A,Z,U).
label_state(A,Y) :- start_tree(A,X), internalrule(A,X,'text',type,Y). 
label_state(A,Y) :- start_tree(A,X), internalrule(A,X,'comment',type,Y). 
label_state(A,Y) :- start_tree(A,X), internalrule(A,X,'doc',type,Y). 

%% the booleans K are not yet used everywhere to ensure one-x

acc_label(A,X,X) :- label_state(A,X).
acc_label(A,X,Z) :- acc_label(A,X,Y), internalrule(A,Y,L,T,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), elserule(A,Y,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), typedelserule(A,Y,T,Z).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(K,A,Z), applyrule(A,X1,Z,Y).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(K,A,Z), applyelserule(A,X1,Y), no_applyrule(A,X1,Z).

acc_label_tree(A,X,Z) :- acc_label(A,X,Y), treerule(A,Y,Z).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% types of label states
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

elem(A,NS,N,U)   :- start_tree(A,X), internalrule(A,X,'elem',type,NS),internal_or_else(A,NS,N),
		    internal_or_else(A,N,U).
att(A,NS,N,U)    :- start_tree(A,X), internalrule(A,X,'att',type,NS), internal_or_else(A,NS,N),
                    internal_or_else(A,N,U).
text(A,Y)	 :- start_tree(A,X), internalrule(A,X,'text',type,Y). 
comment(A,Y)     :- start_tree(A,X), internalrule(A,X,'comment',type,Y). 
doc(A,Y)         :- start_tree(A,X), internalrule(A,X,'doc',type,Y).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule
% negative information (NEGATIVE=true)
%   no_applyrule
%   no_elserule
%   no_treerule
%   no_internalrule
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(stepwise,'step[&lt;elem⋅_⋅_⋅x⋅T&gt;⋅T]').
expression(stepwise,'d1e34').
var(stepwise,'x',1).
no_typedelserule(stepwise,2,type).
no_typedelserule(stepwise,4,type).
no_typedelserule(stepwise,5,type).
no_typedelserule(stepwise,8,type).
no_typedelserule(stepwise,12,type).
no_typedelserule(stepwise,2,var).
no_typedelserule(stepwise,4,var).
no_typedelserule(stepwise,5,var).
no_typedelserule(stepwise,8,var).
no_typedelserule(stepwise,12,var).
state(stepwise,2).
state(stepwise,4).
state(stepwise,5).
state(stepwise,6).
state(stepwise,7).
state(stepwise,8).
state(stepwise,9).
state(stepwise,11).
state(stepwise,12).
state(stepwise,14).
start_tree(stepwise,5).
start_tree(stepwise,11).
initial(stepwise,2).
not_sink(stepwise,2).
not_sink(stepwise,4).
not_sink(stepwise,5).
not_sink(stepwise,6).
not_sink(stepwise,7).
not_sink(stepwise,8).
not_sink(stepwise,9).
not_sink(stepwise,11).
not_sink(stepwise,12).
not_sink(stepwise,14).
else(stepwise,12).
nonelse(stepwise,2).
nonelse(stepwise,4).
nonelse(stepwise,5).
nonelse(stepwise,6).
nonelse(stepwise,7).
nonelse(stepwise,8).
nonelse(stepwise,9).
nonelse(stepwise,11).
nonelse(stepwise,14).
final(stepwise,14).
nonfinal(stepwise,2).
nonfinal(stepwise,4).
nonfinal(stepwise,5).
nonfinal(stepwise,6).
nonfinal(stepwise,7).
nonfinal(stepwise,8).
nonfinal(stepwise,9).
nonfinal(stepwise,11).
nonfinal(stepwise,12).
copy_of(stepwise,4,4).
copy_of(stepwise,5,4).
copy_of(stepwise,6,4).
copy_of(stepwise,7,4).
copy_of(stepwise,8,4).
copy_of(stepwise,9,4).
copy_of(stepwise,11,12).
copy_of(stepwise,12,12).
tree_state(stepwise,4).
tree_istate(stepwise,4,5).
 tree_state(stepwise,12).
tree_istate(stepwise,12,11).
 hedge_state(stepwise,2).
    no_applyrule(stepwise,2,12).
	  no_applyelserule(stepwise,2).
no_elserule(stepwise,2).
	% %
hedge_state(stepwise,5).
    no_applyrule(stepwise,5,4).
	  no_applyrule(stepwise,5,12).
	  no_applyelserule(stepwise,5).
no_elserule(stepwise,5).
	% %
hedge_state(stepwise,6).
    no_applyrule(stepwise,6,4).
	  no_applyrule(stepwise,6,12).
	  no_applyelserule(stepwise,6).
hedge_state(stepwise,7).
    no_applyrule(stepwise,7,4).
	  no_applyrule(stepwise,7,12).
	  no_applyelserule(stepwise,7).
hedge_state(stepwise,8).
    no_applyrule(stepwise,8,4).
	  no_applyrule(stepwise,8,12).
	  no_applyelserule(stepwise,8).
no_elserule(stepwise,8).
	% %
hedge_state(stepwise,9).
    no_applyrule(stepwise,9,4).
	  no_applyrule(stepwise,9,12).
	  hedge_state(stepwise,11).
    no_applyrule(stepwise,11,4).
	  no_applyrule(stepwise,11,12).
	  hedge_state(stepwise,14).
    no_applyrule(stepwise,14,4).
	  no_applyrule(stepwise,14,12).
	  internalrule(stepwise,5,'elem',type,6).
elserule(stepwise,6,7).
elserule(stepwise,7,8).
applyelserule(stepwise,9,9).
no_applyrule(stepwise,9,4).
no_applyrule(stepwise,9,12).
elserule(stepwise,9,9).
internalrule(stepwise,8,'x',var,9).
treerule(stepwise,9,4).
applyelserule(stepwise,14,14).
no_applyrule(stepwise,14,4).
no_applyrule(stepwise,14,12).
elserule(stepwise,14,14).
applyelserule(stepwise,11,11).
no_applyrule(stepwise,11,4).
no_applyrule(stepwise,11,12).
elserule(stepwise,11,11).
treerule(stepwise,11,12).
applyrule(stepwise,2,4,14).
no_elserule(stepwise,2).
no_elserule(stepwise,4).
no_elserule(stepwise,5).
no_elserule(stepwise,8).
no_elserule(stepwise,12).
no_treerule(stepwise,2).
no_treerule(stepwise,4).
no_treerule(stepwise,5).
no_treerule(stepwise,6).
no_treerule(stepwise,7).
no_treerule(stepwise,8).
no_treerule(stepwise,12).
no_treerule(stepwise,14).
label(stepwise,'elem',type,1).
no_internalrule(stepwise,2,'elem',type).
no_internalrule(stepwise,4,'elem',type).
no_internalrule(stepwise,6,'elem',type).
no_internalrule(stepwise,7,'elem',type).
no_internalrule(stepwise,8,'elem',type).
no_internalrule(stepwise,9,'elem',type).
no_internalrule(stepwise,11,'elem',type).
no_internalrule(stepwise,12,'elem',type).
no_internalrule(stepwise,14,'elem',type).
label(stepwise,'x',var,2).
no_internalrule(stepwise,2,'x',var).
no_internalrule(stepwise,4,'x',var).
no_internalrule(stepwise,5,'x',var).
no_internalrule(stepwise,6,'x',var).
no_internalrule(stepwise,7,'x',var).
no_internalrule(stepwise,9,'x',var).
no_internalrule(stepwise,11,'x',var).
no_internalrule(stepwise,12,'x',var).
no_internalrule(stepwise,14,'x',var).
max_label_id(stepwise,2).
    %%% different origins %%%%%%%
    
