%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Accessible states of stepwise hedge automata A
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% whether variable is allowed or not

kind(0).
kind(1).

%% whether we started a tree or a hedge

start(t).
start(h).

%% types different from type var


nonvar(type).
nonvar(char).
nonvar(negvar).
nonvar(name).
nonvar(namespace).

%% starting trees and hedges

acc(t,0,A,X) :- start_tree(A,X).
acc(h,0,A,X) :- initial(A,X).

%% ending trees

acc_tree_state(K,A,Z) :- acc(t,K,A,X), treerule(A,X,Z).
acc_tree_state(K,A,Y) :- acc_tree_state(K,A,X), is(A,X,Y).

%% internal rules

internalrule_all(A,X,L,T,Y) :- internalrule(A,X,L,T,Y).
internalrule_all(A,X,L,T,Y) :- elserule(A,X,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,L,T,Y) :- typedelserule(A,X,T,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- elserule(A,X,Y), typedelserule(A,L,T,Z).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- typedelserule(A,X,T,Y).

acc(S,K,A,Y) :- acc(S,K,A,X), internalrule_all(A,X,L,T  ,Y), nonvar(T).
acc(S,1,A,Y) :- acc(S,0,A,X), internalrule_all(A,X,L,var,Y).

%% epsilon rules

acc(S,K,A,Y) :- acc(S,K,A,X), epsilonrule(A,X,Y), kind(K).

%% apply rules

applyrule_all(A,X,Z,Y) :- applyrule(A,X,Z,Y).
applyrule_all(A,X,Z,Y) :- applyelserule(A,X,Y), no_applyrule(A,X,Z).

acc(S,K,A,Y) :- acc(S,K,A,X), acc_tree_state(0,A,Z), applyrule_all(A,X,Z,Y).
acc(S,1,A,Y) :- acc(S,0,A,X), acc_tree_state(1,A,Z), applyrule_all(A,X,Z,Y).

%% compatibility with previous predicates

acc_h(A,X) :- acc(h,1,A,X).
acc_t(A,X) :- acc(t,1,A,X).




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% label states where XML labels end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

internal_or_else(A,X,Y) :- internalrule(A,X,L,T,Y).
internal_or_else(A,X,Y) :- elserule(A,X,Y).
internal_or_else(A,X,Y) :- typedelserule(A,X,T,Y).

label_state(A,U) :- initial(A,X), internalrule(A,X,'elem',type,Y),internal_or_else(A,Y,Z),
		    internal_or_else(A,Z,U).
label_state(A,U) :- initial(A,X), internalrule(A,X,'att',type,Y), internal_or_else(A,Y,Z),
                    internal_or_else(A,Z,U).
label_state(A,Y) :- initial(A,X), internalrule(A,X,'text',type,Y). 
label_state(A,Y) :- initial(A,X), internalrule(A,X,'comment',type,Y). 
label_state(A,Y) :- initial(A,X), internalrule(A,X,'doc',type,Y). 

acc_label(A,X,X) :- label_state(A,X).
acc_label(A,X,Z) :- acc_label(A,X,Y), internalrule(A,Y,L,T,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), elserule(A,Y,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), typedelserule(A,Y,T,Z).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(A,Z), applyrule(A,X1,Z,Y).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(A,Z), applyelserule(A,X1,Y), no_applyrule(A,X1,Z).

acc_label_tree(A,X,Z) :- acc_label(A,X,Y), treerule(A,Y,Z).


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule
% negative information (NEGATIVE=true)
%   no_applyrule
%   no_elserule
%   no_treerule
%   no_internalrule
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(stepwise,'mini(det_and[xml-document][one-x][schema-clean[pc[step[(&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅_⋅_⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T+(μx_d1.(&amp;lt;elem⋅default⋅refentry⋅x⋅T&amp;gt;⋅T_bl_Z+_bl_Z&amp;lt;(elem⋅_⋅__bl_Z+_bl_Zdoc)⋅_⋅T⋅x_d1&amp;gt;⋅T)))]]]])').
xpath(stepwise,'*|.//refentry').
var(stepwise,'x',1).
no_typedelserule(stepwise,23,var).
no_typedelserule(stepwise,17,var).
no_typedelserule(stepwise,6,var).
no_typedelserule(stepwise,11,var).
no_typedelserule(stepwise,14,var).
no_typedelserule(stepwise,15,var).
no_typedelserule(stepwise,10,var).
no_typedelserule(stepwise,12,var).
no_typedelserule(stepwise,9,var).
no_typedelserule(stepwise,23,type).
no_typedelserule(stepwise,17,type).
no_typedelserule(stepwise,6,type).
no_typedelserule(stepwise,11,type).
no_typedelserule(stepwise,14,type).
no_typedelserule(stepwise,15,type).
no_typedelserule(stepwise,10,type).
no_typedelserule(stepwise,12,type).
no_typedelserule(stepwise,9,type).
no_typedelserule(stepwise,23,name).
no_typedelserule(stepwise,17,name).
no_typedelserule(stepwise,6,name).
no_typedelserule(stepwise,11,name).
no_typedelserule(stepwise,14,name).
no_typedelserule(stepwise,15,name).
no_typedelserule(stepwise,10,name).
no_typedelserule(stepwise,12,name).
no_typedelserule(stepwise,9,name).
no_typedelserule(stepwise,23,namespace).
no_typedelserule(stepwise,17,namespace).
no_typedelserule(stepwise,6,namespace).
no_typedelserule(stepwise,11,namespace).
no_typedelserule(stepwise,14,namespace).
no_typedelserule(stepwise,15,namespace).
no_typedelserule(stepwise,10,namespace).
no_typedelserule(stepwise,12,namespace).
no_typedelserule(stepwise,9,namespace).
no_typedelserule(stepwise,23,negvar).
no_typedelserule(stepwise,17,negvar).
no_typedelserule(stepwise,6,negvar).
no_typedelserule(stepwise,11,negvar).
no_typedelserule(stepwise,14,negvar).
no_typedelserule(stepwise,15,negvar).
no_typedelserule(stepwise,10,negvar).
no_typedelserule(stepwise,12,negvar).
no_typedelserule(stepwise,9,negvar).
state(stepwise,3).
state(stepwise,2).
state(stepwise,23).
state(stepwise,17).
state(stepwise,4).
state(stepwise,6).
state(stepwise,11).
state(stepwise,0).
state(stepwise,14).
state(stepwise,15).
state(stepwise,8).
state(stepwise,7).
state(stepwise,1).
state(stepwise,10).
state(stepwise,12).
state(stepwise,9).
state(stepwise,5).
start_tree(stepwise,0).
initial(stepwise,0).
origin_of(stepwise,3,'39','10','14','27','4','7','29','18','8','25','2','17').
origin_of(stepwise,2,'10','35','39','1').
origin_of(stepwise,23,'40','23','12','24','28','19','21','11').
origin_of(stepwise,17,'5','40','19','28').
origin_of(stepwise,4,'9','14','34','39','25','9').
origin_of(stepwise,6,'42','44','17','36','18','37','39','6','26','16','5','22','20').
origin_of(stepwise,11,'22','40','11','19').
origin_of(stepwise,0,'7','23','12','32','21','39','4','6','29','31','0').
origin_of(stepwise,14,'28','39','25').
origin_of(stepwise,15,'42','44','17','19','36','18','37','39','20','26').
origin_of(stepwise,8,'10','35','39','15','13').
origin_of(stepwise,7,'10','35','26','39','15','13').
origin_of(stepwise,1,'33','24','39','8','13','3').
origin_of(stepwise,10,'42','44','16','17','36','18','37','39','15','20').
origin_of(stepwise,12,'40','11','19').
origin_of(stepwise,9,'42','27','44','16','17','36','18','37','39','15','20').
origin_of(stepwise,5,'9','14','34','39','9').
not_sink(stepwise,3).
not_sink(stepwise,2).
not_sink(stepwise,23).
not_sink(stepwise,17).
not_sink(stepwise,4).
not_sink(stepwise,6).
not_sink(stepwise,11).
not_sink(stepwise,0).
not_sink(stepwise,14).
not_sink(stepwise,15).
not_sink(stepwise,8).
not_sink(stepwise,7).
not_sink(stepwise,1).
not_sink(stepwise,10).
not_sink(stepwise,12).
not_sink(stepwise,9).
not_sink(stepwise,5).
else(stepwise,23).
else(stepwise,17).
else(stepwise,11).
else(stepwise,12).
nonelse(stepwise,3).
nonelse(stepwise,2).
nonelse(stepwise,4).
nonelse(stepwise,6).
nonelse(stepwise,0).
nonelse(stepwise,14).
nonelse(stepwise,15).
nonelse(stepwise,8).
nonelse(stepwise,7).
nonelse(stepwise,1).
nonelse(stepwise,10).
nonelse(stepwise,9).
nonelse(stepwise,5).
final(stepwise,14).
nonfinal(stepwise,3).
nonfinal(stepwise,2).
nonfinal(stepwise,23).
nonfinal(stepwise,17).
nonfinal(stepwise,4).
nonfinal(stepwise,6).
nonfinal(stepwise,11).
nonfinal(stepwise,0).
nonfinal(stepwise,15).
nonfinal(stepwise,8).
nonfinal(stepwise,7).
nonfinal(stepwise,1).
nonfinal(stepwise,10).
nonfinal(stepwise,12).
nonfinal(stepwise,9).
nonfinal(stepwise,5).
tree_state(stepwise,23).
tree_istate(stepwise,23,0).
 tree_state(stepwise,17).
tree_istate(stepwise,17,0).
 tree_state(stepwise,11).
tree_istate(stepwise,11,0).
 tree_state(stepwise,12).
tree_istate(stepwise,12,0).
 hedge_state(stepwise,3).
    no_applyelserule(stepwise,3).
hedge_state(stepwise,2).
    no_applyrule(stepwise,2,23).
	  no_applyrule(stepwise,2,17).
	  no_applyrule(stepwise,2,11).
	  no_applyrule(stepwise,2,12).
	  no_applyelserule(stepwise,2).
hedge_state(stepwise,4).
    no_applyrule(stepwise,4,23).
	  no_applyrule(stepwise,4,17).
	  no_applyrule(stepwise,4,11).
	  no_applyrule(stepwise,4,12).
	  no_applyelserule(stepwise,4).
hedge_state(stepwise,6).
    no_applyelserule(stepwise,6).
no_elserule(stepwise,6).
	% %
hedge_state(stepwise,0).
    no_applyelserule(stepwise,0).
hedge_state(stepwise,14).
    no_applyrule(stepwise,14,17).
	  no_applyrule(stepwise,14,11).
	  no_applyrule(stepwise,14,12).
	  no_applyelserule(stepwise,14).
no_elserule(stepwise,14).
	% %
hedge_state(stepwise,15).
    no_applyrule(stepwise,15,17).
	  no_applyrule(stepwise,15,11).
	  no_applyrule(stepwise,15,12).
	  no_applyelserule(stepwise,15).
no_elserule(stepwise,15).
	% %
hedge_state(stepwise,8).
    no_applyrule(stepwise,8,23).
	  no_applyrule(stepwise,8,17).
	  no_applyrule(stepwise,8,11).
	  no_applyrule(stepwise,8,12).
	  no_applyelserule(stepwise,8).
hedge_state(stepwise,7).
    no_applyrule(stepwise,7,23).
	  no_applyrule(stepwise,7,17).
	  no_applyrule(stepwise,7,11).
	  no_applyrule(stepwise,7,12).
	  no_applyelserule(stepwise,7).
hedge_state(stepwise,1).
    no_applyrule(stepwise,1,23).
	  no_applyrule(stepwise,1,17).
	  no_applyrule(stepwise,1,11).
	  no_applyrule(stepwise,1,12).
	  no_applyelserule(stepwise,1).
hedge_state(stepwise,10).
    no_applyrule(stepwise,10,17).
	  no_applyrule(stepwise,10,11).
	  no_applyrule(stepwise,10,12).
	  no_applyelserule(stepwise,10).
no_elserule(stepwise,10).
	% %
hedge_state(stepwise,9).
    no_applyrule(stepwise,9,17).
	  no_applyrule(stepwise,9,11).
	  no_applyrule(stepwise,9,12).
	  no_applyelserule(stepwise,9).
no_elserule(stepwise,9).
	% %
hedge_state(stepwise,5).
    no_applyrule(stepwise,5,23).
	  no_applyrule(stepwise,5,17).
	  no_applyrule(stepwise,5,11).
	  no_applyrule(stepwise,5,12).
	  no_applyelserule(stepwise,5).
applyrule(stepwise,3,11,3).
applyrule(stepwise,10,23,10).
applyrule(stepwise,9,23,9).
internalrule(stepwise,0,'elem',type,1).
applyrule(stepwise,3,12,3).
applyrule(stepwise,14,23,14).
elserule(stepwise,5,8).
treerule(stepwise,9,11).
internalrule(stepwise,7,'x',var,9).
applyrule(stepwise,6,11,9).
applyrule(stepwise,0,23,3).
applyrule(stepwise,0,11,14).
applyrule(stepwise,6,12,15).
treerule(stepwise,6,23).
applyrule(stepwise,6,23,6).
internalrule(stepwise,1,'default',namespace,4).
elserule(stepwise,0,3).
applyrule(stepwise,6,17,6).
applyrule(stepwise,3,23,3).
treerule(stepwise,15,17).
internalrule(stepwise,4,'refentry',name,7).
applyrule(stepwise,15,23,15).
internalrule(stepwise,0,'doc',type,2).
elserule(stepwise,2,6).
internalrule(stepwise,8,'x',var,10).
applyrule(stepwise,3,17,3).
elserule(stepwise,1,5).
elserule(stepwise,4,8).
treerule(stepwise,3,23).
elserule(stepwise,8,6).
applyrule(stepwise,0,12,3).
treerule(stepwise,10,12).
applyrule(stepwise,0,17,14).
elserule(stepwise,7,6).
elserule(stepwise,3,3).
no_elserule(stepwise,23).
no_elserule(stepwise,17).
no_elserule(stepwise,6).
no_elserule(stepwise,11).
no_elserule(stepwise,14).
no_elserule(stepwise,15).
no_elserule(stepwise,10).
no_elserule(stepwise,12).
no_elserule(stepwise,9).
no_treerule(stepwise,2).
no_treerule(stepwise,23).
no_treerule(stepwise,17).
no_treerule(stepwise,4).
no_treerule(stepwise,11).
no_treerule(stepwise,0).
no_treerule(stepwise,14).
no_treerule(stepwise,8).
no_treerule(stepwise,7).
no_treerule(stepwise,1).
no_treerule(stepwise,12).
no_treerule(stepwise,5).
label(stepwise,'x',var,5).
no_internalrule(stepwise,3,'x',var).
no_internalrule(stepwise,2,'x',var).
no_internalrule(stepwise,23,'x',var).
no_internalrule(stepwise,17,'x',var).
no_internalrule(stepwise,4,'x',var).
no_internalrule(stepwise,6,'x',var).
no_internalrule(stepwise,11,'x',var).
no_internalrule(stepwise,0,'x',var).
no_internalrule(stepwise,14,'x',var).
no_internalrule(stepwise,15,'x',var).
no_internalrule(stepwise,1,'x',var).
no_internalrule(stepwise,10,'x',var).
no_internalrule(stepwise,12,'x',var).
no_internalrule(stepwise,9,'x',var).
no_internalrule(stepwise,5,'x',var).
label(stepwise,'doc',type,2).
no_internalrule(stepwise,3,'doc',type).
no_internalrule(stepwise,2,'doc',type).
no_internalrule(stepwise,23,'doc',type).
no_internalrule(stepwise,17,'doc',type).
no_internalrule(stepwise,4,'doc',type).
no_internalrule(stepwise,6,'doc',type).
no_internalrule(stepwise,11,'doc',type).
no_internalrule(stepwise,14,'doc',type).
no_internalrule(stepwise,15,'doc',type).
no_internalrule(stepwise,8,'doc',type).
no_internalrule(stepwise,7,'doc',type).
no_internalrule(stepwise,1,'doc',type).
no_internalrule(stepwise,10,'doc',type).
no_internalrule(stepwise,12,'doc',type).
no_internalrule(stepwise,9,'doc',type).
no_internalrule(stepwise,5,'doc',type).
label(stepwise,'text',type,11).
no_internalrule(stepwise,3,'text',type).
no_internalrule(stepwise,2,'text',type).
no_internalrule(stepwise,23,'text',type).
no_internalrule(stepwise,17,'text',type).
no_internalrule(stepwise,4,'text',type).
no_internalrule(stepwise,6,'text',type).
no_internalrule(stepwise,11,'text',type).
no_internalrule(stepwise,0,'text',type).
no_internalrule(stepwise,14,'text',type).
no_internalrule(stepwise,15,'text',type).
no_internalrule(stepwise,8,'text',type).
no_internalrule(stepwise,7,'text',type).
no_internalrule(stepwise,1,'text',type).
no_internalrule(stepwise,10,'text',type).
no_internalrule(stepwise,12,'text',type).
no_internalrule(stepwise,9,'text',type).
no_internalrule(stepwise,5,'text',type).
label(stepwise,'refentry',name,4).
no_internalrule(stepwise,3,'refentry',name).
no_internalrule(stepwise,2,'refentry',name).
no_internalrule(stepwise,23,'refentry',name).
no_internalrule(stepwise,17,'refentry',name).
no_internalrule(stepwise,6,'refentry',name).
no_internalrule(stepwise,11,'refentry',name).
no_internalrule(stepwise,0,'refentry',name).
no_internalrule(stepwise,14,'refentry',name).
no_internalrule(stepwise,15,'refentry',name).
no_internalrule(stepwise,8,'refentry',name).
no_internalrule(stepwise,7,'refentry',name).
no_internalrule(stepwise,1,'refentry',name).
no_internalrule(stepwise,10,'refentry',name).
no_internalrule(stepwise,12,'refentry',name).
no_internalrule(stepwise,9,'refentry',name).
no_internalrule(stepwise,5,'refentry',name).
label(stepwise,'default',namespace,1).
no_internalrule(stepwise,3,'default',namespace).
no_internalrule(stepwise,2,'default',namespace).
no_internalrule(stepwise,23,'default',namespace).
no_internalrule(stepwise,17,'default',namespace).
no_internalrule(stepwise,4,'default',namespace).
no_internalrule(stepwise,6,'default',namespace).
no_internalrule(stepwise,11,'default',namespace).
no_internalrule(stepwise,0,'default',namespace).
no_internalrule(stepwise,14,'default',namespace).
no_internalrule(stepwise,15,'default',namespace).
no_internalrule(stepwise,8,'default',namespace).
no_internalrule(stepwise,7,'default',namespace).
no_internalrule(stepwise,10,'default',namespace).
no_internalrule(stepwise,12,'default',namespace).
no_internalrule(stepwise,9,'default',namespace).
no_internalrule(stepwise,5,'default',namespace).
label(stepwise,'elem',type,3).
no_internalrule(stepwise,3,'elem',type).
no_internalrule(stepwise,2,'elem',type).
no_internalrule(stepwise,23,'elem',type).
no_internalrule(stepwise,17,'elem',type).
no_internalrule(stepwise,4,'elem',type).
no_internalrule(stepwise,6,'elem',type).
no_internalrule(stepwise,11,'elem',type).
no_internalrule(stepwise,14,'elem',type).
no_internalrule(stepwise,15,'elem',type).
no_internalrule(stepwise,8,'elem',type).
no_internalrule(stepwise,7,'elem',type).
no_internalrule(stepwise,1,'elem',type).
no_internalrule(stepwise,10,'elem',type).
no_internalrule(stepwise,12,'elem',type).
no_internalrule(stepwise,9,'elem',type).
no_internalrule(stepwise,5,'elem',type).
label(stepwise,'att',type,7).
no_internalrule(stepwise,3,'att',type).
no_internalrule(stepwise,2,'att',type).
no_internalrule(stepwise,23,'att',type).
no_internalrule(stepwise,17,'att',type).
no_internalrule(stepwise,4,'att',type).
no_internalrule(stepwise,6,'att',type).
no_internalrule(stepwise,11,'att',type).
no_internalrule(stepwise,0,'att',type).
no_internalrule(stepwise,14,'att',type).
no_internalrule(stepwise,15,'att',type).
no_internalrule(stepwise,8,'att',type).
no_internalrule(stepwise,7,'att',type).
no_internalrule(stepwise,1,'att',type).
no_internalrule(stepwise,10,'att',type).
no_internalrule(stepwise,12,'att',type).
no_internalrule(stepwise,9,'att',type).
no_internalrule(stepwise,5,'att',type).
label(stepwise,'x',negvar,12).
no_internalrule(stepwise,3,'x',negvar).
no_internalrule(stepwise,2,'x',negvar).
no_internalrule(stepwise,23,'x',negvar).
no_internalrule(stepwise,17,'x',negvar).
no_internalrule(stepwise,4,'x',negvar).
no_internalrule(stepwise,6,'x',negvar).
no_internalrule(stepwise,11,'x',negvar).
no_internalrule(stepwise,0,'x',negvar).
no_internalrule(stepwise,14,'x',negvar).
no_internalrule(stepwise,15,'x',negvar).
no_internalrule(stepwise,8,'x',negvar).
no_internalrule(stepwise,7,'x',negvar).
no_internalrule(stepwise,1,'x',negvar).
no_internalrule(stepwise,10,'x',negvar).
no_internalrule(stepwise,12,'x',negvar).
no_internalrule(stepwise,9,'x',negvar).
no_internalrule(stepwise,5,'x',negvar).
label(stepwise,'comment',type,8).
no_internalrule(stepwise,3,'comment',type).
no_internalrule(stepwise,2,'comment',type).
no_internalrule(stepwise,23,'comment',type).
no_internalrule(stepwise,17,'comment',type).
no_internalrule(stepwise,4,'comment',type).
no_internalrule(stepwise,6,'comment',type).
no_internalrule(stepwise,11,'comment',type).
no_internalrule(stepwise,0,'comment',type).
no_internalrule(stepwise,14,'comment',type).
no_internalrule(stepwise,15,'comment',type).
no_internalrule(stepwise,8,'comment',type).
no_internalrule(stepwise,7,'comment',type).
no_internalrule(stepwise,1,'comment',type).
no_internalrule(stepwise,10,'comment',type).
no_internalrule(stepwise,12,'comment',type).
no_internalrule(stepwise,9,'comment',type).
no_internalrule(stepwise,5,'comment',type).
max_label_id(stepwise,12).
    %%% different origins %%%%%%%
    
