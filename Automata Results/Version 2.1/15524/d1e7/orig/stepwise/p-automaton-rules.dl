%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  intersection of stepwise hedge automata A and B
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% variables X,Y for states,
%%           L,T for labels and their types
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% hedge states
p_state(X,Y)   :- p_start_tree(X,Y).
p_state(X,Y)   :- p_initial(X,Y).
p_state(X1,Y1) :- p_elserule(X,Y,X1,Y1).
p_state(X1,Y1) :- p_typedelserule(X,Y,T,X1,Y1).
p_state(X1,Y1) :- p_typedelseArule(X,Y,T,X1,Y1).
p_state(X1,Y1) :- p_typedelseBrule(X,Y,T,X1,Y1).
p_state(X3,Y3) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyelserule(X1,Y1,X3,Y3).
p_state(X3,Y3) :- p_applyAelserule(X1,Y1,X3,Y3).
p_state(X3,Y3) :- p_applyBelserule(X1,Y1,X3,Y3).
p_state(X1,Y1) :- p_internalrule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_internalArule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_internalBrule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_epsilonArule(X,Y,X1,Y1).
p_state(X1,Y1) :- p_epsilonBrule(X,Y,X1,Y1).
p_state(IX,IY) :- p_tree_istate(X2,Y2,IX,IY).

%%% tree states that are copies of hedge states
p_copy_of(X1,X2,Z1,Z2) :- p_state(X1,X2), copy_of(A,X1,Z1), state(A,Z1),
                          copy_of(B,X2,Z2), state(B,Z2), inter(A,B,C).

%%% tree states and istates of tree states
p_tree_state(X2,Y2)        :- p_tree_istate(X2,Y2,IX,IY).
p_tree_istate(X2,Y2,IX,IY) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyelserule(X1,Y1,X3,Y3), 
                               tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).

p_tree_state(X2,Y2) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyelserule(X1,Y1,X3,Y3), tree_state(A,X2), tree_state(B,Y2), inter(A,B,C).  

p_tree_state(Z1,Z2) :- p_copy_of(X1,X2,Z1,Z2).
p_tree_state(U,V)   :- p_treerule(X,Y,U,V).

%%% distinguished states
p_initial(X,Y)    :- initial(A,X), initial(B,Y), inter(A,B,C).
p_else(X,Y)       :- p_tree_state(X,Y), else(A,X), else(B,Y),  inter(A,B,C).
p_elseA(X,Y)      :- else(A,X), p_tree_state(X,Y), inter(A,B,C).
p_elseB(X,Y)      :- p_tree_state(X,Y), else(B,Y), inter(A,B,C).
p_final(X,Y)      :- final(A,X), final(B,Y), p_state(X,Y), inter(A,B,C).
p_finalA(X,Y)      :- final(A,X), p_state(X,Y), inter(A,B,C).
p_finalB(X,Y)      :- final(B,Y), p_state(X,Y), inter(A,B,C).
p_start_tree(X,Y) :- start_tree(A,X), start_tree(B,Y), inter(A,B,C).
p_sinkA(X,Y)       :- sink(A,X), state(B,Y), inter(A,B,C).
p_sinkB(X,Y)       :- state(A,X), sink(B,Y), inter(A,B,C).
p_sink(X,Y)        :- p_sinkA(X,Y).
p_sink(X,Y)        :- p_sinkB(X,Y).

%% origins
%  p_originA_of(X,Y,X1,Y2)   :- origin_of(A,X,X1,X2), state(B,Y), inter(A,B,C).

%%% internalrules

internalfromelse(B,Y,L,T,Y1) :- elserule(B,Y,Y1), no_internalrule(B,Y,L,T).
internalfromelse(B,Y,L,T,Y1) :- typedelserule(B,Y,T,Y1), no_internalrule(B,Y,L,T).
internalfromelse(B,Y,L,T,Y1) :- elserule(B,Y,Y1), not_in_signature(B,L,T).
internalfromelse(B,Y,L,T,Y1) :- typedelserule(B,Y,T,Y1), not_in_signature(B,L,T).


% internal / else
p_internalArule(X,Y,L,T,X1,Y1) :- internalrule(A,X,L,T,X1), internalfromelse(B,Y,L,T,Y1),
                                  p_state(X,Y), inter(A,B,C).
% else / internal
p_internalBrule(X,Y,L,T,X1,Y1) :- internalfromelse(A,X,L,T,X1), internalrule(B,Y,L,T,Y1),
			       	  p_state(X,Y), inter(A,B,C).

% internal / internal
p_internalrule(X,Y,L,T,X1,Y1) :- internalrule(A,X,L,T,X1), internalrule(B,Y,L,T,Y1),
			      	 p_state(X,Y), inter(A,B,C).

%%% else rules typed and untyped

% else / else
p_elserule(X,Y,X1,Y1) :- elserule(A,X,X1), elserule(B,Y,Y1), p_state(X,Y), inter(A,B,C).

% typed_else / typed_else
p_typedelserule(X,Y,T,X1,Y1) :- typedelserule(A,X,T,X1), typedelserule(B,Y,T,Y1),
			     p_state(X,Y), inter(A,B,C).
% typed_else / else
p_typedelseArule(X,Y,T,X1,Y1) :- typedelserule(A,X,T,X1), elserule(B,Y,Y1),
			      p_state(X,Y), inter(A,B,C).

% else / typed_else
p_typedelseBrule(X,Y,T,X1,Y1) :- elserule(A,X,X1), typedelserule(B,Y,T,Y1),
			      p_state(X,Y), inter(A,B,C).


%%% applyrules

applyfromelse(B,Y1,Y2,Y3) :- else(B,Y2), no_applyrule(B,Y1,Y2), applyelserule(B,Y1,Y3).

% apply / apply
p_applyrule(X1,Y1,X2,Y2,X3,Y3) :- applyrule(A,X1,X2,X3), applyrule(B,Y1,Y2,Y3),
			          p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
% apply / applyelse
p_applyArule(X1,Y1,X2,Y2,X3,Y3) :- applyrule(A,X1,X2,X3), applyfromelse(B,Y1,Y2,Y3),
				p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
p_applyAelserule(X1,Y1,X2,else,X3,Y3) :- applyrule(A,X1,X2,X3), applyelserule(B,Y1,Y3),
				      p_state(X1,Y1), else(B,Y2),
				      p_tree_state(X2,Y2), inter(A,B,C).
% applyelse / apply
p_applyBrule(X1,Y1,X2,Y2,X3,Y3) :- applyfromelse(A,X1,X2,X3), applyrule(B,Y1,Y2,Y3),
				p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
p_applyBelserule(X1,Y1,else,Y2,X3,Y3) :- applyelserule(A,X1,X3), applyrule(B,Y1,Y2,Y3),
				      p_state(X1,Y1), else(A,Y1),
				      p_tree_state(X2,Y2), inter(A,B,C).
% applyelse / applyelse
p_applyelserule(X1,Y1,X3,Y3) :- applyelserule(A,X1,X3), applyelserule(B,Y1,Y3),
			     else(A,X2), no_applyrule(A,X1,X2),
			     else(B,Y2), no_applyrule(B,Y1,Y2),
			     p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).

% else states
p_else(X,Y) :-  else(A,X), else(B,Y), inter(A,B,C).

% tree / tree
p_treerule(X,Y,X1,Y1) :- treerule(A,X,X1), treerule(B,Y,Y1), p_state(X,Y), inter(A,B,C). 

% epsilon / skip
p_epsilonArule(X,Y,X1,Y) :- epsilonrule(A,X,X1), p_state(X,Y), inter(A,B,C).

% skip / epsilon
p_epsilonBrule(X,Y,X,Y1) :- epsilonrule(B,Y,Y1), p_state(X,Y), inter(A,B,C).

% difference of signature
no_internalrule(A,X,L,T) :- not_in_signature(A,L,T), state(A,X), inter(A,B,C).
no_internalrule(B,Y,L,T) :- not_in_signature(B,L,T), state(B,Y), inter(A,B,C).

% title
p_title(X,Y) :- title(A,X), title(B,Y), inter(A,B,C).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% co-accessibility
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p_state(co_acc,X,Y) :- p_final(X,Y).
p_state(co_acc,X,Y) :- p_treerule(X,Y,X1,Y1), p_tree_state(co_acc,X1,Y1).
p_state(co_acc,X,Y) :- p_applyelserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).

p_tree_state(co_acc,X2,Y2) :- p_applyelserule(co_acc,X,Y,X1,Y1),
			      p_state(co_acc,X1,Y1), p_else(X2,Y2).

p_state(co_acc,X1,Y1) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_state(co_acc,X1,Y1)      :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_state(co_acc,X1,Y1)      :- p_applyrule(X1,Y1,X2,Y2,X3,Y3),  p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3),  p_state(co_acc,X3,Y3).
p_state(co_acc,X,Y)   :- p_internalrule(X,Y,L,T,X1,Y1),  p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_internalArule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_internalBrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_epsilonArule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_epsilonBrule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_elserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_typedelseArule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_typedelseBrule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_typedelserule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).

p_initial(co_acc,X,Y) :- p_state(co_acc,X,Y), p_initial(X,Y).
p_final(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_final(X,Y).
p_finalA(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_finalA(X,Y).
p_finalB(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_finalB(X,Y).
p_sink(co_acc,X,Y)    :- p_state(co_acc,X,Y), p_sink(X,Y).
p_sinkA(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_sinkA(X,Y).
p_else(co_acc,X,Y)    :- p_tree_state(co_acc,X,Y), p_else(X,Y).
p_elseA(co_acc,X,Y)   :- p_tree_state(co_acc,X,Y), p_elseA(X,Y).
p_elseB(co_acc,X,Y)   :- p_tree_state(co_acc,X,Y), p_elseB(X,Y).
p_start_tree(co_acc,X,Y) :- p_state(co_acc,X,Y), p_start_tree(X,Y).

p_copy_of(co_acc,X,Y,X1,Y1)     :- p_copy_of(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_tree_istate(co_acc,X,Y,X1,Y1) :- p_tree_istate(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_internalrule(co_acc,X,Y,L,T,X1,Y1)  :- p_internalrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_internalArule(co_acc,X,Y,L,T,X1,Y1) :- p_internalArule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_internalBrule(co_acc,X,Y,L,T,X1,Y1) :- p_internalBrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_epsilonArule(co_acc,X,Y,X1,Y1) :- p_epsilonArule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_epsilonBrule(co_acc,X,Y,X1,Y1) :- p_epsilonBrule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).

p_treerule(co_acc,X,Y,X1,Y1) :- p_treerule(X,Y,X1,Y1), p_tree_state(co_acc,X1,Y1).	
p_elserule(co_acc,X,Y,X1,Y1) :- p_elserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelserule(co_acc,X,Y,T,X1,Y1) :- p_typedelserule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelseArule(co_acc,X,Y,T,X1,Y1) :- p_typedelseArule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelseBrule(co_acc,X,Y,T,X1,Y1) :- p_typedelseBrule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_applyrule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyArule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyArule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyBrule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyBrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).

p_applyelserule(co_acc,X,Y,X2,Y2) :- p_applyelserule(X,Y,X2,Y2), p_state(co_acc,X2,Y2).
p_applyAelserule(co_acc,X,Y,X1,Y1,X2,Y2) :- p_applyAelserule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyelseBrule(co_acc,X,Y,X1,Y1,X2,Y2) :- p_applyelseBrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).


n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalrule(X1,Y1,L,T,X,Y).

n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalBrule(X1,Y1,L,T,X,Y).

n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalArule(X1,Y1,L,T,X,Y).

p_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- n_internalrule(co_acc,X1,Y1,L,T,X,Y).
p_internalArule(co_acc,X1,Y1,L,T,X,Y) :- n_internalArule(co_acc,X1,Y1,L,T,X,Y).
p_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- n_internalBrule(co_acc,X1,Y1,L,T,X,Y).

needed_p_state(co_acc,X,Y)            :- n_internalrule(co_acc,X1,Y1,L,T,X,Y). 
needed_p_stateA(co_acc,X,Y)           :- n_internalArule(co_acc,X1,Y1,L,T,X,Y).
needed_p_state (co_acc,X,Y)           :- n_internalBrule(co_acc,X1,Y1,L,T,X,Y).

p_originA(X,U,Y,Z) :- p_state(X,U),      origin_of(A,X,Y,Z), inter(A,B,C).
p_originA(X,U,Y,Z) :- p_tree_state(X,U), origin_of(A,X,Y,Z), inter(A,B,C).
p_labelA(L,T,Id)   :- label(A,L,T,Id), inter(A,B,C).
p_labelB(L,T,Id)   :- label(B,L,T,Id), inter(A,B,C).
p_max_label_idA(N)   :- max_label_id(A,N), inter(A,B,C).
p_max_label_idB(N)   :- max_label_id(B,N), inter(A,B,C).

p_originA(co_acc,X,U,Y,Z) :- p_originA(X,U,Y,Z), p_state(co_acc,X,U).
p_originA(co_acc,X,U,Y,Z) :- p_originA(X,U,Y,Z), p_tree_state(co_acc,X,U).
p_labelA(co_acc,L,T,Id)   :- p_labelA(L,T,Id).
p_labelB(co_acc,L,T,Id)   :- p_labelB(L,T,Id).
p_max_label_idA(co_acc,N)   :- p_max_label_idA(N).
p_max_label_idB(co_acc,N)   :- p_max_label_idB(N).


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXPRESSION=d1e9 AUTOMATON=stepwise
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule

% negative information only for elserules and applyelserules (NEGATIVE=false)
%   no_applyrule
%   no_internalrule
  
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(auto1,'step[&lt;doc⋅_⋅T&gt;]').
expression(auto1,'d1e9').
no_typedelserule(auto1,2,type).
no_typedelserule(auto1,3,type).
no_typedelserule(auto1,6,type).
no_typedelserule(auto1,9,type).
no_typedelserule(auto1,11,type).
state(auto1,2).
state(auto1,3).
state(auto1,4).
state(auto1,5).
state(auto1,6).
state(auto1,7).
state(auto1,9).
state(auto1,11).
start_tree(auto1,3).
start_tree(auto1,5).
initial(auto1,9).
not_sink(auto1,2).
not_sink(auto1,3).
not_sink(auto1,4).
not_sink(auto1,5).
not_sink(auto1,6).
not_sink(auto1,7).
not_sink(auto1,9).
not_sink(auto1,11).
else(auto1,6).
nonelse(auto1,2).
nonelse(auto1,3).
nonelse(auto1,4).
nonelse(auto1,5).
nonelse(auto1,7).
nonelse(auto1,9).
nonelse(auto1,11).
final(auto1,11).
nonfinal(auto1,2).
nonfinal(auto1,3).
nonfinal(auto1,4).
nonfinal(auto1,5).
nonfinal(auto1,6).
nonfinal(auto1,7).
nonfinal(auto1,9).
copy_of(auto1,2,2).
copy_of(auto1,3,2).
copy_of(auto1,4,2).
copy_of(auto1,5,6).
copy_of(auto1,6,6).
copy_of(auto1,7,2).
tree_state(auto1,2).
tree_istate(auto1,2,3).
 tree_state(auto1,6).
tree_istate(auto1,6,5).
 hedge_state(auto1,3).
    hedge_state(auto1,4).
    hedge_state(auto1,5).
    hedge_state(auto1,7).
    hedge_state(auto1,9).
    hedge_state(auto1,11).
    applyrule(auto1,9,2,11).
internalrule(auto1,3,'doc',type,4).
applyelserule(auto1,7,7).
no_applyrule(auto1,7,6).
no_applyrule(auto1,7,2).
elserule(auto1,7,7).
applyelserule(auto1,5,5).
no_applyrule(auto1,5,6).
no_applyrule(auto1,5,2).
elserule(auto1,5,5).
treerule(auto1,5,6).
elserule(auto1,4,7).
treerule(auto1,7,2).
label(auto1,'doc',type,1).
no_internalrule(auto1,4,'doc',type).
no_internalrule(auto1,5,'doc',type).
no_internalrule(auto1,7,'doc',type).
max_label_id(auto1,1).
    %%% different origins %%%%%%%
    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXPRESSION=d1e26 AUTOMATON=stepwise
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule

% negative information only for elserules and applyelserules (NEGATIVE=false)
%   no_applyrule
%   no_internalrule
  
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(auto2,'step[(μx_d1.(&lt;elem⋅h⋅section⋅x⋅T⋅&lt;att⋅default⋅data-type⋅_⋅t⋅i⋅t⋅l⋅e⋅p⋅a⋅g⋅e&gt;⋅T&gt;⋅T_bl_Z+_bl_Z&lt;(elem⋅_⋅__bl_Z+_bl_Zdoc)⋅_⋅T⋅x_d1&gt;⋅T))]').
expression(auto2,'d1e26').
var(auto2,'x',1).
no_typedelserule(auto2,1,char).
no_typedelserule(auto2,2,char).
no_typedelserule(auto2,3,char).
no_typedelserule(auto2,4,char).
no_typedelserule(auto2,5,char).
no_typedelserule(auto2,6,char).
no_typedelserule(auto2,7,char).
no_typedelserule(auto2,8,char).
no_typedelserule(auto2,9,char).
no_typedelserule(auto2,10,char).
no_typedelserule(auto2,12,char).
no_typedelserule(auto2,13,char).
no_typedelserule(auto2,14,char).
no_typedelserule(auto2,15,char).
no_typedelserule(auto2,16,char).
no_typedelserule(auto2,17,char).
no_typedelserule(auto2,18,char).
no_typedelserule(auto2,19,char).
no_typedelserule(auto2,20,char).
no_typedelserule(auto2,21,char).
no_typedelserule(auto2,22,char).
no_typedelserule(auto2,26,char).
no_typedelserule(auto2,27,char).
no_typedelserule(auto2,28,char).
no_typedelserule(auto2,29,char).
no_typedelserule(auto2,33,char).
no_typedelserule(auto2,35,char).
no_typedelserule(auto2,37,char).
no_typedelserule(auto2,39,char).
no_typedelserule(auto2,41,char).
no_typedelserule(auto2,1,type).
no_typedelserule(auto2,2,type).
no_typedelserule(auto2,3,type).
no_typedelserule(auto2,4,type).
no_typedelserule(auto2,5,type).
no_typedelserule(auto2,6,type).
no_typedelserule(auto2,7,type).
no_typedelserule(auto2,8,type).
no_typedelserule(auto2,9,type).
no_typedelserule(auto2,10,type).
no_typedelserule(auto2,12,type).
no_typedelserule(auto2,13,type).
no_typedelserule(auto2,14,type).
no_typedelserule(auto2,15,type).
no_typedelserule(auto2,16,type).
no_typedelserule(auto2,17,type).
no_typedelserule(auto2,18,type).
no_typedelserule(auto2,19,type).
no_typedelserule(auto2,20,type).
no_typedelserule(auto2,21,type).
no_typedelserule(auto2,22,type).
no_typedelserule(auto2,26,type).
no_typedelserule(auto2,27,type).
no_typedelserule(auto2,28,type).
no_typedelserule(auto2,29,type).
no_typedelserule(auto2,33,type).
no_typedelserule(auto2,35,type).
no_typedelserule(auto2,37,type).
no_typedelserule(auto2,39,type).
no_typedelserule(auto2,41,type).
no_typedelserule(auto2,1,name).
no_typedelserule(auto2,2,name).
no_typedelserule(auto2,3,name).
no_typedelserule(auto2,4,name).
no_typedelserule(auto2,5,name).
no_typedelserule(auto2,6,name).
no_typedelserule(auto2,7,name).
no_typedelserule(auto2,8,name).
no_typedelserule(auto2,9,name).
no_typedelserule(auto2,10,name).
no_typedelserule(auto2,12,name).
no_typedelserule(auto2,13,name).
no_typedelserule(auto2,14,name).
no_typedelserule(auto2,15,name).
no_typedelserule(auto2,16,name).
no_typedelserule(auto2,17,name).
no_typedelserule(auto2,18,name).
no_typedelserule(auto2,19,name).
no_typedelserule(auto2,20,name).
no_typedelserule(auto2,21,name).
no_typedelserule(auto2,22,name).
no_typedelserule(auto2,26,name).
no_typedelserule(auto2,27,name).
no_typedelserule(auto2,28,name).
no_typedelserule(auto2,29,name).
no_typedelserule(auto2,33,name).
no_typedelserule(auto2,35,name).
no_typedelserule(auto2,37,name).
no_typedelserule(auto2,39,name).
no_typedelserule(auto2,41,name).
no_typedelserule(auto2,1,namespace).
no_typedelserule(auto2,2,namespace).
no_typedelserule(auto2,3,namespace).
no_typedelserule(auto2,4,namespace).
no_typedelserule(auto2,5,namespace).
no_typedelserule(auto2,6,namespace).
no_typedelserule(auto2,7,namespace).
no_typedelserule(auto2,8,namespace).
no_typedelserule(auto2,9,namespace).
no_typedelserule(auto2,10,namespace).
no_typedelserule(auto2,12,namespace).
no_typedelserule(auto2,13,namespace).
no_typedelserule(auto2,14,namespace).
no_typedelserule(auto2,15,namespace).
no_typedelserule(auto2,16,namespace).
no_typedelserule(auto2,17,namespace).
no_typedelserule(auto2,18,namespace).
no_typedelserule(auto2,19,namespace).
no_typedelserule(auto2,20,namespace).
no_typedelserule(auto2,21,namespace).
no_typedelserule(auto2,22,namespace).
no_typedelserule(auto2,26,namespace).
no_typedelserule(auto2,27,namespace).
no_typedelserule(auto2,28,namespace).
no_typedelserule(auto2,29,namespace).
no_typedelserule(auto2,33,namespace).
no_typedelserule(auto2,35,namespace).
no_typedelserule(auto2,37,namespace).
no_typedelserule(auto2,39,namespace).
no_typedelserule(auto2,41,namespace).
no_typedelserule(auto2,1,var).
no_typedelserule(auto2,2,var).
no_typedelserule(auto2,3,var).
no_typedelserule(auto2,4,var).
no_typedelserule(auto2,5,var).
no_typedelserule(auto2,6,var).
no_typedelserule(auto2,7,var).
no_typedelserule(auto2,8,var).
no_typedelserule(auto2,9,var).
no_typedelserule(auto2,10,var).
no_typedelserule(auto2,12,var).
no_typedelserule(auto2,13,var).
no_typedelserule(auto2,14,var).
no_typedelserule(auto2,15,var).
no_typedelserule(auto2,16,var).
no_typedelserule(auto2,17,var).
no_typedelserule(auto2,18,var).
no_typedelserule(auto2,19,var).
no_typedelserule(auto2,20,var).
no_typedelserule(auto2,21,var).
no_typedelserule(auto2,22,var).
no_typedelserule(auto2,26,var).
no_typedelserule(auto2,27,var).
no_typedelserule(auto2,28,var).
no_typedelserule(auto2,29,var).
no_typedelserule(auto2,33,var).
no_typedelserule(auto2,35,var).
no_typedelserule(auto2,37,var).
no_typedelserule(auto2,39,var).
no_typedelserule(auto2,41,var).
state(auto2,1).
state(auto2,2).
state(auto2,3).
state(auto2,4).
state(auto2,5).
state(auto2,6).
state(auto2,7).
state(auto2,8).
state(auto2,9).
state(auto2,10).
state(auto2,11).
state(auto2,12).
state(auto2,13).
state(auto2,14).
state(auto2,15).
state(auto2,16).
state(auto2,17).
state(auto2,18).
state(auto2,19).
state(auto2,20).
state(auto2,21).
state(auto2,22).
state(auto2,23).
state(auto2,24).
state(auto2,25).
state(auto2,26).
state(auto2,27).
state(auto2,28).
state(auto2,29).
state(auto2,30).
state(auto2,31).
state(auto2,32).
state(auto2,33).
state(auto2,34).
state(auto2,35).
state(auto2,36).
state(auto2,37).
state(auto2,38).
state(auto2,39).
state(auto2,40).
state(auto2,41).
state(auto2,42).
start_tree(auto2,3).
start_tree(auto2,8).
start_tree(auto2,28).
start_tree(auto2,29).
start_tree(auto2,36).
initial(auto2,1).
initial(auto2,26).
not_sink(auto2,1).
not_sink(auto2,2).
not_sink(auto2,3).
not_sink(auto2,4).
not_sink(auto2,5).
not_sink(auto2,6).
not_sink(auto2,7).
not_sink(auto2,8).
not_sink(auto2,9).
not_sink(auto2,10).
not_sink(auto2,11).
not_sink(auto2,12).
not_sink(auto2,13).
not_sink(auto2,14).
not_sink(auto2,15).
not_sink(auto2,16).
not_sink(auto2,17).
not_sink(auto2,18).
not_sink(auto2,19).
not_sink(auto2,20).
not_sink(auto2,21).
not_sink(auto2,22).
not_sink(auto2,23).
not_sink(auto2,24).
not_sink(auto2,25).
not_sink(auto2,26).
not_sink(auto2,27).
not_sink(auto2,28).
not_sink(auto2,29).
not_sink(auto2,30).
not_sink(auto2,31).
not_sink(auto2,32).
not_sink(auto2,33).
not_sink(auto2,34).
not_sink(auto2,35).
not_sink(auto2,36).
not_sink(auto2,37).
not_sink(auto2,38).
not_sink(auto2,39).
not_sink(auto2,40).
not_sink(auto2,41).
not_sink(auto2,42).
else(auto2,37).
nonelse(auto2,1).
nonelse(auto2,2).
nonelse(auto2,3).
nonelse(auto2,4).
nonelse(auto2,5).
nonelse(auto2,6).
nonelse(auto2,7).
nonelse(auto2,8).
nonelse(auto2,9).
nonelse(auto2,10).
nonelse(auto2,11).
nonelse(auto2,12).
nonelse(auto2,13).
nonelse(auto2,14).
nonelse(auto2,15).
nonelse(auto2,16).
nonelse(auto2,17).
nonelse(auto2,18).
nonelse(auto2,19).
nonelse(auto2,20).
nonelse(auto2,21).
nonelse(auto2,22).
nonelse(auto2,23).
nonelse(auto2,24).
nonelse(auto2,25).
nonelse(auto2,26).
nonelse(auto2,27).
nonelse(auto2,28).
nonelse(auto2,29).
nonelse(auto2,30).
nonelse(auto2,31).
nonelse(auto2,32).
nonelse(auto2,33).
nonelse(auto2,34).
nonelse(auto2,35).
nonelse(auto2,36).
nonelse(auto2,38).
nonelse(auto2,39).
nonelse(auto2,40).
nonelse(auto2,41).
nonelse(auto2,42).
final(auto2,25).
final(auto2,38).
nonfinal(auto2,1).
nonfinal(auto2,2).
nonfinal(auto2,3).
nonfinal(auto2,4).
nonfinal(auto2,5).
nonfinal(auto2,6).
nonfinal(auto2,7).
nonfinal(auto2,8).
nonfinal(auto2,9).
nonfinal(auto2,10).
nonfinal(auto2,11).
nonfinal(auto2,12).
nonfinal(auto2,13).
nonfinal(auto2,14).
nonfinal(auto2,15).
nonfinal(auto2,16).
nonfinal(auto2,17).
nonfinal(auto2,18).
nonfinal(auto2,19).
nonfinal(auto2,20).
nonfinal(auto2,21).
nonfinal(auto2,22).
nonfinal(auto2,23).
nonfinal(auto2,24).
nonfinal(auto2,26).
nonfinal(auto2,27).
nonfinal(auto2,28).
nonfinal(auto2,29).
nonfinal(auto2,30).
nonfinal(auto2,31).
nonfinal(auto2,32).
nonfinal(auto2,33).
nonfinal(auto2,34).
nonfinal(auto2,35).
nonfinal(auto2,36).
nonfinal(auto2,37).
nonfinal(auto2,39).
nonfinal(auto2,40).
nonfinal(auto2,41).
nonfinal(auto2,42).
copy_of(auto2,2,2).
copy_of(auto2,3,2).
copy_of(auto2,4,2).
copy_of(auto2,5,2).
copy_of(auto2,6,2).
copy_of(auto2,7,7).
copy_of(auto2,8,7).
copy_of(auto2,9,7).
copy_of(auto2,10,7).
copy_of(auto2,11,7).
copy_of(auto2,12,7).
copy_of(auto2,13,7).
copy_of(auto2,14,7).
copy_of(auto2,15,7).
copy_of(auto2,16,7).
copy_of(auto2,17,7).
copy_of(auto2,18,7).
copy_of(auto2,19,7).
copy_of(auto2,20,7).
copy_of(auto2,21,7).
copy_of(auto2,22,2).
copy_of(auto2,23,2).
copy_of(auto2,24,2).
copy_of(auto2,27,27).
copy_of(auto2,28,27).
copy_of(auto2,29,27).
copy_of(auto2,30,27).
copy_of(auto2,31,27).
copy_of(auto2,32,27).
copy_of(auto2,33,27).
copy_of(auto2,34,27).
copy_of(auto2,35,27).
copy_of(auto2,36,37).
copy_of(auto2,37,37).
copy_of(auto2,39,27).
copy_of(auto2,40,27).
copy_of(auto2,41,27).
copy_of(auto2,42,27).
tree_state(auto2,2).
tree_istate(auto2,2,3).
 tree_state(auto2,7).
tree_istate(auto2,7,8).
 tree_state(auto2,27).
tree_istate(auto2,27,28).
 tree_istate(auto2,27,29).
 tree_state(auto2,37).
tree_istate(auto2,37,36).
 hedge_state(auto2,1).
    hedge_state(auto2,3).
    hedge_state(auto2,4).
    hedge_state(auto2,5).
    hedge_state(auto2,6).
    hedge_state(auto2,8).
    hedge_state(auto2,9).
    hedge_state(auto2,10).
    hedge_state(auto2,11).
    hedge_state(auto2,12).
    hedge_state(auto2,13).
    hedge_state(auto2,14).
    hedge_state(auto2,15).
    hedge_state(auto2,16).
    hedge_state(auto2,17).
    hedge_state(auto2,18).
    hedge_state(auto2,19).
    hedge_state(auto2,20).
    hedge_state(auto2,21).
    hedge_state(auto2,22).
    hedge_state(auto2,23).
    hedge_state(auto2,24).
    hedge_state(auto2,25).
    hedge_state(auto2,26).
    hedge_state(auto2,28).
    hedge_state(auto2,29).
    hedge_state(auto2,30).
    hedge_state(auto2,31).
    hedge_state(auto2,32).
    hedge_state(auto2,33).
    hedge_state(auto2,34).
    hedge_state(auto2,35).
    hedge_state(auto2,36).
    hedge_state(auto2,38).
    hedge_state(auto2,39).
    hedge_state(auto2,40).
    hedge_state(auto2,41).
    hedge_state(auto2,42).
    internalrule(auto2,3,'elem',type,4).
internalrule(auto2,4,'h',namespace,5).
internalrule(auto2,5,'section',name,6).
applyelserule(auto2,23,23).
no_applyrule(auto2,23,7).
no_applyrule(auto2,23,2).
no_applyrule(auto2,23,27).
no_applyrule(auto2,23,37).
elserule(auto2,23,23).
internalrule(auto2,6,'x',var,23).
internalrule(auto2,8,'att',type,9).
internalrule(auto2,9,'default',namespace,10).
internalrule(auto2,10,'data-type',name,11).
internalrule(auto2,20,'t',char,12).
internalrule(auto2,12,'i',char,13).
internalrule(auto2,13,'t',char,14).
internalrule(auto2,14,'l',char,15).
internalrule(auto2,15,'e',char,16).
internalrule(auto2,16,'p',char,17).
internalrule(auto2,17,'a',char,18).
internalrule(auto2,19,'e',char,21).
internalrule(auto2,18,'g',char,19).
elserule(auto2,11,20).
treerule(auto2,21,7).
applyelserule(auto2,24,24).
no_applyrule(auto2,24,7).
no_applyrule(auto2,24,2).
no_applyrule(auto2,24,27).
no_applyrule(auto2,24,37).
elserule(auto2,24,24).
applyrule(auto2,22,7,24).
applyelserule(auto2,23,22).
no_applyrule(auto2,23,7).
no_applyrule(auto2,23,2).
no_applyrule(auto2,23,27).
no_applyrule(auto2,23,37).
elserule(auto2,23,22).
internalrule(auto2,6,'x',var,22).
treerule(auto2,24,2).
applyelserule(auto2,25,25).
no_applyrule(auto2,25,7).
no_applyrule(auto2,25,2).
no_applyrule(auto2,25,27).
no_applyrule(auto2,25,37).
elserule(auto2,25,25).
applyrule(auto2,1,2,25).
internalrule(auto2,28,'elem',type,30).
elserule(auto2,30,31).
elserule(auto2,31,32).
internalrule(auto2,29,'doc',type,32).
applyelserule(auto2,34,34).
no_applyrule(auto2,34,7).
no_applyrule(auto2,34,2).
no_applyrule(auto2,34,27).
no_applyrule(auto2,34,37).
elserule(auto2,34,34).
elserule(auto2,32,34).
applyelserule(auto2,34,33).
no_applyrule(auto2,34,7).
no_applyrule(auto2,34,2).
no_applyrule(auto2,34,27).
no_applyrule(auto2,34,37).
elserule(auto2,34,33).
elserule(auto2,32,33).
treerule(auto2,35,27).
applyelserule(auto2,38,38).
no_applyrule(auto2,38,7).
no_applyrule(auto2,38,2).
no_applyrule(auto2,38,27).
no_applyrule(auto2,38,37).
elserule(auto2,38,38).
applyelserule(auto2,36,36).
no_applyrule(auto2,36,7).
no_applyrule(auto2,36,2).
no_applyrule(auto2,36,27).
no_applyrule(auto2,36,37).
elserule(auto2,36,36).
treerule(auto2,36,37).
applyrule(auto2,26,27,38).
applyelserule(auto2,40,40).
no_applyrule(auto2,40,7).
no_applyrule(auto2,40,2).
no_applyrule(auto2,40,27).
no_applyrule(auto2,40,37).
elserule(auto2,40,40).
applyrule(auto2,39,2,40).
applyelserule(auto2,42,42).
no_applyrule(auto2,42,7).
no_applyrule(auto2,42,2).
no_applyrule(auto2,42,27).
no_applyrule(auto2,42,37).
elserule(auto2,42,42).
applyrule(auto2,41,27,42).
epsilonrule(auto2,40,35).
epsilonrule(auto2,42,35).
epsilonrule(auto2,33,39).
epsilonrule(auto2,33,41).
label(auto2,'a',char,1).
no_internalrule(auto2,11,'a',char).
no_internalrule(auto2,23,'a',char).
no_internalrule(auto2,24,'a',char).
no_internalrule(auto2,25,'a',char).
no_internalrule(auto2,30,'a',char).
no_internalrule(auto2,31,'a',char).
no_internalrule(auto2,32,'a',char).
no_internalrule(auto2,34,'a',char).
no_internalrule(auto2,36,'a',char).
no_internalrule(auto2,38,'a',char).
no_internalrule(auto2,40,'a',char).
no_internalrule(auto2,42,'a',char).
label(auto2,'att',type,2).
no_internalrule(auto2,11,'att',type).
no_internalrule(auto2,23,'att',type).
no_internalrule(auto2,24,'att',type).
no_internalrule(auto2,25,'att',type).
no_internalrule(auto2,30,'att',type).
no_internalrule(auto2,31,'att',type).
no_internalrule(auto2,32,'att',type).
no_internalrule(auto2,34,'att',type).
no_internalrule(auto2,36,'att',type).
no_internalrule(auto2,38,'att',type).
no_internalrule(auto2,40,'att',type).
no_internalrule(auto2,42,'att',type).
label(auto2,'data-type',name,3).
no_internalrule(auto2,11,'data-type',name).
no_internalrule(auto2,23,'data-type',name).
no_internalrule(auto2,24,'data-type',name).
no_internalrule(auto2,25,'data-type',name).
no_internalrule(auto2,30,'data-type',name).
no_internalrule(auto2,31,'data-type',name).
no_internalrule(auto2,32,'data-type',name).
no_internalrule(auto2,34,'data-type',name).
no_internalrule(auto2,36,'data-type',name).
no_internalrule(auto2,38,'data-type',name).
no_internalrule(auto2,40,'data-type',name).
no_internalrule(auto2,42,'data-type',name).
label(auto2,'default',namespace,4).
no_internalrule(auto2,11,'default',namespace).
no_internalrule(auto2,23,'default',namespace).
no_internalrule(auto2,24,'default',namespace).
no_internalrule(auto2,25,'default',namespace).
no_internalrule(auto2,30,'default',namespace).
no_internalrule(auto2,31,'default',namespace).
no_internalrule(auto2,32,'default',namespace).
no_internalrule(auto2,34,'default',namespace).
no_internalrule(auto2,36,'default',namespace).
no_internalrule(auto2,38,'default',namespace).
no_internalrule(auto2,40,'default',namespace).
no_internalrule(auto2,42,'default',namespace).
label(auto2,'doc',type,5).
no_internalrule(auto2,11,'doc',type).
no_internalrule(auto2,23,'doc',type).
no_internalrule(auto2,24,'doc',type).
no_internalrule(auto2,25,'doc',type).
no_internalrule(auto2,30,'doc',type).
no_internalrule(auto2,31,'doc',type).
no_internalrule(auto2,32,'doc',type).
no_internalrule(auto2,34,'doc',type).
no_internalrule(auto2,36,'doc',type).
no_internalrule(auto2,38,'doc',type).
no_internalrule(auto2,40,'doc',type).
no_internalrule(auto2,42,'doc',type).
label(auto2,'e',char,6).
no_internalrule(auto2,11,'e',char).
no_internalrule(auto2,23,'e',char).
no_internalrule(auto2,24,'e',char).
no_internalrule(auto2,25,'e',char).
no_internalrule(auto2,30,'e',char).
no_internalrule(auto2,31,'e',char).
no_internalrule(auto2,32,'e',char).
no_internalrule(auto2,34,'e',char).
no_internalrule(auto2,36,'e',char).
no_internalrule(auto2,38,'e',char).
no_internalrule(auto2,40,'e',char).
no_internalrule(auto2,42,'e',char).
label(auto2,'elem',type,7).
no_internalrule(auto2,11,'elem',type).
no_internalrule(auto2,23,'elem',type).
no_internalrule(auto2,24,'elem',type).
no_internalrule(auto2,25,'elem',type).
no_internalrule(auto2,30,'elem',type).
no_internalrule(auto2,31,'elem',type).
no_internalrule(auto2,32,'elem',type).
no_internalrule(auto2,34,'elem',type).
no_internalrule(auto2,36,'elem',type).
no_internalrule(auto2,38,'elem',type).
no_internalrule(auto2,40,'elem',type).
no_internalrule(auto2,42,'elem',type).
label(auto2,'g',char,8).
no_internalrule(auto2,11,'g',char).
no_internalrule(auto2,23,'g',char).
no_internalrule(auto2,24,'g',char).
no_internalrule(auto2,25,'g',char).
no_internalrule(auto2,30,'g',char).
no_internalrule(auto2,31,'g',char).
no_internalrule(auto2,32,'g',char).
no_internalrule(auto2,34,'g',char).
no_internalrule(auto2,36,'g',char).
no_internalrule(auto2,38,'g',char).
no_internalrule(auto2,40,'g',char).
no_internalrule(auto2,42,'g',char).
label(auto2,'h',namespace,9).
no_internalrule(auto2,11,'h',namespace).
no_internalrule(auto2,23,'h',namespace).
no_internalrule(auto2,24,'h',namespace).
no_internalrule(auto2,25,'h',namespace).
no_internalrule(auto2,30,'h',namespace).
no_internalrule(auto2,31,'h',namespace).
no_internalrule(auto2,32,'h',namespace).
no_internalrule(auto2,34,'h',namespace).
no_internalrule(auto2,36,'h',namespace).
no_internalrule(auto2,38,'h',namespace).
no_internalrule(auto2,40,'h',namespace).
no_internalrule(auto2,42,'h',namespace).
label(auto2,'i',char,10).
no_internalrule(auto2,11,'i',char).
no_internalrule(auto2,23,'i',char).
no_internalrule(auto2,24,'i',char).
no_internalrule(auto2,25,'i',char).
no_internalrule(auto2,30,'i',char).
no_internalrule(auto2,31,'i',char).
no_internalrule(auto2,32,'i',char).
no_internalrule(auto2,34,'i',char).
no_internalrule(auto2,36,'i',char).
no_internalrule(auto2,38,'i',char).
no_internalrule(auto2,40,'i',char).
no_internalrule(auto2,42,'i',char).
label(auto2,'l',char,11).
no_internalrule(auto2,11,'l',char).
no_internalrule(auto2,23,'l',char).
no_internalrule(auto2,24,'l',char).
no_internalrule(auto2,25,'l',char).
no_internalrule(auto2,30,'l',char).
no_internalrule(auto2,31,'l',char).
no_internalrule(auto2,32,'l',char).
no_internalrule(auto2,34,'l',char).
no_internalrule(auto2,36,'l',char).
no_internalrule(auto2,38,'l',char).
no_internalrule(auto2,40,'l',char).
no_internalrule(auto2,42,'l',char).
label(auto2,'p',char,12).
no_internalrule(auto2,11,'p',char).
no_internalrule(auto2,23,'p',char).
no_internalrule(auto2,24,'p',char).
no_internalrule(auto2,25,'p',char).
no_internalrule(auto2,30,'p',char).
no_internalrule(auto2,31,'p',char).
no_internalrule(auto2,32,'p',char).
no_internalrule(auto2,34,'p',char).
no_internalrule(auto2,36,'p',char).
no_internalrule(auto2,38,'p',char).
no_internalrule(auto2,40,'p',char).
no_internalrule(auto2,42,'p',char).
label(auto2,'section',name,13).
no_internalrule(auto2,11,'section',name).
no_internalrule(auto2,23,'section',name).
no_internalrule(auto2,24,'section',name).
no_internalrule(auto2,25,'section',name).
no_internalrule(auto2,30,'section',name).
no_internalrule(auto2,31,'section',name).
no_internalrule(auto2,32,'section',name).
no_internalrule(auto2,34,'section',name).
no_internalrule(auto2,36,'section',name).
no_internalrule(auto2,38,'section',name).
no_internalrule(auto2,40,'section',name).
no_internalrule(auto2,42,'section',name).
label(auto2,'t',char,14).
no_internalrule(auto2,11,'t',char).
no_internalrule(auto2,23,'t',char).
no_internalrule(auto2,24,'t',char).
no_internalrule(auto2,25,'t',char).
no_internalrule(auto2,30,'t',char).
no_internalrule(auto2,31,'t',char).
no_internalrule(auto2,32,'t',char).
no_internalrule(auto2,34,'t',char).
no_internalrule(auto2,36,'t',char).
no_internalrule(auto2,38,'t',char).
no_internalrule(auto2,40,'t',char).
no_internalrule(auto2,42,'t',char).
label(auto2,'x',var,15).
no_internalrule(auto2,11,'x',var).
no_internalrule(auto2,23,'x',var).
no_internalrule(auto2,24,'x',var).
no_internalrule(auto2,25,'x',var).
no_internalrule(auto2,30,'x',var).
no_internalrule(auto2,31,'x',var).
no_internalrule(auto2,32,'x',var).
no_internalrule(auto2,34,'x',var).
no_internalrule(auto2,36,'x',var).
no_internalrule(auto2,38,'x',var).
no_internalrule(auto2,40,'x',var).
no_internalrule(auto2,42,'x',var).
max_label_id(auto2,15).
    %%% different origins %%%%%%%
    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% not in signature

      %%%%% difference of signatures %%%%%%%%%%%
      
    
not_in_signature(auto1,'a',char).
not_in_signature(auto1,'att',type).
not_in_signature(auto1,'data-type',name).
not_in_signature(auto1,'default',namespace).
not_in_signature(auto1,'e',char).
not_in_signature(auto1,'elem',type).
not_in_signature(auto1,'g',char).
not_in_signature(auto1,'h',namespace).
not_in_signature(auto1,'i',char).
not_in_signature(auto1,'l',char).
not_in_signature(auto1,'p',char).
not_in_signature(auto1,'section',name).
not_in_signature(auto1,'t',char).
not_in_signature(auto1,'x',var).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% intersection auto=auto1.auto2
inter(auto1,auto2,auto).
