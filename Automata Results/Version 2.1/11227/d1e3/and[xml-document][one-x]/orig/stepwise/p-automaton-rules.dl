%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  intersection of stepwise hedge automata A and B
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% variables X,Y for states,
%%           L,T for labels and their types
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% hedge states
p_state(X,Y)   :- p_start_tree(X,Y).
p_state(X,Y)   :- p_initial(X,Y).
p_state(X1,Y1) :- p_elserule(X,Y,X1,Y1).
p_state(X1,Y1) :- p_typedelserule(X,Y,T,X1,Y1).
p_state(X1,Y1) :- p_typedelseArule(X,Y,T,X1,Y1).
p_state(X1,Y1) :- p_typedelseBrule(X,Y,T,X1,Y1).
p_state(X3,Y3) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyelserule(X1,Y1,X3,Y3).
p_state(X3,Y3) :- p_applyAelserule(X1,Y1,X3,Y3).
p_state(X3,Y3) :- p_applyBelserule(X1,Y1,X3,Y3).
p_state(X1,Y1) :- p_internalrule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_internalArule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_internalBrule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_epsilonArule(X,Y,X1,Y1).
p_state(X1,Y1) :- p_epsilonBrule(X,Y,X1,Y1).
p_state(IX,IY) :- p_tree_istate(X2,Y2,IX,IY).

%%% tree states that are copies of hedge states
p_copy_of(X1,X2,Z1,Z2) :- p_state(X1,X2), copy_of(A,X1,Z1), state(A,Z1),
                          copy_of(B,X2,Z2), state(B,Z2), inter(A,B,C).

%%% tree states and istates of tree states
p_tree_state(X2,Y2)        :- p_tree_istate(X2,Y2,IX,IY).
p_tree_istate(X2,Y2,IX,IY) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyelserule(X1,Y1,X3,Y3), 
                               tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).

p_tree_state(X2,Y2) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyelserule(X1,Y1,X3,Y3), tree_state(A,X2), tree_state(B,Y2), inter(A,B,C).  

p_tree_state(Z1,Z2) :- p_copy_of(X1,X2,Z1,Z2).
p_tree_state(U,V)   :- p_treerule(X,Y,U,V).

%%% distinguished states
p_initial(X,Y)    :- initial(A,X), initial(B,Y), inter(A,B,C).
p_else(X,Y)       :- p_tree_state(X,Y), else(A,X), else(B,Y),  inter(A,B,C).
p_elseA(X,Y)      :- else(A,X), p_tree_state(X,Y), inter(A,B,C).
p_elseB(X,Y)      :- p_tree_state(X,Y), else(B,Y), inter(A,B,C).
p_final(X,Y)      :- final(A,X), final(B,Y), p_state(X,Y), inter(A,B,C).
p_start_tree(X,Y) :- start_tree(A,X), start_tree(B,Y), inter(A,B,C).
p_sinkA(X,Y)       :- sink(A,X), state(B,Y), inter(A,B,C).
p_sinkB(X,Y)       :- state(A,X), sink(B,Y), inter(A,B,C).
p_sink(X,Y)        :- p_sinkA(X,Y).
p_sink(X,Y)        :- p_sinkB(X,Y).

%% origins
%  p_originA_of(X,Y,X1,Y2)   :- origin_of(A,X,X1,X2), state(B,Y), inter(A,B,C).

%%% internalrules

internalfromelse(B,Y,L,T,Y1) :- elserule(B,Y,Y1), no_internalrule(B,Y,L,T).
internalfromelse(B,Y,L,T,Y1) :- typedelserule(B,Y,T,Y1), no_internalrule(B,Y,L,T).
internalfromelse(B,Y,L,T,Y1) :- elserule(B,Y,Y1), not_in_signature(B,L,T).
internalfromelse(B,Y,L,T,Y1) :- typedelserule(B,Y,T,Y1), not_in_signature(B,L,T).


% internal / else
p_internalArule(X,Y,L,T,X1,Y1) :- internalrule(A,X,L,T,X1), internalfromelse(B,Y,L,T,Y1),
                                  p_state(X,Y), inter(A,B,C).
% else / internal
p_internalBrule(X,Y,L,T,X1,Y1) :- internalfromelse(A,X,L,T,X1), internalrule(B,Y,L,T,Y1),
			       	  p_state(X,Y), inter(A,B,C).

% internal / internal
p_internalrule(X,Y,L,T,X1,Y1) :- internalrule(A,X,L,T,X1), internalrule(B,Y,L,T,Y1),
			      	 p_state(X,Y), inter(A,B,C).

%%% else rules typed and untyped

% else / else
p_elserule(X,Y,X1,Y1) :- elserule(A,X,X1), elserule(B,Y,Y1), p_state(X,Y), inter(A,B,C).

% typed_else / typed_else
p_typedelserule(X,Y,T,X1,Y1) :- typedelserule(A,X,T,X1), typedelserule(B,Y,T,Y1),
			     p_state(X,Y), inter(A,B,C).
% typed_else / else
p_typedelseArule(X,Y,T,X1,Y1) :- typedelserule(A,X,T,X1), elserule(B,Y,Y1),
			      p_state(X,Y), inter(A,B,C).

% else / typed_else
p_typedelseBrule(X,Y,T,X1,Y1) :- elserule(A,X,X1), typedelserule(B,Y,T,Y1),
			      p_state(X,Y), inter(A,B,C).


%%% applyrules

applyfromelse(B,Y1,Y2,Y3) :- else(B,Y2), no_applyrule(B,Y1,Y2), applyelserule(B,Y1,Y3).

% apply / apply
p_applyrule(X1,Y1,X2,Y2,X3,Y3) :- applyrule(A,X1,X2,X3), applyrule(B,Y1,Y2,Y3),
			          p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
% apply / applyelse
p_applyArule(X1,Y1,X2,Y2,X3,Y3) :- applyrule(A,X1,X2,X3), applyfromelse(B,Y1,Y2,Y3),
				p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
p_applyAelserule(X1,Y1,X2,else,X3,Y3) :- applyrule(A,X1,X2,X3), applyelserule(B,Y1,Y3),
				      p_state(X1,Y1), else(B,Y2),
				      p_tree_state(X2,Y2), inter(A,B,C).
% applyelse / apply
p_applyBrule(X1,Y1,X2,Y2,X3,Y3) :- applyfromelse(A,X1,X2,X3), applyrule(B,Y1,Y2,Y3),
				p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
p_applyBelserule(X1,Y1,else,Y2,X3,Y3) :- applyelserule(A,X1,X3), applyrule(B,Y1,Y2,Y3),
				      p_state(X1,Y1), else(A,Y1),
				      p_tree_state(X2,Y2), inter(A,B,C).
% applyelse / applyelse
p_applyelserule(X1,Y1,X3,Y3) :- applyelserule(A,X1,X3), applyelserule(B,Y1,Y3),
			     else(A,X2), no_applyrule(A,X1,X2),
			     else(B,Y2), no_applyrule(B,Y1,Y2),
			     p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).

% else states
p_else(X,Y) :-  else(A,X), else(B,Y), inter(A,B,C).

% tree / tree
p_treerule(X,Y,X1,Y1) :- treerule(A,X,X1), treerule(B,Y,Y1), p_state(X,Y), inter(A,B,C). 

% epsilon / skip
p_epsilonArule(X,Y,X1,Y) :- epsilonrule(A,X,X1), p_state(X,Y), inter(A,B,C).

% skip / epsilon
p_epsilonBrule(X,Y,X,Y1) :- epsilonrule(B,Y,Y1), p_state(X,Y), inter(A,B,C).

% difference of signature
no_internalrule(A,X,L,T) :- not_in_signature(A,L,T), state(A,X), inter(A,B,C).
no_internalrule(B,Y,L,T) :- not_in_signature(B,L,T), state(B,Y), inter(A,B,C).

% title
p_title(X,Y) :- title(A,X), title(B,Y), inter(A,B,C).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% co-accessibility
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p_state(co_acc,X,Y) :- p_final(X,Y).
p_state(co_acc,X,Y) :- p_treerule(X,Y,X1,Y1), p_tree_state(co_acc,X1,Y1).
p_state(co_acc,X,Y) :- p_applyelserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).

p_tree_state(co_acc,X2,Y2) :- p_applyelserule(co_acc,X,Y,X1,Y1),
			      p_state(co_acc,X1,Y1), p_else(X2,Y2).

p_state(co_acc,X1,Y1) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_state(co_acc,X1,Y1)      :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_state(co_acc,X1,Y1)      :- p_applyrule(X1,Y1,X2,Y2,X3,Y3),  p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3),  p_state(co_acc,X3,Y3).
p_state(co_acc,X,Y)   :- p_internalrule(X,Y,L,T,X1,Y1),  p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_internalArule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_internalBrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_epsilonArule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_epsilonBrule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_elserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_typedelseArule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_typedelseBrule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).

p_initial(co_acc,X,Y) :- p_state(co_acc,X,Y), p_initial(X,Y).
p_final(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_final(X,Y).
p_sink(co_acc,X,Y)    :- p_state(co_acc,X,Y), p_sink(X,Y).
p_sinkA(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_sinkA(X,Y).
p_else(co_acc,X,Y)    :- p_tree_state(co_acc,X,Y), p_else(X,Y).
p_elseA(co_acc,X,Y)   :- p_tree_state(co_acc,X,Y), p_elseA(X,Y).
p_elseB(co_acc,X,Y)   :- p_tree_state(co_acc,X,Y), p_elseB(X,Y).
p_start_tree(co_acc,X,Y) :- p_state(co_acc,X,Y), p_start_tree(X,Y).

p_copy_of(co_acc,X,Y,X1,Y1)     :- p_copy_of(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_tree_istate(co_acc,X,Y,X1,Y1) :- p_tree_istate(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_internalrule(co_acc,X,Y,L,T,X1,Y1)  :- p_internalrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_internalArule(co_acc,X,Y,L,T,X1,Y1) :- p_internalArule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_internalBrule(co_acc,X,Y,L,T,X1,Y1) :- p_internalBrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_epsilonArule(co_acc,X,Y,X1,Y1) :- p_epsilonArule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_epsilonBrule(co_acc,X,Y,X1,Y1) :- p_epsilonBrule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).

p_treerule(co_acc,X,Y,X1,Y1) :- p_treerule(X,Y,X1,Y1), p_tree_state(co_acc,X1,Y1).	
p_elserule(co_acc,X,Y,X1,Y1) :- p_elserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelserule(co_acc,X,Y,T,X1,Y1) :- p_typedelserule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelseArule(co_acc,X,Y,T,X1,Y1) :- p_typedelseArule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelseBrule(co_acc,X,Y,T,X1,Y1) :- p_typedelseBrule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_applyrule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyArule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyArule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyBrule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyBrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).

p_applyelserule(co_acc,X,Y,X2,Y2) :- p_applyelserule(X,Y,X2,Y2), p_state(co_acc,X2,Y2).
p_applyAelserule(co_acc,X,Y,X1,Y1,X2,Y2) :- p_applyAelserule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyelseBrule(co_acc,X,Y,X1,Y1,X2,Y2) :- p_applyelseBrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).


n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalrule(X1,Y1,L,T,X,Y).

n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalBrule(X1,Y1,L,T,X,Y).

n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalArule(X1,Y1,L,T,X,Y).

p_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- n_internalrule(co_acc,X1,Y1,L,T,X,Y).
p_internalArule(co_acc,X1,Y1,L,T,X,Y) :- n_internalArule(co_acc,X1,Y1,L,T,X,Y).
p_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- n_internalBrule(co_acc,X1,Y1,L,T,X,Y).

needed_p_state(co_acc,X,Y)            :- n_internalrule(co_acc,X1,Y1,L,T,X,Y). 
needed_p_stateA(co_acc,X,Y)           :- n_internalArule(co_acc,X1,Y1,L,T,X,Y).
needed_p_state (co_acc,X,Y)           :- n_internalBrule(co_acc,X1,Y1,L,T,X,Y).

p_originA(X,U,Y,Z) :- p_state(X,U),      origin_of(A,X,Y,Z), inter(A,B,C).
p_originA(X,U,Y,Z) :- p_tree_state(X,U), origin_of(A,X,Y,Z), inter(A,B,C).
p_labelA(L,T,Id)   :- label(A,L,T,Id), inter(A,B,C).
p_labelB(L,T,Id)   :- label(B,L,T,Id), inter(A,B,C).
p_max_label_idA(N)   :- max_label_id(A,N), inter(A,B,C).
p_max_label_idB(N)   :- max_label_id(B,N), inter(A,B,C).

p_originA(co_acc,X,U,Y,Z) :- p_originA(X,U,Y,Z), p_state(co_acc,X,U).
p_originA(co_acc,X,U,Y,Z) :- p_originA(X,U,Y,Z), p_tree_state(co_acc,X,U).
p_labelA(co_acc,L,T,Id)   :- p_labelA(L,T,Id).
p_labelB(co_acc,L,T,Id)   :- p_labelB(L,T,Id).
p_max_label_idA(co_acc,N)   :- p_max_label_idA(N).
p_max_label_idB(co_acc,N)   :- p_max_label_idB(N).


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXPRESSION=xml-document AUTOMATON=det-stepwise
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule

% negative information only for elserules and applyelserules (NEGATIVE=false)
%   no_applyrule
%   no_internalrule
  
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(auto1,'det[stepwise[&lt;doc⋅x+¬x⋅stepwise[]&gt;+(μelement.&lt;elem⋅_namespace⋅_name⋅(x_bl_Z+_bl_Z¬x)⋅(&lt;att⋅_namespace⋅_name⋅(x_bl_Z+_bl_Z¬x)⋅(_char)*&gt;)*⋅(((&lt;text⋅(x_bl_Z+_bl_Z¬x)⋅(_char)*&gt;_bl_Z+_bl_Z&lt;comment⋅(x_bl_Z+_bl_Z¬x)⋅(_char)*&gt;)_bl_Z+_bl_Zelement))*&gt;)*]]').
var(auto1,'x',1).
no_typedelserule(auto1,19,name).
no_typedelserule(auto1,29,name).
no_typedelserule(auto1,28,name).
no_typedelserule(auto1,3,name).
no_typedelserule(auto1,17,name).
no_typedelserule(auto1,25,name).
no_typedelserule(auto1,2,name).
no_typedelserule(auto1,1,name).
no_typedelserule(auto1,18,name).
no_typedelserule(auto1,0,name).
no_typedelserule(auto1,5,name).
no_typedelserule(auto1,26,name).
no_typedelserule(auto1,16,name).
no_typedelserule(auto1,20,name).
no_typedelserule(auto1,13,name).
no_typedelserule(auto1,22,name).
no_typedelserule(auto1,11,name).
no_typedelserule(auto1,7,name).
no_typedelserule(auto1,6,name).
no_typedelserule(auto1,19,char).
no_typedelserule(auto1,29,char).
no_typedelserule(auto1,28,char).
no_typedelserule(auto1,3,char).
no_typedelserule(auto1,17,char).
no_typedelserule(auto1,25,char).
no_typedelserule(auto1,2,char).
no_typedelserule(auto1,10,char).
no_typedelserule(auto1,1,char).
no_typedelserule(auto1,0,char).
no_typedelserule(auto1,5,char).
no_typedelserule(auto1,26,char).
no_typedelserule(auto1,16,char).
no_typedelserule(auto1,20,char).
no_typedelserule(auto1,8,char).
no_typedelserule(auto1,13,char).
no_typedelserule(auto1,22,char).
no_typedelserule(auto1,11,char).
no_typedelserule(auto1,6,char).
no_typedelserule(auto1,19,namespace).
no_typedelserule(auto1,29,namespace).
no_typedelserule(auto1,28,namespace).
no_typedelserule(auto1,17,namespace).
no_typedelserule(auto1,25,namespace).
no_typedelserule(auto1,2,namespace).
no_typedelserule(auto1,10,namespace).
no_typedelserule(auto1,1,namespace).
no_typedelserule(auto1,18,namespace).
no_typedelserule(auto1,0,namespace).
no_typedelserule(auto1,26,namespace).
no_typedelserule(auto1,16,namespace).
no_typedelserule(auto1,20,namespace).
no_typedelserule(auto1,8,namespace).
no_typedelserule(auto1,13,namespace).
no_typedelserule(auto1,22,namespace).
no_typedelserule(auto1,11,namespace).
no_typedelserule(auto1,7,namespace).
no_typedelserule(auto1,6,namespace).
state(auto1,19).
state(auto1,29).
state(auto1,28).
state(auto1,3).
state(auto1,17).
state(auto1,25).
state(auto1,2).
state(auto1,10).
state(auto1,1).
state(auto1,18).
state(auto1,0).
state(auto1,5).
state(auto1,26).
state(auto1,16).
state(auto1,20).
state(auto1,8).
state(auto1,13).
state(auto1,22).
state(auto1,11).
state(auto1,7).
state(auto1,6).
start_tree(auto1,0).
initial(auto1,0).
                     not_sink(auto1,19).
not_sink(auto1,29).
not_sink(auto1,28).
not_sink(auto1,3).
not_sink(auto1,17).
not_sink(auto1,25).
not_sink(auto1,2).
not_sink(auto1,10).
not_sink(auto1,1).
not_sink(auto1,18).
not_sink(auto1,0).
not_sink(auto1,5).
not_sink(auto1,26).
not_sink(auto1,16).
not_sink(auto1,20).
not_sink(auto1,8).
not_sink(auto1,13).
not_sink(auto1,22).
not_sink(auto1,11).
not_sink(auto1,7).
not_sink(auto1,6).
nonelse(auto1,19).
nonelse(auto1,29).
nonelse(auto1,28).
nonelse(auto1,3).
nonelse(auto1,17).
nonelse(auto1,25).
nonelse(auto1,2).
nonelse(auto1,10).
nonelse(auto1,1).
nonelse(auto1,18).
nonelse(auto1,0).
nonelse(auto1,5).
nonelse(auto1,26).
nonelse(auto1,16).
nonelse(auto1,20).
nonelse(auto1,8).
nonelse(auto1,13).
nonelse(auto1,22).
nonelse(auto1,11).
nonelse(auto1,7).
nonelse(auto1,6).
final(auto1,29).
final(auto1,26).
nonfinal(auto1,19).
nonfinal(auto1,28).
nonfinal(auto1,3).
nonfinal(auto1,17).
nonfinal(auto1,25).
nonfinal(auto1,2).
nonfinal(auto1,10).
nonfinal(auto1,1).
nonfinal(auto1,18).
nonfinal(auto1,0).
nonfinal(auto1,5).
nonfinal(auto1,16).
nonfinal(auto1,20).
nonfinal(auto1,8).
nonfinal(auto1,13).
nonfinal(auto1,22).
nonfinal(auto1,11).
nonfinal(auto1,7).
nonfinal(auto1,6).
tree_state(auto1,19).
tree_istate(auto1,19,0).
 tree_state(auto1,28).
tree_istate(auto1,28,0).
 tree_state(auto1,22).
tree_istate(auto1,22,0).
 tree_state(auto1,11).
tree_istate(auto1,11,0).
 hedge_state(auto1,29).
    hedge_state(auto1,3).
    hedge_state(auto1,17).
    hedge_state(auto1,25).
    hedge_state(auto1,2).
    hedge_state(auto1,10).
    hedge_state(auto1,1).
    hedge_state(auto1,18).
    hedge_state(auto1,0).
    hedge_state(auto1,5).
    hedge_state(auto1,26).
    hedge_state(auto1,16).
    hedge_state(auto1,20).
    hedge_state(auto1,8).
    hedge_state(auto1,13).
    hedge_state(auto1,7).
    hedge_state(auto1,6).
    internalrule(auto1,2,'x',negvar,7).
internalrule(auto1,16,'x',var,18).
typedelserule(auto1,8,name,13).
applyrule(auto1,20,19,20).
internalrule(auto1,0,'att',type,5).
typedelserule(auto1,18,char,18).
applyrule(auto1,20,11,20).
internalrule(auto1,16,'x',negvar,18).
applyrule(auto1,26,19,26).
applyrule(auto1,17,11,20).
applyrule(auto1,0,19,26).
treerule(auto1,17,19).
internalrule(auto1,1,'x',negvar,6).
applyrule(auto1,6,19,25).
internalrule(auto1,13,'x',negvar,17).
typedelserule(auto1,5,namespace,10).
internalrule(auto1,0,'text',type,2).
typedelserule(auto1,3,namespace,8).
internalrule(auto1,0,'elem',type,3).
typedelserule(auto1,10,name,16).
applyrule(auto1,17,19,20).
internalrule(auto1,13,'x',var,17).
internalrule(auto1,0,'doc',type,1).
treerule(auto1,7,11).
treerule(auto1,20,19).
applyrule(auto1,0,28,29).
internalrule(auto1,2,'x',var,7).
internalrule(auto1,0,'comment',type,2).
treerule(auto1,25,28).
typedelserule(auto1,7,char,7).
treerule(auto1,18,22).
internalrule(auto1,1,'x',var,6).
applyrule(auto1,17,22,17).
label(auto1,'text',type,5).
no_internalrule(auto1,3,'text',type).
no_internalrule(auto1,10,'text',type).
no_internalrule(auto1,18,'text',type).
no_internalrule(auto1,5,'text',type).
no_internalrule(auto1,8,'text',type).
no_internalrule(auto1,7,'text',type).
label(auto1,'doc',type,3).
no_internalrule(auto1,3,'doc',type).
no_internalrule(auto1,10,'doc',type).
no_internalrule(auto1,18,'doc',type).
no_internalrule(auto1,5,'doc',type).
no_internalrule(auto1,8,'doc',type).
no_internalrule(auto1,7,'doc',type).
label(auto1,'x',negvar,6).
no_internalrule(auto1,3,'x',negvar).
no_internalrule(auto1,10,'x',negvar).
no_internalrule(auto1,18,'x',negvar).
no_internalrule(auto1,5,'x',negvar).
no_internalrule(auto1,8,'x',negvar).
no_internalrule(auto1,7,'x',negvar).
label(auto1,'x',var,7).
no_internalrule(auto1,3,'x',var).
no_internalrule(auto1,10,'x',var).
no_internalrule(auto1,18,'x',var).
no_internalrule(auto1,5,'x',var).
no_internalrule(auto1,8,'x',var).
no_internalrule(auto1,7,'x',var).
label(auto1,'comment',type,2).
no_internalrule(auto1,3,'comment',type).
no_internalrule(auto1,10,'comment',type).
no_internalrule(auto1,18,'comment',type).
no_internalrule(auto1,5,'comment',type).
no_internalrule(auto1,8,'comment',type).
no_internalrule(auto1,7,'comment',type).
label(auto1,'att',type,1).
no_internalrule(auto1,3,'att',type).
no_internalrule(auto1,10,'att',type).
no_internalrule(auto1,18,'att',type).
no_internalrule(auto1,5,'att',type).
no_internalrule(auto1,8,'att',type).
no_internalrule(auto1,7,'att',type).
label(auto1,'elem',type,4).
no_internalrule(auto1,3,'elem',type).
no_internalrule(auto1,10,'elem',type).
no_internalrule(auto1,18,'elem',type).
no_internalrule(auto1,5,'elem',type).
no_internalrule(auto1,8,'elem',type).
no_internalrule(auto1,7,'elem',type).
max_label_id(auto1,7).
    %%% different origins %%%%%%%
    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXPRESSION=one-x AUTOMATON=det-stepwise
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule

% negative information only for elserules and applyelserules (NEGATIVE=false)
%   no_applyrule
%   no_internalrule
  
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(auto2,'det[stepwise[stepwise[det[pc[stepwise[(μone.(x_bl_Z+_bl_Z(μzero.((one-xd1e19_bl_Z+_bl_Z&lt;zero&gt;))*)⋅(x_bl_Z+_bl_Z&lt;one&gt;)⋅(μzero.((one-xd1e56_bl_Z+_bl_Z&lt;zero&gt;))*)))]]]]]]').
var(auto2,'x',1).
state(auto2,1).
state(auto2,4).
state(auto2,0).
state(auto2,2).
state(auto2,3).
start_tree(auto2,0).
initial(auto2,0).
     sink(auto2,3).
not_sink(auto2,1).
not_sink(auto2,4).
not_sink(auto2,0).
not_sink(auto2,2).
else(auto2,4).
else(auto2,2).
nonelse(auto2,1).
nonelse(auto2,0).
nonelse(auto2,3).
final(auto2,1).
nonfinal(auto2,4).
nonfinal(auto2,0).
nonfinal(auto2,2).
nonfinal(auto2,3).
tree_state(auto2,4).
tree_istate(auto2,4,0).
 tree_state(auto2,2).
tree_istate(auto2,2,0).
 hedge_state(auto2,1).
    hedge_state(auto2,0).
    hedge_state(auto2,3).
    applyrule(auto2,0,4,1).
internalrule(auto2,1,'x',var,3).
elserule(auto2,0,0).
elserule(auto2,1,1).
treerule(auto2,0,2).
treerule(auto2,1,4).
applyrule(auto2,0,2,0).
applyrule(auto2,1,2,1).
internalrule(auto2,0,'x',var,1).
label(auto2,'x',var,1).
max_label_id(auto2,1).
    %%% different origins %%%%%%%
    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% not in signature

      %%%%% difference of signatures %%%%%%%%%%%
      
    not_in_signature(auto2,'text',type).
not_in_signature(auto2,'doc',type).
not_in_signature(auto2,'x',negvar).
not_in_signature(auto2,'comment',type).
not_in_signature(auto2,'att',type).
not_in_signature(auto2,'elem',type).


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% intersection auto=auto1.auto2
inter(auto1,auto2,auto).
