diff_final(A,X,X1) :- final(A,X), nonfinal(A,X1), hedge_state(A,X1).
diff_type(A,X,X1)  :- hedge_state(A,X), tree_state(A,X1).
diff_sym(A,X1,X)   :- diff(A,X,X1).
diff_else(A,X,X1)  :- else(A,X), nonelse(A,X1), tree_state(A,X1).

internal(A,X,L,Type,Y)  :- internalrule(A,X,L,Type,Y).
internal(A,X,L,Type,Y)  :- no_internalrule(A,X,L,Type), elserule(A,X,Y).
internal(A,X,L,Type,Y)  :- no_internalrule(A,X,L,Type), typedelserule(A,X,Type,Y).
no_internal(A,X,L,Type) :- hedge_state(A,X), no_internalrule(A,X,L,Type),
                           no_elserule(A,X), no_typedelserule(A,X,Type).

apply(A,X,Y,Z)  :- applyrule(A,X,Y,Z).
apply(A,X,Y,Z)  :- applyelse(A,X,Y,Z).

applyelse(A,X,Y,Z)  :- applyelserule(A,X,Z), else(A,Y), no_applyrule(A,X,Y).
no_apply(A,X,Y)     :- no_applyrule(A,X,Y),
		       no_applyelserule_with(A,X,Y), tree_state(A,Y).
no_applyelserule_with(A,X,Y) :- no_applyelserule(A,X), tree_state(A,Y). 
no_applyelserule_with(A,X,Y) :- applyelserule(A,X,Z),
			        nonelse(A,Y), tree_state(Y).
no_applyelserule_with(A,X,Y) :- applyelserule(A,X,Z),
			        applyrule(A,X,Y,Z1).

type_lab(A,T) :- typedeleserule(A,X,T,Y).
type_lab(A,T) :- no_typedeleserule(A,X,T).
typedelserule(A,X,T,Y) :- elserule(A,X,Y), type_lab(A,T).

diff_typedelse(A,X,T,X1) :- typedelserule(A,X,T,Y), no_typedelserule(A,X1,T).
% self_diff_typedelse(X)   :- diff_typedelse(A,X,T,X).
diff(A,X,X1) :- diff_typedelse(A,X,T,X1).
diff(A,X,X1) :- diff_typedelserule(A,X,T,X1).

diff(A,X,X1)      :- diff_tree(A,X,X1).
% self_diff_tree(X) :- diff_tree(A,X,X).
diff(A,X,X1)      :- diff_type(A,X,X1).
% self_diff_type(X) :- diff_type(A,X,X).
diff(A,X,X1)      :- diff_final(A,X,X1).
% self_diff_final(X):- diff_final(A,X,X).
diff(A,X,X1)      :- diff_else(A,X,X1).
% self_diff_else(X) :- diff_else(A,X,X).

diff(A,X,X1) :- diff_treerule(A,X,X1).
diff(A,X,X1) :- diff_no_treerule(A,X,X1).
diff(A,X,X1) :- diff_apply(A,X,X1).
diff(A,X,X1) :- diff_no_apply(A,X,X1).
diff(A,X,X1) :- diff_internal(A,X,X1).

diff(A,X,X1) :- diff_no_internal(A,X,X1).
diff(A,X,X1) :- diff_nonelse(A,X,X1).
diff(A,X,X1) :- diff_origin(A,X,X1).
diff(A,X,X1) :- diff_sym(A,X,X1).
diff(A,X,X1) :- diff_elserule(A,X,X1).
diff(A,X,X1) :- diff_no_elserule(A,X,X1).
diff(A,X,X1) :- diff_no_typedelserule(A,X,X1).

diff_apply_from1(A,X,X1,Z,Z1)  :- apply(A,X,Y,Z), apply(A,X1,Y,Z1), diff(A,Z,Z1).
diff_apply_from2(A,Y,Y1,Z,Z1)  :- apply(A,X,Y,Z), apply(A,X,Y1,Z1), diff(A,Z,Z1).
% self_debug_diff_apply_from1(X,Y,Z,Z1) :- apply(A,X,Y,Z), apply(A,X,Y,Z1), diff(A,Z,Z1).
% self_debug_diff_apply_from2(Y,X,Z,Z1) :- apply(A,X,Y,Z), apply(A,X,Y,Z1), diff(A,Z,Z1).
diff_apply(A,X,X1)             :- diff_apply_from1(A,X,X1,Z,Z1).
diff_apply(A,X,X1)             :- diff_apply_from2(A,X,X1,Z,Z1).

diff_no_apply_from1(A,X,X1,Y,Z):- apply(A,X,Y,Z), no_apply(A,X1,Y).
diff_no_apply_from2(A,Y,Y1,X,Z):- apply(A,X,Y,Z), no_apply(A,X,Y1).
% self_debug_diff_no_apply_from1(X,Y,Z) :- apply(A,X,Y,Z), no_apply(A,X,Y).
% self_debug_diff_no_apply_from2(Y,X,Z) :- apply(A,X,Y,Z), no_apply(A,X,Y).
diff_no_apply(A,X,X1)          :- diff_no_apply_from1(A,X,X1,Y,Z).
diff_no_apply(A,X,X1)          :- diff_no_apply_from2(A,X,X1,Y,Z).

diff_treerule(A,X,X1)     :- treerule(A,X,Y), treerule(A,X1,Y1), diff(A,Y,Y1).
% self_debug_diff_treerule(X,Y,Y1) :- treerule(A,X,Y), treerule(A,X,Y1), diff(A,Y,Y1).
diff_no_treerule(A,X,X1)  :- treerule(A,X,Y), no_treerule(A,X1), hedge_state(A,X1).
% self_diff_no_treerule(X)  :-  treerule(A,X,Y), no_treerule(A,X), hedge_state(A,X).

diff_internal(A,X,X1) :- diff_internal_from(A,X,X1,L,Type,Y,Y1).
diff_internal_from(A,X,X1,L,Type,Y,Y1)
                      :- internal(A,X,L,Type,Y),
                         internal(A,X1,L,Type,Y1), diff(A,Y,Y1).
% self_debug_diff_internal_from(A,X,L,Type,Y,Y1):-diff_internal_from(A,X,X,L,Type,Y,Y1).
			 
diff_no_internal(A,X,X1) :- no_internal(A,X,L,Type), internal(A,X1,L,Type,Y1).
% self_debug_diff_no_internal(X,L,Type,Y1) :- no_internal(A,X,L,Type), internal(A,X,L,Type,Y1).

diff_elserule(A,X,X1) :- elserule(A,X,Y), elserule(A,X1,Y1), diff(A,Y,Y1).
% self_debug_diff_elserule(X,Y,Y1) :- elserule(A,X,Y), elserule(A,X,Y1), diff(A,Y,Y1).
diff_typedelserule(A,X,T,X1) :- typedelserule(A,X,T,Y), typedelserule(A,X1,T,Y1), diff(A,Y,Y1).
% self_diff_typedelserule(X)   :- typedelserule(A,X,T,Y), typedelserule(A,X,T,Y1),  diff(A,Y,Y1).
% self_debug_diff_typedelserule(X,Y,Y1) :- typedelserule(A,X,T,Y), typedelserule(A,X,T,Y1), diff(A,Y,Y1).

diff_no_elserule(A,X,X1) :- elserule(A,X,Y), no_elserule(A,X1).
diff_no_typedelserule(A,X,X1) :- typedelserule(A,X,T,Y), no_elserule(A,X1),no_typedelserule(A,X1,T).

% self_diff_no_elserule(X) :- diff_no_elserule(A,X,X).

% don't fusion states with different schema origins
%    (Joachim: should first diff_orig rule be switched on or off?)

% diff_origin(A,X1,X) :- origin_of(A,X1,Y1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).
% self_diff_origin(X) :- origin_of(A,XY1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).
% self_debug_diff_origin(X,Y1,Z1,Y,Z) :- origin_of(A,X,Y1,Z1), origin_of(A,X,Y,Z), diff_orig(A,Z,Z1).

% for minimization replace rule(A,X,Y,Z) by rule(A,min(X),min(Y),min(Z)) 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule
% negative information (NEGATIVE=true)
%   no_applyrule
%   no_elserule
%   no_treerule
%   no_internalrule
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(step,'stepwise[]').
var(step,'x',1).
no_typedelserule(step,1,type).
no_typedelserule(step,2,type).
no_typedelserule(step,3,type).
no_typedelserule(step,4,type).
no_typedelserule(step,5,type).
no_typedelserule(step,6,type).
no_typedelserule(step,7,type).
no_typedelserule(step,8,type).
no_typedelserule(step,9,type).
no_typedelserule(step,10,type).
no_typedelserule(step,13,type).
no_typedelserule(step,16,type).
no_typedelserule(step,19,type).
no_typedelserule(step,1,var).
no_typedelserule(step,2,var).
no_typedelserule(step,3,var).
no_typedelserule(step,4,var).
no_typedelserule(step,5,var).
no_typedelserule(step,6,var).
no_typedelserule(step,7,var).
no_typedelserule(step,8,var).
no_typedelserule(step,9,var).
no_typedelserule(step,10,var).
no_typedelserule(step,13,var).
no_typedelserule(step,16,var).
no_typedelserule(step,19,var).
state(step,1).
state(step,2).
state(step,3).
state(step,4).
state(step,5).
state(step,6).
state(step,7).
state(step,8).
state(step,9).
state(step,10).
state(step,11).
state(step,12).
state(step,13).
state(step,14).
state(step,15).
state(step,16).
state(step,17).
state(step,18).
state(step,19).
start_tree(step,18).
initial(step,18).
origin_of(step,1,'4','1').
origin_of(step,2,'6','1').
origin_of(step,3,'5','1').
origin_of(step,4,'3','1').
origin_of(step,5,'11','4').
origin_of(step,6,'8','4').
origin_of(step,7,'14','2').
origin_of(step,8,'10','1').
origin_of(step,9,'7','1').
origin_of(step,10,'13','1').
origin_of(step,11,'4','0').
origin_of(step,12,'6','0').
origin_of(step,13,'5','0').
origin_of(step,14,'3','0').
origin_of(step,15,'2','0').
origin_of(step,16,'9','1').
origin_of(step,17,'1','0').
origin_of(step,18,'0','0').
origin_of(step,19,'12','1').
sink(step,1).
sink(step,2).
sink(step,3).
sink(step,4).
not_sink(step,5).
not_sink(step,6).
not_sink(step,7).
not_sink(step,8).
not_sink(step,9).
not_sink(step,10).
not_sink(step,11).
not_sink(step,12).
not_sink(step,13).
not_sink(step,14).
not_sink(step,15).
not_sink(step,16).
not_sink(step,17).
not_sink(step,18).
not_sink(step,19).
else(step,5).
else(step,6).
else(step,7).
nonelse(step,1).
nonelse(step,2).
nonelse(step,3).
nonelse(step,4).
nonelse(step,8).
nonelse(step,9).
nonelse(step,10).
nonelse(step,11).
nonelse(step,12).
nonelse(step,13).
nonelse(step,14).
nonelse(step,15).
nonelse(step,16).
nonelse(step,17).
nonelse(step,18).
nonelse(step,19).
final(step,16).
final(step,19).
nonfinal(step,1).
nonfinal(step,2).
nonfinal(step,3).
nonfinal(step,4).
nonfinal(step,5).
nonfinal(step,6).
nonfinal(step,7).
nonfinal(step,8).
nonfinal(step,9).
nonfinal(step,10).
nonfinal(step,11).
nonfinal(step,12).
nonfinal(step,13).
nonfinal(step,14).
nonfinal(step,15).
nonfinal(step,17).
nonfinal(step,18).
copy_of(step,5,5).
copy_of(step,6,6).
copy_of(step,7,7).
tree_state(step,5).
tree_istate(step,5,18).
 tree_state(step,6).
tree_istate(step,6,18).
 tree_state(step,7).
tree_istate(step,7,18).
 hedge_state(step,1).
    no_applyrule(step,1,5).
	  no_applyrule(step,1,6).
	  no_applyrule(step,1,7).
	  no_applyelserule(step,1).
no_elserule(step,1).
	% %
hedge_state(step,2).
    no_applyrule(step,2,5).
	  no_applyrule(step,2,6).
	  no_applyrule(step,2,7).
	  no_applyelserule(step,2).
no_elserule(step,2).
	% %
hedge_state(step,3).
    no_applyrule(step,3,5).
	  no_applyrule(step,3,6).
	  no_applyrule(step,3,7).
	  no_applyelserule(step,3).
no_elserule(step,3).
	% %
hedge_state(step,4).
    no_applyrule(step,4,5).
	  no_applyrule(step,4,6).
	  no_applyrule(step,4,7).
	  no_applyelserule(step,4).
no_elserule(step,4).
	% %
hedge_state(step,8).
    no_applyrule(step,8,5).
	  no_applyrule(step,8,6).
	  no_applyelserule(step,8).
no_elserule(step,8).
	% %
hedge_state(step,9).
    no_applyrule(step,9,5).
	  no_applyrule(step,9,6).
	  no_applyelserule(step,9).
no_elserule(step,9).
	% %
hedge_state(step,10).
    no_applyrule(step,10,5).
	  no_applyrule(step,10,6).
	  no_applyelserule(step,10).
no_elserule(step,10).
	% %
hedge_state(step,11).
    no_applyrule(step,11,5).
	  no_applyrule(step,11,6).
	  no_applyrule(step,11,7).
	  no_applyelserule(step,11).
hedge_state(step,12).
    no_applyrule(step,12,5).
	  no_applyrule(step,12,6).
	  no_applyrule(step,12,7).
	  no_applyelserule(step,12).
hedge_state(step,13).
    no_applyelserule(step,13).
no_elserule(step,13).
	% %
hedge_state(step,14).
    no_applyrule(step,14,5).
	  no_applyrule(step,14,6).
	  no_applyelserule(step,14).
hedge_state(step,15).
    no_applyrule(step,15,5).
	  no_applyrule(step,15,6).
	  no_applyrule(step,15,7).
	  no_applyelserule(step,15).
hedge_state(step,16).
    no_applyrule(step,16,5).
	  no_applyrule(step,16,6).
	  no_applyelserule(step,16).
no_elserule(step,16).
	% %
hedge_state(step,17).
    no_applyrule(step,17,5).
	  no_applyrule(step,17,6).
	  no_applyrule(step,17,7).
	  no_applyelserule(step,17).
hedge_state(step,18).
    no_applyelserule(step,18).
hedge_state(step,19).
    no_applyrule(step,19,5).
	  no_applyrule(step,19,6).
	  no_applyelserule(step,19).
no_elserule(step,19).
	% %
applyrule(step,8,7,8).
applyrule(step,9,7,9).
applyrule(step,10,7,10).
applyrule(step,13,5,10).
applyrule(step,13,6,8).
applyrule(step,13,7,13).
applyrule(step,14,7,14).
applyrule(step,16,7,16).
applyrule(step,18,5,19).
applyrule(step,18,6,16).
applyrule(step,18,7,14).
applyrule(step,19,7,19).
elserule(step,12,13).
elserule(step,11,12).
elserule(step,14,14).
elserule(step,15,13).
elserule(step,17,11).
elserule(step,18,14).
treerule(step,8,5).
treerule(step,9,6).
treerule(step,10,5).
treerule(step,13,7).
treerule(step,14,7).
internalrule(step,11,'x',var,2).
internalrule(step,14,'x',var,4).
internalrule(step,15,'x',var,3).
internalrule(step,17,'x',var,1).
internalrule(step,18,'x',var,4).
internalrule(step,18,'elem',type,17).
internalrule(step,18,'doc',type,15).
internalrule(step,12,'x',var,9).
no_elserule(step,1).
no_elserule(step,2).
no_elserule(step,3).
no_elserule(step,4).
no_elserule(step,5).
no_elserule(step,6).
no_elserule(step,7).
no_elserule(step,8).
no_elserule(step,9).
no_elserule(step,10).
no_elserule(step,13).
no_elserule(step,16).
no_elserule(step,19).
no_treerule(step,1).
no_treerule(step,2).
no_treerule(step,3).
no_treerule(step,4).
no_treerule(step,5).
no_treerule(step,6).
no_treerule(step,7).
no_treerule(step,11).
no_treerule(step,12).
no_treerule(step,15).
no_treerule(step,16).
no_treerule(step,17).
no_treerule(step,18).
no_treerule(step,19).
label(step,'doc',type,1).
no_internalrule(step,1,'doc',type).
no_internalrule(step,2,'doc',type).
no_internalrule(step,3,'doc',type).
no_internalrule(step,4,'doc',type).
no_internalrule(step,5,'doc',type).
no_internalrule(step,6,'doc',type).
no_internalrule(step,7,'doc',type).
no_internalrule(step,8,'doc',type).
no_internalrule(step,9,'doc',type).
no_internalrule(step,10,'doc',type).
no_internalrule(step,11,'doc',type).
no_internalrule(step,12,'doc',type).
no_internalrule(step,13,'doc',type).
no_internalrule(step,14,'doc',type).
no_internalrule(step,15,'doc',type).
no_internalrule(step,16,'doc',type).
no_internalrule(step,17,'doc',type).
no_internalrule(step,19,'doc',type).
label(step,'elem',type,2).
no_internalrule(step,1,'elem',type).
no_internalrule(step,2,'elem',type).
no_internalrule(step,3,'elem',type).
no_internalrule(step,4,'elem',type).
no_internalrule(step,5,'elem',type).
no_internalrule(step,6,'elem',type).
no_internalrule(step,7,'elem',type).
no_internalrule(step,8,'elem',type).
no_internalrule(step,9,'elem',type).
no_internalrule(step,10,'elem',type).
no_internalrule(step,11,'elem',type).
no_internalrule(step,12,'elem',type).
no_internalrule(step,13,'elem',type).
no_internalrule(step,14,'elem',type).
no_internalrule(step,15,'elem',type).
no_internalrule(step,16,'elem',type).
no_internalrule(step,17,'elem',type).
no_internalrule(step,19,'elem',type).
label(step,'x',var,3).
no_internalrule(step,1,'x',var).
no_internalrule(step,2,'x',var).
no_internalrule(step,3,'x',var).
no_internalrule(step,4,'x',var).
no_internalrule(step,5,'x',var).
no_internalrule(step,6,'x',var).
no_internalrule(step,7,'x',var).
no_internalrule(step,8,'x',var).
no_internalrule(step,9,'x',var).
no_internalrule(step,10,'x',var).
no_internalrule(step,13,'x',var).
no_internalrule(step,16,'x',var).
no_internalrule(step,19,'x',var).
max_label_id(step,3).
    %%% different origins %%%%%%%
    diff_orig(step,1,0).
	  diff_orig(step,2,0).
	  diff_orig(step,2,1).
	  diff_orig(step,4,0).
	  diff_orig(step,4,1).
	  diff_orig(step,4,2).
	  
