%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Accessible states of stepwise hedge automata A
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% whether variable is allowed or not

kind(0).
kind(1).

%% whether we started a tree or a hedge

start(t).
start(h).

%% types different from type var


nonvar(type).
nonvar(char).
nonvar(negvar).
nonvar(name).
nonvar(namespace).

%% starting trees and hedges

acc(t,0,A,X) :- start_tree(A,X).
acc(h,0,A,X) :- initial(A,X).

%% ending trees

acc_tree_state(K,A,Z) :- acc(t,K,A,X), treerule(A,X,Z).
acc_tree_state(K,A,Y) :- acc_tree_state(K,A,X), is(A,X,Y).

%% internal rules

internalrule_all(A,X,L,T,Y) :- internalrule(A,X,L,T,Y).
internalrule_all(A,X,L,T,Y) :- elserule(A,X,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,L,T,Y) :- typedelserule(A,X,T,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- elserule(A,X,Y), typedelserule(A,L,T,Z).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- typedelserule(A,X,T,Y).

acc(S,K,A,Y) :- acc(S,K,A,X), internalrule_all(A,X,L,T  ,Y), nonvar(T).
acc(S,1,A,Y) :- acc(S,0,A,X), internalrule_all(A,X,L,var,Y).

%% epsilon rules

acc(S,K,A,Y) :- acc(S,K,A,X), epsilonrule(A,X,Y), kind(K).

%% apply rules

applyrule_all(A,X,Z,Y) :- applyrule(A,X,Z,Y).
applyrule_all(A,X,Z,Y) :- applyelserule(A,X,Y), no_applyrule(A,X,Z).

acc(S,K,A,Y) :- acc(S,K,A,X), acc_tree_state(0,A,Z), applyrule_all(A,X,Z,Y).
acc(S,1,A,Y) :- acc(S,0,A,X), acc_tree_state(1,A,Z), applyrule_all(A,X,Z,Y).

%% compatibility with previous predicates

acc_h(A,X) :- acc(h,K,A,X).
acc_t(A,X) :- acc_tree_state(K,A,X).
acc_t(A,X) :- acc(t,K,A,X).




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% label states where XML labels end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

internal_or_else(A,X,Y) :- internalrule(A,X,L,T,Y).
internal_or_else(A,X,Y) :- elserule(A,X,Y).
internal_or_else(A,X,Y) :- typedelserule(A,X,T,Y).

label_state(A,U) :- start_tree(A,X), internalrule(A,X,'elem',type,Y),internal_or_else(A,Y,Z),
		    internal_or_else(A,Z,U).
label_state(A,U) :- start_tree(A,X), internalrule(A,X,'att',type,Y), internal_or_else(A,Y,Z),
                    internal_or_else(A,Z,U).
label_state(A,Y) :- start_tree(A,X), internalrule(A,X,'text',type,Y). 
label_state(A,Y) :- start_tree(A,X), internalrule(A,X,'comment',type,Y). 
label_state(A,Y) :- start_tree(A,X), internalrule(A,X,'doc',type,Y). 

%% the booleans K are not yet used everywhere to ensure one-x

acc_label(A,X,X) :- label_state(A,X).
acc_label(A,X,Z) :- acc_label(A,X,Y), internalrule(A,Y,L,T,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), elserule(A,Y,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), typedelserule(A,Y,T,Z).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(K,A,Z), applyrule(A,X1,Z,Y).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(K,A,Z), applyelserule(A,X1,Y), no_applyrule(A,X1,Z).

acc_label_tree(A,X,Z) :- acc_label(A,X,Y), treerule(A,Y,Z).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% types of label states
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

elem(A,NS,N,U)   :- start_tree(A,X), internalrule(A,X,'elem',type,NS),internal_or_else(A,NS,N),
		    internal_or_else(A,N,U).
att(A,NS,N,U)    :- start_tree(A,X), internalrule(A,X,'att',type,NS), internal_or_else(A,NS,N),
                    internal_or_else(A,N,U).
text(A,Y)	 :- start_tree(A,X), internalrule(A,X,'text',type,Y). 
comment(A,Y)     :- start_tree(A,X), internalrule(A,X,'comment',type,Y). 
doc(A,Y)         :- start_tree(A,X), internalrule(A,X,'doc',type,Y).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule
% negative information (NEGATIVE=true)
%   no_applyrule
%   no_elserule
%   no_treerule
%   no_internalrule
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(stepwise,'step[&lt;elem⋅h⋅figure⋅_⋅T⋅&lt;att⋅default⋅data-type⋅_⋅c⋅o⋅v⋅e⋅r&gt;⋅T&gt;⋅T]').
expression(stepwise,'d1e33').
no_typedelserule(stepwise,2,type).
no_typedelserule(stepwise,4,type).
no_typedelserule(stepwise,5,type).
no_typedelserule(stepwise,6,type).
no_typedelserule(stepwise,7,type).
no_typedelserule(stepwise,9,type).
no_typedelserule(stepwise,10,type).
no_typedelserule(stepwise,11,type).
no_typedelserule(stepwise,12,type).
no_typedelserule(stepwise,14,type).
no_typedelserule(stepwise,15,type).
no_typedelserule(stepwise,16,type).
no_typedelserule(stepwise,17,type).
no_typedelserule(stepwise,18,type).
no_typedelserule(stepwise,19,type).
no_typedelserule(stepwise,20,type).
no_typedelserule(stepwise,25,type).
no_typedelserule(stepwise,2,char).
no_typedelserule(stepwise,4,char).
no_typedelserule(stepwise,5,char).
no_typedelserule(stepwise,6,char).
no_typedelserule(stepwise,7,char).
no_typedelserule(stepwise,9,char).
no_typedelserule(stepwise,10,char).
no_typedelserule(stepwise,11,char).
no_typedelserule(stepwise,12,char).
no_typedelserule(stepwise,14,char).
no_typedelserule(stepwise,15,char).
no_typedelserule(stepwise,16,char).
no_typedelserule(stepwise,17,char).
no_typedelserule(stepwise,18,char).
no_typedelserule(stepwise,19,char).
no_typedelserule(stepwise,20,char).
no_typedelserule(stepwise,25,char).
no_typedelserule(stepwise,2,name).
no_typedelserule(stepwise,4,name).
no_typedelserule(stepwise,5,name).
no_typedelserule(stepwise,6,name).
no_typedelserule(stepwise,7,name).
no_typedelserule(stepwise,9,name).
no_typedelserule(stepwise,10,name).
no_typedelserule(stepwise,11,name).
no_typedelserule(stepwise,12,name).
no_typedelserule(stepwise,14,name).
no_typedelserule(stepwise,15,name).
no_typedelserule(stepwise,16,name).
no_typedelserule(stepwise,17,name).
no_typedelserule(stepwise,18,name).
no_typedelserule(stepwise,19,name).
no_typedelserule(stepwise,20,name).
no_typedelserule(stepwise,25,name).
no_typedelserule(stepwise,2,namespace).
no_typedelserule(stepwise,4,namespace).
no_typedelserule(stepwise,5,namespace).
no_typedelserule(stepwise,6,namespace).
no_typedelserule(stepwise,7,namespace).
no_typedelserule(stepwise,9,namespace).
no_typedelserule(stepwise,10,namespace).
no_typedelserule(stepwise,11,namespace).
no_typedelserule(stepwise,12,namespace).
no_typedelserule(stepwise,14,namespace).
no_typedelserule(stepwise,15,namespace).
no_typedelserule(stepwise,16,namespace).
no_typedelserule(stepwise,17,namespace).
no_typedelserule(stepwise,18,namespace).
no_typedelserule(stepwise,19,namespace).
no_typedelserule(stepwise,20,namespace).
no_typedelserule(stepwise,25,namespace).
state(stepwise,2).
state(stepwise,4).
state(stepwise,5).
state(stepwise,6).
state(stepwise,7).
state(stepwise,8).
state(stepwise,9).
state(stepwise,10).
state(stepwise,11).
state(stepwise,12).
state(stepwise,13).
state(stepwise,14).
state(stepwise,15).
state(stepwise,16).
state(stepwise,17).
state(stepwise,18).
state(stepwise,19).
state(stepwise,20).
state(stepwise,21).
state(stepwise,22).
state(stepwise,24).
state(stepwise,25).
state(stepwise,27).
start_tree(stepwise,5).
start_tree(stepwise,10).
start_tree(stepwise,24).
initial(stepwise,2).
not_sink(stepwise,2).
not_sink(stepwise,4).
not_sink(stepwise,5).
not_sink(stepwise,6).
not_sink(stepwise,7).
not_sink(stepwise,8).
not_sink(stepwise,9).
not_sink(stepwise,10).
not_sink(stepwise,11).
not_sink(stepwise,12).
not_sink(stepwise,13).
not_sink(stepwise,14).
not_sink(stepwise,15).
not_sink(stepwise,16).
not_sink(stepwise,17).
not_sink(stepwise,18).
not_sink(stepwise,19).
not_sink(stepwise,20).
not_sink(stepwise,21).
not_sink(stepwise,22).
not_sink(stepwise,24).
not_sink(stepwise,25).
not_sink(stepwise,27).
nonelse(stepwise,2).
nonelse(stepwise,4).
nonelse(stepwise,5).
nonelse(stepwise,6).
nonelse(stepwise,7).
nonelse(stepwise,8).
nonelse(stepwise,9).
nonelse(stepwise,10).
nonelse(stepwise,11).
nonelse(stepwise,12).
nonelse(stepwise,13).
nonelse(stepwise,14).
nonelse(stepwise,15).
nonelse(stepwise,16).
nonelse(stepwise,17).
nonelse(stepwise,18).
nonelse(stepwise,19).
nonelse(stepwise,20).
nonelse(stepwise,21).
nonelse(stepwise,22).
nonelse(stepwise,24).
nonelse(stepwise,25).
nonelse(stepwise,27).
final(stepwise,27).
nonfinal(stepwise,2).
nonfinal(stepwise,4).
nonfinal(stepwise,5).
nonfinal(stepwise,6).
nonfinal(stepwise,7).
nonfinal(stepwise,8).
nonfinal(stepwise,9).
nonfinal(stepwise,10).
nonfinal(stepwise,11).
nonfinal(stepwise,12).
nonfinal(stepwise,13).
nonfinal(stepwise,14).
nonfinal(stepwise,15).
nonfinal(stepwise,16).
nonfinal(stepwise,17).
nonfinal(stepwise,18).
nonfinal(stepwise,19).
nonfinal(stepwise,20).
nonfinal(stepwise,21).
nonfinal(stepwise,22).
nonfinal(stepwise,24).
nonfinal(stepwise,25).
copy_of(stepwise,4,4).
copy_of(stepwise,5,4).
copy_of(stepwise,6,4).
copy_of(stepwise,7,4).
copy_of(stepwise,8,4).
copy_of(stepwise,9,9).
copy_of(stepwise,10,9).
copy_of(stepwise,11,9).
copy_of(stepwise,12,9).
copy_of(stepwise,13,9).
copy_of(stepwise,14,9).
copy_of(stepwise,15,9).
copy_of(stepwise,16,9).
copy_of(stepwise,17,9).
copy_of(stepwise,18,9).
copy_of(stepwise,19,9).
copy_of(stepwise,20,4).
copy_of(stepwise,21,4).
copy_of(stepwise,22,4).
copy_of(stepwise,24,25).
copy_of(stepwise,25,25).
tree_state(stepwise,4).
tree_istate(stepwise,4,5).
 tree_state(stepwise,9).
tree_istate(stepwise,9,10).
 tree_state(stepwise,25).
tree_istate(stepwise,25,24).
 hedge_state(stepwise,2).
    no_applyrule(stepwise,2,9).
	  no_applyrule(stepwise,2,25).
	  no_applyelserule(stepwise,2).
no_elserule(stepwise,2).
	% %
hedge_state(stepwise,5).
    no_applyrule(stepwise,5,4).
	  no_applyrule(stepwise,5,9).
	  no_applyrule(stepwise,5,25).
	  no_applyelserule(stepwise,5).
no_elserule(stepwise,5).
	% %
hedge_state(stepwise,6).
    no_applyrule(stepwise,6,4).
	  no_applyrule(stepwise,6,9).
	  no_applyrule(stepwise,6,25).
	  no_applyelserule(stepwise,6).
no_elserule(stepwise,6).
	% %
hedge_state(stepwise,7).
    no_applyrule(stepwise,7,4).
	  no_applyrule(stepwise,7,9).
	  no_applyrule(stepwise,7,25).
	  no_applyelserule(stepwise,7).
no_elserule(stepwise,7).
	% %
hedge_state(stepwise,8).
    no_applyrule(stepwise,8,4).
	  no_applyrule(stepwise,8,9).
	  no_applyrule(stepwise,8,25).
	  no_applyelserule(stepwise,8).
hedge_state(stepwise,10).
    no_applyrule(stepwise,10,4).
	  no_applyrule(stepwise,10,9).
	  no_applyrule(stepwise,10,25).
	  no_applyelserule(stepwise,10).
no_elserule(stepwise,10).
	% %
hedge_state(stepwise,11).
    no_applyrule(stepwise,11,4).
	  no_applyrule(stepwise,11,9).
	  no_applyrule(stepwise,11,25).
	  no_applyelserule(stepwise,11).
no_elserule(stepwise,11).
	% %
hedge_state(stepwise,12).
    no_applyrule(stepwise,12,4).
	  no_applyrule(stepwise,12,9).
	  no_applyrule(stepwise,12,25).
	  no_applyelserule(stepwise,12).
no_elserule(stepwise,12).
	% %
hedge_state(stepwise,13).
    no_applyrule(stepwise,13,4).
	  no_applyrule(stepwise,13,9).
	  no_applyrule(stepwise,13,25).
	  no_applyelserule(stepwise,13).
hedge_state(stepwise,14).
    no_applyrule(stepwise,14,4).
	  no_applyrule(stepwise,14,9).
	  no_applyrule(stepwise,14,25).
	  no_applyelserule(stepwise,14).
no_elserule(stepwise,14).
	% %
hedge_state(stepwise,15).
    no_applyrule(stepwise,15,4).
	  no_applyrule(stepwise,15,9).
	  no_applyrule(stepwise,15,25).
	  no_applyelserule(stepwise,15).
no_elserule(stepwise,15).
	% %
hedge_state(stepwise,16).
    no_applyrule(stepwise,16,4).
	  no_applyrule(stepwise,16,9).
	  no_applyrule(stepwise,16,25).
	  no_applyelserule(stepwise,16).
no_elserule(stepwise,16).
	% %
hedge_state(stepwise,17).
    no_applyrule(stepwise,17,4).
	  no_applyrule(stepwise,17,9).
	  no_applyrule(stepwise,17,25).
	  no_applyelserule(stepwise,17).
no_elserule(stepwise,17).
	% %
hedge_state(stepwise,18).
    no_applyrule(stepwise,18,4).
	  no_applyrule(stepwise,18,9).
	  no_applyrule(stepwise,18,25).
	  no_applyelserule(stepwise,18).
no_elserule(stepwise,18).
	% %
hedge_state(stepwise,19).
    no_applyrule(stepwise,19,4).
	  no_applyrule(stepwise,19,9).
	  no_applyrule(stepwise,19,25).
	  no_applyelserule(stepwise,19).
no_elserule(stepwise,19).
	% %
hedge_state(stepwise,20).
    no_applyrule(stepwise,20,4).
	  no_applyrule(stepwise,20,25).
	  no_applyelserule(stepwise,20).
no_elserule(stepwise,20).
	% %
hedge_state(stepwise,21).
    no_applyrule(stepwise,21,4).
	  no_applyrule(stepwise,21,9).
	  no_applyelserule(stepwise,21).
hedge_state(stepwise,22).
    no_applyrule(stepwise,22,4).
	  no_applyrule(stepwise,22,9).
	  no_applyelserule(stepwise,22).
hedge_state(stepwise,24).
    no_applyrule(stepwise,24,4).
	  no_applyrule(stepwise,24,9).
	  no_applyelserule(stepwise,24).
hedge_state(stepwise,27).
    no_applyrule(stepwise,27,4).
	  no_applyrule(stepwise,27,9).
	  no_applyelserule(stepwise,27).
internalrule(stepwise,5,'elem',type,6).
internalrule(stepwise,6,'h',namespace,7).
internalrule(stepwise,7,'figure',name,8).
applyrule(stepwise,21,25,21).
elserule(stepwise,21,21).
elserule(stepwise,8,21).
internalrule(stepwise,10,'att',type,11).
internalrule(stepwise,11,'default',namespace,12).
internalrule(stepwise,12,'data-type',name,13).
internalrule(stepwise,18,'c',char,14).
internalrule(stepwise,14,'o',char,15).
internalrule(stepwise,15,'v',char,16).
internalrule(stepwise,17,'r',char,19).
internalrule(stepwise,16,'e',char,17).
elserule(stepwise,13,18).
treerule(stepwise,19,9).
applyrule(stepwise,22,25,22).
elserule(stepwise,22,22).
applyrule(stepwise,20,9,22).
applyrule(stepwise,21,25,20).
elserule(stepwise,21,20).
elserule(stepwise,8,20).
treerule(stepwise,22,4).
applyrule(stepwise,27,25,27).
elserule(stepwise,27,27).
applyrule(stepwise,24,25,24).
elserule(stepwise,24,24).
treerule(stepwise,24,25).
applyrule(stepwise,2,4,27).
no_elserule(stepwise,2).
no_elserule(stepwise,4).
no_elserule(stepwise,5).
no_elserule(stepwise,6).
no_elserule(stepwise,7).
no_elserule(stepwise,9).
no_elserule(stepwise,10).
no_elserule(stepwise,11).
no_elserule(stepwise,12).
no_elserule(stepwise,14).
no_elserule(stepwise,15).
no_elserule(stepwise,16).
no_elserule(stepwise,17).
no_elserule(stepwise,18).
no_elserule(stepwise,19).
no_elserule(stepwise,20).
no_elserule(stepwise,25).
no_treerule(stepwise,2).
no_treerule(stepwise,4).
no_treerule(stepwise,5).
no_treerule(stepwise,6).
no_treerule(stepwise,7).
no_treerule(stepwise,8).
no_treerule(stepwise,9).
no_treerule(stepwise,10).
no_treerule(stepwise,11).
no_treerule(stepwise,12).
no_treerule(stepwise,13).
no_treerule(stepwise,14).
no_treerule(stepwise,15).
no_treerule(stepwise,16).
no_treerule(stepwise,17).
no_treerule(stepwise,18).
no_treerule(stepwise,20).
no_treerule(stepwise,21).
no_treerule(stepwise,25).
no_treerule(stepwise,27).
label(stepwise,'att',type,1).
no_internalrule(stepwise,2,'att',type).
no_internalrule(stepwise,4,'att',type).
no_internalrule(stepwise,5,'att',type).
no_internalrule(stepwise,6,'att',type).
no_internalrule(stepwise,7,'att',type).
no_internalrule(stepwise,8,'att',type).
no_internalrule(stepwise,9,'att',type).
no_internalrule(stepwise,11,'att',type).
no_internalrule(stepwise,12,'att',type).
no_internalrule(stepwise,13,'att',type).
no_internalrule(stepwise,14,'att',type).
no_internalrule(stepwise,15,'att',type).
no_internalrule(stepwise,16,'att',type).
no_internalrule(stepwise,17,'att',type).
no_internalrule(stepwise,18,'att',type).
no_internalrule(stepwise,19,'att',type).
no_internalrule(stepwise,20,'att',type).
no_internalrule(stepwise,21,'att',type).
no_internalrule(stepwise,22,'att',type).
no_internalrule(stepwise,24,'att',type).
no_internalrule(stepwise,25,'att',type).
no_internalrule(stepwise,27,'att',type).
label(stepwise,'c',char,2).
no_internalrule(stepwise,2,'c',char).
no_internalrule(stepwise,4,'c',char).
no_internalrule(stepwise,5,'c',char).
no_internalrule(stepwise,6,'c',char).
no_internalrule(stepwise,7,'c',char).
no_internalrule(stepwise,8,'c',char).
no_internalrule(stepwise,9,'c',char).
no_internalrule(stepwise,10,'c',char).
no_internalrule(stepwise,11,'c',char).
no_internalrule(stepwise,12,'c',char).
no_internalrule(stepwise,13,'c',char).
no_internalrule(stepwise,14,'c',char).
no_internalrule(stepwise,15,'c',char).
no_internalrule(stepwise,16,'c',char).
no_internalrule(stepwise,17,'c',char).
no_internalrule(stepwise,19,'c',char).
no_internalrule(stepwise,20,'c',char).
no_internalrule(stepwise,21,'c',char).
no_internalrule(stepwise,22,'c',char).
no_internalrule(stepwise,24,'c',char).
no_internalrule(stepwise,25,'c',char).
no_internalrule(stepwise,27,'c',char).
label(stepwise,'data-type',name,3).
no_internalrule(stepwise,2,'data-type',name).
no_internalrule(stepwise,4,'data-type',name).
no_internalrule(stepwise,5,'data-type',name).
no_internalrule(stepwise,6,'data-type',name).
no_internalrule(stepwise,7,'data-type',name).
no_internalrule(stepwise,8,'data-type',name).
no_internalrule(stepwise,9,'data-type',name).
no_internalrule(stepwise,10,'data-type',name).
no_internalrule(stepwise,11,'data-type',name).
no_internalrule(stepwise,13,'data-type',name).
no_internalrule(stepwise,14,'data-type',name).
no_internalrule(stepwise,15,'data-type',name).
no_internalrule(stepwise,16,'data-type',name).
no_internalrule(stepwise,17,'data-type',name).
no_internalrule(stepwise,18,'data-type',name).
no_internalrule(stepwise,19,'data-type',name).
no_internalrule(stepwise,20,'data-type',name).
no_internalrule(stepwise,21,'data-type',name).
no_internalrule(stepwise,22,'data-type',name).
no_internalrule(stepwise,24,'data-type',name).
no_internalrule(stepwise,25,'data-type',name).
no_internalrule(stepwise,27,'data-type',name).
label(stepwise,'default',namespace,4).
no_internalrule(stepwise,2,'default',namespace).
no_internalrule(stepwise,4,'default',namespace).
no_internalrule(stepwise,5,'default',namespace).
no_internalrule(stepwise,6,'default',namespace).
no_internalrule(stepwise,7,'default',namespace).
no_internalrule(stepwise,8,'default',namespace).
no_internalrule(stepwise,9,'default',namespace).
no_internalrule(stepwise,10,'default',namespace).
no_internalrule(stepwise,12,'default',namespace).
no_internalrule(stepwise,13,'default',namespace).
no_internalrule(stepwise,14,'default',namespace).
no_internalrule(stepwise,15,'default',namespace).
no_internalrule(stepwise,16,'default',namespace).
no_internalrule(stepwise,17,'default',namespace).
no_internalrule(stepwise,18,'default',namespace).
no_internalrule(stepwise,19,'default',namespace).
no_internalrule(stepwise,20,'default',namespace).
no_internalrule(stepwise,21,'default',namespace).
no_internalrule(stepwise,22,'default',namespace).
no_internalrule(stepwise,24,'default',namespace).
no_internalrule(stepwise,25,'default',namespace).
no_internalrule(stepwise,27,'default',namespace).
label(stepwise,'e',char,5).
no_internalrule(stepwise,2,'e',char).
no_internalrule(stepwise,4,'e',char).
no_internalrule(stepwise,5,'e',char).
no_internalrule(stepwise,6,'e',char).
no_internalrule(stepwise,7,'e',char).
no_internalrule(stepwise,8,'e',char).
no_internalrule(stepwise,9,'e',char).
no_internalrule(stepwise,10,'e',char).
no_internalrule(stepwise,11,'e',char).
no_internalrule(stepwise,12,'e',char).
no_internalrule(stepwise,13,'e',char).
no_internalrule(stepwise,14,'e',char).
no_internalrule(stepwise,15,'e',char).
no_internalrule(stepwise,17,'e',char).
no_internalrule(stepwise,18,'e',char).
no_internalrule(stepwise,19,'e',char).
no_internalrule(stepwise,20,'e',char).
no_internalrule(stepwise,21,'e',char).
no_internalrule(stepwise,22,'e',char).
no_internalrule(stepwise,24,'e',char).
no_internalrule(stepwise,25,'e',char).
no_internalrule(stepwise,27,'e',char).
label(stepwise,'elem',type,6).
no_internalrule(stepwise,2,'elem',type).
no_internalrule(stepwise,4,'elem',type).
no_internalrule(stepwise,6,'elem',type).
no_internalrule(stepwise,7,'elem',type).
no_internalrule(stepwise,8,'elem',type).
no_internalrule(stepwise,9,'elem',type).
no_internalrule(stepwise,10,'elem',type).
no_internalrule(stepwise,11,'elem',type).
no_internalrule(stepwise,12,'elem',type).
no_internalrule(stepwise,13,'elem',type).
no_internalrule(stepwise,14,'elem',type).
no_internalrule(stepwise,15,'elem',type).
no_internalrule(stepwise,16,'elem',type).
no_internalrule(stepwise,17,'elem',type).
no_internalrule(stepwise,18,'elem',type).
no_internalrule(stepwise,19,'elem',type).
no_internalrule(stepwise,20,'elem',type).
no_internalrule(stepwise,21,'elem',type).
no_internalrule(stepwise,22,'elem',type).
no_internalrule(stepwise,24,'elem',type).
no_internalrule(stepwise,25,'elem',type).
no_internalrule(stepwise,27,'elem',type).
label(stepwise,'figure',name,7).
no_internalrule(stepwise,2,'figure',name).
no_internalrule(stepwise,4,'figure',name).
no_internalrule(stepwise,5,'figure',name).
no_internalrule(stepwise,6,'figure',name).
no_internalrule(stepwise,8,'figure',name).
no_internalrule(stepwise,9,'figure',name).
no_internalrule(stepwise,10,'figure',name).
no_internalrule(stepwise,11,'figure',name).
no_internalrule(stepwise,12,'figure',name).
no_internalrule(stepwise,13,'figure',name).
no_internalrule(stepwise,14,'figure',name).
no_internalrule(stepwise,15,'figure',name).
no_internalrule(stepwise,16,'figure',name).
no_internalrule(stepwise,17,'figure',name).
no_internalrule(stepwise,18,'figure',name).
no_internalrule(stepwise,19,'figure',name).
no_internalrule(stepwise,20,'figure',name).
no_internalrule(stepwise,21,'figure',name).
no_internalrule(stepwise,22,'figure',name).
no_internalrule(stepwise,24,'figure',name).
no_internalrule(stepwise,25,'figure',name).
no_internalrule(stepwise,27,'figure',name).
label(stepwise,'h',namespace,8).
no_internalrule(stepwise,2,'h',namespace).
no_internalrule(stepwise,4,'h',namespace).
no_internalrule(stepwise,5,'h',namespace).
no_internalrule(stepwise,7,'h',namespace).
no_internalrule(stepwise,8,'h',namespace).
no_internalrule(stepwise,9,'h',namespace).
no_internalrule(stepwise,10,'h',namespace).
no_internalrule(stepwise,11,'h',namespace).
no_internalrule(stepwise,12,'h',namespace).
no_internalrule(stepwise,13,'h',namespace).
no_internalrule(stepwise,14,'h',namespace).
no_internalrule(stepwise,15,'h',namespace).
no_internalrule(stepwise,16,'h',namespace).
no_internalrule(stepwise,17,'h',namespace).
no_internalrule(stepwise,18,'h',namespace).
no_internalrule(stepwise,19,'h',namespace).
no_internalrule(stepwise,20,'h',namespace).
no_internalrule(stepwise,21,'h',namespace).
no_internalrule(stepwise,22,'h',namespace).
no_internalrule(stepwise,24,'h',namespace).
no_internalrule(stepwise,25,'h',namespace).
no_internalrule(stepwise,27,'h',namespace).
label(stepwise,'o',char,9).
no_internalrule(stepwise,2,'o',char).
no_internalrule(stepwise,4,'o',char).
no_internalrule(stepwise,5,'o',char).
no_internalrule(stepwise,6,'o',char).
no_internalrule(stepwise,7,'o',char).
no_internalrule(stepwise,8,'o',char).
no_internalrule(stepwise,9,'o',char).
no_internalrule(stepwise,10,'o',char).
no_internalrule(stepwise,11,'o',char).
no_internalrule(stepwise,12,'o',char).
no_internalrule(stepwise,13,'o',char).
no_internalrule(stepwise,15,'o',char).
no_internalrule(stepwise,16,'o',char).
no_internalrule(stepwise,17,'o',char).
no_internalrule(stepwise,18,'o',char).
no_internalrule(stepwise,19,'o',char).
no_internalrule(stepwise,20,'o',char).
no_internalrule(stepwise,21,'o',char).
no_internalrule(stepwise,22,'o',char).
no_internalrule(stepwise,24,'o',char).
no_internalrule(stepwise,25,'o',char).
no_internalrule(stepwise,27,'o',char).
label(stepwise,'r',char,10).
no_internalrule(stepwise,2,'r',char).
no_internalrule(stepwise,4,'r',char).
no_internalrule(stepwise,5,'r',char).
no_internalrule(stepwise,6,'r',char).
no_internalrule(stepwise,7,'r',char).
no_internalrule(stepwise,8,'r',char).
no_internalrule(stepwise,9,'r',char).
no_internalrule(stepwise,10,'r',char).
no_internalrule(stepwise,11,'r',char).
no_internalrule(stepwise,12,'r',char).
no_internalrule(stepwise,13,'r',char).
no_internalrule(stepwise,14,'r',char).
no_internalrule(stepwise,15,'r',char).
no_internalrule(stepwise,16,'r',char).
no_internalrule(stepwise,18,'r',char).
no_internalrule(stepwise,19,'r',char).
no_internalrule(stepwise,20,'r',char).
no_internalrule(stepwise,21,'r',char).
no_internalrule(stepwise,22,'r',char).
no_internalrule(stepwise,24,'r',char).
no_internalrule(stepwise,25,'r',char).
no_internalrule(stepwise,27,'r',char).
label(stepwise,'v',char,11).
no_internalrule(stepwise,2,'v',char).
no_internalrule(stepwise,4,'v',char).
no_internalrule(stepwise,5,'v',char).
no_internalrule(stepwise,6,'v',char).
no_internalrule(stepwise,7,'v',char).
no_internalrule(stepwise,8,'v',char).
no_internalrule(stepwise,9,'v',char).
no_internalrule(stepwise,10,'v',char).
no_internalrule(stepwise,11,'v',char).
no_internalrule(stepwise,12,'v',char).
no_internalrule(stepwise,13,'v',char).
no_internalrule(stepwise,14,'v',char).
no_internalrule(stepwise,16,'v',char).
no_internalrule(stepwise,17,'v',char).
no_internalrule(stepwise,18,'v',char).
no_internalrule(stepwise,19,'v',char).
no_internalrule(stepwise,20,'v',char).
no_internalrule(stepwise,21,'v',char).
no_internalrule(stepwise,22,'v',char).
no_internalrule(stepwise,24,'v',char).
no_internalrule(stepwise,25,'v',char).
no_internalrule(stepwise,27,'v',char).
max_label_id(stepwise,11).
    %%% different origins %%%%%%%
    
