%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% User input are subsets Q of states of A for which to compute:
%%
%%    safe_sel^A(Q) and safe_ref^A(Q)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% These subsets Q are specified by facts of the form:
%%
%%    safe_sel_of(A,Q,X) and safe_rej_of(A,Q,X)
%%
%% which specify the elements X of the subsets Q. The
%% elements X in safe_sel^A(Q) and safe_ref^A(Q) are specified
%% by facts of the form:
%%
%%    safe_sel(A,Q,X) and safe_rej(A,Q,X) 
%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% the default subset provided is the subset of final states

safe_sel_of(A,final,X) :- final(A,X).
safe_rej_of(A,final,X) :- final(A,X).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% inverse delta accessibility
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

invacc_delta(A,Q,X) :- safe_rej_of(A,Q,X). 
invacc_delta(A,Q,X) :- step_nonvar(A,X,Y), invacc_delta(A,Q,Y).
invacc_delta(A,Q,X) :- step_var(A,X,Y), invacc_delta(A,Q,Y).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% inverse delta accessibility without variables
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

invacc_delta_nonvar(A,Q,X) :- hedge_state(A,X), safe_sel_of(A,Q,_),
                              not safe_sel_of(A,Q,X). 
invacc_delta_nonvar(A,Q,X) :- step_nonvar(A,X,Y),
                              invacc_delta_nonvar(A,Q,Y).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% safety for selection and selection
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


safe_rej(A,Q,X) :- hedge_state(A,X), safe_rej_of(A,Q,_),
		   not invacc_delta(A,Q,X).

safe_sel(A,Q,X) :- hedge_state(A,X), safe_sel_of(A,Q,_),
		   not invacc_delta_nonvar(A,Q,X).




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% delta steps
%% - with and without variables
%% - hedge, final, and nonfinal state
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% start datalog
% parse file ../FXP/../automata-store/lick/forward/version-0.6/11160/schema-product/step/det-stepwise-rules.dl
% process 5506 clauses
% computing fixpoint...
% done.
% facts matching pattern nonfinal(X0, X1):
  nonfinal(stepwise, 10).
  nonfinal(stepwise, 65).
  nonfinal(stepwise, 31).
  nonfinal(stepwise, 7).
  nonfinal(stepwise, 55).
  nonfinal(stepwise, 74).
  nonfinal(stepwise, 78).
  nonfinal(stepwise, 21).
  nonfinal(stepwise, 32).
  nonfinal(stepwise, 19).
  nonfinal(stepwise, 75).
  nonfinal(stepwise, 80).
  nonfinal(stepwise, 59).
  nonfinal(stepwise, 2).
  nonfinal(stepwise, 44).
  nonfinal(stepwise, 38).
  nonfinal(stepwise, 0).
  nonfinal(stepwise, 54).
  nonfinal(stepwise, 1).
  nonfinal(stepwise, 60).
  nonfinal(stepwise, 41).
  nonfinal(stepwise, 69).
  nonfinal(stepwise, 63).
  nonfinal(stepwise, 81).
  nonfinal(stepwise, 64).
  nonfinal(stepwise, 53).
  nonfinal(stepwise, 35).
  nonfinal(stepwise, 6).
  nonfinal(stepwise, 4).
  nonfinal(stepwise, 39).
  nonfinal(stepwise, 62).
  nonfinal(stepwise, 58).
  nonfinal(stepwise, 72).
  nonfinal(stepwise, 25).
  nonfinal(stepwise, 76).
  nonfinal(stepwise, 66).
  nonfinal(stepwise, 79).
  nonfinal(stepwise, 71).
  nonfinal(stepwise, 57).
  nonfinal(stepwise, 40).
  nonfinal(stepwise, 61).
  nonfinal(stepwise, 48).
  nonfinal(stepwise, 37).
  nonfinal(stepwise, 49).
  nonfinal(stepwise, 56).
  nonfinal(stepwise, 52).
  nonfinal(stepwise, 68).
  nonfinal(stepwise, 11).
  nonfinal(stepwise, 22).
  nonfinal(stepwise, 5).
  nonfinal(stepwise, 36).
  nonfinal(stepwise, 34).
  nonfinal(stepwise, 70).
  nonfinal(stepwise, 9).
  nonfinal(stepwise, 17).
  nonfinal(stepwise, 42).
  nonfinal(stepwise, 20).
  nonfinal(stepwise, 46).
  nonfinal(stepwise, 73).
  nonfinal(stepwise, 23).
  nonfinal(stepwise, 47).
  nonfinal(stepwise, 12).
  nonfinal(stepwise, 30).
  nonfinal(stepwise, 27).
  nonfinal(stepwise, 24).
  nonfinal(stepwise, 50).
  nonfinal(stepwise, 67).
  nonfinal(stepwise, 33).
  nonfinal(stepwise, 13).
  nonfinal(stepwise, 28).
  nonfinal(stepwise, 45).
  nonfinal(stepwise, 51).
  nonfinal(stepwise, 43).
  nonfinal(stepwise, 3).
  nonfinal(stepwise, 16).
  nonfinal(stepwise, 8).
  nonfinal(stepwise, 77).
  nonfinal(stepwise, 15).
  nonfinal(stepwise, 29).
  nonfinal(stepwise, 26).
  nonfinal(stepwise, 18).
  nonfinal(stepwise, 14).
% facts matching pattern final(X0, X1):
  final(stepwise, 94).
  final(stepwise, 89).
  final(stepwise, 82).
  final(stepwise, 88).
  final(stepwise, 90).
  final(stepwise, 84).
  final(stepwise, 96).
  final(stepwise, 86).
  final(stepwise, 83).
  final(stepwise, 95).
  final(stepwise, 87).
  final(stepwise, 93).
  final(stepwise, 85).
  final(stepwise, 91).
  final(stepwise, 92).
  final(stepwise, 97).
% facts matching pattern hedge_state(X0, X1):
  hedge_state(stepwise, 65).
  hedge_state(stepwise, 7).
  hedge_state(stepwise, 55).
  hedge_state(stepwise, 21).
  hedge_state(stepwise, 94).
  hedge_state(stepwise, 19).
  hedge_state(stepwise, 59).
  hedge_state(stepwise, 2).
  hedge_state(stepwise, 96).
  hedge_state(stepwise, 83).
  hedge_state(stepwise, 0).
  hedge_state(stepwise, 87).
  hedge_state(stepwise, 54).
  hedge_state(stepwise, 1).
  hedge_state(stepwise, 60).
  hedge_state(stepwise, 41).
  hedge_state(stepwise, 97).
  hedge_state(stepwise, 63).
  hedge_state(stepwise, 64).
  hedge_state(stepwise, 53).
  hedge_state(stepwise, 35).
  hedge_state(stepwise, 6).
  hedge_state(stepwise, 4).
  hedge_state(stepwise, 39).
  hedge_state(stepwise, 89).
  hedge_state(stepwise, 82).
  hedge_state(stepwise, 62).
  hedge_state(stepwise, 58).
  hedge_state(stepwise, 25).
  hedge_state(stepwise, 86).
  hedge_state(stepwise, 57).
  hedge_state(stepwise, 61).
  hedge_state(stepwise, 37).
  hedge_state(stepwise, 49).
  hedge_state(stepwise, 56).
  hedge_state(stepwise, 52).
  hedge_state(stepwise, 11).
  hedge_state(stepwise, 22).
  hedge_state(stepwise, 5).
  hedge_state(stepwise, 90).
  hedge_state(stepwise, 84).
  hedge_state(stepwise, 9).
  hedge_state(stepwise, 17).
  hedge_state(stepwise, 20).
  hedge_state(stepwise, 95).
  hedge_state(stepwise, 23).
  hedge_state(stepwise, 93).
  hedge_state(stepwise, 47).
  hedge_state(stepwise, 12).
  hedge_state(stepwise, 85).
  hedge_state(stepwise, 30).
  hedge_state(stepwise, 27).
  hedge_state(stepwise, 92).
  hedge_state(stepwise, 24).
  hedge_state(stepwise, 50).
  hedge_state(stepwise, 33).
  hedge_state(stepwise, 13).
  hedge_state(stepwise, 28).
  hedge_state(stepwise, 45).
  hedge_state(stepwise, 51).
  hedge_state(stepwise, 43).
  hedge_state(stepwise, 88).
  hedge_state(stepwise, 3).
  hedge_state(stepwise, 16).
  hedge_state(stepwise, 8).
  hedge_state(stepwise, 15).
  hedge_state(stepwise, 29).
  hedge_state(stepwise, 91).
  hedge_state(stepwise, 26).
  hedge_state(stepwise, 18).
  hedge_state(stepwise, 14).
% facts matching pattern step_var(X0, X1, X2):
  step_var(stepwise, 35, 63).
  step_var(stepwise, 35, 55).
  step_var(stepwise, 35, 65).
  step_var(stepwise, 35, 53).
  step_var(stepwise, 35, 51).
  step_var(stepwise, 35, 59).
  step_var(stepwise, 35, 57).
  step_var(stepwise, 35, 61).
  step_var(stepwise, 13, 24).
  step_var(stepwise, 19, 30).
  step_var(stepwise, 6, 64).
  step_var(stepwise, 6, 56).
  step_var(stepwise, 6, 52).
  step_var(stepwise, 6, 62).
  step_var(stepwise, 6, 58).
  step_var(stepwise, 6, 54).
  step_var(stepwise, 6, 60).
  step_var(stepwise, 6, 50).
  step_var(stepwise, 16, 27).
  step_var(stepwise, 17, 28).
  step_var(stepwise, 0, 94).
  step_var(stepwise, 0, 89).
  step_var(stepwise, 0, 82).
  step_var(stepwise, 0, 88).
  step_var(stepwise, 0, 84).
  step_var(stepwise, 0, 90).
  step_var(stepwise, 0, 96).
  step_var(stepwise, 0, 83).
  step_var(stepwise, 0, 86).
  step_var(stepwise, 0, 95).
  step_var(stepwise, 0, 87).
  step_var(stepwise, 0, 93).
  step_var(stepwise, 0, 91).
  step_var(stepwise, 0, 85).
  step_var(stepwise, 0, 92).
  step_var(stepwise, 0, 97).
  step_var(stepwise, 15, 26).
  step_var(stepwise, 23, 55).
  step_var(stepwise, 23, 65).
  step_var(stepwise, 23, 63).
  step_var(stepwise, 23, 53).
  step_var(stepwise, 23, 51).
  step_var(stepwise, 23, 59).
  step_var(stepwise, 23, 57).
  step_var(stepwise, 23, 61).
  step_var(stepwise, 12, 22).
  step_var(stepwise, 18, 29).
  step_var(stepwise, 14, 25).
% facts matching pattern step_nonvar(X0, X1, X2):
  step_nonvar(stepwise, 7, 11).
  step_nonvar(stepwise, 55, 55).
  step_nonvar(stepwise, 65, 65).
  step_nonvar(stepwise, 49, 49).
  step_nonvar(stepwise, 21, 21).
  step_nonvar(stepwise, 19, 23).
  step_nonvar(stepwise, 5, 5).
  step_nonvar(stepwise, 11, 21).
  step_nonvar(stepwise, 22, 33).
  step_nonvar(stepwise, 22, 22).
  step_nonvar(stepwise, 59, 59).
  step_nonvar(stepwise, 2, 6).
  step_nonvar(stepwise, 83, 83).
  step_nonvar(stepwise, 9, 20).
  step_nonvar(stepwise, 17, 23).
  step_nonvar(stepwise, 20, 23).
  step_nonvar(stepwise, 95, 95).
  step_nonvar(stepwise, 1, 5).
  step_nonvar(stepwise, 87, 87).
  step_nonvar(stepwise, 0, 4).
  step_nonvar(stepwise, 0, 3).
  step_nonvar(stepwise, 0, 2).
  step_nonvar(stepwise, 0, 1).
  step_nonvar(stepwise, 23, 35).
  step_nonvar(stepwise, 23, 23).
  step_nonvar(stepwise, 41, 41).
  step_nonvar(stepwise, 93, 93).
  step_nonvar(stepwise, 47, 47).
  step_nonvar(stepwise, 85, 85).
  step_nonvar(stepwise, 12, 23).
  step_nonvar(stepwise, 27, 43).
  step_nonvar(stepwise, 27, 27).
  step_nonvar(stepwise, 24, 24).
  step_nonvar(stepwise, 24, 37).
  step_nonvar(stepwise, 30, 49).
  step_nonvar(stepwise, 30, 30).
  step_nonvar(stepwise, 97, 97).
  step_nonvar(stepwise, 63, 63).
  step_nonvar(stepwise, 53, 53).
  step_nonvar(stepwise, 35, 35).
  step_nonvar(stepwise, 39, 39).
  step_nonvar(stepwise, 4, 8).
  step_nonvar(stepwise, 4, 9).
  step_nonvar(stepwise, 13, 23).
  step_nonvar(stepwise, 33, 33).
  step_nonvar(stepwise, 51, 51).
  step_nonvar(stepwise, 43, 43).
  step_nonvar(stepwise, 28, 28).
  step_nonvar(stepwise, 28, 45).
  step_nonvar(stepwise, 45, 45).
  step_nonvar(stepwise, 89, 89).
  step_nonvar(stepwise, 3, 7).
  step_nonvar(stepwise, 16, 23).
  step_nonvar(stepwise, 8, 19).
  step_nonvar(stepwise, 8, 13).
  step_nonvar(stepwise, 8, 16).
  step_nonvar(stepwise, 8, 17).
  step_nonvar(stepwise, 8, 20).
  step_nonvar(stepwise, 8, 15).
  step_nonvar(stepwise, 8, 12).
  step_nonvar(stepwise, 8, 18).
  step_nonvar(stepwise, 8, 14).
  step_nonvar(stepwise, 25, 39).
  step_nonvar(stepwise, 25, 25).
  step_nonvar(stepwise, 15, 23).
  step_nonvar(stepwise, 57, 57).
  step_nonvar(stepwise, 29, 47).
  step_nonvar(stepwise, 29, 29).
  step_nonvar(stepwise, 91, 91).
  step_nonvar(stepwise, 37, 37).
  step_nonvar(stepwise, 18, 23).
  step_nonvar(stepwise, 61, 61).
  step_nonvar(stepwise, 26, 41).
  step_nonvar(stepwise, 26, 26).
  step_nonvar(stepwise, 14, 23).
% max_heap_size: 1006592; minor_collections: 13; major collections: 2

