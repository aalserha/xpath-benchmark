# MAIN	= XPathMark
# MAIN  = jo
# MAIN  = Antonio/CIAA
# MAIN 	= quadratic
MAIN  = lick/forward

-include Makefiles/$(MAIN).mk

nRegExp	= nRegExp

# SCHEMA-CLEAN-CO-ACC=false


STATISTICS_DIR  = ../automata-store/lick/forward/version-0.6

# OUTPUT_DIR	= ../automata-store/lick/forward/version-0.51
OUTPUT_DIR	= ../automata-store/lick/forward/version-0.6
# OUTPUT_DIR	= ../automata-store/lick/forward/version-jo
# OUTPUT_DIR	= s
# VALIDATE	= true

# SOURCE	= schema-product/mini
nRegExp-TARGET	= many
# TIMEOUT	= 500

AUTOMATON	= det-stepwise
# EXPRESSION	= 14340
# EXPRESSION	= 18330
# EXPRESSION	= 15461a
EXPRESSION	= 07113a
# EXPRESSION	= 07113
# EXPRESSION	= A1

# SCHEMA-CLEAN-CO-ACC = false
# INTERSECTION-CO-ACC = false

# EXPRESSION	= Q1.1
# EXPRESSION 	= Q3.4
# EXPRESSION	= 13804
# EXPRESSION	= 15809
# EXPRESSION	= 15809
# EXPRESSION	= 18330
# EXPRESSION	= 00744
# EXPRESSION	= 4

# nRegExp	= nRegExp
# nRegExp-one-TARGETS = create-schema-det
# ALL_TARGET = evaluate
# AUTOMATA-LIST	= XML-Prag

PRINT_FAILED_RUNS = true

t29:
	@echo USER=$(USER)

s100:
	@echo SAMPLES="$(SAMPLES)"

pro:
	@echo "//query[@id='$(ID)']/@project"
	@echo PROJECT=$(PROJECT)

watch:
	tail -f t.txt

t22:
	echo nRegExp=$(nRegExp)
	echo OUTPUT_DIR=$(OUTPUT_DIR)
	echo COLLECTION=$(COLLECTION)
