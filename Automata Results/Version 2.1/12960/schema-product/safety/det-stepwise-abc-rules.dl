%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% User input are subsets Q of states of A for which to compute:
%%
%%    safe_sel^A(Q) and safe_ref^A(Q)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% These subsets Q are specified by facts of the form:
%%
%%    safe_sel_of(A,Q,X) and safe_rej_of(A,Q,X)
%%
%% which specify the elements X of the subsets Q. The
%% elements X in safe_sel^A(Q) and safe_ref^A(Q) are specified
%% by facts of the form:
%%
%%    safe_sel(A,Q,X) and safe_rej(A,Q,X) 
%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% the default subset provided is the subset of final states

safe_sel_of(A,final,X) :- final(A,X).
safe_rej_of(A,final,X) :- final(A,X).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% inverse delta accessibility
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

invacc_delta(A,Q,X) :- safe_rej_of(A,Q,X). 
invacc_delta(A,Q,X) :- step_nonvar(A,X,Y), invacc_delta(A,Q,Y).
invacc_delta(A,Q,X) :- step_var(A,X,Y), invacc_delta(A,Q,Y).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% inverse delta accessibility without variables
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

invacc_delta_nonvar(A,Q,X) :- hedge_state(A,X), safe_sel_of(A,Q,_),
                              not safe_sel_of(A,Q,X). 
invacc_delta_nonvar(A,Q,X) :- step_nonvar(A,X,Y),
                              invacc_delta_nonvar(A,Q,Y).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% safety for selection and selection
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


safe_rej(A,Q,X) :- hedge_state(A,X), safe_rej_of(A,Q,_),
		   not invacc_delta(A,Q,X).

safe_sel(A,Q,X) :- hedge_state(A,X), safe_sel_of(A,Q,_),
		   not invacc_delta_nonvar(A,Q,X).




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% delta steps
%% - with and without variables
%% - hedge, final, and nonfinal state
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% start datalog
% parse file ../FXP/../automata-store/lick/forward/version-0.6/12960/schema-product/step/det-stepwise-rules.dl
% process 2410 clauses
% computing fixpoint...
% done.
% facts matching pattern nonfinal(X0, X1):
  nonfinal(stepwise, 10).
  nonfinal(stepwise, 7).
  nonfinal(stepwise, 31).
  nonfinal(stepwise, 21).
  nonfinal(stepwise, 32).
  nonfinal(stepwise, 49).
  nonfinal(stepwise, 52).
  nonfinal(stepwise, 19).
  nonfinal(stepwise, 5).
  nonfinal(stepwise, 11).
  nonfinal(stepwise, 22).
  nonfinal(stepwise, 36).
  nonfinal(stepwise, 34).
  nonfinal(stepwise, 2).
  nonfinal(stepwise, 9).
  nonfinal(stepwise, 44).
  nonfinal(stepwise, 20).
  nonfinal(stepwise, 17).
  nonfinal(stepwise, 42).
  nonfinal(stepwise, 46).
  nonfinal(stepwise, 0).
  nonfinal(stepwise, 38).
  nonfinal(stepwise, 1).
  nonfinal(stepwise, 23).
  nonfinal(stepwise, 41).
  nonfinal(stepwise, 47).
  nonfinal(stepwise, 12).
  nonfinal(stepwise, 30).
  nonfinal(stepwise, 24).
  nonfinal(stepwise, 27).
  nonfinal(stepwise, 50).
  nonfinal(stepwise, 35).
  nonfinal(stepwise, 39).
  nonfinal(stepwise, 4).
  nonfinal(stepwise, 6).
  nonfinal(stepwise, 13).
  nonfinal(stepwise, 33).
  nonfinal(stepwise, 45).
  nonfinal(stepwise, 51).
  nonfinal(stepwise, 28).
  nonfinal(stepwise, 43).
  nonfinal(stepwise, 8).
  nonfinal(stepwise, 3).
  nonfinal(stepwise, 16).
  nonfinal(stepwise, 25).
  nonfinal(stepwise, 15).
  nonfinal(stepwise, 40).
  nonfinal(stepwise, 29).
  nonfinal(stepwise, 48).
  nonfinal(stepwise, 37).
  nonfinal(stepwise, 26).
  nonfinal(stepwise, 18).
  nonfinal(stepwise, 14).
% facts matching pattern final(X0, X1):
  final(stepwise, 53).
  final(stepwise, 54).
% facts matching pattern hedge_state(X0, X1):
  hedge_state(stepwise, 10).
  hedge_state(stepwise, 7).
  hedge_state(stepwise, 31).
  hedge_state(stepwise, 21).
  hedge_state(stepwise, 49).
  hedge_state(stepwise, 19).
  hedge_state(stepwise, 5).
  hedge_state(stepwise, 11).
  hedge_state(stepwise, 22).
  hedge_state(stepwise, 2).
  hedge_state(stepwise, 9).
  hedge_state(stepwise, 44).
  hedge_state(stepwise, 20).
  hedge_state(stepwise, 17).
  hedge_state(stepwise, 54).
  hedge_state(stepwise, 0).
  hedge_state(stepwise, 1).
  hedge_state(stepwise, 23).
  hedge_state(stepwise, 47).
  hedge_state(stepwise, 12).
  hedge_state(stepwise, 30).
  hedge_state(stepwise, 24).
  hedge_state(stepwise, 27).
  hedge_state(stepwise, 53).
  hedge_state(stepwise, 35).
  hedge_state(stepwise, 39).
  hedge_state(stepwise, 4).
  hedge_state(stepwise, 6).
  hedge_state(stepwise, 13).
  hedge_state(stepwise, 33).
  hedge_state(stepwise, 45).
  hedge_state(stepwise, 28).
  hedge_state(stepwise, 43).
  hedge_state(stepwise, 8).
  hedge_state(stepwise, 3).
  hedge_state(stepwise, 16).
  hedge_state(stepwise, 25).
  hedge_state(stepwise, 15).
  hedge_state(stepwise, 40).
  hedge_state(stepwise, 29).
  hedge_state(stepwise, 48).
  hedge_state(stepwise, 37).
  hedge_state(stepwise, 14).
% facts matching pattern step_var(X0, X1, X2):
  step_var(stepwise, 31, 44).
  step_var(stepwise, 31, 40).
  step_var(stepwise, 31, 48).
  step_var(stepwise, 21, 35).
  step_var(stepwise, 21, 44).
  step_var(stepwise, 21, 40).
  step_var(stepwise, 21, 48).
  step_var(stepwise, 19, 45).
  step_var(stepwise, 19, 44).
  step_var(stepwise, 19, 40).
  step_var(stepwise, 19, 48).
  step_var(stepwise, 5, 49).
  step_var(stepwise, 22, 44).
  step_var(stepwise, 22, 40).
  step_var(stepwise, 22, 48).
  step_var(stepwise, 22, 37).
  step_var(stepwise, 28, 39).
  step_var(stepwise, 28, 43).
  step_var(stepwise, 28, 47).
  step_var(stepwise, 16, 24).
  step_var(stepwise, 20, 39).
  step_var(stepwise, 20, 43).
  step_var(stepwise, 20, 47).
  step_var(stepwise, 0, 53).
  step_var(stepwise, 0, 54).
  step_var(stepwise, 23, 44).
  step_var(stepwise, 23, 40).
  step_var(stepwise, 23, 48).
  step_var(stepwise, 29, 35).
  step_var(stepwise, 29, 44).
  step_var(stepwise, 29, 40).
  step_var(stepwise, 29, 48).
  step_var(stepwise, 30, 44).
  step_var(stepwise, 30, 40).
  step_var(stepwise, 30, 37).
  step_var(stepwise, 30, 48).
  step_var(stepwise, 27, 45).
  step_var(stepwise, 27, 44).
  step_var(stepwise, 27, 40).
  step_var(stepwise, 27, 48).
% facts matching pattern step_nonvar(X0, X1, X2):
  step_nonvar(stepwise, 10, 10).
  step_nonvar(stepwise, 7, 16).
  step_nonvar(stepwise, 7, 15).
  step_nonvar(stepwise, 31, 31).
  step_nonvar(stepwise, 21, 21).
  step_nonvar(stepwise, 21, 29).
  step_nonvar(stepwise, 19, 19).
  step_nonvar(stepwise, 19, 27).
  step_nonvar(stepwise, 11, 19).
  step_nonvar(stepwise, 22, 22).
  step_nonvar(stepwise, 22, 30).
  step_nonvar(stepwise, 2, 7).
  step_nonvar(stepwise, 2, 6).
  step_nonvar(stepwise, 2, 8).
  step_nonvar(stepwise, 9, 17).
  step_nonvar(stepwise, 17, 25).
  step_nonvar(stepwise, 44, 44).
  step_nonvar(stepwise, 20, 28).
  step_nonvar(stepwise, 20, 20).
  step_nonvar(stepwise, 1, 5).
  step_nonvar(stepwise, 0, 4).
  step_nonvar(stepwise, 0, 3).
  step_nonvar(stepwise, 0, 2).
  step_nonvar(stepwise, 0, 1).
  step_nonvar(stepwise, 23, 31).
  step_nonvar(stepwise, 23, 23).
  step_nonvar(stepwise, 47, 47).
  step_nonvar(stepwise, 12, 20).
  step_nonvar(stepwise, 27, 27).
  step_nonvar(stepwise, 24, 33).
  step_nonvar(stepwise, 24, 24).
  step_nonvar(stepwise, 30, 30).
  step_nonvar(stepwise, 53, 53).
  step_nonvar(stepwise, 35, 35).
  step_nonvar(stepwise, 33, 33).
  step_nonvar(stepwise, 39, 39).
  step_nonvar(stepwise, 4, 10).
  step_nonvar(stepwise, 6, 13).
  step_nonvar(stepwise, 6, 11).
  step_nonvar(stepwise, 6, 15).
  step_nonvar(stepwise, 6, 12).
  step_nonvar(stepwise, 6, 14).
  step_nonvar(stepwise, 13, 21).
  step_nonvar(stepwise, 28, 28).
  step_nonvar(stepwise, 43, 43).
  step_nonvar(stepwise, 45, 45).
  step_nonvar(stepwise, 3, 9).
  step_nonvar(stepwise, 16, 23).
  step_nonvar(stepwise, 8, 15).
  step_nonvar(stepwise, 25, 25).
  step_nonvar(stepwise, 15, 23).
  step_nonvar(stepwise, 40, 40).
  step_nonvar(stepwise, 29, 29).
  step_nonvar(stepwise, 48, 48).
  step_nonvar(stepwise, 37, 37).
  step_nonvar(stepwise, 14, 22).
% max_heap_size: 499712; minor_collections: 6; major collections: 0

