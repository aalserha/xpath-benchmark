%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  intersection of stepwise hedge automata A and B
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% variables X,Y for states,
%%           L,T for labels and their types
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% hedge states
p_state(X,Y)   :- p_start_tree(X,Y).
p_state(X,Y)   :- p_initial(X,Y).
p_state(X1,Y1) :- p_elserule(X,Y,X1,Y1).
p_state(X1,Y1) :- p_typedelserule(X,Y,T,X1,Y1).
p_state(X1,Y1) :- p_typedelseArule(X,Y,T,X1,Y1).
p_state(X1,Y1) :- p_typedelseBrule(X,Y,T,X1,Y1).
p_state(X3,Y3) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3).
p_state(X3,Y3) :- p_applyelserule(X1,Y1,X3,Y3).
p_state(X3,Y3) :- p_applyAelserule(X1,Y1,X3,Y3).
p_state(X3,Y3) :- p_applyBelserule(X1,Y1,X3,Y3).
p_state(X1,Y1) :- p_internalrule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_internalArule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_internalBrule(X,Y,L,T,X1,Y1).
p_state(X1,Y1) :- p_epsilonArule(X,Y,X1,Y1).
p_state(X1,Y1) :- p_epsilonBrule(X,Y,X1,Y1).
p_state(IX,IY) :- p_tree_istate(X2,Y2,IX,IY).

%%% tree states that are copies of hedge states
p_copy_of(X1,X2,Z1,Z2) :- p_state(X1,X2), copy_of(A,X1,Z1), state(A,Z1),
                          copy_of(B,X2,Z2), state(B,Z2), inter(A,B,C).

%%% tree states and istates of tree states
p_tree_state(X2,Y2)        :- p_tree_istate(X2,Y2,IX,IY).
p_tree_istate(X2,Y2,IX,IY) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), 
                              tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).
p_tree_istate(X2,Y2,IX,IY) :- p_applyelserule(X1,Y1,X3,Y3), 
                               tree_istate(A,X2,IX), tree_istate(B,Y2,IY), inter(A,B,C).

p_tree_state(X2,Y2) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3).
p_tree_state(X2,Y2) :- p_applyelserule(X1,Y1,X3,Y3), tree_state(A,X2), tree_state(B,Y2), inter(A,B,C).  

p_tree_state(Z1,Z2) :- p_copy_of(X1,X2,Z1,Z2).
p_tree_state(U,V)   :- p_treerule(X,Y,U,V).

%%% distinguished states
p_initial(X,Y)    :- initial(A,X), initial(B,Y), inter(A,B,C).
p_else(X,Y)       :- p_tree_state(X,Y), else(A,X), else(B,Y),  inter(A,B,C).
p_elseA(X,Y)      :- else(A,X), p_tree_state(X,Y), inter(A,B,C).
p_elseB(X,Y)      :- p_tree_state(X,Y), else(B,Y), inter(A,B,C).
p_final(X,Y)      :- final(A,X), final(B,Y), p_state(X,Y), inter(A,B,C).
p_finalA(X,Y)      :- final(A,X), p_state(X,Y), inter(A,B,C).
p_finalB(X,Y)      :- final(B,Y), p_state(X,Y), inter(A,B,C).
p_start_tree(X,Y) :- start_tree(A,X), start_tree(B,Y), inter(A,B,C).
p_sinkA(X,Y)       :- sink(A,X), state(B,Y), inter(A,B,C).
p_sinkB(X,Y)       :- state(A,X), sink(B,Y), inter(A,B,C).
p_sink(X,Y)        :- p_sinkA(X,Y).
p_sink(X,Y)        :- p_sinkB(X,Y).

%% origins
%  p_originA_of(X,Y,X1,Y2)   :- origin_of(A,X,X1,X2), state(B,Y), inter(A,B,C).

%%% internalrules

internalfromelse(B,Y,L,T,Y1) :- elserule(B,Y,Y1), no_internalrule(B,Y,L,T).
internalfromelse(B,Y,L,T,Y1) :- typedelserule(B,Y,T,Y1), no_internalrule(B,Y,L,T).
internalfromelse(B,Y,L,T,Y1) :- elserule(B,Y,Y1), not_in_signature(B,L,T).
internalfromelse(B,Y,L,T,Y1) :- typedelserule(B,Y,T,Y1), not_in_signature(B,L,T).


% internal / else
p_internalArule(X,Y,L,T,X1,Y1) :- internalrule(A,X,L,T,X1), internalfromelse(B,Y,L,T,Y1),
                                  p_state(X,Y), inter(A,B,C).
% else / internal
p_internalBrule(X,Y,L,T,X1,Y1) :- internalfromelse(A,X,L,T,X1), internalrule(B,Y,L,T,Y1),
			       	  p_state(X,Y), inter(A,B,C).

% internal / internal
p_internalrule(X,Y,L,T,X1,Y1) :- internalrule(A,X,L,T,X1), internalrule(B,Y,L,T,Y1),
			      	 p_state(X,Y), inter(A,B,C).

%%% else rules typed and untyped

% else / else
p_elserule(X,Y,X1,Y1) :- elserule(A,X,X1), elserule(B,Y,Y1), p_state(X,Y), inter(A,B,C).

% typed_else / typed_else
p_typedelserule(X,Y,T,X1,Y1) :- typedelserule(A,X,T,X1), typedelserule(B,Y,T,Y1),
			     p_state(X,Y), inter(A,B,C).
% typed_else / else
p_typedelseArule(X,Y,T,X1,Y1) :- typedelserule(A,X,T,X1), elserule(B,Y,Y1),
			      p_state(X,Y), inter(A,B,C).

% else / typed_else
p_typedelseBrule(X,Y,T,X1,Y1) :- elserule(A,X,X1), typedelserule(B,Y,T,Y1),
			      p_state(X,Y), inter(A,B,C).


%%% applyrules

applyfromelse(B,Y1,Y2,Y3) :- else(B,Y2), no_applyrule(B,Y1,Y2), applyelserule(B,Y1,Y3).

% apply / apply
p_applyrule(X1,Y1,X2,Y2,X3,Y3) :- applyrule(A,X1,X2,X3), applyrule(B,Y1,Y2,Y3),
			          p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
% apply / applyelse
p_applyArule(X1,Y1,X2,Y2,X3,Y3) :- applyrule(A,X1,X2,X3), applyfromelse(B,Y1,Y2,Y3),
				p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
p_applyAelserule(X1,Y1,X2,else,X3,Y3) :- applyrule(A,X1,X2,X3), applyelserule(B,Y1,Y3),
				      p_state(X1,Y1), else(B,Y2),
				      p_tree_state(X2,Y2), inter(A,B,C).
% applyelse / apply
p_applyBrule(X1,Y1,X2,Y2,X3,Y3) :- applyfromelse(A,X1,X2,X3), applyrule(B,Y1,Y2,Y3),
				p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).
p_applyBelserule(X1,Y1,else,Y2,X3,Y3) :- applyelserule(A,X1,X3), applyrule(B,Y1,Y2,Y3),
				      p_state(X1,Y1), else(A,Y1),
				      p_tree_state(X2,Y2), inter(A,B,C).
% applyelse / applyelse
p_applyelserule(X1,Y1,X3,Y3) :- applyelserule(A,X1,X3), applyelserule(B,Y1,Y3),
			     else(A,X2), no_applyrule(A,X1,X2),
			     else(B,Y2), no_applyrule(B,Y1,Y2),
			     p_state(X1,Y1), p_tree_state(X2,Y2), inter(A,B,C).

% else states
p_else(X,Y) :-  else(A,X), else(B,Y), inter(A,B,C).

% tree / tree
p_treerule(X,Y,X1,Y1) :- treerule(A,X,X1), treerule(B,Y,Y1), p_state(X,Y), inter(A,B,C). 

% epsilon / skip
p_epsilonArule(X,Y,X1,Y) :- epsilonrule(A,X,X1), p_state(X,Y), inter(A,B,C).

% skip / epsilon
p_epsilonBrule(X,Y,X,Y1) :- epsilonrule(B,Y,Y1), p_state(X,Y), inter(A,B,C).

% difference of signature
no_internalrule(A,X,L,T) :- not_in_signature(A,L,T), state(A,X), inter(A,B,C).
no_internalrule(B,Y,L,T) :- not_in_signature(B,L,T), state(B,Y), inter(A,B,C).

% title
p_title(X,Y) :- title(A,X), title(B,Y), inter(A,B,C).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% co-accessibility
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p_state(co_acc,X,Y) :- p_final(X,Y).
p_state(co_acc,X,Y) :- p_treerule(X,Y,X1,Y1), p_tree_state(co_acc,X1,Y1).
p_state(co_acc,X,Y) :- p_applyelserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).

p_tree_state(co_acc,X2,Y2) :- p_applyelserule(co_acc,X,Y,X1,Y1),
			      p_state(co_acc,X1,Y1), p_else(X2,Y2).

p_state(co_acc,X1,Y1) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyArule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_state(co_acc,X1,Y1)      :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyBrule(X1,Y1,X2,Y2,X3,Y3), p_state(co_acc,X3,Y3).
p_state(co_acc,X1,Y1)      :- p_applyrule(X1,Y1,X2,Y2,X3,Y3),  p_state(co_acc,X3,Y3).
p_tree_state(co_acc,X2,Y2) :- p_applyrule(X1,Y1,X2,Y2,X3,Y3),  p_state(co_acc,X3,Y3).
p_state(co_acc,X,Y)   :- p_internalrule(X,Y,L,T,X1,Y1),  p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_internalArule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_internalBrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_epsilonArule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_epsilonBrule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_elserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_typedelseArule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_typedelseBrule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_state(co_acc,X,Y)   :- p_typedelserule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).

p_initial(co_acc,X,Y) :- p_state(co_acc,X,Y), p_initial(X,Y).
p_final(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_final(X,Y).
p_finalA(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_finalA(X,Y).
p_finalB(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_finalB(X,Y).
p_sink(co_acc,X,Y)    :- p_state(co_acc,X,Y), p_sink(X,Y).
p_sinkA(co_acc,X,Y)   :- p_state(co_acc,X,Y), p_sinkA(X,Y).
p_else(co_acc,X,Y)    :- p_tree_state(co_acc,X,Y), p_else(X,Y).
p_elseA(co_acc,X,Y)   :- p_tree_state(co_acc,X,Y), p_elseA(X,Y).
p_elseB(co_acc,X,Y)   :- p_tree_state(co_acc,X,Y), p_elseB(X,Y).
p_start_tree(co_acc,X,Y) :- p_state(co_acc,X,Y), p_start_tree(X,Y).

p_copy_of(co_acc,X,Y,X1,Y1)     :- p_copy_of(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_tree_istate(co_acc,X,Y,X1,Y1) :- p_tree_istate(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_internalrule(co_acc,X,Y,L,T,X1,Y1)  :- p_internalrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_internalArule(co_acc,X,Y,L,T,X1,Y1) :- p_internalArule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_internalBrule(co_acc,X,Y,L,T,X1,Y1) :- p_internalBrule(X,Y,L,T,X1,Y1), p_state(co_acc,X1,Y1).
p_epsilonArule(co_acc,X,Y,X1,Y1) :- p_epsilonArule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_epsilonBrule(co_acc,X,Y,X1,Y1) :- p_epsilonBrule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).

p_treerule(co_acc,X,Y,X1,Y1) :- p_treerule(X,Y,X1,Y1), p_tree_state(co_acc,X1,Y1).	
p_elserule(co_acc,X,Y,X1,Y1) :- p_elserule(X,Y,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelserule(co_acc,X,Y,T,X1,Y1) :- p_typedelserule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelseArule(co_acc,X,Y,T,X1,Y1) :- p_typedelseArule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_typedelseBrule(co_acc,X,Y,T,X1,Y1) :- p_typedelseBrule(X,Y,T,X1,Y1), p_state(co_acc,X1,Y1).
p_applyrule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyArule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyArule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyBrule(co_acc,X,Y,X1,Y1,X2,Y2)  :- p_applyBrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).

p_applyelserule(co_acc,X,Y,X2,Y2) :- p_applyelserule(X,Y,X2,Y2), p_state(co_acc,X2,Y2).
p_applyAelserule(co_acc,X,Y,X1,Y1,X2,Y2) :- p_applyAelserule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).
p_applyelseBrule(co_acc,X,Y,X1,Y1,X2,Y2) :- p_applyelseBrule(X,Y,X1,Y1,X2,Y2), p_state(co_acc,X2,Y2).


n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalrule(X1,Y1,L,T,X,Y).
n_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalrule(X1,Y1,L,T,X,Y).

n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalBrule(X1,Y1,L,T,X,Y).
n_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalBrule(X1,Y1,L,T,X,Y).

n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_elserule(co_acc,X1,Y1,X2,Y2),         p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelserule(co_acc,X1,T,Y1,X2,Y2),  p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseArule(co_acc,X1,Y1,T,X2,Y2), p_internalArule(X1,Y1,L,T,X,Y).
n_internalArule(co_acc,X1,Y1,L,T,X,Y) :- p_typedelseBrule(co_acc,X1,Y1,T,X2,Y2), p_internalArule(X1,Y1,L,T,X,Y).

p_internalrule(co_acc,X1,Y1,L,T,X,Y)  :- n_internalrule(co_acc,X1,Y1,L,T,X,Y).
p_internalArule(co_acc,X1,Y1,L,T,X,Y) :- n_internalArule(co_acc,X1,Y1,L,T,X,Y).
p_internalBrule(co_acc,X1,Y1,L,T,X,Y) :- n_internalBrule(co_acc,X1,Y1,L,T,X,Y).

needed_p_state(co_acc,X,Y)            :- n_internalrule(co_acc,X1,Y1,L,T,X,Y). 
needed_p_stateA(co_acc,X,Y)           :- n_internalArule(co_acc,X1,Y1,L,T,X,Y).
needed_p_state (co_acc,X,Y)           :- n_internalBrule(co_acc,X1,Y1,L,T,X,Y).

p_originA(X,U,Y,Z) :- p_state(X,U),      origin_of(A,X,Y,Z), inter(A,B,C).
p_originA(X,U,Y,Z) :- p_tree_state(X,U), origin_of(A,X,Y,Z), inter(A,B,C).
p_labelA(L,T,Id)   :- label(A,L,T,Id), inter(A,B,C).
p_labelB(L,T,Id)   :- label(B,L,T,Id), inter(A,B,C).
p_max_label_idA(N)   :- max_label_id(A,N), inter(A,B,C).
p_max_label_idB(N)   :- max_label_id(B,N), inter(A,B,C).

p_originA(co_acc,X,U,Y,Z) :- p_originA(X,U,Y,Z), p_state(co_acc,X,U).
p_originA(co_acc,X,U,Y,Z) :- p_originA(X,U,Y,Z), p_tree_state(co_acc,X,U).
p_labelA(co_acc,L,T,Id)   :- p_labelA(L,T,Id).
p_labelB(co_acc,L,T,Id)   :- p_labelB(L,T,Id).
p_max_label_idA(co_acc,N)   :- p_max_label_idA(N).
p_max_label_idB(co_acc,N)   :- p_max_label_idB(N).


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXPRESSION=d1e174 AUTOMATON=stepwise
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule

% negative information only for elserules and applyelserules (NEGATIVE=false)
%   no_applyrule
%   no_internalrule
  
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(auto1,'step[&lt;elem⋅h⋅pre⋅_⋅T&gt;⋅T]').
expression(auto1,'d1e174').
no_typedelserule(auto1,2,type).
no_typedelserule(auto1,4,type).
no_typedelserule(auto1,5,type).
no_typedelserule(auto1,6,type).
no_typedelserule(auto1,7,type).
no_typedelserule(auto1,12,type).
no_typedelserule(auto1,2,namespace).
no_typedelserule(auto1,4,namespace).
no_typedelserule(auto1,5,namespace).
no_typedelserule(auto1,6,namespace).
no_typedelserule(auto1,7,namespace).
no_typedelserule(auto1,12,namespace).
no_typedelserule(auto1,2,name).
no_typedelserule(auto1,4,name).
no_typedelserule(auto1,5,name).
no_typedelserule(auto1,6,name).
no_typedelserule(auto1,7,name).
no_typedelserule(auto1,12,name).
state(auto1,2).
state(auto1,4).
state(auto1,5).
state(auto1,6).
state(auto1,7).
state(auto1,8).
state(auto1,9).
state(auto1,11).
state(auto1,12).
state(auto1,14).
start_tree(auto1,5).
start_tree(auto1,11).
initial(auto1,2).
not_sink(auto1,2).
not_sink(auto1,4).
not_sink(auto1,5).
not_sink(auto1,6).
not_sink(auto1,7).
not_sink(auto1,8).
not_sink(auto1,9).
not_sink(auto1,11).
not_sink(auto1,12).
not_sink(auto1,14).
else(auto1,12).
nonelse(auto1,2).
nonelse(auto1,4).
nonelse(auto1,5).
nonelse(auto1,6).
nonelse(auto1,7).
nonelse(auto1,8).
nonelse(auto1,9).
nonelse(auto1,11).
nonelse(auto1,14).
final(auto1,14).
nonfinal(auto1,2).
nonfinal(auto1,4).
nonfinal(auto1,5).
nonfinal(auto1,6).
nonfinal(auto1,7).
nonfinal(auto1,8).
nonfinal(auto1,9).
nonfinal(auto1,11).
nonfinal(auto1,12).
copy_of(auto1,4,4).
copy_of(auto1,5,4).
copy_of(auto1,6,4).
copy_of(auto1,7,4).
copy_of(auto1,8,4).
copy_of(auto1,9,4).
copy_of(auto1,11,12).
copy_of(auto1,12,12).
tree_state(auto1,4).
tree_istate(auto1,4,5).
 tree_state(auto1,12).
tree_istate(auto1,12,11).
 hedge_state(auto1,2).
    hedge_state(auto1,5).
    hedge_state(auto1,6).
    hedge_state(auto1,7).
    hedge_state(auto1,8).
    hedge_state(auto1,9).
    hedge_state(auto1,11).
    hedge_state(auto1,14).
    internalrule(auto1,5,'elem',type,6).
internalrule(auto1,6,'h',namespace,7).
internalrule(auto1,7,'pre',name,8).
applyelserule(auto1,9,9).
no_applyrule(auto1,9,4).
no_applyrule(auto1,9,12).
elserule(auto1,9,9).
elserule(auto1,8,9).
treerule(auto1,9,4).
applyelserule(auto1,14,14).
no_applyrule(auto1,14,4).
no_applyrule(auto1,14,12).
elserule(auto1,14,14).
applyelserule(auto1,11,11).
no_applyrule(auto1,11,4).
no_applyrule(auto1,11,12).
elserule(auto1,11,11).
treerule(auto1,11,12).
applyrule(auto1,2,4,14).
label(auto1,'elem',type,1).
no_internalrule(auto1,8,'elem',type).
no_internalrule(auto1,9,'elem',type).
no_internalrule(auto1,11,'elem',type).
no_internalrule(auto1,14,'elem',type).
label(auto1,'h',namespace,2).
no_internalrule(auto1,8,'h',namespace).
no_internalrule(auto1,9,'h',namespace).
no_internalrule(auto1,11,'h',namespace).
no_internalrule(auto1,14,'h',namespace).
label(auto1,'pre',name,3).
no_internalrule(auto1,8,'pre',name).
no_internalrule(auto1,9,'pre',name).
no_internalrule(auto1,11,'pre',name).
no_internalrule(auto1,14,'pre',name).
max_label_id(auto1,3).
    %%% different origins %%%%%%%
    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXPRESSION=d1e206 AUTOMATON=stepwise
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule

% negative information only for elserules and applyelserules (NEGATIVE=false)
%   no_applyrule
%   no_internalrule
  
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(auto2,'step[(μx_d1.(&lt;text⋅x⋅T&gt;⋅T_bl_Z+_bl_Z&lt;(elem⋅_⋅__bl_Z+_bl_Zdoc)⋅_⋅T⋅x_d1&gt;⋅T))]').
expression(auto2,'d1e206').
var(auto2,'x',1).
no_typedelserule(auto2,1,type).
no_typedelserule(auto2,2,type).
no_typedelserule(auto2,3,type).
no_typedelserule(auto2,4,type).
no_typedelserule(auto2,7,type).
no_typedelserule(auto2,8,type).
no_typedelserule(auto2,9,type).
no_typedelserule(auto2,10,type).
no_typedelserule(auto2,14,type).
no_typedelserule(auto2,16,type).
no_typedelserule(auto2,18,type).
no_typedelserule(auto2,20,type).
no_typedelserule(auto2,22,type).
no_typedelserule(auto2,1,var).
no_typedelserule(auto2,2,var).
no_typedelserule(auto2,3,var).
no_typedelserule(auto2,4,var).
no_typedelserule(auto2,7,var).
no_typedelserule(auto2,8,var).
no_typedelserule(auto2,9,var).
no_typedelserule(auto2,10,var).
no_typedelserule(auto2,14,var).
no_typedelserule(auto2,16,var).
no_typedelserule(auto2,18,var).
no_typedelserule(auto2,20,var).
no_typedelserule(auto2,22,var).
state(auto2,1).
state(auto2,2).
state(auto2,3).
state(auto2,4).
state(auto2,5).
state(auto2,6).
state(auto2,7).
state(auto2,8).
state(auto2,9).
state(auto2,10).
state(auto2,11).
state(auto2,12).
state(auto2,13).
state(auto2,14).
state(auto2,15).
state(auto2,16).
state(auto2,17).
state(auto2,18).
state(auto2,19).
state(auto2,20).
state(auto2,21).
state(auto2,22).
state(auto2,23).
start_tree(auto2,3).
start_tree(auto2,9).
start_tree(auto2,10).
start_tree(auto2,17).
initial(auto2,1).
initial(auto2,7).
not_sink(auto2,1).
not_sink(auto2,2).
not_sink(auto2,3).
not_sink(auto2,4).
not_sink(auto2,5).
not_sink(auto2,6).
not_sink(auto2,7).
not_sink(auto2,8).
not_sink(auto2,9).
not_sink(auto2,10).
not_sink(auto2,11).
not_sink(auto2,12).
not_sink(auto2,13).
not_sink(auto2,14).
not_sink(auto2,15).
not_sink(auto2,16).
not_sink(auto2,17).
not_sink(auto2,18).
not_sink(auto2,19).
not_sink(auto2,20).
not_sink(auto2,21).
not_sink(auto2,22).
not_sink(auto2,23).
else(auto2,18).
nonelse(auto2,1).
nonelse(auto2,2).
nonelse(auto2,3).
nonelse(auto2,4).
nonelse(auto2,5).
nonelse(auto2,6).
nonelse(auto2,7).
nonelse(auto2,8).
nonelse(auto2,9).
nonelse(auto2,10).
nonelse(auto2,11).
nonelse(auto2,12).
nonelse(auto2,13).
nonelse(auto2,14).
nonelse(auto2,15).
nonelse(auto2,16).
nonelse(auto2,17).
nonelse(auto2,19).
nonelse(auto2,20).
nonelse(auto2,21).
nonelse(auto2,22).
nonelse(auto2,23).
final(auto2,6).
final(auto2,19).
nonfinal(auto2,1).
nonfinal(auto2,2).
nonfinal(auto2,3).
nonfinal(auto2,4).
nonfinal(auto2,5).
nonfinal(auto2,7).
nonfinal(auto2,8).
nonfinal(auto2,9).
nonfinal(auto2,10).
nonfinal(auto2,11).
nonfinal(auto2,12).
nonfinal(auto2,13).
nonfinal(auto2,14).
nonfinal(auto2,15).
nonfinal(auto2,16).
nonfinal(auto2,17).
nonfinal(auto2,18).
nonfinal(auto2,20).
nonfinal(auto2,21).
nonfinal(auto2,22).
nonfinal(auto2,23).
copy_of(auto2,2,2).
copy_of(auto2,3,2).
copy_of(auto2,4,2).
copy_of(auto2,5,2).
copy_of(auto2,8,8).
copy_of(auto2,9,8).
copy_of(auto2,10,8).
copy_of(auto2,11,8).
copy_of(auto2,12,8).
copy_of(auto2,13,8).
copy_of(auto2,14,8).
copy_of(auto2,15,8).
copy_of(auto2,16,8).
copy_of(auto2,17,18).
copy_of(auto2,18,18).
copy_of(auto2,20,8).
copy_of(auto2,21,8).
copy_of(auto2,22,8).
copy_of(auto2,23,8).
tree_state(auto2,2).
tree_istate(auto2,2,3).
 tree_state(auto2,8).
tree_istate(auto2,8,9).
 tree_istate(auto2,8,10).
 tree_state(auto2,18).
tree_istate(auto2,18,17).
 hedge_state(auto2,1).
    hedge_state(auto2,3).
    hedge_state(auto2,4).
    hedge_state(auto2,5).
    hedge_state(auto2,6).
    hedge_state(auto2,7).
    hedge_state(auto2,9).
    hedge_state(auto2,10).
    hedge_state(auto2,11).
    hedge_state(auto2,12).
    hedge_state(auto2,13).
    hedge_state(auto2,14).
    hedge_state(auto2,15).
    hedge_state(auto2,16).
    hedge_state(auto2,17).
    hedge_state(auto2,19).
    hedge_state(auto2,20).
    hedge_state(auto2,21).
    hedge_state(auto2,22).
    hedge_state(auto2,23).
    internalrule(auto2,3,'text',type,4).
applyelserule(auto2,5,5).
no_applyrule(auto2,5,2).
no_applyrule(auto2,5,8).
no_applyrule(auto2,5,18).
elserule(auto2,5,5).
internalrule(auto2,4,'x',var,5).
treerule(auto2,5,2).
applyelserule(auto2,6,6).
no_applyrule(auto2,6,2).
no_applyrule(auto2,6,8).
no_applyrule(auto2,6,18).
elserule(auto2,6,6).
applyrule(auto2,1,2,6).
internalrule(auto2,9,'elem',type,11).
elserule(auto2,11,12).
elserule(auto2,12,13).
internalrule(auto2,10,'doc',type,13).
applyelserule(auto2,15,15).
no_applyrule(auto2,15,2).
no_applyrule(auto2,15,8).
no_applyrule(auto2,15,18).
elserule(auto2,15,15).
elserule(auto2,13,15).
applyelserule(auto2,15,14).
no_applyrule(auto2,15,2).
no_applyrule(auto2,15,8).
no_applyrule(auto2,15,18).
elserule(auto2,15,14).
elserule(auto2,13,14).
treerule(auto2,16,8).
applyelserule(auto2,19,19).
no_applyrule(auto2,19,2).
no_applyrule(auto2,19,8).
no_applyrule(auto2,19,18).
elserule(auto2,19,19).
applyelserule(auto2,17,17).
no_applyrule(auto2,17,2).
no_applyrule(auto2,17,8).
no_applyrule(auto2,17,18).
elserule(auto2,17,17).
treerule(auto2,17,18).
applyrule(auto2,7,8,19).
applyelserule(auto2,21,21).
no_applyrule(auto2,21,2).
no_applyrule(auto2,21,8).
no_applyrule(auto2,21,18).
elserule(auto2,21,21).
applyrule(auto2,20,2,21).
applyelserule(auto2,23,23).
no_applyrule(auto2,23,2).
no_applyrule(auto2,23,8).
no_applyrule(auto2,23,18).
elserule(auto2,23,23).
applyrule(auto2,22,8,23).
epsilonrule(auto2,21,16).
epsilonrule(auto2,23,16).
epsilonrule(auto2,14,20).
epsilonrule(auto2,14,22).
label(auto2,'doc',type,1).
no_internalrule(auto2,5,'doc',type).
no_internalrule(auto2,6,'doc',type).
no_internalrule(auto2,11,'doc',type).
no_internalrule(auto2,12,'doc',type).
no_internalrule(auto2,13,'doc',type).
no_internalrule(auto2,15,'doc',type).
no_internalrule(auto2,17,'doc',type).
no_internalrule(auto2,19,'doc',type).
no_internalrule(auto2,21,'doc',type).
no_internalrule(auto2,23,'doc',type).
label(auto2,'elem',type,2).
no_internalrule(auto2,5,'elem',type).
no_internalrule(auto2,6,'elem',type).
no_internalrule(auto2,11,'elem',type).
no_internalrule(auto2,12,'elem',type).
no_internalrule(auto2,13,'elem',type).
no_internalrule(auto2,15,'elem',type).
no_internalrule(auto2,17,'elem',type).
no_internalrule(auto2,19,'elem',type).
no_internalrule(auto2,21,'elem',type).
no_internalrule(auto2,23,'elem',type).
label(auto2,'text',type,3).
no_internalrule(auto2,5,'text',type).
no_internalrule(auto2,6,'text',type).
no_internalrule(auto2,11,'text',type).
no_internalrule(auto2,12,'text',type).
no_internalrule(auto2,13,'text',type).
no_internalrule(auto2,15,'text',type).
no_internalrule(auto2,17,'text',type).
no_internalrule(auto2,19,'text',type).
no_internalrule(auto2,21,'text',type).
no_internalrule(auto2,23,'text',type).
label(auto2,'x',var,4).
no_internalrule(auto2,5,'x',var).
no_internalrule(auto2,6,'x',var).
no_internalrule(auto2,11,'x',var).
no_internalrule(auto2,12,'x',var).
no_internalrule(auto2,13,'x',var).
no_internalrule(auto2,15,'x',var).
no_internalrule(auto2,17,'x',var).
no_internalrule(auto2,19,'x',var).
no_internalrule(auto2,21,'x',var).
no_internalrule(auto2,23,'x',var).
max_label_id(auto2,4).
    %%% different origins %%%%%%%
    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% not in signature

      %%%%% difference of signatures %%%%%%%%%%%
      
    not_in_signature(auto2,'h',namespace).
not_in_signature(auto2,'pre',name).

not_in_signature(auto1,'doc',type).
not_in_signature(auto1,'text',type).
not_in_signature(auto1,'x',var).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% intersection auto=auto1.auto2
inter(auto1,auto2,auto).
