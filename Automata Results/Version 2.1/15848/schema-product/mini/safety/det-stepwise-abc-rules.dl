%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% User input are subsets Q of states of A for which to compute:
%%
%%    safe_sel^A(Q) and safe_ref^A(Q)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% These subsets Q are specified by facts of the form:
%%
%%    safe_sel_of(A,Q,X) and safe_rej_of(A,Q,X)
%%
%% which specify the elements X of the subsets Q. The
%% elements X in safe_sel^A(Q) and safe_ref^A(Q) are specified
%% by facts of the form:
%%
%%    safe_sel(A,Q,X) and safe_rej(A,Q,X) 
%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% the default subset provided is the subset of final states

safe_sel_of(A,final,X) :- final(A,X).
safe_rej_of(A,final,X) :- final(A,X).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% inverse delta accessibility
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

invacc_delta(A,Q,X) :- safe_rej_of(A,Q,X). 
invacc_delta(A,Q,X) :- step_nonvar(A,X,Y), invacc_delta(A,Q,Y).
invacc_delta(A,Q,X) :- step_var(A,X,Y), invacc_delta(A,Q,Y).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% inverse delta accessibility without variables
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

invacc_delta_nonvar(A,Q,X) :- hedge_state(A,X), safe_sel_of(A,Q,_),
                              not safe_sel_of(A,Q,X). 
invacc_delta_nonvar(A,Q,X) :- step_nonvar(A,X,Y),
                              invacc_delta_nonvar(A,Q,Y).

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% safety for selection and selection
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


safe_rej(A,Q,X) :- hedge_state(A,X), safe_rej_of(A,Q,_),
		   not invacc_delta(A,Q,X).

safe_sel(A,Q,X) :- hedge_state(A,X), safe_sel_of(A,Q,_),
		   not invacc_delta_nonvar(A,Q,X).




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% delta steps
%% - with and without variables
%% - hedge, final, and nonfinal state
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% start datalog
% parse file ../FXP/../automata-store/lick/forward/version-0.6/15848/schema-product/mini/step/det-stepwise-rules.dl
% process 1555 clauses
% computing fixpoint...
% done.
% facts matching pattern nonfinal(X0, X1):
  nonfinal(stepwise, 31).
  nonfinal(stepwise, 10).
  nonfinal(stepwise, 7).
  nonfinal(stepwise, 32).
  nonfinal(stepwise, 21).
  nonfinal(stepwise, 19).
  nonfinal(stepwise, 11).
  nonfinal(stepwise, 22).
  nonfinal(stepwise, 5).
  nonfinal(stepwise, 34).
  nonfinal(stepwise, 2).
  nonfinal(stepwise, 20).
  nonfinal(stepwise, 9).
  nonfinal(stepwise, 17).
  nonfinal(stepwise, 1).
  nonfinal(stepwise, 0).
  nonfinal(stepwise, 23).
  nonfinal(stepwise, 12).
  nonfinal(stepwise, 27).
  nonfinal(stepwise, 24).
  nonfinal(stepwise, 30).
  nonfinal(stepwise, 35).
  nonfinal(stepwise, 4).
  nonfinal(stepwise, 33).
  nonfinal(stepwise, 13).
  nonfinal(stepwise, 6).
  nonfinal(stepwise, 28).
  nonfinal(stepwise, 8).
  nonfinal(stepwise, 3).
  nonfinal(stepwise, 16).
  nonfinal(stepwise, 25).
  nonfinal(stepwise, 15).
  nonfinal(stepwise, 29).
  nonfinal(stepwise, 18).
  nonfinal(stepwise, 26).
  nonfinal(stepwise, 14).
% facts matching pattern final(X0, X1):
  final(stepwise, 39).
  final(stepwise, 37).
% facts matching pattern hedge_state(X0, X1):
  hedge_state(stepwise, 31).
  hedge_state(stepwise, 10).
  hedge_state(stepwise, 7).
  hedge_state(stepwise, 32).
  hedge_state(stepwise, 21).
  hedge_state(stepwise, 39).
  hedge_state(stepwise, 4).
  hedge_state(stepwise, 33).
  hedge_state(stepwise, 6).
  hedge_state(stepwise, 11).
  hedge_state(stepwise, 22).
  hedge_state(stepwise, 5).
  hedge_state(stepwise, 8).
  hedge_state(stepwise, 3).
  hedge_state(stepwise, 16).
  hedge_state(stepwise, 25).
  hedge_state(stepwise, 2).
  hedge_state(stepwise, 9).
  hedge_state(stepwise, 17).
  hedge_state(stepwise, 1).
  hedge_state(stepwise, 0).
  hedge_state(stepwise, 23).
  hedge_state(stepwise, 15).
  hedge_state(stepwise, 29).
  hedge_state(stepwise, 12).
  hedge_state(stepwise, 37).
  hedge_state(stepwise, 18).
  hedge_state(stepwise, 26).
  hedge_state(stepwise, 27).
  hedge_state(stepwise, 24).
  hedge_state(stepwise, 14).
% facts matching pattern step_var(X0, X1, X2):
  step_var(stepwise, 21, 32).
  step_var(stepwise, 16, 32).
  step_var(stepwise, 9, 33).
  step_var(stepwise, 0, 39).
  step_var(stepwise, 0, 37).
  step_var(stepwise, 12, 17).
% facts matching pattern step_nonvar(X0, X1, X2):
  step_nonvar(stepwise, 7, 12).
  step_nonvar(stepwise, 10, 14).
  step_nonvar(stepwise, 31, 31).
  step_nonvar(stepwise, 32, 32).
  step_nonvar(stepwise, 21, 21).
  step_nonvar(stepwise, 6, 11).
  step_nonvar(stepwise, 4, 9).
  step_nonvar(stepwise, 5, 10).
  step_nonvar(stepwise, 5, 11).
  step_nonvar(stepwise, 22, 15).
  step_nonvar(stepwise, 22, 23).
  step_nonvar(stepwise, 11, 15).
  step_nonvar(stepwise, 3, 8).
  step_nonvar(stepwise, 8, 8).
  step_nonvar(stepwise, 16, 21).
  step_nonvar(stepwise, 16, 16).
  step_nonvar(stepwise, 2, 7).
  step_nonvar(stepwise, 25, 15).
  step_nonvar(stepwise, 25, 26).
  step_nonvar(stepwise, 17, 17).
  step_nonvar(stepwise, 17, 29).
  step_nonvar(stepwise, 0, 4).
  step_nonvar(stepwise, 0, 3).
  step_nonvar(stepwise, 0, 2).
  step_nonvar(stepwise, 0, 1).
  step_nonvar(stepwise, 1, 6).
  step_nonvar(stepwise, 1, 5).
  step_nonvar(stepwise, 23, 15).
  step_nonvar(stepwise, 23, 24).
  step_nonvar(stepwise, 15, 15).
  step_nonvar(stepwise, 12, 16).
  step_nonvar(stepwise, 29, 31).
  step_nonvar(stepwise, 29, 29).
  step_nonvar(stepwise, 37, 37).
  step_nonvar(stepwise, 27, 15).
  step_nonvar(stepwise, 18, 22).
  step_nonvar(stepwise, 18, 15).
  step_nonvar(stepwise, 26, 15).
  step_nonvar(stepwise, 26, 27).
  step_nonvar(stepwise, 24, 25).
  step_nonvar(stepwise, 24, 15).
  step_nonvar(stepwise, 14, 15).
  step_nonvar(stepwise, 14, 18).
% max_heap_size: 372736; minor_collections: 4; major collections: 0

