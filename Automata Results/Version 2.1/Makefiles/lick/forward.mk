COLLECTIONS     = FromXPath
COLLECTION	= LickForward-0.5
#-equal
STATISTICS_DIR  = ../automata-store/lick/forward/version-0.6
EXPRESSION	= 18330
PSEUDOCOMPLETION= true

nRegExp-TARGET  = many
nRegExp-one-TARGETS=create present
AUTOMATON	= det-stepwise
SCHEMA_DIR	= ../automata-store/Schemas
SCHEMA		= and[xml-document][one-x]
SCHEMA-CLEAN    = true
SCHEMA-CLEAN-CO-ACC=true
CO-ACC		= false
INTERSECTION-CO-ACC=true

OUTPUT_DIR	= ../automata-store/lick/forward/version-0.7/$(USER)

# SAMPLES	= ../automata-store/lick/samples.xml
MINIMIZATION    = true
PRESENT         = false
SAFEVAR		= false
SAFEVAR-FALSE   = false
SAFEVAR-TRUE	= false
# whether to use determinization in decompositions
DECOMP_DET	= false

SCHEMA-DET	= true
det-VALUE	= default
SHOW-VALUE	= false
MAX-STATES	= 10
DOC		= orig

LICKSMALL	= lick-small
#PROJECT		= $(shell xmlstarlet sel -T -t -m "//xpath[query[@id='$(EXPRESSION)']]/@filename"  -v "substring-before(substring-after(.,'/home/schmitz/research/devel/xpparser/benchmark/'),'/')" -o "" ../Testing/$(LICKSMALL).xml)

PROJECT		= $(shell xmlstarlet sel -T -t -m "//FXPs/FXP[@name='$(EXPRESSION)']/@project"  -v "." -o "" $(COLLECTIONS)/$(COLLECTION).xml)


DOCUMENTS	= Documents
DOCUMENT_DIR 	= $(DOCUMENTS)/$(PROJECT)
DOCS		= $(notdir $(basename $(wildcard ../Testing/$(DOCUMENT_DIR)/*.xml)))
# DOCS 		= $(shell find ../Testing/$(DOCUMENT_DIR) -name "*.xml" -exec basename \{} .xml \;)


AUTOMATA-LIST	= NextPlus
VALIDATE 	= false

ANSWER-DIR	= query-answers
map 		= $(foreach a,$(2),$(call $(1),$(a)))
to-SAMPLE	= ../Testing/$(ANSWER-DIR)/sample-$(PROJECT)-$(1).xml
SAMPLES		= $(call map,to-SAMPLE,$(DOCS))
# SAMPLES	= ../Testing/Created-$(USER)/samples-$(PROJECT).xml
TEST-SAMPLES	= false

t23:
	@echo PROJET='$(PROJECT)'
	@echo DOCUMENT_DIR=$(DOCUMENT_DIR)
	@echo DOCS=$(DOCS)
	@echo SAMPLES=$(SAMPLES)
	@echo CMD:  xmlstarlet sel -T -t -m "//queries/query[@id='$(EXPRESSION)']/@project"  -v "." -o "" ../Testing/$(LICKSMALL).xml

