%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Accessible states of stepwise hedge automata A
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% whether variable is allowed or not

kind(0).
kind(1).

%% whether we started a tree or a hedge

start(t).
start(h).

%% types different from type var


nonvar(type).
nonvar(char).
nonvar(negvar).
nonvar(name).
nonvar(namespace).

%% starting trees and hedges

acc(t,0,A,X) :- start_tree(A,X).
acc(h,0,A,X) :- initial(A,X).

%% ending trees

acc_tree_state(K,A,Z) :- acc(t,K,A,X), treerule(A,X,Z).
acc_tree_state(K,A,Y) :- acc_tree_state(K,A,X), is(A,X,Y).

%% internal rules

internalrule_all(A,X,L,T,Y) :- internalrule(A,X,L,T,Y).
internalrule_all(A,X,L,T,Y) :- elserule(A,X,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,L,T,Y) :- typedelserule(A,X,T,Y), no_internalrule(A,X,L,T).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- elserule(A,X,Y), typedelserule(A,L,T,Z).
internalrule_all(A,X,out_of_signature_4711,T,Y) :- typedelserule(A,X,T,Y).

acc(S,K,A,Y) :- acc(S,K,A,X), internalrule_all(A,X,L,T  ,Y), nonvar(T).
acc(S,1,A,Y) :- acc(S,0,A,X), internalrule_all(A,X,L,var,Y).

%% epsilon rules

acc(S,K,A,Y) :- acc(S,K,A,X), epsilonrule(A,X,Y), kind(K).

%% apply rules

applyrule_all(A,X,Z,Y) :- applyrule(A,X,Z,Y).
applyrule_all(A,X,Z,Y) :- applyelserule(A,X,Y), no_applyrule(A,X,Z).

acc(S,K,A,Y) :- acc(S,K,A,X), acc_tree_state(0,A,Z), applyrule_all(A,X,Z,Y).
acc(S,1,A,Y) :- acc(S,0,A,X), acc_tree_state(1,A,Z), applyrule_all(A,X,Z,Y).

%% compatibility with previous predicates

acc_h(A,X) :- acc(h,1,A,X).
acc_t(A,X) :- acc(t,1,A,X).




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% label states where XML labels end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

internal_or_else(A,X,Y) :- internalrule(A,X,L,T,Y).
internal_or_else(A,X,Y) :- elserule(A,X,Y).
internal_or_else(A,X,Y) :- typedelserule(A,X,T,Y).

label_state(A,U) :- initial(A,X), internalrule(A,X,'elem',type,Y),internal_or_else(A,Y,Z),
		    internal_or_else(A,Z,U).
label_state(A,U) :- initial(A,X), internalrule(A,X,'att',type,Y), internal_or_else(A,Y,Z),
                    internal_or_else(A,Z,U).
label_state(A,Y) :- initial(A,X), internalrule(A,X,'text',type,Y). 
label_state(A,Y) :- initial(A,X), internalrule(A,X,'comment',type,Y). 
label_state(A,Y) :- initial(A,X), internalrule(A,X,'doc',type,Y). 

acc_label(A,X,X) :- label_state(A,X).
acc_label(A,X,Z) :- acc_label(A,X,Y), internalrule(A,Y,L,T,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), elserule(A,Y,Z).
acc_label(A,X,Z) :- acc_label(A,X,Y), typedelserule(A,Y,T,Z).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(A,Z), applyrule(A,X1,Z,Y).
acc_label(A,X,Y) :- acc_label(A,X,X1), acc_tree_state(A,Z), applyelserule(A,X1,Y), no_applyrule(A,X1,Z).

acc_label_tree(A,X,Z) :- acc_label(A,X,Y), treerule(A,Y,Z).


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% stepwise automaton 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
% extensional predicates:
%        state, initial, sink
%        final, nonfinal, else, nonelse
%        start_tree, tree_state, tree_istate, copy_of
%        internalrule,
%        elserule, 
%        treerule, 
%        epsilonrule, 
%        title, expression, xpath
% for stepwise automata
%   applyrule,
%   applyelserule, else
%   else
%   typedelserule
% negative information (NEGATIVE=true)
%   no_applyrule
%   no_elserule
%   no_treerule
%   no_internalrule
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title(stepwise,'mini(det_and[xml-document][one-x][schema-clean[pc[step[((((((((((((((((&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅default⋅chapter⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T+&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅default⋅appendix⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T)+&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅default⋅epigraph⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T)+&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅default⋅warning⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T)+&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅default⋅preface⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T)+&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅default⋅index⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T)+&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅default⋅colophon⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T)+&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅default⋅glossary⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T)+&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅default⋅biblioentry⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T)+&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅default⋅bibliography⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T)+&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅default⋅dedication⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T)+&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅default⋅sidebar⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T)+&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅default⋅footnote⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T)+&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅default⋅glossterm⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T)+&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅default⋅glossdef⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T)+&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅default⋅bridgehead⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T)+&amp;lt;(elem⋅_⋅_+doc)⋅_⋅T⋅&amp;lt;elem⋅default⋅part⋅x⋅T&amp;gt;⋅T&amp;gt;⋅T)]]]])').
xpath(stepwise,'chapter_bl_Z|appendix_bl_Z|epigraph_bl_Z|warning_bl_Z|preface_bl_Z|index_bl_Z|colophon_bl_Z|glossary_bl_Z|biblioentry_bl_Z|bibliography_bl_Z|dedication_bl_Z|sidebar_bl_Z|footnote_bl_Z|glossterm_bl_Z|glossdef_bl_Z|bridgehead_bl_Z|part').
var(stepwise,'x',1).
no_typedelserule(stepwise,92,name).
no_typedelserule(stepwise,24,name).
no_typedelserule(stepwise,6,name).
no_typedelserule(stepwise,109,name).
no_typedelserule(stepwise,75,name).
no_typedelserule(stepwise,41,name).
no_typedelserule(stepwise,58,name).
no_typedelserule(stepwise,92,var).
no_typedelserule(stepwise,24,var).
no_typedelserule(stepwise,6,var).
no_typedelserule(stepwise,109,var).
no_typedelserule(stepwise,75,var).
no_typedelserule(stepwise,41,var).
no_typedelserule(stepwise,58,var).
no_typedelserule(stepwise,92,type).
no_typedelserule(stepwise,24,type).
no_typedelserule(stepwise,6,type).
no_typedelserule(stepwise,109,type).
no_typedelserule(stepwise,75,type).
no_typedelserule(stepwise,41,type).
no_typedelserule(stepwise,58,type).
no_typedelserule(stepwise,92,negvar).
no_typedelserule(stepwise,24,negvar).
no_typedelserule(stepwise,6,negvar).
no_typedelserule(stepwise,109,negvar).
no_typedelserule(stepwise,75,negvar).
no_typedelserule(stepwise,41,negvar).
no_typedelserule(stepwise,58,negvar).
no_typedelserule(stepwise,92,namespace).
no_typedelserule(stepwise,24,namespace).
no_typedelserule(stepwise,6,namespace).
no_typedelserule(stepwise,109,namespace).
no_typedelserule(stepwise,75,namespace).
no_typedelserule(stepwise,41,namespace).
no_typedelserule(stepwise,58,namespace).
state(stepwise,1).
state(stepwise,92).
state(stepwise,24).
state(stepwise,6).
state(stepwise,109).
state(stepwise,0).
state(stepwise,75).
state(stepwise,3).
state(stepwise,4).
state(stepwise,41).
state(stepwise,7).
state(stepwise,2).
state(stepwise,58).
state(stepwise,5).
start_tree(stepwise,0).
initial(stepwise,0).
origin_of(stepwise,1,'127','195','263','110','59','229','93','76','268','200','25','280','42','161','149','285','183','8','13','251','212','166','144','81','292','178','234','47','132','217','64','246','98','115','30','3').
origin_of(stepwise,92,'20','292','25','29').
origin_of(stepwise,24,'120','51','205','238','272','255','171','289','137','119','154','256','292','170','239','136','85','69','86','35','273','34','52','68','16','221','204','17','188','153','102','222','187','18','103','290','15','20').
origin_of(stepwise,6,'120','51','205','238','272','255','171','289','137','119','154','221','256','292','170','239','136','85','69','86','35','273','34','52','68','204','17','188','153','102','222','187','18','103','290','6','26','16','5','22','15','20').
origin_of(stepwise,109,'293','23','12','24','28','19','21','11').
origin_of(stepwise,0,'160','29','46','63','279','157','108','92','261','194','55','7','276','177','131','225','23','38','41','109','278','58','228','165','75','12','182','125','72','242','250','24','244','284','89','21','40','211','208','227','114','106','233','143','4','216','193','140','174','199','210','91','292','97','148','6','123','267','159','57','259','80','126','74','176','262','191','245','142','0').
origin_of(stepwise,75,'293','5','19','28').
origin_of(stepwise,3,'292','10','14','27','4','7','29','18','8','25','2','17').
origin_of(stepwise,4,'247','9','264','26','162','145','213','65','252','167','77','133','286','218','281','31','201','196','111','14','43','116','94','48','128','99','184','230','235','292','150','269','60','82','179','9').
origin_of(stepwise,41,'293','11','19').
origin_of(stepwise,7,'163','10','95','265','231','180','282','292','61','129','112','248','146','27','197','44','214','15','78','13').
origin_of(stepwise,2,'163','10','95','265','231','180','282','292','61','129','112','248','78','146','27','197','44','214','1','13').
origin_of(stepwise,58,'120','51','205','238','272','255','171','289','137','119','154','221','256','292','239','136','85','69','86','35','273','34','52','68','204','17','188','153','170','19','102','222','187','18','103','290','20','26').
origin_of(stepwise,5,'77','281','247','196','9','111','264','26','43','94','162','145','128','230','292','213','60','179','9').
not_sink(stepwise,1).
not_sink(stepwise,92).
not_sink(stepwise,24).
not_sink(stepwise,6).
not_sink(stepwise,109).
not_sink(stepwise,0).
not_sink(stepwise,75).
not_sink(stepwise,3).
not_sink(stepwise,4).
not_sink(stepwise,41).
not_sink(stepwise,7).
not_sink(stepwise,2).
not_sink(stepwise,58).
not_sink(stepwise,5).
else(stepwise,109).
else(stepwise,75).
else(stepwise,41).
nonelse(stepwise,1).
nonelse(stepwise,92).
nonelse(stepwise,24).
nonelse(stepwise,6).
nonelse(stepwise,0).
nonelse(stepwise,3).
nonelse(stepwise,4).
nonelse(stepwise,7).
nonelse(stepwise,2).
nonelse(stepwise,58).
nonelse(stepwise,5).
final(stepwise,92).
nonfinal(stepwise,1).
nonfinal(stepwise,24).
nonfinal(stepwise,6).
nonfinal(stepwise,109).
nonfinal(stepwise,0).
nonfinal(stepwise,75).
nonfinal(stepwise,3).
nonfinal(stepwise,4).
nonfinal(stepwise,41).
nonfinal(stepwise,7).
nonfinal(stepwise,2).
nonfinal(stepwise,58).
nonfinal(stepwise,5).
tree_state(stepwise,109).
tree_istate(stepwise,109,0).
 tree_state(stepwise,75).
tree_istate(stepwise,75,0).
 tree_state(stepwise,41).
tree_istate(stepwise,41,0).
 hedge_state(stepwise,1).
    no_applyrule(stepwise,1,109).
	  no_applyrule(stepwise,1,75).
	  no_applyrule(stepwise,1,41).
	  no_applyelserule(stepwise,1).
hedge_state(stepwise,92).
    no_applyrule(stepwise,92,75).
	  no_applyrule(stepwise,92,41).
	  no_applyelserule(stepwise,92).
no_elserule(stepwise,92).
	% %
hedge_state(stepwise,24).
    no_applyrule(stepwise,24,75).
	  no_applyrule(stepwise,24,41).
	  no_applyelserule(stepwise,24).
no_elserule(stepwise,24).
	% %
hedge_state(stepwise,6).
    no_applyelserule(stepwise,6).
no_elserule(stepwise,6).
	% %
hedge_state(stepwise,0).
    no_applyelserule(stepwise,0).
hedge_state(stepwise,3).
    no_applyelserule(stepwise,3).
hedge_state(stepwise,4).
    no_applyrule(stepwise,4,109).
	  no_applyrule(stepwise,4,75).
	  no_applyrule(stepwise,4,41).
	  no_applyelserule(stepwise,4).
hedge_state(stepwise,7).
    no_applyrule(stepwise,7,109).
	  no_applyrule(stepwise,7,75).
	  no_applyrule(stepwise,7,41).
	  no_applyelserule(stepwise,7).
hedge_state(stepwise,2).
    no_applyrule(stepwise,2,109).
	  no_applyrule(stepwise,2,75).
	  no_applyrule(stepwise,2,41).
	  no_applyelserule(stepwise,2).
hedge_state(stepwise,58).
    no_applyrule(stepwise,58,75).
	  no_applyrule(stepwise,58,41).
	  no_applyelserule(stepwise,58).
no_elserule(stepwise,58).
	% %
hedge_state(stepwise,5).
    no_applyrule(stepwise,5,109).
	  no_applyrule(stepwise,5,75).
	  no_applyrule(stepwise,5,41).
	  no_applyelserule(stepwise,5).
applyrule(stepwise,6,41,58).
applyrule(stepwise,0,41,3).
applyrule(stepwise,0,75,92).
internalrule(stepwise,4,'sidebar',name,7).
internalrule(stepwise,4,'bridgehead',name,7).
applyrule(stepwise,3,41,3).
applyrule(stepwise,3,75,3).
treerule(stepwise,58,75).
applyrule(stepwise,6,75,6).
internalrule(stepwise,7,'x',var,24).
internalrule(stepwise,1,'default',namespace,4).
elserule(stepwise,5,2).
applyrule(stepwise,92,109,92).
treerule(stepwise,6,109).
elserule(stepwise,0,3).
internalrule(stepwise,4,'colophon',name,7).
internalrule(stepwise,4,'part',name,7).
internalrule(stepwise,4,'chapter',name,7).
internalrule(stepwise,4,'epigraph',name,7).
internalrule(stepwise,4,'glossary',name,7).
applyrule(stepwise,6,109,6).
internalrule(stepwise,0,'doc',type,2).
applyrule(stepwise,3,109,3).
internalrule(stepwise,4,'glossterm',name,7).
treerule(stepwise,3,109).
internalrule(stepwise,0,'elem',type,1).
elserule(stepwise,2,6).
internalrule(stepwise,4,'dedication',name,7).
elserule(stepwise,1,5).
treerule(stepwise,24,41).
internalrule(stepwise,4,'footnote',name,7).
applyrule(stepwise,0,109,3).
internalrule(stepwise,4,'appendix',name,7).
internalrule(stepwise,4,'index',name,7).
applyrule(stepwise,24,109,24).
internalrule(stepwise,4,'biblioentry',name,7).
applyrule(stepwise,58,109,58).
internalrule(stepwise,4,'warning',name,7).
elserule(stepwise,7,6).
elserule(stepwise,3,3).
internalrule(stepwise,4,'preface',name,7).
internalrule(stepwise,4,'bibliography',name,7).
elserule(stepwise,4,2).
internalrule(stepwise,4,'glossdef',name,7).
no_elserule(stepwise,92).
no_elserule(stepwise,24).
no_elserule(stepwise,6).
no_elserule(stepwise,109).
no_elserule(stepwise,75).
no_elserule(stepwise,41).
no_elserule(stepwise,58).
no_treerule(stepwise,1).
no_treerule(stepwise,92).
no_treerule(stepwise,109).
no_treerule(stepwise,0).
no_treerule(stepwise,75).
no_treerule(stepwise,4).
no_treerule(stepwise,41).
no_treerule(stepwise,7).
no_treerule(stepwise,2).
no_treerule(stepwise,5).
label(stepwise,'glossterm',name,15).
no_internalrule(stepwise,1,'glossterm',name).
no_internalrule(stepwise,92,'glossterm',name).
no_internalrule(stepwise,24,'glossterm',name).
no_internalrule(stepwise,6,'glossterm',name).
no_internalrule(stepwise,109,'glossterm',name).
no_internalrule(stepwise,0,'glossterm',name).
no_internalrule(stepwise,75,'glossterm',name).
no_internalrule(stepwise,3,'glossterm',name).
no_internalrule(stepwise,41,'glossterm',name).
no_internalrule(stepwise,7,'glossterm',name).
no_internalrule(stepwise,2,'glossterm',name).
no_internalrule(stepwise,58,'glossterm',name).
no_internalrule(stepwise,5,'glossterm',name).
label(stepwise,'chapter',name,5).
no_internalrule(stepwise,1,'chapter',name).
no_internalrule(stepwise,92,'chapter',name).
no_internalrule(stepwise,24,'chapter',name).
no_internalrule(stepwise,6,'chapter',name).
no_internalrule(stepwise,109,'chapter',name).
no_internalrule(stepwise,0,'chapter',name).
no_internalrule(stepwise,75,'chapter',name).
no_internalrule(stepwise,3,'chapter',name).
no_internalrule(stepwise,41,'chapter',name).
no_internalrule(stepwise,7,'chapter',name).
no_internalrule(stepwise,2,'chapter',name).
no_internalrule(stepwise,58,'chapter',name).
no_internalrule(stepwise,5,'chapter',name).
label(stepwise,'x',var,21).
no_internalrule(stepwise,1,'x',var).
no_internalrule(stepwise,92,'x',var).
no_internalrule(stepwise,24,'x',var).
no_internalrule(stepwise,6,'x',var).
no_internalrule(stepwise,109,'x',var).
no_internalrule(stepwise,0,'x',var).
no_internalrule(stepwise,75,'x',var).
no_internalrule(stepwise,3,'x',var).
no_internalrule(stepwise,4,'x',var).
no_internalrule(stepwise,41,'x',var).
no_internalrule(stepwise,2,'x',var).
no_internalrule(stepwise,58,'x',var).
no_internalrule(stepwise,5,'x',var).
label(stepwise,'colophon',name,6).
no_internalrule(stepwise,1,'colophon',name).
no_internalrule(stepwise,92,'colophon',name).
no_internalrule(stepwise,24,'colophon',name).
no_internalrule(stepwise,6,'colophon',name).
no_internalrule(stepwise,109,'colophon',name).
no_internalrule(stepwise,0,'colophon',name).
no_internalrule(stepwise,75,'colophon',name).
no_internalrule(stepwise,3,'colophon',name).
no_internalrule(stepwise,41,'colophon',name).
no_internalrule(stepwise,7,'colophon',name).
no_internalrule(stepwise,2,'colophon',name).
no_internalrule(stepwise,58,'colophon',name).
no_internalrule(stepwise,5,'colophon',name).
label(stepwise,'comment',type,24).
no_internalrule(stepwise,1,'comment',type).
no_internalrule(stepwise,92,'comment',type).
no_internalrule(stepwise,24,'comment',type).
no_internalrule(stepwise,6,'comment',type).
no_internalrule(stepwise,109,'comment',type).
no_internalrule(stepwise,0,'comment',type).
no_internalrule(stepwise,75,'comment',type).
no_internalrule(stepwise,3,'comment',type).
no_internalrule(stepwise,4,'comment',type).
no_internalrule(stepwise,41,'comment',type).
no_internalrule(stepwise,7,'comment',type).
no_internalrule(stepwise,2,'comment',type).
no_internalrule(stepwise,58,'comment',type).
no_internalrule(stepwise,5,'comment',type).
label(stepwise,'doc',type,9).
no_internalrule(stepwise,1,'doc',type).
no_internalrule(stepwise,92,'doc',type).
no_internalrule(stepwise,24,'doc',type).
no_internalrule(stepwise,6,'doc',type).
no_internalrule(stepwise,109,'doc',type).
no_internalrule(stepwise,75,'doc',type).
no_internalrule(stepwise,3,'doc',type).
no_internalrule(stepwise,4,'doc',type).
no_internalrule(stepwise,41,'doc',type).
no_internalrule(stepwise,7,'doc',type).
no_internalrule(stepwise,2,'doc',type).
no_internalrule(stepwise,58,'doc',type).
no_internalrule(stepwise,5,'doc',type).
label(stepwise,'x',negvar,28).
no_internalrule(stepwise,1,'x',negvar).
no_internalrule(stepwise,92,'x',negvar).
no_internalrule(stepwise,24,'x',negvar).
no_internalrule(stepwise,6,'x',negvar).
no_internalrule(stepwise,109,'x',negvar).
no_internalrule(stepwise,0,'x',negvar).
no_internalrule(stepwise,75,'x',negvar).
no_internalrule(stepwise,3,'x',negvar).
no_internalrule(stepwise,4,'x',negvar).
no_internalrule(stepwise,41,'x',negvar).
no_internalrule(stepwise,7,'x',negvar).
no_internalrule(stepwise,2,'x',negvar).
no_internalrule(stepwise,58,'x',negvar).
no_internalrule(stepwise,5,'x',negvar).
label(stepwise,'part',name,17).
no_internalrule(stepwise,1,'part',name).
no_internalrule(stepwise,92,'part',name).
no_internalrule(stepwise,24,'part',name).
no_internalrule(stepwise,6,'part',name).
no_internalrule(stepwise,109,'part',name).
no_internalrule(stepwise,0,'part',name).
no_internalrule(stepwise,75,'part',name).
no_internalrule(stepwise,3,'part',name).
no_internalrule(stepwise,41,'part',name).
no_internalrule(stepwise,7,'part',name).
no_internalrule(stepwise,2,'part',name).
no_internalrule(stepwise,58,'part',name).
no_internalrule(stepwise,5,'part',name).
label(stepwise,'bibliography',name,3).
no_internalrule(stepwise,1,'bibliography',name).
no_internalrule(stepwise,92,'bibliography',name).
no_internalrule(stepwise,24,'bibliography',name).
no_internalrule(stepwise,6,'bibliography',name).
no_internalrule(stepwise,109,'bibliography',name).
no_internalrule(stepwise,0,'bibliography',name).
no_internalrule(stepwise,75,'bibliography',name).
no_internalrule(stepwise,3,'bibliography',name).
no_internalrule(stepwise,41,'bibliography',name).
no_internalrule(stepwise,7,'bibliography',name).
no_internalrule(stepwise,2,'bibliography',name).
no_internalrule(stepwise,58,'bibliography',name).
no_internalrule(stepwise,5,'bibliography',name).
label(stepwise,'dedication',name,7).
no_internalrule(stepwise,1,'dedication',name).
no_internalrule(stepwise,92,'dedication',name).
no_internalrule(stepwise,24,'dedication',name).
no_internalrule(stepwise,6,'dedication',name).
no_internalrule(stepwise,109,'dedication',name).
no_internalrule(stepwise,0,'dedication',name).
no_internalrule(stepwise,75,'dedication',name).
no_internalrule(stepwise,3,'dedication',name).
no_internalrule(stepwise,41,'dedication',name).
no_internalrule(stepwise,7,'dedication',name).
no_internalrule(stepwise,2,'dedication',name).
no_internalrule(stepwise,58,'dedication',name).
no_internalrule(stepwise,5,'dedication',name).
label(stepwise,'default',namespace,8).
no_internalrule(stepwise,92,'default',namespace).
no_internalrule(stepwise,24,'default',namespace).
no_internalrule(stepwise,6,'default',namespace).
no_internalrule(stepwise,109,'default',namespace).
no_internalrule(stepwise,0,'default',namespace).
no_internalrule(stepwise,75,'default',namespace).
no_internalrule(stepwise,3,'default',namespace).
no_internalrule(stepwise,4,'default',namespace).
no_internalrule(stepwise,41,'default',namespace).
no_internalrule(stepwise,7,'default',namespace).
no_internalrule(stepwise,2,'default',namespace).
no_internalrule(stepwise,58,'default',namespace).
no_internalrule(stepwise,5,'default',namespace).
label(stepwise,'att',type,23).
no_internalrule(stepwise,1,'att',type).
no_internalrule(stepwise,92,'att',type).
no_internalrule(stepwise,24,'att',type).
no_internalrule(stepwise,6,'att',type).
no_internalrule(stepwise,109,'att',type).
no_internalrule(stepwise,0,'att',type).
no_internalrule(stepwise,75,'att',type).
no_internalrule(stepwise,3,'att',type).
no_internalrule(stepwise,4,'att',type).
no_internalrule(stepwise,41,'att',type).
no_internalrule(stepwise,7,'att',type).
no_internalrule(stepwise,2,'att',type).
no_internalrule(stepwise,58,'att',type).
no_internalrule(stepwise,5,'att',type).
label(stepwise,'biblioentry',name,2).
no_internalrule(stepwise,1,'biblioentry',name).
no_internalrule(stepwise,92,'biblioentry',name).
no_internalrule(stepwise,24,'biblioentry',name).
no_internalrule(stepwise,6,'biblioentry',name).
no_internalrule(stepwise,109,'biblioentry',name).
no_internalrule(stepwise,0,'biblioentry',name).
no_internalrule(stepwise,75,'biblioentry',name).
no_internalrule(stepwise,3,'biblioentry',name).
no_internalrule(stepwise,41,'biblioentry',name).
no_internalrule(stepwise,7,'biblioentry',name).
no_internalrule(stepwise,2,'biblioentry',name).
no_internalrule(stepwise,58,'biblioentry',name).
no_internalrule(stepwise,5,'biblioentry',name).
label(stepwise,'appendix',name,1).
no_internalrule(stepwise,1,'appendix',name).
no_internalrule(stepwise,92,'appendix',name).
no_internalrule(stepwise,24,'appendix',name).
no_internalrule(stepwise,6,'appendix',name).
no_internalrule(stepwise,109,'appendix',name).
no_internalrule(stepwise,0,'appendix',name).
no_internalrule(stepwise,75,'appendix',name).
no_internalrule(stepwise,3,'appendix',name).
no_internalrule(stepwise,41,'appendix',name).
no_internalrule(stepwise,7,'appendix',name).
no_internalrule(stepwise,2,'appendix',name).
no_internalrule(stepwise,58,'appendix',name).
no_internalrule(stepwise,5,'appendix',name).
label(stepwise,'warning',name,20).
no_internalrule(stepwise,1,'warning',name).
no_internalrule(stepwise,92,'warning',name).
no_internalrule(stepwise,24,'warning',name).
no_internalrule(stepwise,6,'warning',name).
no_internalrule(stepwise,109,'warning',name).
no_internalrule(stepwise,0,'warning',name).
no_internalrule(stepwise,75,'warning',name).
no_internalrule(stepwise,3,'warning',name).
no_internalrule(stepwise,41,'warning',name).
no_internalrule(stepwise,7,'warning',name).
no_internalrule(stepwise,2,'warning',name).
no_internalrule(stepwise,58,'warning',name).
no_internalrule(stepwise,5,'warning',name).
label(stepwise,'sidebar',name,19).
no_internalrule(stepwise,1,'sidebar',name).
no_internalrule(stepwise,92,'sidebar',name).
no_internalrule(stepwise,24,'sidebar',name).
no_internalrule(stepwise,6,'sidebar',name).
no_internalrule(stepwise,109,'sidebar',name).
no_internalrule(stepwise,0,'sidebar',name).
no_internalrule(stepwise,75,'sidebar',name).
no_internalrule(stepwise,3,'sidebar',name).
no_internalrule(stepwise,41,'sidebar',name).
no_internalrule(stepwise,7,'sidebar',name).
no_internalrule(stepwise,2,'sidebar',name).
no_internalrule(stepwise,58,'sidebar',name).
no_internalrule(stepwise,5,'sidebar',name).
label(stepwise,'glossdef',name,14).
no_internalrule(stepwise,1,'glossdef',name).
no_internalrule(stepwise,92,'glossdef',name).
no_internalrule(stepwise,24,'glossdef',name).
no_internalrule(stepwise,6,'glossdef',name).
no_internalrule(stepwise,109,'glossdef',name).
no_internalrule(stepwise,0,'glossdef',name).
no_internalrule(stepwise,75,'glossdef',name).
no_internalrule(stepwise,3,'glossdef',name).
no_internalrule(stepwise,41,'glossdef',name).
no_internalrule(stepwise,7,'glossdef',name).
no_internalrule(stepwise,2,'glossdef',name).
no_internalrule(stepwise,58,'glossdef',name).
no_internalrule(stepwise,5,'glossdef',name).
label(stepwise,'text',type,27).
no_internalrule(stepwise,1,'text',type).
no_internalrule(stepwise,92,'text',type).
no_internalrule(stepwise,24,'text',type).
no_internalrule(stepwise,6,'text',type).
no_internalrule(stepwise,109,'text',type).
no_internalrule(stepwise,0,'text',type).
no_internalrule(stepwise,75,'text',type).
no_internalrule(stepwise,3,'text',type).
no_internalrule(stepwise,4,'text',type).
no_internalrule(stepwise,41,'text',type).
no_internalrule(stepwise,7,'text',type).
no_internalrule(stepwise,2,'text',type).
no_internalrule(stepwise,58,'text',type).
no_internalrule(stepwise,5,'text',type).
label(stepwise,'glossary',name,13).
no_internalrule(stepwise,1,'glossary',name).
no_internalrule(stepwise,92,'glossary',name).
no_internalrule(stepwise,24,'glossary',name).
no_internalrule(stepwise,6,'glossary',name).
no_internalrule(stepwise,109,'glossary',name).
no_internalrule(stepwise,0,'glossary',name).
no_internalrule(stepwise,75,'glossary',name).
no_internalrule(stepwise,3,'glossary',name).
no_internalrule(stepwise,41,'glossary',name).
no_internalrule(stepwise,7,'glossary',name).
no_internalrule(stepwise,2,'glossary',name).
no_internalrule(stepwise,58,'glossary',name).
no_internalrule(stepwise,5,'glossary',name).
label(stepwise,'footnote',name,12).
no_internalrule(stepwise,1,'footnote',name).
no_internalrule(stepwise,92,'footnote',name).
no_internalrule(stepwise,24,'footnote',name).
no_internalrule(stepwise,6,'footnote',name).
no_internalrule(stepwise,109,'footnote',name).
no_internalrule(stepwise,0,'footnote',name).
no_internalrule(stepwise,75,'footnote',name).
no_internalrule(stepwise,3,'footnote',name).
no_internalrule(stepwise,41,'footnote',name).
no_internalrule(stepwise,7,'footnote',name).
no_internalrule(stepwise,2,'footnote',name).
no_internalrule(stepwise,58,'footnote',name).
no_internalrule(stepwise,5,'footnote',name).
label(stepwise,'elem',type,10).
no_internalrule(stepwise,1,'elem',type).
no_internalrule(stepwise,92,'elem',type).
no_internalrule(stepwise,24,'elem',type).
no_internalrule(stepwise,6,'elem',type).
no_internalrule(stepwise,109,'elem',type).
no_internalrule(stepwise,75,'elem',type).
no_internalrule(stepwise,3,'elem',type).
no_internalrule(stepwise,4,'elem',type).
no_internalrule(stepwise,41,'elem',type).
no_internalrule(stepwise,7,'elem',type).
no_internalrule(stepwise,2,'elem',type).
no_internalrule(stepwise,58,'elem',type).
no_internalrule(stepwise,5,'elem',type).
label(stepwise,'index',name,16).
no_internalrule(stepwise,1,'index',name).
no_internalrule(stepwise,92,'index',name).
no_internalrule(stepwise,24,'index',name).
no_internalrule(stepwise,6,'index',name).
no_internalrule(stepwise,109,'index',name).
no_internalrule(stepwise,0,'index',name).
no_internalrule(stepwise,75,'index',name).
no_internalrule(stepwise,3,'index',name).
no_internalrule(stepwise,41,'index',name).
no_internalrule(stepwise,7,'index',name).
no_internalrule(stepwise,2,'index',name).
no_internalrule(stepwise,58,'index',name).
no_internalrule(stepwise,5,'index',name).
label(stepwise,'preface',name,18).
no_internalrule(stepwise,1,'preface',name).
no_internalrule(stepwise,92,'preface',name).
no_internalrule(stepwise,24,'preface',name).
no_internalrule(stepwise,6,'preface',name).
no_internalrule(stepwise,109,'preface',name).
no_internalrule(stepwise,0,'preface',name).
no_internalrule(stepwise,75,'preface',name).
no_internalrule(stepwise,3,'preface',name).
no_internalrule(stepwise,41,'preface',name).
no_internalrule(stepwise,7,'preface',name).
no_internalrule(stepwise,2,'preface',name).
no_internalrule(stepwise,58,'preface',name).
no_internalrule(stepwise,5,'preface',name).
label(stepwise,'epigraph',name,11).
no_internalrule(stepwise,1,'epigraph',name).
no_internalrule(stepwise,92,'epigraph',name).
no_internalrule(stepwise,24,'epigraph',name).
no_internalrule(stepwise,6,'epigraph',name).
no_internalrule(stepwise,109,'epigraph',name).
no_internalrule(stepwise,0,'epigraph',name).
no_internalrule(stepwise,75,'epigraph',name).
no_internalrule(stepwise,3,'epigraph',name).
no_internalrule(stepwise,41,'epigraph',name).
no_internalrule(stepwise,7,'epigraph',name).
no_internalrule(stepwise,2,'epigraph',name).
no_internalrule(stepwise,58,'epigraph',name).
no_internalrule(stepwise,5,'epigraph',name).
label(stepwise,'bridgehead',name,4).
no_internalrule(stepwise,1,'bridgehead',name).
no_internalrule(stepwise,92,'bridgehead',name).
no_internalrule(stepwise,24,'bridgehead',name).
no_internalrule(stepwise,6,'bridgehead',name).
no_internalrule(stepwise,109,'bridgehead',name).
no_internalrule(stepwise,0,'bridgehead',name).
no_internalrule(stepwise,75,'bridgehead',name).
no_internalrule(stepwise,3,'bridgehead',name).
no_internalrule(stepwise,41,'bridgehead',name).
no_internalrule(stepwise,7,'bridgehead',name).
no_internalrule(stepwise,2,'bridgehead',name).
no_internalrule(stepwise,58,'bridgehead',name).
no_internalrule(stepwise,5,'bridgehead',name).
max_label_id(stepwise,28).
    %%% different origins %%%%%%%
    
