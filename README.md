# XPath Benchmark V2.1

This is a benchmark of regular XPath queries [1] collected from Lick and Schmitz benchmark [2][3]. It contains the compilation of 79 queries to stepwise hedge automata (SHA).

# Example
 XPath query 00744 : **.//@id | .//@xml:id**

Deterministic SHA for query 00744 :

![](/Automata%20Results/Version%202.1/00744/det-stepwise.png?raw=true "00744")



# References
* [1]Al Serhali, A., Niehren, J.: A Benchmark Collection of Deterministic Automata for XPath Queries [Link](https://hal.inria.fr/hal-03527888)
* [2] Lick, A., Sylvain, S.: XPath Benchmark [Link](https://archive.softwareheritage.org/browse/directory/1ea68cf5bb3f9f3f2fe8c7995f1802ebadf17fb5)
* [3] Lick, A.: Logique de requêtes à la XPath : systèmes de preuve et pertinence pratique. Theses, Université Paris-Saclay (Jul 2019) [Link](https://tel.archives-ouvertes.fr/tel-02276423)

